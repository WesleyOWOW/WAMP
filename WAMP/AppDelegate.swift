import UIKit
import Sentry
import SwiftyBeaver
import FacebookCore
import KeychainAccess
import UserNotifications

let log = SwiftyBeaver.self
let keychain = Keychain(service: "com.wamp.authentication")
let api_url = "https://staging.wearematchplay.com/v1"

let apiDateFormatter: DateFormatter = {
    let fmt = DateFormatter()
    fmt.dateFormat = "yyyy-MM-dd HH:mm:ss"
    fmt.timeZone = TimeZone(abbreviation: "UTC")
    return fmt
}()


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    public private(set) static var shared: AppDelegate!
    var window: UIWindow?

    override init() {
        super.init()
        AppDelegate.shared = self
    }
    
    
    

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        // Set up Swiftbeaver logging. (Log remotely to the cloud)
        let console = ConsoleDestination()
        let file = FileDestination()
        let cloud = SBPlatformDestination(appID: "36rR6o", appSecret: "oZjx1foenFDdb4qpPxrCtF53pngmcals", encryptionKey: "KyTMcfp4ShdiYnvrbEnSbqL7dGtuxbjd")
        log.addDestination(console)
        log.addDestination(cloud)
        log.addDestination(file)
        
        // Set up Sentry, for exception catching
        do {
            Client.shared = try Client(dsn: "https://337e3c96a46f451e9229db77abd1bcf2:ff5eccf269644562b992d1ca95bb816e@sentry.io/290910")
            try Client.shared?.startCrashHandler()
        } catch let error {
            log.error("Could not initiate sentry: \(error)")
        }
        
         self.setupNotifications()
        
        // Setup Facebook
        FacebookCore.SDKApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        
        
        return true
    }
    
    func setupNotifications() {
        guard AuthenticationManager.shared.isLoggedin, AuthenticationManager.shared.token != nil else {
            // only do this when logged in
            return
        }
        
        log.info("Setting up notifications")
        if #available(iOS 10.0, *) {
            let center = UNUserNotificationCenter.current()
            
            center.requestAuthorization(options: [.alert, .sound, .badge]) { granted, error in
                if granted {
                    log.info("Requesting device token")
                    DispatchQueue.main.async {
                        UIApplication.shared.registerForRemoteNotifications()
                    }
                }
            }
        } else {
            // Notifications not supported on iOS <10.0
        }
    }

    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        User.me.pushNotificationDeviceToken(deviceToken)
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        log.error("Could not register for remote notifications: \(error)")
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Send event when the app becomes active
        NotificationCenter.default.post(name: Notification.Name("backToFront"), object: nil)
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Send event when the app becomes active
        NotificationCenter.default.post(name: Notification.Name("backToFront"), object: nil)
    }
    
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        let handled = FacebookCore.SDKApplicationDelegate.shared.application(app, open: url, options: options)
        
        return handled
    }
    
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject]) {
        NotificationCenter.default.post(name: Notification.Name("refreshNotifications"), object: nil)
        print("NOTIFICATION BIATCH", userInfo)
    }
    
    
    func reset() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateInitialViewController()
        window?.rootViewController = vc
    }
}
