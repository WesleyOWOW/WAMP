import UIKit

public enum Team : String {
    case red
    case blue
    case neutral
    
    public var softColor: UIColor {
        switch self {
        case .red: return #colorLiteral(red: 0.8798167109, green: 0.2084800005, blue: 0.2136861682, alpha: 1)
        default: return #colorLiteral(red: 0, green: 0.4619901776, blue: 0.9635531306, alpha: 1)
        }
    }
    
    public var color: UIColor {
        switch self {
        case .red: return #colorLiteral(red: 1, green: 0.2, blue: 0.2666666667, alpha: 1)
        default: return #colorLiteral(red: 0, green: 0.4823529412, blue: 1, alpha: 1)
        }
    }
    
    public var opposite: Team {
        if self == .neutral { return .neutral }
        
        return self == .red ? .blue : .red
    }
}


public extension Team {
    public var holeResultValue: HoleResultValue {
        switch self {
        case .red:
            return .red
        case .blue:
            return .blue
        case .neutral:
            return .square
        }
    }
}
