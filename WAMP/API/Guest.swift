/// Guest related logic. The `Guest` class is currently only used with concept matches; APIMatches use the same profile class for every user.

import Foundation
import Alamofire
import SwiftyJSON
import RxSwift

public class Guest : UserProtocol {
    public var matchCount: Int = 0
    
    public var followerCount: Int = 0
    
    public var followingCount: Int = 0
    
    public var following: Bool = false
    
    public var privacy: Bool = false
    
    public var id: String
    public var username: String { return firstName }
    public var firstName: String
    public var lastName: String
    public var handicap: Double
    public var team: Team = .neutral
    public let verified = false
    
    public init(json: JSON) {
        self.id = json["id"].stringValue
        self.firstName = json["first_name"].stringValue
        self.lastName = json["last_name"].stringValue
        self.handicap = json["handicap"].doubleValue
    }
    
    public init(id: String, firstName: String, lastName: String, handicap: Double) {
        self.id = id
        self.firstName = firstName
        self.lastName = lastName
        self.handicap = handicap
    }
    
    public static func fetchAll() -> Observable<[Guest]> {
        return Observable.create { observer in
            let request = Alamofire.request(
                api_url + "/guests",
                method: .get,
                headers: AuthenticationManager.shared.headers)
                .validate()
                .responseJSON { response in
                    switch response.result {
                    case .failure(let error):
                        observer.onError(error)
                    case .success(let value):
                        // map to guest instances and return to observer
                        let json = JSON(value)
                        observer.onNext(json.map { Guest(json: $0.1) })
                        observer.onCompleted()
                    }
            }
            
            return Disposables.create(with: request.cancel)
        }
    }
    
    public static func create(firstName: String, lastName: String, handicap: Double, photo: UIImage?) -> Observable<Guest> {
        return Observable.create { observer in
            let parameters: Parameters = [
                "first_name": firstName,
                "last_name": "abc",
                "handicap": handicap
            ]
            
            let request = Alamofire.request(
                api_url + "/guests",
                method: .post,
                parameters: parameters,
                encoding: JSONEncoding.default,
                headers: AuthenticationManager.shared.headers)
                .validate()
                .responseJSON { response in
                    switch response.result {
                    case .failure(let error):
                        observer.onError(error)
                        if let data = response.data {
                            let json = String(data: data, encoding: String.Encoding.utf8)
                            log.error("Failure Response: \(json)")
                        }
                        log.error("Can't create guest account. Data: \n\(response.data)")
                    case .success(let value):
                        let json = JSON(value)
                        let guestId = json["id"].stringValue
                        
                        let guest = Guest(id: guestId, firstName: firstName, lastName: lastName, handicap: handicap)
                        
                        // upload the photo, if present
                        if let photo = photo, let jpeg = UIImageJPEGRepresentation(photo, 0.8) {
                            Alamofire.upload(jpeg,
                                             to: api_url + "/guests/\(guestId)/profile-picture",
                                method: .post,
                                headers: AuthenticationManager.shared.headers)
                                .validate()
                                .response { response in
                                    if let error = response.error {
                                        observer.onError(error)
                                    } else {
                                        observer.onNext(guest)
                                        observer.onCompleted()
                                    }
                            }
                        } else {
                            observer.onNext(guest)
                            observer.onCompleted()
                        }
                    }
            }
            
            return Disposables.create(with: request.cancel)
        }
    }
}


