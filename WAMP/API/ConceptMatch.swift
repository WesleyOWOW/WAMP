import Foundation
import Alamofire
import SwiftyJSON
import SwiftNotificationCenter
import SwiftyUserDefaults
import RxSwift

public enum ConceptMatchRestorationError : Error {
    case missingData
    case invalidPlayer
    case unfindablePlayer
}

fileprivate extension Int64 {
    var int: Int {
        return Int(self)
    }
}

public class ConceptMatch : Match {
    public var egcHandicaps: [Double] = []
    
    /// Custom text that goes with the match
    public var text: String = ""
    
    /// The winning team, or `nil` for square
    public var winningTeam: Team?
    
    /// The type of this game, like single or double
    public var type: MatchType
    
    /// The final score of the match
    public var score: MatchplayScore = .init(0, 0) // "AS"
    
    /// The players that have taken part in this match
    public var players: [MatchPlayer]
    
    /// The match date
    public var date: Date? = Date()
    
    /// The primary team
    public var team1: Team {
        return Team.blue
    }
    
    /// The amount of photos associated with the match
    public var photoCount: Int { return photos.count }
    
    /// The course of this match, if known
    public var course: Course?
    
    /// Fetches the given photo
    public func getPhoto(index: Int) -> Photo? {
        if index < photos.count {
            return photos[index]
        }
        
        return nil
    }
    
    public var photos = [Photo]()
    
    public static var hasSavedMatch: Bool {
        return Defaults[.savedMatch] != nil
    }
    
    public func saveAsConcept() {
        /* serialization format
         text: String
         winningTeam: rawValue of the enum or nil
         type: rawValue of the enum
         scoreWinningScore: score.winningScore
         scoreHolesNotPlayed: score.scoreHolesNotPlayed
         date: Date or nil
         team1: not serialized, always blue
         courseId: Int
         
         photos not serialized
         
         players array contains dictionaries with these entries:
         userId: String
         team: String
         scores: may be an array of integers
         strokes: Int
         playingHandicap: Double
         */
        var match: [String:Any] = [
            "text": self.text,
            "type": self.type.rawValue,
            "scoreWinningScore": self.score.winningScore,
            "scoreHolesNotPlayed": self.score.holesNotPlayed,
            "players": self.players.map { (player: MatchPlayer) -> [String:Any] in
                var storedPlayer: [String:Any] = [
                    "userId": player.profile.id,
                    "team": player.team.rawValue,
                    "strokes": player.strokes,
                    "playingHandicap": player.playingHandicap
                ]
                storedPlayer["scores"] = player.scores
                return storedPlayer
            }
        ]
        
        match["winningTeam"] = self.winningTeam?.rawValue
        match["date"] = self.date
        match["courseId"] = self.course?.id
        
        Defaults[.savedMatch] = match
    }
    
    public static func deleteSavedConcept() {
        Defaults[.savedMatch] = nil
    }
    
    public static func restoreSavedMatch(callback: @escaping (Result<ConceptMatch>) -> ()) {
        func callbackFailure(_ error: Error) {
            Defaults[.savedMatch] = nil
            callback(.failure(error))
        }
        
        DispatchQueue.global(qos: .userInitiated).async {
            guard
                let dict = Defaults[.savedMatch],
                let typeString = dict["type"] as? String,
                let type = MatchType(rawValue: typeString),
                let scoreWinningScore = dict["scoreWinningScore"] as? Int,
                let scoreHolesNotPlayed = dict["scoreHolesNotPlayed"] as? Int,
                var playersToParse = dict["players"] as? [[String: Any]]
                else {
                    callbackFailure(ConceptMatchRestorationError.missingData)
                    return
            }
            
            // Fetch the course
            var course: Course?
            var players: [MatchPlayer] = []
            
            func finalize() {
                let match = ConceptMatch(type, players: players.map { $0.profile })
                match.players = players
                match.text = dict["text"] as? String ?? ""
                match.date = dict["date"] as? Date
                match.score = MatchplayScore(scoreWinningScore, scoreHolesNotPlayed)
                match.course = course
                callback(.success(match))
            }
            
            func fetchPlayers() {
                func parseNextPlayer() {
                    guard playersToParse.count > 0 else {
                        finalize()
                        return
                    }
                    
                    let rawPlayer = playersToParse.removeFirst()
                    
                    guard
                        let userId = rawPlayer["userId"] as? String,
                        let teamString = rawPlayer["team"] as? String,
                        let team = Team(rawValue: teamString),
                        let strokes = rawPlayer["strokes"] as? Int,
                        let playingHandicap = rawPlayer["playingHandicap"] as? Double else {
                            callbackFailure(ConceptMatchRestorationError.invalidPlayer)
                            return
                    }
                    
                    let scores: [Int?]? = (rawPlayer["scores"] as? [Any])?.map { $0 as? Int }
                    
                    
                    User.fetch(id: userId) { result in
                        switch result {
                        case .success(let user):
                            let matchPlayer: MatchPlayer = (profile: user, team: team, scores: scores, tee: nil, strokes: strokes, playingHandicap: playingHandicap)
                            
                            players.append(matchPlayer)
                            parseNextPlayer()
                        case .failure(let error):
                            callback(.failure(error))
                        }
                    }
                }
                
                parseNextPlayer()
            }
            
            if let courseId = dict["courseId"] as? Int {
                Course.byId(courseId) { result in
                    switch result {
                    case .success(let fetchedCourse):
                        course = fetchedCourse
                        fetchPlayers()
                    case .failure(let error):
                        callbackFailure(error)
                    }
                }
            } else {
                fetchPlayers()
            }
        }
    }
    
    /// Initializes a new, empty concept match
    public init(_ type: MatchType, players: [UserProtocol]) {
        self.type = type
        
        self.players = []
        let primaryTeam = Team.blue
        for (index, profile) in players.enumerated() {
            let isOnPrimaryTeam = index < (players.count / 2)
            let matchPlayer: MatchPlayer = (
                profile: profile,
                team: isOnPrimaryTeam ? primaryTeam : primaryTeam.opposite,
                scores: nil,
                tee: nil,
                strokes: 0,
                playingHandicap: 0
            )
            self.players.append(matchPlayer)
            self.egcHandicaps.append(profile.handicap)
        }
    }
    
    /// Initializes every player with empty scores for every hole
    /// The score card editor uses this function for creating a new match
    /// The reason this isn't done in init, is that a match may actually not have detailed scores per hole but only a final score
    public func initializeHoles() {
        let holeCount = players[0].tee?.holes.count ?? 18
        for (index, _) in players.enumerated() {
            players[index].scores = Array(repeating: nil, count: holeCount)
        }
    }
    
    public func isHoleWinner(for hole: Int, and player: MatchPlayer) -> Bool {
        var winningPlayer: MatchPlayer = players.first!
        var holes = [[HolePlayer]]()
        
        for (_, player) in players.enumerated() {
            guard let scores = player.scores else {
                // We need the scores of all players to be able to summarize the match
                return false
            }
            
            // Create the arrays for every hole, if needed
            if holes.count == 0 {
                holes = Array(repeating: [], count: scores.count)
            }
            
            // Sanity check.
            guard scores.count == holes.count else {
                // The holes don't match up!
                log.error("Amount of hole scores not the same for every player in a match. Match: \(self)")
                return false
            }
            
            if winningPlayer.scores![hole] == nil {
                return false
            }
            
            print ("Current score for hole ", hole, winningPlayer.scores![hole], player.scores![hole])
            
           
            if (winningPlayer.scores![hole])! > 99 {
                return false
            }
            
            if winningPlayer.scores![hole] == 0 {
                return false
            }
            
            if winningPlayer.scores![hole]! < 0 {
                return true
            }
            
            
            
            if winningPlayer.scores![hole] == nil {
                return false
            } else if let competing = scores[hole], let winningScore = winningPlayer.scores![hole], competing < winningScore{
                winningPlayer = player
            }
            
        }
        
        return winningPlayer.profile.id == player.profile.id
    }
    
    public func adjustStrokesAccordingToTees() {
        // Adjust the player handicaps
        for (index, var player) in players.enumerated() {
            guard let tee = player.tee else {
                log.warning("Cannot adjust strokes for player \(index) because teebox is not set")
                continue
            }
            
            player.playingHandicap = calculateCourseHandicap(handicap: player.profile.handicap, slope: Int(tee.slope ?? 113))
            players[index] = player
        }
        
        // Now that all the playing handicaps are calculated, we can calculate the number of strokes and assign them
        for (index, numberOfStrokes) in numberOfStrokesPerPlayer(withPlayingHandicaps: players.map { $0.playingHandicap }, matchType: self.type).enumerated() {
            players[index].strokes = numberOfStrokes
        }
    }
}

public enum MatchPostError : Error {
    case matchPostFailed(Error)
    case photoUploadFailed(Error)
}

extension ConceptMatch {
    private var winningTeamIndexForJSON: Any {
        switch winningTeam {
        case .some(team1): return 1
        case .some(team2): return 2
        default: return NSNull()
        }
    }
    
    /// Posts the match to the server
    /// The observer accepts a Double which represents the upload progress
    public func post() -> Observable<Double> {
        return Observable.create { observer in
            let apiPlayers: [Any] = self.players.map { player in
                return [
                    "id": player.profile.id,
                    "tee_id": player.tee?.id ?? NSNull(),
                    "scores": player.scores?.map { score -> Any in return score ?? NSNull() } ?? NSNull(),
                    "playing_handicap": player.playingHandicap,
                    "stroke_count": player.strokes
                    ] as [String : Any]
            }
            
            
            
            
            var body: [String : Any] = [
                "winning_score": self.score.winningScore,
                "holes_not_played": self.score.holesNotPlayed,
                "winning_team": self.winningTeamIndexForJSON,
                
                "course_id": (self.course?.id ?? NSNull()) as Any,
                "type": self.type.rawValue,
                "players": apiPlayers,
                "description": self.text,
                "final_score": self.score.rendered
            ]
            
            if let date = self.date {
                body["played_at"] = "\(date)".prefix(10)
                
            }
            
            let request = Alamofire.request(api_url + "/matches",
                                            method: .post,
                                            parameters: body,
                                            encoding: JSONEncoding.default,
                                            headers: AuthenticationManager.shared.headers
                ).validate().responseJSON { response in
                    switch response.result {
                    case .failure(let error):
                        if let data = response.data {
                            let json = String(data: data, encoding: String.Encoding.utf8)
                            print("Match post error Failure Response: \(json)")
                        }
                        observer.onError(MatchPostError.matchPostFailed(error))
                    case .success(let value):
                        let json = JSON(value)
                        
                        // Push recent contacts
                        RecentContactsManager.push(profiles: self.players.map { $0.profile })
                        NotificationCenter.default.post(name: Notification.Name("refreshFeed"), object: nil)
                        self.uploadPhotos(for: observer, id: json["id"].intValue)
                    }
            }
            
            print(request, body)
            
            return Disposables.create(with: request.cancel)
        }
    }
    
    private func uploadPhotos(for observer: AnyObserver<Double>, id: Int) {
        func performRefresh() {
//            Broadcaster.notify(Refreshable.self) {
//                $0.refresh(entity: .match)
//            }
//
//            Broadcaster.notify(NewMatchPostedNotificationReceiving.self) {
//                $0.newMatchPosted()
//            }
        }
        
        guard photos.count > 0 else {
            observer.onCompleted()
            performRefresh()
            return
        }
        
        log.info("uploading photos for match with id \(id)")
        
        let body: [[String : Any]] = photos.map { photo in
            let data = UIImageJPEGRepresentation((photo.image?.resizeWithPercent(percentage: 0.6)!)!, 0.8)!
            
            
            
            return [
                "photo": data.base64EncodedString(),
                "about": photo.comment
            ]
        }
        
        
        do {
            let jsonData = try JSON(body).rawData(options: [])
            Alamofire.upload(jsonData, to: api_url + "/matches/\(id)/photos", method: .post, headers: [
                "Authorization": AuthenticationManager.shared.headers["Authorization"]!,
                "Content-Type": "application/json; charset=utf-8"])
                .uploadProgress { progress in
                    observer.onNext(progress.fractionCompleted)
                }.validate().responseJSON { response in
                    switch response.result {
                    case .failure(let error):
                        observer.onError(MatchPostError.photoUploadFailed(error))
                    case .success:
                        observer.onCompleted()
                        Defaults[.savedMatch] = nil // remove saved match if present if the post has succeeded
                        performRefresh()
                    }
            }
        } catch {
            observer.onError(MatchPostError.photoUploadFailed(error))
        }
    }
}
