import Alamofire
import SwiftyJSON
import SwiftyUserDefaults

let currentUserInstance: CurrentUser = CurrentUser()
class CurrentUser: UserProtocol {
    // Singleton, since there's only ONE user logged in at a time.
    class var shared: CurrentUser {
        return currentUserInstance
    }
    
    var id: String
    var firstName: String
    var lastName: String
    var username: String
    var handicap: Double
    var email: String
    var privacy: Bool
    var verified: Bool
    var matchCount: Int
    var followerCount: Int
    var followingCount: Int
    var following: Bool
    var savePicturesToLibrary: Bool
    var homeCourseId: Int
    var homeCourseName: String
    
    init() {
        self.id = Defaults[.userId]
        self.firstName = Defaults[.firstName]
        self.lastName = Defaults[.lastName]
        self.username = Defaults[.username]
        self.handicap = Defaults[.handicap]
        self.privacy = Defaults[.userIsPrivate]
        self.verified = Defaults[.userIsVerified]
        self.email = Defaults[.email]
        self.matchCount = Defaults[.matchCount]
        self.followerCount = Defaults[.followerCount]
        self.followingCount = Defaults[.followingCount]
        self.following = true
        self.savePicturesToLibrary = Defaults[.savePhotos]
        self.homeCourseId = Defaults[.homeCourseId]
        self.homeCourseName = Defaults[.homeCourseName]
    }
    
    public func save() {
        Defaults[.userId] = self.id
        Defaults[.username] = self.username
        Defaults[.firstName] = self.firstName
        Defaults[.lastName] = self.lastName
        Defaults[.handicap] = self.handicap
        Defaults[.homeCourseId] = self.homeCourseId
        Defaults[.homeCourseName] = self.homeCourseName
        Defaults[.savePhotos] = self.savePicturesToLibrary
        Defaults[.email] = self.email
        Defaults[.matchCount] = self.matchCount
        Defaults[.followerCount] = self.followerCount
        Defaults[.followingCount] = self.followingCount
        Defaults[.userIsVerified] = self.verified
        Defaults[.userIsPrivate] = self.privacy
        //TODO: Refresh profile
    }
    
    public func updateUser(callback: @escaping (Result<Void>) -> ()) {
        let params : [String : Any] = [
            "username" : self.username,
            "first_name" : self.firstName,
            "last_name" : self.lastName,
            "handicap" : self.handicap,
            "home_course" : self.homeCourseId,
            "email" : self.email,
            "privacy" : self.privacy
        ]
        
        
        Alamofire.request("\(api_url)/users/me", method: .put, parameters: params, encoding: JSONEncoding.default, headers: AuthenticationManager.shared.headers).validate().responseJSON { response in
            guard response.value != nil else {
                log.error("Failed updating user with id: \(self.id), error: \(response)")
                callback(.failure(response.error!))
                return
            }
            
            self.save()
            
            callback(.success())
        }
    }
    
    fileprivate func refreshHomeCourseData() {
        Course.byId(self.homeCourseId) { response in
            switch response {
            case .success(let course):
                self.homeCourseName = course.name
                Defaults[.homeCourseName] = course.name
            case .failure(let error):
                log.error("Failed to get home course for user: \(self.id). Error: \(error)")
            }
        }
    }
    
    
    public func updateLocal(callback: @escaping (Result<Void>) -> ()) {
        Alamofire.request("\(api_url)/users/me", method: .get, headers: AuthenticationManager.shared.headers).validate().responseJSON { response in
            
                guard let value = response.value else {
                    log.error("Failed updating user with ID: \(self.id). Response: \(response)")
                    callback(.failure(response.error!))
                    return
                }
                
                let json = JSON(value)
                let user = User.me
            
                user.id = json["id"].stringValue
                user.username = json["username"].stringValue
                user.firstName = json["first_name"].stringValue
                user.lastName = json["last_name"].stringValue
                user.handicap = json["handicap"].doubleValue
                user.matchCount = json["match_count"].intValue
                user.email = json["email"].stringValue
                user.followerCount = json["follower_count"].intValue
                user.followingCount = json["following_count"].intValue
                user.verified = json["verified"].boolValue
                user.privacy = json["privacy"].boolValue
                user.homeCourseId = json["home_course"].intValue
            
                log.verbose("updated user id: \(self.id)")
            
                self.refreshHomeCourseData()
                self.canSavePhotos()
                user.save()
                callback(.success())
        }
    }
    
    public func canSavePhotos() {
        var found = false
        
        Alamofire.request("\(api_url)/users/me/settings", headers: AuthenticationManager.shared.headers).validate().responseJSON { response in
                switch response.result {
                case .failure(let error):
                    log.error("Error while trying to update user with ID: \(self.id). Error: \(error)")
                case .success(let data):
                    for json in JSON(data).array! {
                        if json["key"].intValue == 10000 {
                            self.setSavePhotos(json["value"].boolValue)
                            found = true
                        }
                    }
                }
        }
        
        if !found {
            self.setSavePhotos(false)
        }
    }
    
    public func pushNotificationDeviceToken(_ deviceToken: Data) {
        Alamofire.request("\(api_url)/users/me/ios-notification-token", method: .put, parameters: ["token": deviceToken.base64EncodedString()], encoding: JSONEncoding.default, headers: AuthenticationManager.shared.headers).validate().responseJSON { response in
                switch response.result {
                case .success(_):
                    log.error("Pushing notification token succeeded")
                case .failure(let error):
                    log.error("Pushing notification token failed: \(error)")
                }
        }
    }
    
    public func updateHandicap(_ newValue: Double, handler: @escaping (Result<Void>) -> ()) {
       self.handicap = newValue
       self.save()
        
        Alamofire.request("\(api_url)/users/me",
                          method: .put,
                          parameters: ["handicap": newValue],
                          encoding: JSONEncoding.default,
                          headers: AuthenticationManager.shared.headers)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .success:
                    log.info("Handicap update to \(newValue) succeeded")
                    handler(.success(()))
                case .failure(let error):
                    log.error("Could not update handicap for user with id \(self.id) to \(newValue): \(error)")
                    handler(.failure(error))
                }
        }
    }
   
}


extension CurrentUser {
    public func setFirstname(_ firstName: String) {
        self.firstName = firstName
        save()
    }
    
    public func setHomeCourse(_ homeCourseId: Int, _ homeCourseName: String) {
        self.homeCourseId = homeCourseId
        self.homeCourseName = homeCourseName
        save()
        refreshHomeCourseData()
    }
    
    public func setLastname(_ lastName: String) {
        self.lastName = lastName
        save()
    }
    
    
    public func setEmail(_ email: String) {
        self.email = email
        save()
    }
    
    public func setHandicap(_ handicap: Double) {
        self.handicap = handicap
        save()
    }
    
    public func setPrivacy(_ isPrivate: Bool) {
        self.privacy = isPrivate
        save()
    }
    
    
    public func setAvatar(_ image: UIImage) {
        
        let data = UIImageJPEGRepresentation(image.resizeWithPercent(percentage: 0.5)!, 0.8)
        _ = Alamofire.upload(data!, to: "\(api_url)/users/me/profile-picture", method: .post, headers: AuthenticationManager.shared.headers)
    }
    
    public func setSavePhotos(_ option: Bool) {
        self.savePicturesToLibrary = option
    
        var request = try! URLRequest(url: "\(api_url)/users/me/settings", method: .put, headers: AuthenticationManager.shared.headers)
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpBody = try! JSONSerialization.data(withJSONObject: ["settings": ["10000" : option]], options: [])
        
        Alamofire.request(request).validate().responseJSON { response in
            switch response.result {
            case .failure(let error):
                log.error("Could not update the 'save photos' setting, error: \(error)")
                assertionFailure()
            case .success( _):
                log.info("Save photos settings updated")
            }
        }
    }
}

