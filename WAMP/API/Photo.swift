import UIKit

public final class Photo {
    public var url: URL?
    public var image: UIImage?
    public var hole: Int? = nil
    public var comment: String = ""
    
    public init(image: UIImage) {
        self.image = image
    }
    
    public init(url: URL) {
        self.url = url
    }
    
    public var description: String {
        var desc = "Photo"
        
        if let url = url {
            desc += " with URL \(url)"
        }
        
        if let hole = hole {
            desc += " with hole \(hole)"
        }
        
        if comment.count > 0 {
            desc += " comment: \"\(comment)\""
        }
        
        return desc
    }
}

extension Photo : Equatable {
    public static func ==(lhs: Photo, rhs: Photo) -> Bool {
        /// Not entirely correct, but serves the purpose here
        return lhs === rhs
    }
}
