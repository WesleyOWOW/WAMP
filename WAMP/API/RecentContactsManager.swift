import SwiftyUserDefaults

public typealias RecentContact = (id: String, name: String, avatarURL: URL)

/// Manages the recent contacts, stored in the user defaults
/// At most 10 recent contacts will be saved
public enum RecentContactsManager {
    /// Loads the recent contacts from the user defaults
    public static func fetch() -> [RecentContact] {
        return deserializeRecentContacts(from: Defaults[.recentContacts])
    }
    
    /// Pushes the given profiles onto the recent contacts stack, saving them in the user defaults
    public static func push(profiles: [UserProtocol]) {
        var recents = deserializeRecentContacts(from: Defaults[.recentContacts])
        recents += profiles.filter { !$0.isMe }.map { (id: $0.id, name: "\($0.firstName) \($0.lastName)", avatarURL: $0.avatarURL) }
        
        // remove duplicates
        var result = [RecentContact]()
        for recent in recents {
            guard !result.contains(where: { $0.id == recent.id }) else { continue }
            result.append(recent)
        }
        
        Defaults[.recentContacts] = serializeRecentContacts(from: Array(result.suffix(10)))
    }
    
    /// Helper function to serialize recent contacts
    private static func serializeRecentContacts(from source: [RecentContact]) -> [Any] {
        return source.map { ["name": $0.name, "url": $0.avatarURL.absoluteString, "id": $0.id] as [String: Any] }
    }
    
    /// Helper function to deserialize recent contacts
    private static func deserializeRecentContacts(from source: [Any]) -> [RecentContact] {
        return source.flatMap { $0 as? [String: Any] }.flatMap {
            guard let id = $0["id"] as? String, let name = $0["name"] as? String, let urlString = $0["url"] as? String, let url = URL(string: urlString) else {
                return nil
            }
            
            return (id: id, name: name, avatarURL: url)
        }
    }
    
    /// Removes all stored recent contacts
    public static func reset() {
        Defaults[.recentContacts] = []
    }
}

