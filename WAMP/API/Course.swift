import Foundation
import Alamofire
import CodableAlamofire

public class Course : Codable {
    public var id: Int = 0
    public var name: String = ""
    public var country: String?
    public var city: String?
    public var lat: Double!
    public var long: Double!
    public var tees: [Tee]!
    
    /// Downloads the course with the given ID
    public static func byId(_ courseId: Int, callback: @escaping (Result<Course>) -> ()) {
        Alamofire.request("\(api_url)/courses/\(courseId)", method: .get, headers: AuthenticationManager.shared.headers) .responseDecodableObject { (response: DataResponse<Course>) in
                callback(response.result)
        }
    }
    
    /// Fetches the nearby courses
    public static func findNearby(latitude: Double, longitude: Double, callback: @escaping (Result<[Course]>) -> ()) -> DataRequest {
        return Alamofire.request("\(api_url)/courses/nearby", method: .get, parameters: ["lat": latitude, "long": longitude]).responseDecodableObject { (response: DataResponse<[Course]>) in
                callback(response.result)
        }
    }
    
    public static func find(query: String, callback: @escaping (Result<[Course]>) -> ()) -> DataRequest {
        return Alamofire.request("\(api_url)/courses/search", method: .post, parameters: ["query": query], encoding: JSONEncoding.default).responseDecodableObject { (response: DataResponse<[Course]>) in
                callback(response.result)
        }
    }
}

public class Tee : Codable {
    public var id: Int!
    public var courseId: Int!
    public var name: String = ""
    public var slope: Int?
    public var rating: Double?
    public var holes: [Hole]!
    
    private enum CodingKeys : String, CodingKey {
        case id
        case courseId = "course_id"
        case name
        case slope
        case rating
        case holes
    }
}

public class Hole : Codable {
    public var id: Int!
    public var teeId: Int!
    public var hole: Int!
    public var par: Int?
    public var tee: Int?
    public var strokeIndex: Int?
    
    private enum CodingKeys : String, CodingKey {
        case id, hole, par, tee
        case teeId = "tee_id"
        case strokeIndex = "stroke_index"
    }
}

