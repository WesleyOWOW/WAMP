import RxSwift
import Alamofire
import SwiftyJSON

// MARK -- Protocol
public protocol UserProtocol : class {
    var id: String { get }
    var firstName: String { get }
    var lastName: String { get }
    var username: String { get }
    var handicap: Double { get }
    var privacy: Bool { get }
    var verified: Bool { get }
    var matchCount: Int { get }
    var followerCount: Int { get }
    var followingCount: Int { get }
    var following: Bool { get }
}

extension UserProtocol {
    public var avatarURL: URL {
        return URL(string: api_url + "/users/" + self.id + "/profile-picture?size=210")!
    }
    
    public var fullsizeAvatarURL: URL {
        return URL(string: api_url + "/users/" + self.id + "/profile-picture")!
    }
    
    public func renderHandicap(includeSuffix: Bool) -> String {
        return self.handicap.renderAsHandicap(includeSuffix: includeSuffix)
    }
    
    public var isMe: Bool {
        return self.id == User.me.id
    }
    
    public typealias Stats = (group: String, wins: Int, losses: Int, ties: Int)
    public func fetchStats() -> Observable<[Stats]> {
        return Observable.create { observer in
            let request = Alamofire.request(
                api_url + "/users/\(self.id)/stats",
                method: .get,
                headers: AuthenticationManager.shared.headers)
                .validate()
                .responseJSON { response in
                    switch response.result {
                    case .failure(let error):
                        observer.onError(error)
                    case .success(let value):
                        // map to stat instances and return to observer
                        // sample json: {"matches":12,"followers":12,"following":10,"stats":{"2017":{"single":{"wins":1,"losses":5,"ties":1},"double":{"wins":4,"losses":1,"ties":0},"bestball":{"wins":0,"losses":0,"ties":0},"foursome":{"wins":0,"losses":0,"ties":0},"total":{"wins":5,"losses":6,"ties":1}}}}
                        let json = JSON(value)
                        let stats = json["stats"].map { (group, stats) -> Stats in
                            return (group, stats["wins"].intValue, stats["losses"].intValue, stats["ties"].intValue)
                        }
                        
                        print("stats", stats)
                        
                        observer.onNext(stats)
                        observer.onCompleted()
                    }
            }
            
            return Disposables.create(with: request.cancel)
        }
    }
    
    public typealias ComparisonStats = (group: String, leftWins: Int, ties: Int, rightWins: Int)
    public func fetchComparisonStats() -> Observable<[ComparisonStats]> {
        return Observable.create { observer in
            let request = Alamofire.request(
                api_url + "/users/\(User.me.id)/stats/\(self.id)",
                method: .get,
                headers: AuthenticationManager.shared.headers)
                .validate()
                .responseJSON { response in
                    switch response.result {
                    case .failure(let error):
                        observer.onError(error)
                    case .success(let value):
                        let json = JSON(value)
                        let stats = json.map { (arg) -> ComparisonStats in
                            let (gameType, json) = arg // because otherwise it does not compile at the moment
                            return (gameType, json["left"]["amount"].intValue, json["tie"]["amount"].intValue, json["right"]["amount"].intValue)
                        }
                        
                        observer.onNext(stats)
                        observer.onCompleted()
                    }
            }
            
            return Disposables.create(with: request.cancel)
        }
    }
    
    func fetchFollowing(callback: @escaping (Result<[User]>) -> ()) {
        fetchRelatedUserList(lastPathComponent: "following", callback: callback)
    }
    
    func fetchFollowers(callback: @escaping (Result<[User]>) -> ()) {
        fetchRelatedUserList(lastPathComponent: "followers", callback: callback)
    }
    
    private func fetchRelatedUserList(lastPathComponent: String, callback: @escaping (Result<[User]>) -> ()) {
        Alamofire.request(
            api_url + "/users/\(self.id)/\(lastPathComponent)",
            method: .get,
            headers: AuthenticationManager.shared.headers)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .failure(let error):
                    callback(.failure(error))
                case .success(let value):
                    // map to guest instances and return to callback
                    let json = JSON(value)
                    callback(.success(json.flatMap { try? User(json: $0.1) }))
                }
        }
    }
    
    
}

// The base user class. Handles stuff accessible for ALL users. Not just the currently logged in user.
class User: UserProtocol {
    var id: String
    var firstName: String
    var lastName: String
    var username: String
    var handicap: Double
    var privacy: Bool
    var verified: Bool
    var email: String
    var matchCount: Int
    var followerCount: Int
    var followingCount: Int
    var following: Bool
    
    public init(json: JSON) throws {
        self.id = json["id"].stringValue
        self.username = json["username"].string ?? "guest"
        self.firstName = json["first_name"].stringValue
        self.lastName = json["last_name"].stringValue
        self.handicap = json["handicap"].doubleValue
        self.following = json["is_following"].boolValue
        self.email = json["email"].stringValue
        self.matchCount = json["match_count"].intValue
        self.followerCount = json["follower_count"].intValue
        self.followingCount = json["following_count"].intValue
        self.verified = json["verified"].boolValue
        self.privacy = json["privacy"].boolValue
    }
    
    public static var me: CurrentUser {
        return CurrentUser.shared
    }
    
    public static func fetch(id: String, callback: @escaping (Result<User>) -> ()) {
        Alamofire.request(
            "\(api_url)/users/\(id)",
            method: .get,
            headers: AuthenticationManager.shared.headers)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .failure(let error):
                    callback(.failure(error))
                case .success(let value):
                    let json = JSON(value)
                    do {
                        callback(.success(try User(json: json)))
                    } catch {
                        callback(.failure(error))
                    }
                }
        }
    }
    
    public func toggleFollow(callback: @escaping (Result<Void>) -> ()) {
        let url = api_url + "/users/" + self.id + (self.following ? "/unfollow" : "/follow")
        
        log.verbose("toggling follow using url: \(url)")
        
        Alamofire.request(url,
                          method: .get,
                          headers: AuthenticationManager.shared.headers)
            .validate()
            .responseJSON { response in
                log.verbose("follow toggle status code: \(response.response!.statusCode)")
                switch response.result {
                case .failure(let error):
                    callback(.failure(error))
                case .success(_):
                    self.following = !self.following
                    
                    if self.following {
                        self.followerCount += 1
                    } else {
                        self.followerCount -= 1
                    }
                    
                    callback(.success())
                
                    //TODO: Refresh profile
                }
        }
    }
}

public class Player : UserProtocol {
    
    
    public var id: String
    public var firstName: String
    public var lastName: String
    public var username: String
    public var handicap: Double
    public var team: Team
    public var verified: Bool
    public var privacy: Bool
    public var isFollowing: Bool
    
    // Not needed. (Can't delete due to the protocol.
    public var matchCount: Int = 0
    public var followerCount: Int = 0
    public var followingCount: Int = 0
    public var following: Bool = true
    
    public init?(json: JSON, team: Team? = nil) {
        self.id = json["id"].stringValue
        self.firstName = json["first_name"].stringValue
        self.lastName = json["last_name"].stringValue
        self.username = json["username"].string ?? "guest"
        self.handicap = json["handicap"].doubleValue
        self.team = team ?? Team(rawValue: json["team"].stringValue) ?? .neutral
        self.verified = json["verified"].boolValue
        self.isFollowing = json["is_following"].boolValue
        self.privacy = json["privacy"].boolValue
    }
    
    
    public func toggleFollow(callback: @escaping (Result<Void>) -> ()) {
        let url = api_url + "/users/" + self.id + (self.following ? "/unfollow" : "/follow")
        
        log.verbose("toggling follow using url: \(url)")
        
        Alamofire.request(url,
                          method: .get,
                          headers: AuthenticationManager.shared.headers)
            .validate()
            .responseJSON { response in
                log.verbose("follow toggle status code: \(response.response!.statusCode)")
                switch response.result {
                case .failure(let error):
                    callback(.failure(error))
                case .success(_):
                    self.following = !self.following
                    
                    if self.following {
                        self.followerCount += 1
                    } else {
                        self.followerCount -= 1
                    }
                    
                    callback(.success())
                    
                    //TODO: Refresh profile
                }
        }
    }
}


