import SwiftyJSON
import Alamofire
import SwiftNotificationCenter

enum CourseError : Error {
    case courseNotFound(id: Int)
}

enum APIMatchError : Error {
    case invalidJSON
}


public class APIMatch : Match {
    /// Custom text that goes with the match
    public var text: String
    
    /// The match date
    public var date: Date?
    
    public var dateString: String
    
    /// The remote match id
    public var id: Int
    
    /// The winning team, or `nil` for square
    public var winningTeam: Team?
    
    /// The type of this game, like single or double
    public var type: MatchType
    
    /// The final score of the match
    public var score: MatchplayScore
    
    /// The players that have taken part in this match
    public var players: [MatchPlayer]
    
    /// The primary team
    public var team1: Team {
        return players.first!.team
    }
    
    /// The amount of photos associated with the match
    public var photoCount: Int
    
    /// The amount of likes
    public var likeCount: Int
    
    /// The amount of comments
    public var commentCount: Int
    
    /// The course of this match, if known
    public var course: Course?
    
    /// If the user has liked the match
    public var liked: Bool
    
    /// If the player has commented on the match
    public var commented: Bool
    
    
    /// The original JSON this match was constructed with
    public var originalJSON: JSON
    
    /// Fetches the given photo
    public func getPhoto(index: Int) -> Photo? {
        if index < photos.count {
            return photos[index]
        }
        
        return nil
    }
    
    var photos: [Photo]
    
    private init(text: String, date: Date?, dateString: String, id: Int, winningTeam: Team?, type: MatchType, score: MatchplayScore, players: [MatchPlayer], photoCount: Int, likeCount: Int, commentCount: Int, course: Course?, liked: Bool, commented: Bool, originalJSON: JSON, photos: [Photo]) {
        self.text = text
        self.date = date
        self.id = id
        self.winningTeam = winningTeam
        self.type = type
        self.score = score
        self.players = players
        self.photoCount = photoCount
        self.likeCount = likeCount
        self.commentCount = commentCount
        self.course = course
        self.liked = liked
        self.commented = commented
        self.originalJSON = originalJSON
        self.dateString = dateString
        self.photos = photos
    }
    
    public static func load(json: JSON, callback: (Result<APIMatch>) -> ()) {
        guard
            let id = json["id"].int,
            let type = MatchType(rawValue: json["type"].stringValue),
            let winningScore = json["winning_score"].int,
            let holesNotPlayed = json["holes_not_played"].int,
            let photoCount = json["photo_count"].int
            else {
                log.error("invalid match JSON, id: \(json["id"].stringValue)")

                callback(.failure(APIMatchError.invalidJSON))
                return
        }
        
        let team1 = Team.blue
        
        let score = MatchplayScore(winningScore, holesNotPlayed)
        
        func winningTeamFromJSON() -> Team? {
            if score.isSquare {
                return nil
            }
            
            
            switch json["winning_team"].intValue {
            case 1: return team1
            case 2: return team1.opposite
            default: return nil
            }
        }
        
        let winningTeam = winningTeamFromJSON()
        let liked = json["liked"].boolValue
        let commented = json["commented"].boolValue
        let likeCount = json["like_count"].intValue
        let commentCount = json["comment_count"].intValue
        let text = json["description"].stringValue
        var course: Course? = nil
        
        // This would be nicer if everything here would be Decodable, but hey
        if let courseDictionary = json["course"].dictionaryObject {
            do {
                let jsonData = try JSONSerialization.data(withJSONObject: courseDictionary)
                course = try JSONDecoder().decode(Course.self, from: jsonData)
            } catch {
                log.error("EWA")
                callback(.failure(error))
                return
            }
        }
        
        let players = json["players"].flatMap { (indexString, playerJson) -> MatchPlayer? in
            guard let index = Int(indexString) else {
                log.error("Invalid index \(indexString) in players object on match \(json["id"].stringValue)")
                return nil
            }
            
            // Parse the team
            let team: Team
            if index < (json["players"].count / 2) {
                team = team1
            } else {
                team = team1.opposite
            }
            
            // Get the profile
            guard let profile = Player(json: playerJson, team: team) else {
                return nil
            }
            
            // Parse the scores, if existent
            var scores: [Int?]? = nil
            if let scoreJson = playerJson["scores"].array {
                scores = scoreJson.map { $0.int }
            }
            
            var tee: Tee? = nil
            if let teeId = playerJson["tee_id"].int {
                tee = course?.tees.first(where: { $0.id == teeId })
            }
            
            return (profile, team, scores, tee, playerJson["stroke_count"].intValue, playerJson["playing_handicap"].doubleValue)
        }
        
        let photos = json["photos"].flatMap { (_, photoJson) -> Photo? in
            guard let url = URL(string: photoJson["url"].stringValue + "?size=640") else {
                log.error("invalid photo url in photoJson \(photoJson)")
                return nil
            }
            
            let photo = Photo(url: url)
            photo.comment = photoJson["about"].stringValue
            photo.hole = photoJson["hole"].int
            return photo
        }
        
        if players.count < 2 || (players.count % 2) != 0 {
            log.error("invalid player count \(players.count) for match id \(id)")
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-mm-dd hh:mm:ss"
        dateFormatter.locale = Locale(identifier: "nl")
        let date = dateFormatter.date(from: json["played_at"].stringValue) ?? nil
        
        let match = APIMatch(text: text, date: date, dateString: String(json["played_at"].stringValue.prefix(10)), id: id, winningTeam: winningTeam, type: type, score: score, players: players, photoCount: photoCount, likeCount: likeCount, commentCount: commentCount, course: course, liked: liked, commented: commented, originalJSON: json, photos: photos)
        callback(.success(match))
    }
}

extension APIMatch {
    @discardableResult
    public func toggleLike(callback: @escaping ((Result<Void>) -> ()) = { _ in }) -> DataRequest {
        let url: String
        if self.liked {
            self.likeCount -= 1
            self.liked = false
            url = api_url + "/matches/\(self.id)/unlike"
        } else {
            self.likeCount += 1
            self.liked = true
            url = api_url + "/matches/\(self.id)/like"
        }
        
        return Alamofire.request(url, method: .get, headers: AuthenticationManager.shared.headers)
            .validate(statusCode: 200..<300)
            .responseJSON { response in
                switch response.result {
                case .success(_):
                    callback(.success())
                case .failure(let error):
                    callback(.failure(error))
                }
        }
    }
    
    @discardableResult
    public func delete(callback: @escaping ((Result<Void>) -> ()) = { _ in }) -> DataRequest {
        return Alamofire.request(api_url + "/matches/\(self.id)", method: .delete, headers: AuthenticationManager.shared.headers)
            .validate(statusCode: 200..<300)
            .responseJSON { response in
                switch response.result {
                case .success(_):
                    NotificationCenter.default.post(name: Notification.Name("refreshFeed"), object: nil)
                    BaseViewController.shared.feedViewController.reload()
//                    Broadcaster.notify(Refreshable.self) { $0.refresh(entity: .match) }
                    
                    callback(.success())
                case .failure(let error):
                    callback(.failure(error))
                }
        }
    }
    
    /// Returns if the logged in user can delete this match
    public var canDelete: Bool {
        return players.first?.profile.id == User.me.id
    }
}

extension APIMatch : CustomStringConvertible {
    public var description: String {
        return "APIMatch(id: \(self.id), photoCount: \(self.photoCount), type: \(self.type.rawValue)) with players: \n" + self.players.map{"\($0)"}.joined(separator: "\n")
    }
}

extension APIMatch {
    /// Downloads the match associated with the given ID
    public static func byId(_ id: Int, callback: @escaping (Result<APIMatch>) -> ()) {
        Alamofire.request(api_url + "/matches/\(id)",
            method: .get,
            headers: AuthenticationManager.shared.headers)
            .responseJSON { response in
                switch response.result {
                case .success(let jsonAny):
                    let json = JSON(jsonAny)
                    APIMatch.load(json: json, callback: callback)
                case .failure(let error):
                    callback(.failure(error))
                }
        }
    }
}

