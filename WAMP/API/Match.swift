import UIKit
import Foundation

public enum MatchType : String, Codable {
    case single
    case double = "4BBB"
    
    public init?(rawValue: String) {
        switch rawValue.lowercased() {
        case "single": self = .single
        case "double", "4bbb": self = .double
        default: return nil
        }
    }
    
    public var localizedDescription: String {
        switch self {
        case .single: return "Single"
        case .double: return "4BBB"
        }
    }
}

public enum HoleResultValue {
    case square
    case unplayed
    case red
    case blue
    
    public static let allCases: [HoleResultValue] = [.square, .unplayed, .red, .blue]
    public static var random: HoleResultValue { return allCases.random }
}

/// The result of a matchplay match
public struct MatchplayScore {
    /// The relative winning score. When the score is `1UP`, this will be 1. When te score is `2&1`, this will be 2.
    public var winningScore: Int
    
    /// The amount of holes skipped. For example, when the score is '2&1', this will be 1.
    public var holesNotPlayed: Int
    
    /// A readable string, like "AS", "2&1" or "2UP"
    public var rendered: String {
        if winningScore == 0 {
            return "AS"
        } else if holesNotPlayed == 0 {
            return "\(winningScore)UP"
        } else {
            return "\(winningScore)&\(holesNotPlayed)"
        }
    }
    
    public var finalScore: [String] {
        if winningScore == 0 {
            return ["AS", "AS"]
        } else if holesNotPlayed == 0 {
            return ["\(winningScore)UP", "\(winningScore)UP"]
        } else {
            return ["\(winningScore)", "\(holesNotPlayed)"]
        }
    }
    
    public var nonfinalRendering: String {
        if winningScore == 0 {
            return "AS"
        } else {
            return "\(winningScore)UP"
        }
    }
    
    // Returns if the score is square
    public var isSquare: Bool {
        return winningScore == 0
    }
    
    public init(_ winningScore: Int, _ holesNotPlayed: Int) {
        self.winningScore = winningScore
        self.holesNotPlayed = holesNotPlayed
    }
}

extension MatchplayScore : Equatable {
    public static func ==(lhs: MatchplayScore, rhs: MatchplayScore) -> Bool {
        return lhs.winningScore == rhs.winningScore &&
            lhs.holesNotPlayed == rhs.holesNotPlayed
    }
}

public typealias MatchPlayer = (profile: UserProtocol, team: Team, scores: [Int?]?, tee: Tee?, strokes: Int, playingHandicap: Double)

/// The `Match` protocol defines the read-only API of a Matchplay-match.
public protocol Match : class {
    /// The type of this game, like single or double
    var type: MatchType { get }
    
    /// The final score of the match
    var score: MatchplayScore { get }
    
    /// The players that have taken part in this match
    var players: [MatchPlayer] { get }
    
    /// The primary team. A default implementation is provided
    var team1: Team { get }
    
    /// The secondary team. A default implementation is provided that returns the opposite of `team1`.
    var team2: Team { get }
    
    /// The match date
    var date: Date? { get }
    
    /// Custom text that goes with the match
    var text: String { get }
    
    /// The winning team, or `nil` for square
    var winningTeam: Team? { get }
    
    /// The amount of photos associated with the match
    var photoCount: Int { get }
    
    /// The course of this match, if known
    var course: Course? { get }
    
    /// Fetches the given photo
    func getPhoto(index: Int) -> Photo?
}


extension Match {
    public var team1: Team {
        return players.first!.team
    }
    
    public var team2: Team {
        return team1.opposite
    }
    
    public func isHoleWinner(for hole: Int, and player: MatchPlayer) -> Bool {
        var winningPlayer: MatchPlayer = players.first!
        var holes = [[HolePlayer]]()
        
        for (_, player) in players.enumerated() {
            guard let scores = player.scores else {
                // We need the scores of all players to be able to summarize the match
                return false
            }
            
            // Create the arrays for every hole, if needed
            if holes.count == 0 {
                holes = Array(repeating: [], count: scores.count)
            }
            
            // Sanity check.
            guard scores.count == holes.count else {
                // The holes don't match up!
                log.error("Amount of hole scores not the same for every player in a match. Match: \(self)")
                return false
            }
            
            
            if winningPlayer.scores![hole] == nil {
                return false
            } else if let competing = scores[hole], let winningScore = winningPlayer.scores![hole], competing < winningScore{
                winningPlayer = player
            }

        }
        
        return winningPlayer.profile.id == player.profile.id
    }
    
    typealias HolePlayer = (team: Team, score: Int?, index: Int)
    /// Calculates and returns the score of each team for every hole
    public func getScoresPerTeam() -> [(red: Int?, blue: Int?)]? {
        // First, we transform the players array into another array that has a separate entry for each hole.
        
        var holes = [[HolePlayer]]()
        
        for (playerIndex, player) in players.enumerated() {
            guard let scores = player.scores else {
                // We need the scores of all players to be able to summarize the match
                return nil
            }
            
            // Create the arrays for every hole, if needed
            if holes.count == 0 {
                holes = Array(repeating: [], count: scores.count)
            }
            
            // Sanity check.
            guard scores.count == holes.count else {
                // The holes don't match up!
                log.error("Amount of hole scores not the same for every player in a match. Match: \(self)")
                return nil
            }
            
            // Append the value for every hole to each array
            for (scoreIndex, score) in scores.enumerated() {
                let holePlayer = (player.team, score, playerIndex)
                holes[scoreIndex].append(holePlayer)
            }
        }
        
        // Get the holes with assigned strokes
        let holesWithAssignedStrokePerPlayer = self.holesWithAssignedStrokePerPlayer()
        
        // Now, finally, calculate the scores per team for each hole
        return holes.enumerated().map { (holeIndex, holePlayers) in
            /// Returns the combined score of the given team, or `nil` if no member of the team played on this hole
            func teamScore(_ team: Team) -> Int? {
                let teamPlayers = holePlayers.filter { $0.team == team }
                var teamScore: Int? = nil
                for player in teamPlayers {
                    // First check if we actually have a score for this player
                    guard var playerScore = player.score else {
                        continue
                    }
                    
                    func amountOfStrokes(onHoleWithIndex index: Int) -> Int {
                        return holesWithAssignedStrokePerPlayer[player.index].first { $0.index == holeIndex }?.strokes ?? 0
                    }
                    
                    if playerScore > 0 {
                        playerScore -= amountOfStrokes(onHoleWithIndex: holeIndex)
                    }
                    
                    // Lowest score counts
                    if let currentTeamScore = teamScore, currentTeamScore > playerScore {
                        teamScore = playerScore
                    } else if teamScore == nil {
                        teamScore = playerScore
                    }
                }
                return teamScore
            }
            
            // Calculate the scores for each team
            return(teamScore(.red), teamScore(.blue))
        }
    }
    
    /// Returns all holes where the player gets a stroke
    public func holesWithAssignedStrokePerPlayer() -> [[(index: Int, hole: Hole, strokes: Int)]] {
        return players.map { player in
            guard let tee = player.tee else {
                return []
            }
            
            return numberOfStrokesPerHole(totalStrokes: player.strokes, tee: tee)
        }
    }
    
    // Summarize the score of this match per hole
    public func summarizeHoles() -> [HoleResultValue]? {
        // We use the scores per team to calculate each winning team, of course
        guard let teamScores = getScoresPerTeam() else {
            return nil
        }
        
        return teamScores.map { redScore, blueScore in
            // If one of the teams did not play the hole, we'll mark it as unplayed
            guard let redScore = redScore, let blueScore = blueScore else {
                return .unplayed
            }
            
            if redScore < blueScore {
                return .red
            } else if blueScore < redScore {
                return .blue
            } else {
                return .square
            }
        }
    }
    
    /// Calculates the score of the match
    public func calculateScore() -> (score: MatchplayScore, winningTeam: Team)? {
        guard let summary = summarizeHoles() else {
            return nil
        }
        
        var unplayedHoles = 0
        var redScore = 0
        var blueScore = 0
        
        // Loop over the holes and change the values declared above for each hole
        for holeResult in summary {
            switch holeResult {
            case .unplayed: unplayedHoles += 1 // keep track of the unplayed hole for scores like 2&1
            case .red: blueScore += 1 // lowest score wins, so if red won this hole, blue gets a point
            case .blue: redScore += 1 // 👆
            case .square: continue // square results don't impact the final score
            }
        }
        
        print(unplayedHoles, redScore, blueScore)
        
        // Return the final score
        if redScore < blueScore {
            return (MatchplayScore(blueScore-redScore, unplayedHoles), .red)
        } else if blueScore < redScore {
            return (MatchplayScore(redScore-blueScore, unplayedHoles), .blue)
        } else {
            return (MatchplayScore(0, unplayedHoles), .neutral)
        }
    }
    
    
    // if the match can still be turned around: other wins or all square
    public var otherCanStillWin: Bool {
        guard let score = self.calculateScore()?.score else {
            return true
        }
        
        // If all holes are filled in for all players, the match is finished
        guard !hasScoreForEveryPlayerOnEveryHole else {
            return false
        }
        
        // ensure we have scores for every player on every hole, else the match cannot be finalized
        if let holeCount = players[0].scores?.count {
            for holeIndex in 0..<holeCount {
                let player0HasNoScore = players[0].scores![holeIndex] == nil
                
                for player in players {
                    guard (player.scores![holeIndex] == nil) == player0HasNoScore else {
                        return true
                    }
                }
            }
        }
        
        let result = !(score.winningScore > score.holesNotPlayed)
        
        log.info("otherCanStillWin: \(result) - score: \(score)")
        
        return result
    }
    
    // self explanatory
    public var hasScoreForEveryPlayerOnEveryHole: Bool {
        for player in players {
            guard let scores = player.scores else {
                return false
            }
            
            for hole in scores {
                guard hole != nil else {
                    return false
                }
            }
        }
        
        return true
    }
}


