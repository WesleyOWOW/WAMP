import Alamofire
import Foundation
import SwiftyJSON
import FacebookCore
import FacebookLogin
import AlamofireImage
import SwiftyUserDefaults
import SwiftNotificationCenter

let authenticationManagerInstance: AuthenticationManager = AuthenticationManager()

public enum CreateAccountError : Error {
    case fieldTaken(Field)
    case other(Error)
    case empty(Field)
    case invalid(Field)
    
    public enum Field {
        case username
        case email
        case password
        case password_check
        case firstname
        case lastname
        case handicap
        case homeCourse
    }
}

public enum AuthenticationError : Error {
    case invalidToken
}

class AuthenticationManager {
    // Singeleton
    class var shared: AuthenticationManager {
        return authenticationManagerInstance
    }
    
    public var isLoggedin: Bool
    public var token: String? = nil
    public var headers: HTTPHeaders {
        guard let token = token else {
            return [:]
        }
        
        return [
            "Authorization": "Bearer \(token)",
            "Accept": "application/json"
        ]
    }
    
    // MARK - Initializer
    init() {
        
        
        if let token = keychain["token"], token.count > 5 {
            log.info("Authentication token found in keychain")
            self.token = token
            self.isLoggedin = true
        } else {
            self.isLoggedin = false
        }
        
    
    }
    
    public func logout() {
        keychain["token"] = nil
        self.isLoggedin = false
        self.token = nil
        RecentContactsManager.reset()
        AppDelegate.shared.reset()
    }
}

// Mark - Validation
extension AuthenticationManager {
    public func emailIsTaken(email: String, callback: @escaping (Result<Void>) -> ()) {
        Alamofire.request("\(api_url)/users/check", method: .post, parameters: ["email": email], encoding: JSONEncoding.default).validate().responseJSON { response in
            switch response.result {
                case .failure(_):
                    if let value = response.data {
                        let json = JSON(value)
                        if json["email"].stringValue == "taken" {
                            callback(.failure(CreateAccountError.fieldTaken(.email)))
                        } else {
                            callback(.success())
                        }
                    }
                case .success(let value):
                    let json = JSON(value)
                    if json["email"].stringValue == "taken" {
                        callback(.failure(CreateAccountError.fieldTaken(.email)))
                    } else {
                        callback(.success())
                    }
            }
        }
    }
    
    public func usernameIsTaken(username: String, callback: @escaping (Result<Void>) -> ())  {
        Alamofire.request("\(api_url)/users/check", method: .post, parameters: ["username": username], encoding: JSONEncoding.default).validate() .responseJSON { response in
                switch response.result {
                case .failure(_):
                        if let value = response.data {
                            let json = JSON(value)
                            if json["username"].stringValue == "taken" {
                                callback(.failure(CreateAccountError.fieldTaken(.username)))
                            } else if json["username"].stringValue == "valid" {
                                callback(.success())
                            }
                        }
                case .success(let value):
                    let json = JSON(value)
                    if json["username"].stringValue == "taken" {
                        callback(.failure(CreateAccountError.fieldTaken(.username)))
                    } else if json["username"].stringValue == "valid" {
                        callback(.success())
                    }
                    
                }
        }
    }
}


// Mark - Account creation
extension AuthenticationManager {
    /// Tries to create an account on the server
    public func tryCreateAccount(username: String, firstName: String, lastName: String, email: String, password: String, birthday: Date, handicap: Double, picture: UIImage?, homeCourse: Int?, callback: @escaping (Result<Void>) -> ()) {
        // Formatter for how the API wants it
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        let parameters: Parameters = [
            "auth": "oldschool",
            "email": email,
            "password": password,
            "username": username,
            "first_name": firstName,
            "last_name": lastName,
            "birthday": dateFormatter.string(from: birthday),
            "handicap": handicap,
            "home_course": homeCourse!
        ]
        
        log.info("Sending account creation request")
        
        Alamofire
            .request(api_url + "/users",
                     method: .post,
                     parameters: parameters,
                     encoding: JSONEncoding.default
            )
            .validate()
            .responseJSON { response in
                switch response.result {
                case .failure(let alamofireError):
                    do {
                        let fallbackError = CreateAccountError.other(alamofireError)
                        
                        guard let value = response.data else {
                            throw fallbackError
                        }
                        
                        let json = JSON(value)
                        log.debug("Registration error thrown: \(value)")
                        if json["email"].string == "taken" {
                            throw CreateAccountError.fieldTaken(.email)
                        } else if json["username"].string == "taken" {
                            throw CreateAccountError.fieldTaken(.username)
                        }
                        
                        throw fallbackError
                    } catch {

                        log.error("FATAL - Registration error thrown: \(error)")
                        callback(.failure(error))
                    }
                case .success(let value):
                    let json = JSON(value)
                    guard let token = json["token"].string, token.count > 5 else {
                        callback(.failure(AuthenticationError.invalidToken))
                        return
                    }
                    
                    self.token = token
                    keychain["token"] = token
                    
                    
                    User.me.setAvatar(picture!)
                    
                    
                    User.me.updateLocal { result in
                        if let avatar = picture {
                            User.me.setAvatar(avatar)
                        }
                        self.isLoggedin = true
                        callback(result)
                    }
                }
        }
    }
    
    func random(length: Int = 20) -> String {
        let base = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&()_+=-[]{}\'/?,.<>`~§±"
        var randomString: String = ""
        
        for _ in 0..<length {
            let randomValue = arc4random_uniform(UInt32(base.count))
            randomString += "\(base[base.index(base.startIndex, offsetBy: Int(randomValue))])"
        }
        return randomString
    }
}

// Mark - Login

extension AuthenticationManager {
    public func login(username: String, password: String, callback: @escaping (Result<Void>) -> ()) {
        let parameters: Parameters = [
            "username": username,
            "password": password
        ]
        
        // Reset the recent contacts and the currently ongoing match on login (Since these are in userDefaults)
        
        // TODO: currentMatchManager?
//        RecentContactsManager.reset()
        
        Alamofire
            .request("\(api_url)/login",
                     method: .post,
                     parameters: parameters,
                     encoding: JSONEncoding.default)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    
                    // We need the authentication token!
                    guard let token = json["token"].string, token.count > 5 else {
                        callback(.failure(AuthenticationError.invalidToken))
                        return
                    }
                    
                    self.token = token
                    keychain["token"] = token
                    log.info("Authentication token saved in keychain")
                    self.isLoggedin = true
                    
                    callback(.success())
                case .failure(let error):
                    callback(.failure(error))
                }
        }
    }
    
    public func registerFacebook(socialId: String, username: String, firstName: String, lastName: String, email: String, handicap: Double, picture: UIImage?, homeCourse: Int?, callback: @escaping (Result<Void>) -> ()) {
        let params: Parameters = [
            "email": email,
            "password": self.random(),
            "social_id": socialId,
            "username": username,
            "first_name": firstName,
            "last_name": lastName,
            "handicap": handicap,
            "home_course": homeCourse
        ]
        
        log.info("Facebook login params \(params)")
        
        log.info("Sending Facebook account creation request")
        
        
        Alamofire
            .request(api_url + "/users",
                     method: .post,
                     parameters: params,
                     encoding: JSONEncoding.default
            )
            .validate()
            .responseJSON { response in
                switch response.result {
                case .failure(let alamofireError):
                    do {
                        let fallbackError = CreateAccountError.other(alamofireError)
                        
                        guard let value = response.data else {
                            throw fallbackError
                        }
                        
                        let json = JSON(value)
                        log.debug("Registration error thrown: \(json)")
                        if json["email"].string == "taken" {
                            throw CreateAccountError.fieldTaken(.email)
                        } else if json["username"].string == "taken" {
                            throw CreateAccountError.fieldTaken(.username)
                        }
                        
                        throw fallbackError
                    } catch {
                        log.error("FATAL - Registration error thrown: \(error)")
                        callback(.failure(error))
                    }
                case .success(let value):
                    let json = JSON(value)
                    guard let token = json["token"].string, token.count > 5 else {
                        callback(.failure(AuthenticationError.invalidToken))
                        return
                    }
                    
                    self.token = token
                    keychain["token"] = token
                    self.isLoggedin = true
                    
                    User.me.updateLocal { result in
                        if let avatar = picture {
                            User.me.setAvatar(avatar)
                        }
                        
                        callback(result)
                    }
                }
        }
    }
}
