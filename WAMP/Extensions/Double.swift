import Foundation

extension Double {
    public func renderAsHandicap(includeSuffix: Bool) -> String {
        if self == 0 {
            return "PRO"
        } else if self < 0 {
            return "+\(-self)\(includeSuffix ? "HCP" : "")"
        } else {
            return "\(self)\(includeSuffix ? "HCP" : "")"
        }
    }
}
