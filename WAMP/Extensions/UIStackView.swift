import UIKit

extension UIStackView {
    func removeArrangedSubviews() {
        self.arrangedSubviews.forEach { $0.removeFromSuperview() }
    }
}


