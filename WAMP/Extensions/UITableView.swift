import UIKit

extension UITableView {
    
    func scrollToTop(animated: Bool) {
//        scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: animated)
        self.setContentOffset(CGPoint(x: 0, y: 0), animated: animated)
    }
    
    func scrollToBottom(animated: Bool) {
        let lastSection = numberOfSections-1
        let lastSectionRows = self.numberOfRows(inSection: lastSection)
        
        scrollToRow(at: IndexPath(row: lastSectionRows-1, section: lastSection), at: .bottom, animated: animated)
    }
    
}
