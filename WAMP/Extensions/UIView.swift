import UIKit

let spacerColor = #colorLiteral(red: 0.9450980392, green: 0.9450980392, blue: 0.9450980392, alpha: 1)

extension UIView {
    var isVisible: Bool {
        get {
            return !self.isHidden
        }
        set {
            self.isHidden = !newValue
        }
    }
    
    /// Makes a snapshot image of the view
    func makeSnapshot(backgroundColor: UIColor = .white) -> UIImage {
        UIGraphicsBeginImageContext(self.bounds.size)
        let context = UIGraphicsGetCurrentContext()!
        context.setFillColor(backgroundColor.cgColor)
        context.fill(self.bounds)
        self.layer.render(in: context)
        let image = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        return image
    }
    
    /// Helper function to add the spacers used throughout the app
    func addSpacer(_ axis: UILayoutConstraintAxis, in superview: UIView? = nil, between view1: UIView? = nil, and view2: UIView? = nil) -> UIView {
        let view = UIView(frame: .zero)
        view.backgroundColor = spacerColor
        
        view.snp.makeConstraints { make in
            switch axis {
            case .vertical: make.width.equalTo(1)
            case .horizontal: make.height.equalTo(1)
            }
        }
        
        (superview ?? self).addSubview(view)
        
        // create constraints
        if let view1 = view1, let view2 = view2 {
            let guide = UILayoutGuide()
            (superview ?? self).addLayoutGuide(guide)
            guide.snp.makeConstraints { make in
                switch axis {
                case .vertical:
                    make.leading.equalTo(view1.snp.trailing)
                    make.trailing.equalTo(view2.snp.leading)
                case .horizontal:
                    make.top.equalTo(view1.snp.bottom)
                    make.bottom.equalTo(view2.snp.top)
                }
            }
            view.snp.makeConstraints { make in
                switch axis {
                case .vertical: make.centerX.equalTo(guide)
                case .horizontal: make.centerY.equalTo(guide)
                }
            }
        }
        
        return view
    }
    
    var parentViewController: UIViewController? {
        var parentResponder: UIResponder? = self
        while parentResponder != nil {
            parentResponder = parentResponder!.next
            if let viewController = parentResponder as? UIViewController {
                return viewController
            }
        }
        return nil
    }
}
