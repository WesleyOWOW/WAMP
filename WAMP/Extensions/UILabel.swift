import UIKit

extension UILabel {
    func setCharacterSpacing(_ spacing: Float) {
        let attributedString = NSMutableAttributedString(attributedString: self.attributedText ?? NSAttributedString())
        attributedString.addAttribute(NSKernAttributeName,
                                      value: spacing,
                                      range: NSMakeRange(0, attributedString.length))
        self.attributedText = attributedString
    }
    
    /// Returns a new UILabel that inherits the properties of the receiving UILabel
    func duplicated() -> UILabel {
        let label = UILabel(frame: self.frame)
        
        label.text = self.text
        label.textColor = self.textColor
        label.textAlignment = self.textAlignment
        label.font = self.font
        
        return label
    }
}
