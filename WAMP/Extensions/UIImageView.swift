import UIKit
import Alamofire
import AlamofireImage
import SDWebImage

extension UIImageView {
    func setPhoto(_ photo: Photo) {
        do {
            if let image = photo.image {
                self.image = image
            } else if let url = photo.url {
                self.sd_setShowActivityIndicatorView(true)
                self.sd_setIndicatorStyle(.gray)
                self.sd_setImage(with: url)
//                let request = try URLRequest(url: url, method: .get, headers: AuthenticationManager.shared.headers)
                
//                self.af_setImage(withURLRequest: request, placeholderImage: UIImage(named: "ImageLoading")) { response in
//                    switch response.result {
//                    case .success: return
//                    case .failure:
//
//
//                        if response.response?.statusCode == 404 {
//                            log.error("404")
//                            log.error("url: \(url)")
//                        }
//
//
//                        // base64 workaround
//                        if let data = response.data, let text = String(data: data, encoding: .utf8), let decodedData = Data(base64Encoded: text), let image = UIImage(data: decodedData) {
//                            log.warning("base64 photo: \(url)")
//                            self.image = image
//                        } else if let data = response.data, let image = UIImage(data: data) {
//                            self.image = image
//                        }
//                    }
//                }
            }
        } catch {
            assertionFailure()
        }
    }
}


