import UIKit

extension UIStoryboardSegue {
    /// Returns the root view controller of the segue. If the segue destination is a navigation controller, its root view controller will be returned. Otherwise, the destination view controller is returned.
    var destinationRoot: UIViewController {
        if let navController = destination as? UINavigationController {
            return navController.topViewController!
        } else {
            return destination
        }
    }
}

