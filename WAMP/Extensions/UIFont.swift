import UIKit

extension UIFont {
    static func black(_ size: CGFloat) -> UIFont {
        return UIFont(name: "Gordita-Black", size: size)!
    }
    
    static func bold(_ size: CGFloat) -> UIFont {
        return UIFont(name: "Gordita-Bold", size: size)!
    }
    
    static func medium(_ size: CGFloat) -> UIFont {
        return UIFont(name: "Gordita-Medium", size: size)!
    }
    
    static func light(_ size: CGFloat) -> UIFont {
        return UIFont(name: "Gordita-Light", size: size)!
    }
    
    static func regular(_ size: CGFloat) -> UIFont {
        return UIFont(name: "Gordita-Regular", size: size)!
    }
}

