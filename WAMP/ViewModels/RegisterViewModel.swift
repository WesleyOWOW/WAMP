import UIKit
import RxSwift
import Alamofire

let registerViewModelInstance: RegisterViewModel = RegisterViewModel()

/**
 * View model that handles registering an account. This takes care of all validation and acts as a data bridge between the
 * viewcontroller and the Authetication Manager.
 */

class RegisterViewModel {
    
    // Singleton
    class var shared : RegisterViewModel {
        return registerViewModelInstance
    }
    
    enum accountType {
        case regular
        case facebook
    }
    
    let disposeBag = DisposeBag()
    
    var type: accountType = .regular
    var socialID : String = ""
    var email = Variable<String>("wersjeui@rogkpot.nl")
    var password = Variable<String>("kjnbfekfhn")
    var password_check = Variable<String>("kjnbfekfhn")
    var firstname = Variable<String>("kjnbfekfhn")
    var lastname = Variable<String>("kjnbfekfhn")
    var username = Variable<String>("kjnbfekfhn")
    var passvalid : Bool = false
    var emmailValid : Bool = false
    var handicap = Variable<String>("")
    var homeCourse: Course!
    var busy = Variable<Bool>(false)
    var profilePicture = Variable<UIImage?>(nil)
    var result = Variable<Bool?>(nil)
    var canDoNext = false
    
    var emailIsValid : Observable<Bool> {
        return email.asObservable().map { email in
            let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
            
            let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
            self.emmailValid = emailTest.evaluate(with: email)
            return emailTest.evaluate(with: email)
        }
    }
    
    var passwordIsValid : Observable<Bool> {
        return password.asObservable().map { password in
            return password.count >= 6
        }
    }
    
    var passwordConfirmationIsValid : Observable<Bool> {
        return Observable
            .combineLatest(password.asObservable(),
                           password_check.asObservable(),
                           passwordIsValid,
                           resultSelector: { (password, password_check, passwordIsValid) in
                            
                            return (password, password_check, passwordIsValid)
            }).map {
                password, password_check, passwordIsValid in
                self.passvalid = passwordIsValid && password == password_check
                return passwordIsValid && password == password_check
        }
    }
    
    var imageFilled : Observable<Bool> {
        return profilePicture.asObservable().map { profilePicture in
            return profilePicture != nil
        }
    }
    
    func trySubmit(callback: @escaping (Result<Void>) -> ()) {
    
        if self.type == .regular {
            var homeCourseId = 0
            if let course = homeCourse {
                homeCourseId = course.id
            }
            
            AuthenticationManager.shared.tryCreateAccount(username: username.value, firstName: firstname.value, lastName: lastname.value, email: email.value, password: password.value, birthday: Date(), handicap: Double(handicap.value)!, picture: WampCameraViewModel.shared.currentPhoto.value?.image, homeCourse: homeCourseId) { result in
                switch result {
                case .success:
                    callback(.success())
                case .failure(let error):
                    callback(.failure(error))
                }
            }
        } else {
            self.facebook(callback: callback)
        }
    }
    
    private func facebook(callback: @escaping (Result<Void>) -> ()) {
        var homeCourseId = 0
        if let course = homeCourse {
            homeCourseId = course.id
        }
        
        AuthenticationManager.shared.registerFacebook(socialId: self.socialID, username: self.username.value, firstName: firstname.value, lastName: lastname.value, email: email.value, handicap: Double(handicap.value)!, picture: profilePicture.value, homeCourse: homeCourseId) { result in
            switch result {
            case .success:
                callback(.success())
            case .failure(let error):
                callback(.failure(error))
            }
        }
    }
}

