import Foundation
import RxSwift
import Alamofire
import SwiftyJSON

class SearchViewModel {
    
    var searchText = Variable<String>("")
    
    var searchResults: Observable<[Player]> {
        return searchText.asObservable()
            .throttle(0.3, scheduler: MainScheduler.instance)
            .distinctUntilChanged()
            .flatMapLatest { (query: String) -> Observable<[Player]> in
                return Observable.create { observer in
                    let parameters: Parameters = [
                        "query": query
                    ]
                    
                    log.verbose("Search query: \(query)")
                    
                    let request = Alamofire
                        .request(api_url + "/search",
                                 method: .post,
                                 parameters: parameters,
                                 encoding: JSONEncoding.default,
                                 headers: AuthenticationManager.shared.headers
                        )
                        .validate()
                        .responseJSON { response in
                            guard case .success(let value) = response.result else {
                                log.error("SEARCH UNSUCCESSFUL")
                                //                                observer.onError(response.error!)
                                return
                            }
                            
                            let json = JSON(value)
                            
                            let results = json.flatMap { _, result in
                                Player(json: result)
                            }
                             print("searchResult", results )
                            
                            observer.onNext(results)
                            observer.onCompleted()
                    }
                   
                    return Disposables.create(with: request.cancel)
                }
        }
    }
    
    init() {
        
    }
    
}


