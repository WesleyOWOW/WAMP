import Foundation
import RxSwift
import Alamofire

/**
 * View model that handles logging in. This takes care of all validation and acts as a data bridge between the viewcontroller and the
 * Authetication Manager.
 *
 * Since you normally wanna pass data between ViewControllers using viewModels, these are almost always singletons. However, since
 * logging in is a single ViewController, this one is not.
 */

class LoginViewModel {
    let disposableBag = DisposeBag()
    var username = Variable<String>("")
    var password = Variable<String>("")
    
    var canLogin: Observable<Bool> {
        return Observable.combineLatest(self.username.asObservable(), self.password.asObservable()) { username, password in
            return username.count > 0 && password.count > 0
        }
    }
    
    var passwordWrong = Variable<Bool>(false)
    var loginComplete = Variable<Bool>(false)
    var busy = Variable<Bool>(false)
    
    func tryLogin(callback: @escaping (Result<Void>) -> ()) {
        self.busy.value = true
        
        AuthenticationManager.shared.login(username: self.username.value, password:self.password.value) { result in
            switch result {
            case .success:
                self.loginComplete.value = true
            case .failure(let error):
                log.error("Error while logging in. Error: \(error)")
                self.passwordWrong.value = true
            }
        }
    }
    
    
    
    func tryFacebookLogin() {
//        AuthenticationManager.shared
//            .tryFacebookLogin() { result in
//                switch result {
//                case .success:
//                    self.loginComplete.value = true
//                case .failure:
//                    return
//                }
//        }
    }
    
    init() {
        Observable.of(self.username.asObservable(), self.password.asObservable())
            .merge()
            .subscribe(onNext: { [weak self] _ in
                if self?.passwordWrong.value == true {
                    self?.passwordWrong.value = false
                }
            })
            .disposed(by: self.disposableBag)
    }
}

