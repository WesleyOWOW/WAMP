import Foundation

let createMatchModelInstance: CreateMatchViewModel = CreateMatchViewModel()

class CreateMatchViewModel {
    class var shared: CreateMatchViewModel {
        return createMatchModelInstance
    }
    
    var match: ConceptMatch?
    var matchType: MatchType = .single
    var team1Players: [UserProtocol] = [User.me]
    var team2Players = [UserProtocol]()
}
