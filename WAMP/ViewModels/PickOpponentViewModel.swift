//
//  PickOpponentViewModel.swift
//  WAMP
//

/// ViewModel for PickOpponentViewController

import Foundation
import RxSwift


extension String {
    var folded: String {
        return self.folding(options: [.diacriticInsensitive, .caseInsensitive], locale: .current)
    }
}

class PickOpponentViewModel {
    
    enum PlayerIdentifier {
        case teammate(Int)
        case opponent(Int)
    }
    
    let disposeBag = DisposeBag()
    let searchText = Variable<String>("")
    let visibleGuests = Guest.fetchAll()
    let allFollowing = Variable<[UserProtocol]>([])
    let visibleFollowing = Variable<[UserProtocol]>([])
    
    var visibleContactsAreSearchResult: Observable<Bool> {
        return searchText
            .asObservable()
            .map { $0.count > 0 }
    }
    
    let selectedPlayer = Variable<PlayerIdentifier>(.opponent(0))
    
    let teammate = Variable<UserProtocol?>(nil)
    let opponent1 = Variable<UserProtocol?>(nil)
    let opponent2 = Variable<UserProtocol?>(nil)
    
    /// Returns the variable for the currently selected player
    var selectedPlayerVariable: Variable<UserProtocol?> {
        switch selectedPlayer.value {
        case .opponent(0):
            return opponent1
        case .opponent(1):
            return opponent2
        case .teammate(1):
            return teammate
        default:
            fatalError("invalid selectedPlayer state")
        }
    }
    
    init() {
        User.me.fetchFollowing { result in
            if let players = result.value {
                self.allFollowing.value = players
            }
        }
        
        Observable.combineLatest(searchText.asObservable(), allFollowing.asObservable()) { ($0.searchFriendly, $1) }
            .bind { (searchText, profiles) in
                self.visibleFollowing.value = profiles.filter { profile in
                    if searchText.isEmpty {
                        return true
                        
                    }
                    return profile.username.searchFriendly.contains(searchText) || profile.firstName.searchFriendly.contains(searchText) || profile.lastName.searchFriendly.contains(searchText)
                }
        }.addDisposableTo(disposeBag)
    }
    
}

fileprivate extension String {
    var searchFriendly: String {
        return folding(options: [.caseInsensitive, .diacriticInsensitive], locale: nil)
    }
}
