//
//  ChangePasswordViewModel.swift
//  WAMP
//
//  Created by Wesley Peeters on 24-01-18.
//  Copyright © 2018 Robbert Brandsma. All rights reserved.
//

import Foundation
import RxSwift
import Alamofire



let changePasswordViewModelInstance: ChangePasswordViewModel = ChangePasswordViewModel()
class ChangePasswordViewModel {
    class var shared : ChangePasswordViewModel {
        return changePasswordViewModelInstance
    }
    
    let disposeBag = DisposeBag()

    var current = Variable<String>("")
    var new = Variable<String>("")
    var newRepeat = Variable<String>("")
    var valid = false
    
    var passwordConfirmationIsValid: Observable<Bool> {
        return Observable.combineLatest(new.asObservable(), newRepeat.asObservable(), resultSelector: { (new, newRepeat) in
            return (new, newRepeat)
        }).map { new, newRepeat in
            self.valid = (new == newRepeat)
            return new == newRepeat
        }
    }
    
    func validatePassword(callback: @escaping (Result<Void>) -> ()) {
        print(current.value)
        Alamofire.request(
            api_url + "/password/verify",
            method: .post,
            parameters: [
                "password": current.value,
            ],
            encoding: JSONEncoding.default,
            headers: AuthenticationManager.shared.headers
            )
            .validate()
            .responseJSON { response in
                switch response.result {
                case .failure(let error):
                    callback(.failure(error))
                case .success(let value):
                    callback(.success())
                }
        }
    }
}
