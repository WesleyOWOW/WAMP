//
//  RegisterViewModel.swift
//  We Are Matchplay
//
//  Created by Wesley Peeters on 13/12/2017.
//  Copyright © 2017 OWOW. All rights reserved.
//

import Foundation
import RxSwift
import UIKit


let createGuestViewModelInstance: CreateGuestViewModel = CreateGuestViewModel()


class CreateGuestViewModel {
    class var shared : CreateGuestViewModel {
        return createGuestViewModelInstance
    }
    
    let disposeBag = DisposeBag()

    var name = Variable<String>("")
    var handicap = Variable<String>("")
    var busy = Variable<Bool>(false)
    var profilePicture = Variable<UIImage?>(nil)
    var result = Variable<Bool?>(nil)
    var guest = Variable<Guest?>(nil)
    var error: Error!
  
    var imageFilled : Observable<Bool> {
        return profilePicture.asObservable().map { profilePicture in
            return profilePicture != nil
        }
    }
    
    var hasGuest: Observable<Bool> {
        
        return guest.asObservable().map { guest in
            return guest != nil
        }
    }
//
    func reset() {
        self.guest.value = nil
        self.name.value = ""
        self.profilePicture.value = nil
        self.handicap.value = ""
        WampCameraViewModel.shared.reset()
    }
    
    func trySubmit() {
        self.busy.value = true
        
            
        

    }
    
}

