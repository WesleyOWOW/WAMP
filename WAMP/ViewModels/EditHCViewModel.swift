//
//  EditHCViewModel.swift
//  WAMP
//
//  Created by Wesley Peeters on 21-02-18.
//  Copyright © 2018 Robbert Brandsma. All rights reserved.
//

import Foundation



let homeCourseModelInstance: EditHCViewModel = EditHCViewModel()

class EditHCViewModel {
    class var shared: EditHCViewModel {
        return homeCourseModelInstance
    }
    
    var homeCourse: Course!
    
}
