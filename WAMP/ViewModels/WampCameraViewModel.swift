import Foundation
import RxSwift

let cameraViewModelInstance: WampCameraViewModel = WampCameraViewModel()

class WampCameraViewModel {
    class var shared : WampCameraViewModel {
        return cameraViewModelInstance
    }
    
    var amount: Amount = .single
    let disposeBag = DisposeBag()
    let currentPhoto = Variable<Photo?>(nil)
    var coverPhoto: Photo? = nil
    let allPhotos = Variable<[Photo]>([])
    let mode = Variable<Mode>(.capture)
    var currentPhotoIsCoverPhoto = Variable<Bool>(false)
    var saveToServer = false
    var showSkip = false
    
    enum Mode : Equatable {
        case capture
        case edit(EditMode)
        
        static func ==(lhs: Mode, rhs: Mode) -> Bool {
            switch (lhs, rhs) {
            case (.capture, .capture):
                return true
            case (.edit(let mode1), .edit(let mode2)):
                return mode1 == mode2
            default:
                return false
            }
        }
        
        var editMode: EditMode {
            switch self {
            case .edit(let mode): return mode
            default: return .view
            }
        }
    }
    
    enum EditMode : Equatable {
        case view
        case selectHole
        case addText
    }
    
    enum Amount {
        case single
        case multiple
    }
    
    func reset() {
        self.currentPhoto.value = nil
        self.coverPhoto = nil
        self.allPhotos.value = []
    }
    
    func makeCover() {
        self.coverPhoto = self.currentPhoto.value
        self.sortPhotos()
    }
    
    func trash() {
        if let photo = currentPhoto.value, let index = allPhotos.value.index(of: photo) {
            log.debug("remove at index: \(index)")
            allPhotos.value.remove(at: index)
            currentPhoto.value = allPhotos.value.first
        }
        
        mode.value = .capture
    }
    
    
    
    
    
    func addPhoto(image: UIImage) {
        let photo = Photo(image: image)
        allPhotos.value.append(photo)
        currentPhoto.value = photo
        if coverPhoto == nil {
            coverPhoto = photo
        }
        mode.value = .edit(.view)
    }
    
    func setHole(_ photo: Photo, _ hole: Int) {
        if let photo = currentPhoto.value, let index = allPhotos.value.index(of: photo) {
            let photo = allPhotos.value[index]
            photo.hole = hole
        }
    }
    
    func sortPhotos() {
        allPhotos.value = allPhotos.value.sorted { photo1, photo2 in
            if let hole1 = photo1.hole, let hole2 = photo2.hole {
                return hole1 < hole2
            }
            
            if photo1.hole != nil {
                return true
            }
            
            return false
        }
        
        if let photo = coverPhoto, let index = allPhotos.value.index(of: photo) {
            allPhotos.value.remove(at: index)
            allPhotos.value.insert(photo, at: 0)
        }
    }
    
    init() {
        self.currentPhoto.value = nil
    }
}
