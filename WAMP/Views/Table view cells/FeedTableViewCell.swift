import UIKit

class FeedTableViewCell: UITableViewCell {
    static let identifier = "wamp.FeedTableViewCell"
    var match: APIMatch?
    var item: FeedItem? = nil
    var parentVC: WampViewController!

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    func configure(_ match: APIMatch, parent: UIViewController, _ isJudge: Bool = false) {
        for view in self.contentView.subviews {
            view.removeFromSuperview()
        }
        
        self.item = FeedItem(frame: .zero)
        self.item?.isJudge = isJudge
        self.item?.match = match
        self.match = match
        self.item?.parentVC = parent as! WampViewController

        self.item?.setup()
        self.contentView.addSubview(self.item!)
        self.item?.snp.makeConstraints { make in
            make.trailing.leading.equalToSuperview().inset(16)
            make.top.equalToSuperview()
            make.bottom.equalToSuperview().offset(-50)
        }
        
    }
}

class RandomHeaderCell: UITableViewCell {
    static let identifier = "wamp.RandomHeaderCell"
    var hasBeenSetUp = false
    var label: UILabel!
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        setUp()
    }

    func setUp() {
        if !hasBeenSetUp {
            print("SetUp")
            let header = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width - 32, height: 130))
            label = UILabel(frame: .zero)
            label.font = .bold(32)
            label.textColor = .black
            label.numberOfLines = 0
            
            
            
            header.addSubview(label)
            
            self.contentView.addSubview(header)
            
            header.snp.makeConstraints { make in
                make.trailing.leading.top.equalToSuperview()
                make.height.equalTo(130)
            }
            
            self.backgroundColor = .yellow
            self.contentView.backgroundColor = .blue
            header.backgroundColor = .purple
            
            label.snp.makeConstraints { make in
                make.centerY.trailing.leading.equalToSuperview()
            }
            
            hasBeenSetUp = true
        }
        
        label.text = headerTexts[Int.random(min: 0, max: 4)]
    }
}
