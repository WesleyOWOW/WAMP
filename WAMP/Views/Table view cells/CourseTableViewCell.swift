import UIKit
import SnapKit
import CoreLocation

class CourseTableViewCell: UITableViewCell {
    static let identifier = "wamp.course"
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.contentView.backgroundColor = UIColor(hex: "fcfcfc")
        self.backgroundColor = UIColor(hex: "fcfcfc")
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.contentView.backgroundColor = UIColor(hex: "fcfcfc")
        self.backgroundColor = UIColor(hex: "fcfcfc")
    }
    
    var location: CLLocationCoordinate2D?
    var course: Course!
    
    func make(_ course: Course, location: CLLocationCoordinate2D?) {
        for sv in contentView.subviews {
            sv.removeFromSuperview()
        }
        let nameLabel = UILabel(frame: .zero)
        nameLabel.font = .medium(14)
        
        if course.name.count > 35 {
            nameLabel.text = course.name.prefix(35) + "..."
        } else {
            nameLabel.text = course.name
        }
        
        
        
        contentView.addSubview(nameLabel)
        nameLabel.snp.makeConstraints { make in
            make.top.leading.equalToSuperview().offset(16)
        }
        
        let placeLabel = nameLabel.duplicated()
        placeLabel.textColor = UIColor(hex: "BFBFBF")
        contentView.addSubview(placeLabel)
        placeLabel.text = "\(course.city ?? "Unknown"), \(course.country ?? "Unknown")"
        placeLabel.snp.makeConstraints { make in
            make.leading.equalTo(nameLabel)
            make.top.equalTo(nameLabel.snp.bottom).offset(2)
        }
        
        let distanceLabel = placeLabel.duplicated()
        distanceLabel.textColor = UIColor(hex: "007BFF")
        contentView.addSubview(distanceLabel)
        distanceLabel.snp.makeConstraints { make in
            make.top.trailing.equalToSuperview().inset(16)
        }
        
        if let currentLocation = location, course.lat != 0, course.long != 0 {
            let courseLocation = CLLocation(latitude: course.lat, longitude: course.long)
            let currentLocation = CLLocation(latitude: currentLocation.latitude, longitude: currentLocation.longitude)
            
            let distance = Int(currentLocation.distance(from: courseLocation))
            
            distanceLabel.text = "\(distance / 1000)KM"
        } else {
            distanceLabel.text = "Unknown"
        }
        
        let sep = UIView()
        sep.backgroundColor = .black
        sep.alpha = 0.1
        contentView.addSubview(sep)
        sep.snp.makeConstraints { make in
           make.trailing.leading.equalToSuperview().inset(16)
           make.bottom.equalToSuperview().offset(-1)
        }
        
    }

    
}

