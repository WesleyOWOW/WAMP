import UIKit

class RandomTextTableViewCell: UITableViewCell {
    static let identifier = "wamp.RandomTextTableViewCell"

    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        setUp()
    }
    
    func setUp() {
        
        for subview in self.contentView.subviews {
            subview.removeFromSuperview()
        }
        
        let label = UILabel(frame: .zero)
        label.font = .bold(32)
        label.textColor = .black
        label.numberOfLines = 0
        label.text = headerTexts[Int.random(min: 0, max: 4)]
        self.contentView.addSubview(label)
        label.snp.makeConstraints { make in
            make.trailing.leading.equalToSuperview().inset(16)
            make.top.equalToSuperview()
            make.bottom.equalToSuperview().offset(-32)
        }
    }
}
