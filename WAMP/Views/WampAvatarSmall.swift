import UIKit
import RxSwift
import Alamofire
import AlamofireImage

class WAMPAvatarSmall : UIView {
    
    static let placeholderImage = UIImage(named: "Avatar Placeholder")
    
    static private let imageDownloader = ImageDownloader(
        configuration: ImageDownloader.defaultURLSessionConfiguration(),
        downloadPrioritization: .lifo,
        maximumActiveDownloads: 4,
        imageCache: AutoPurgingImageCache()
    )
    
    static func flushCache() {
        imageDownloader.imageCache?.removeAllImages()
    }
    
    let disposeBag = DisposeBag()
    
    let imageView = UIImageView(frame: .zero)
    
    private func setup() {
        backgroundColor = .clear
        layer.shouldRasterize = true
        layer.rasterizationScale = UIScreen.main.scale
        imageView.backgroundColor = .clear
        addSubview(imageView)
        imageView.snp.makeConstraints { make in
            make.size.equalTo(40)
            make.center.equalToSuperview()
        }
        
        imageView.layer.cornerRadius = 20
        imageView.clipsToBounds = true
        imageView.layer.masksToBounds = true
    }

    
    var verifiedIcon: UIImageView?
    var displaysVerifiedIcon: Bool {
        get {
            return verifiedIcon?.isVisible ?? false
        }
        set {
            if let verifiedIcon = verifiedIcon {
                // Only
                verifiedIcon.isVisible = newValue
            } else if newValue == true {
                let icon = UIImageView(image: UIImage(named: "VerifiedIcon"))
                addSubview(icon)
                icon.snp.makeConstraints { make in
                    make.top.right.equalToSuperview()
                }
                verifiedIcon = icon
            }
        }
    }

    
    var profile: UserProtocol?
    private var image: UIImage? {
        get {
            return imageView.image
        }
        set {
            imageView.image = newValue ?? WAMPAvatarSmall.placeholderImage
            
        }
    }
    
    /// Loads the given profile data into the avatar
    func setProfile(_ profile: UserProtocol?) {
        self.profile = profile
        self.image = nil
        self.displaysVerifiedIcon = false
        imageView.contentMode = .scaleAspectFill
        guard let profile = profile else { return }
        
        _setAvatarURL(profile.avatarURL)
    }
    
    // prevents showing the old requested avatar when scrolling in a list
    private var loadCount = 0
    private func _setAvatarURL(_ avatarURL: URL) {
        loadCount += 1
        let thisLoadCount = loadCount
        
        var urlRequest = URLRequest(url: avatarURL)
        urlRequest.setValue(AuthenticationManager.shared.headers["Authorization"], forHTTPHeaderField: "Authorization")
        
        WAMPAvatarSmall.imageDownloader.download(urlRequest) { response in
            guard self.loadCount == thisLoadCount else {
                return
            }
            
            WAMPAvatarSmall.imageDownloader.imageCache?.removeAllImages()
            WAMPAvatarSmall.imageDownloader.sessionManager.session.configuration.urlCache?.removeAllCachedResponses()
            
            self.image = response.result.value
        }
    }
    
    /// "Public" set avatar URL API
    func setAvatarURL(_ avatarURL: URL) {
        self.profile = nil
        self.image = nil
        _setAvatarURL(avatarURL)
    }
    
    /// Just uses the given image
    func setPhoto(_ photo: UIImage?) {
        loadCount += 1
        
        self.profile = nil
        self.image = photo
    }
    
    // MARK: - Initializer Crap
    convenience init() {
        self.init(frame: CGRect.zero)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
   
    override var contentMode: UIViewContentMode {
        didSet {
            imageView.contentMode = contentMode
        }
    }
    
}


class WAMPAvatarProfile : UIView {
    
    static let placeholderImage = UIImage(named: "Avatar Placeholder")
    
    static private let imageDownloader = ImageDownloader(
        configuration: ImageDownloader.defaultURLSessionConfiguration(),
        downloadPrioritization: .lifo,
        maximumActiveDownloads: 4,
        imageCache: AutoPurgingImageCache()
    )
    
    static func flushCache() {
        imageDownloader.imageCache?.removeAllImages()
    }
    
    let disposeBag = DisposeBag()
    
    let imageView = UIImageView(frame: .zero)
    
    private func setup() {
        backgroundColor = .clear
        layer.shouldRasterize = true
        layer.rasterizationScale = UIScreen.main.scale
        imageView.backgroundColor = .clear
        addSubview(imageView)
        imageView.snp.makeConstraints { make in
            make.size.equalTo(56)
            make.center.equalToSuperview()
        }
        
        imageView.layer.cornerRadius = 28
        imageView.clipsToBounds = true
        imageView.layer.masksToBounds = true
    }
    
    
    var verifiedIcon: UIImageView?
    var displaysVerifiedIcon: Bool {
        get {
            return verifiedIcon?.isVisible ?? false
        }
        set {
            if let verifiedIcon = verifiedIcon {
                // Only
                verifiedIcon.isVisible = newValue
            } else if newValue == true {
                let icon = UIImageView(image: UIImage(named: "VerifiedIcon"))
                addSubview(icon)
                icon.snp.makeConstraints { make in
                    make.top.right.equalToSuperview()
                }
                verifiedIcon = icon
            }
        }
    }
    
    
    var profile: UserProtocol?
    private var image: UIImage? {
        get {
            return imageView.image
        }
        set {
            imageView.image = newValue ?? WAMPAvatarProfile.placeholderImage
            
        }
    }
    
    /// Loads the given profile data into the avatar
    func setProfile(_ profile: UserProtocol?) {
        self.profile = profile
        self.image = nil
        self.displaysVerifiedIcon = false
        imageView.contentMode = .scaleAspectFill
        guard let profile = profile else { return }
        
        _setAvatarURL(profile.avatarURL)
    }
    
    // prevents showing the old requested avatar when scrolling in a list
    private var loadCount = 0
    private func _setAvatarURL(_ avatarURL: URL) {
        loadCount += 1
        let thisLoadCount = loadCount
        
        var urlRequest = URLRequest(url: avatarURL)
        urlRequest.setValue(AuthenticationManager.shared.headers["Authorization"], forHTTPHeaderField: "Authorization")
        
        WAMPAvatarProfile.imageDownloader.download(urlRequest) { response in
            guard self.loadCount == thisLoadCount else {
                return
            }
            
            WAMPAvatarProfile.imageDownloader.imageCache?.removeAllImages()
            WAMPAvatarProfile.imageDownloader.sessionManager.session.configuration.urlCache?.removeAllCachedResponses()
            
            self.image = response.result.value
        }
    }
    
    /// "Public" set avatar URL API
    func setAvatarURL(_ avatarURL: URL) {
        self.profile = nil
        self.image = nil
        _setAvatarURL(avatarURL)
    }
    
    /// Just uses the given image
    func setPhoto(_ photo: UIImage?) {
        loadCount += 1
        
        self.profile = nil
        self.image = photo
    }
    
    // MARK: - Initializer Crap
    convenience init() {
        self.init(frame: CGRect.zero)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    
    override var contentMode: UIViewContentMode {
        didSet {
            imageView.contentMode = contentMode
        }
    }
    
}
