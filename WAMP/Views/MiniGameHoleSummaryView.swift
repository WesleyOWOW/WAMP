import UIKit

// Represents a single hole "circle" on the match preview screen.
/// Each circle indicates the winning team for a single hole.
class MiniHolePreviewCircle : UIView {
    var value: HoleResultValue {
        didSet {
            self.setNeedsDisplay()
        }
    }
    
    var iconColor: UIColor {
        switch value {
        case .square, .unplayed:
            return #colorLiteral(red: 0.8352206349, green: 0.8353412747, blue: 0.8351940513, alpha: 1)
        case .red:
            return #colorLiteral(red: 1, green: 0.3030309081, blue: 0.3337643743, alpha: 1)
        case .blue:
            return #colorLiteral(red: 0, green: 0.5728718638, blue: 1, alpha: 1)
        }
    }
    
    init(value: HoleResultValue) {
        self.value = value
        super.init(frame: .zero)
        self.backgroundColor = .clear
        self.isOpaque = false
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func draw(_ rect: CGRect) {
        let ovalPath = UIBezierPath(ovalIn: self.bounds)
        iconColor.setFill()
        ovalPath.fill()
    }
}


/// Acts as a container for instances of `MiniHolePreviewCircle`
@IBDesignable final class MiniGameHolesSummaryView : UIView {
    var stackView: UIStackView?
    private var circles = [MiniHolePreviewCircle]()
    
    @IBInspectable var circleSize: Int = 8
    
    /// General view setup
    func setup(values: [HoleResultValue]) {
        self.frame.size.height = 40
        // Create the hole views
        for (index, value) in values.enumerated() {
            if index >= circles.count {
                let circle = MiniHolePreviewCircle(value: value)
                
                circle.snp.makeConstraints { make in
                    make.size.equalTo(circleSize).priority(750)
                }
                
                circles.append(circle)
                stackView?.addArrangedSubview(circle) // if it does not exist it will be initialized with the circles
            } else {
                let circle = circles[index]
                circle.value = value
                circle.isHidden = false
            }
        }
        
        // hide other circles
        for circle in circles.suffix(from: values.endIndex) {
            circle.isHidden = true
        }
        
        if self.stackView == nil {
            let stackView = UIStackView(arrangedSubviews: circles)
            stackView.axis = .horizontal
            stackView.spacing = 3
            stackView.distribution = .equalCentering
            addSubview(stackView)
            stackView.snp.makeConstraints { make in
                make.edges.equalToSuperview()
            }
            self.stackView = stackView
        }
    }
    
    override func prepareForInterfaceBuilder() {
        let values = (1...18).map { _ in HoleResultValue.random }
        self.setup(values: values)
    }
}
