import UIKit

class UntappableView : UIView {
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        let view = super.hitTest(point, with: event)
        
        guard view != self else {
            return nil
        }
        
        return view
    }
}
