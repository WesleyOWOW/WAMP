import UIKit
import SnapKit

class CourseCreator: UIView {
    
    var course: Course?
    var mode: CreationMode = .edit
    var holeScrollView: UIScrollView!
    var hasMade = false
    var holes: Int = 18 {
        didSet {
            self.subviews.forEach({ $0.removeFromSuperview() })
            make()
        }
    }
    
    init(course: Course, frame: CGRect) {
        super.init(frame: frame)
        self.translatesAutoresizingMaskIntoConstraints = false
        self.course = course
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.translatesAutoresizingMaskIntoConstraints = false
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if hasMade == false {
            if self.course != nil {
                self.make()
            }
            hasMade = true
        }
    }
    
    func make() {
        makeFirstRow()
        
        if mode == .edit {
            var tees : [UIView] = []
            for tee in self.course!.tees {
                let teeview = makeTee(tee)
                teeview.snp.makeConstraints { make in
                    make.height.equalTo(178)
                }
                tees.append(teeview)
            }
            
            let teeStack = UIStackView(arrangedSubviews: tees)
            teeStack.axis = .vertical
            self.addSubview(teeStack)
            teeStack.snp.makeConstraints { make in
                make.trailing.leading.bottom.equalToSuperview()
                make.top.equalToSuperview().offset(65)
            }
        }
    }
    
    
    private func makeTee(_ tee: Tee) -> UIView {
        let teeView = UIView(frame: .zero)
        let titleView = UIView(frame: .zero)
        
        let nameLabel = UILabel(frame: .zero)
        nameLabel.font = .medium(14)
        nameLabel.text = tee.name
        nameLabel.textAlignment = .left
        
        // MARK - rate
        let rateValue = nameLabel.duplicated()
        rateValue.textColor = #colorLiteral(red: 0.7675911784, green: 0.7676092982, blue: 0.7675995827, alpha: 1)
        if let rating = tee.rating {
            rateValue.text = "\(rating)"
        } else if tee.rating == nil {
            rateValue.text = "Unknown"
        } else {
            rateValue.text = "\(String(describing: tee.rating))"
        }
        
        rateValue.textAlignment = .right
        
        titleView.addSubview(rateValue)
        rateValue.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.trailing.equalToSuperview().inset(16)
        }
        
        let rateLabel = nameLabel.duplicated()
        rateLabel.textColor = .black
        rateLabel.text = "Rate"
        
        titleView.addSubview(rateLabel)
        rateLabel.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.trailing.equalTo(rateValue.snp.leading).offset(-4)
        }
        
        // MARK - Slope
        
        let slopeValue = nameLabel.duplicated()
        slopeValue.textColor = #colorLiteral(red: 0.7675911784, green: 0.7676092982, blue: 0.7675995827, alpha: 1)
        if let slope = tee.slope {
            slopeValue.text = "\(slope)"
        } else if tee.slope == nil {
            slopeValue.text = "Unknown"
        } else {
            slopeValue.text = "\(String(describing: tee.slope))"
        }
        
        slopeValue.textAlignment = .right
        
        titleView.addSubview(slopeValue)
        slopeValue.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.trailing.equalTo(rateLabel.snp.leading).offset(-20)
        }
        
        let slopeLabel = nameLabel.duplicated()
        slopeLabel.textColor = .black
        slopeLabel.text = "Slope"
        
        titleView.addSubview(slopeLabel)
        slopeLabel.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.trailing.equalTo(slopeValue.snp.leading).offset(-4)
        }
        
        // MARK - rest
        
        titleView.addSubview(nameLabel)
        nameLabel.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.leading.equalToSuperview().offset(16)
        }
        
        teeView.addSubview(titleView)
        titleView.snp.makeConstraints { make in
            make.leading.top.trailing.equalToSuperview()
            make.height.equalTo(50)
        }
        
        teeView.snp.makeConstraints { make in
            make.height.equalTo(178)
        }
        
        let sepa = UIView(frame: .zero)
        sepa.backgroundColor = #colorLiteral(red: 0.9450980392, green: 0.9450980392, blue: 0.9450980392, alpha: 1)
        teeView.addSubview(sepa)
        sepa.snp.makeConstraints { make in
            make.trailing.leading.equalToSuperview()
            make.height.equalTo(1)
            make.top.equalTo(titleView.snp.bottom)
        }
        
        
        let parLabel = slopeValue.duplicated()
        parLabel.baselineAdjustment = .alignCenters
        parLabel.textAlignment = .left
        parLabel.text = "PAR"
        
        let hcpLabel = parLabel.duplicated()
        hcpLabel.text = "hcp"
        
        
        
        
        let labelsStack = UIStackView(arrangedSubviews: [parLabel, hcpLabel])
        labelsStack.axis = .vertical
        
        parLabel.snp.makeConstraints { make in
            make.height.equalTo(64)
            
        }
        
        hcpLabel.snp.makeConstraints { make in
            make.height.equalTo(64)
            
        }
        
        teeView.addSubview(labelsStack)
        labelsStack.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(16)
            make.top.equalTo(titleView.snp.bottom).offset(1)
            make.width.equalTo(64)
        }
        labelsStack.backgroundColor = .green
        let separator2 = UIView(frame: .zero)
        separator2.backgroundColor = #colorLiteral(red: 0.9450980392, green: 0.9450980392, blue: 0.9450980392, alpha: 1)
        teeView.addSubview(separator2)
        separator2.snp.makeConstraints { make in
            make.top.bottom.equalTo(labelsStack)
            make.width.equalTo(1)
            make.leading.equalTo(labelsStack.snp.trailing)
        }
        var verticalStacks: [UIStackView] = []
        for hole in tee.holes {
            let parView = UIView(frame: .zero)
            parView.snp.makeConstraints { make in
                make.size.equalTo(64)
            }
            let parInput = UITextField(frame: .zero)
            parInput.textColor = #colorLiteral(red: 0, green: 0.5353117585, blue: 1, alpha: 1)
            if let par = hole.par {
                parInput.text = "\(par)"
            }
            
            parInput.layer.cornerRadius = 24
            parInput.layer.borderColor = #colorLiteral(red: 0, green: 0.5353117585, blue: 1, alpha: 1)
            parInput.layer.borderWidth = 1
            
            parView.addSubview(parInput)
            parInput.snp.makeConstraints { make in
                make.size.equalTo(48)
                make.center.equalToSuperview()
            }
            
            let hcpView = UIView(frame: .zero)
            hcpView.snp.makeConstraints { make in
                make.size.equalTo(64)
            }
            let hcpInput = UITextField(frame: .zero)
            hcpInput.textColor = #colorLiteral(red: 0, green: 0.5353117585, blue: 1, alpha: 1)
            if let par = hole.par {
                parInput.text = "\(par)"
            }
            
            hcpInput.layer.cornerRadius = 24
            hcpInput.layer.borderColor = #colorLiteral(red: 0, green: 0.5353117585, blue: 1, alpha: 1)
            hcpInput.layer.borderWidth = 1
            
            hcpView.addSubview(hcpInput)
            hcpInput.snp.makeConstraints { make in
                make.size.equalTo(48)
                make.center.equalToSuperview()
            }
            
            let vStack = UIStackView(arrangedSubviews: [parView, hcpInput])
            vStack.axis = .vertical
            vStack.spacing = 1
            verticalStacks.append(vStack)
        }
        
        let horizontalStack = UIStackView(arrangedSubviews: verticalStacks)
        horizontalStack.axis = .horizontal
        horizontalStack.spacing = 1
        
        
        let scrollv = UIScrollView(frame: .zero)
        scrollv.addSubview(horizontalStack)
        horizontalStack.snp.makeConstraints { make in
            make.edges.equalToSuperview()
            make.height.equalTo(128)
        }
        scrollv.showsVerticalScrollIndicator = false
        scrollv.showsHorizontalScrollIndicator = false
        for i in 1..<horizontalStack.arrangedSubviews.count {
            let view = horizontalStack.arrangedSubviews[i]
            let before = horizontalStack.arrangedSubviews[i-1]
            
            addSpacer(.vertical, in: scrollv, between: before, and: view).snp.makeConstraints { make in
                make.top.bottom.equalToSuperview()
            }
        }
        
        
        
        teeView.addSubview(scrollv)
        scrollv.snp.makeConstraints { make in
            make.height.equalTo(129)
            make.top.equalTo(labelsStack)
            make.leading.equalTo(labelsStack.snp.trailing).offset(1)
            make.trailing.equalToSuperview()
        }
        
        let separator3 = UIView(frame: .zero)
        separator3.backgroundColor = #colorLiteral(red: 0.9450980392, green: 0.9450980392, blue: 0.9450980392, alpha: 1)
        teeView.addSubview(separator3)
        separator3.snp.makeConstraints { make in
            make.trailing.leading.equalToSuperview()
            make.height.equalTo(1)
            make.top.equalTo(scrollv).offset(64)
        }
        
        let separator = UIView(frame: .zero)
        separator.backgroundColor = #colorLiteral(red: 0.9450980392, green: 0.9450980392, blue: 0.9450980392, alpha: 1)
        teeView.addSubview(separator)
        separator.snp.makeConstraints { make in
            make.trailing.leading.equalToSuperview()
            make.height.equalTo(1)
            make.top.equalTo(scrollv.snp.bottom)
        }
        
        return teeView
    }
    
    func makeFirstRow() {
        var firstRow : [UIView] = []
        let firstView: UIView = UIView(frame: .zero)
        let holeLabel = UILabel(frame: .zero)
        holeLabel.font = .medium(14)
        holeLabel.text = "HOLE"
        holeLabel.textAlignment = .center
        firstRow.append(holeLabel)
        
        for i in 1...self.holes {
            let number = holeLabel.duplicated()
            number.text = "\(i)"
            number.snp.makeConstraints { make in
                make.size.equalTo(64)
            }
            
            firstRow.append(number)
        }
        
        let numberStack = UIStackView(arrangedSubviews: firstRow)
        numberStack.axis = .horizontal
        
        self.holeScrollView = UIScrollView(frame: .zero)
        holeScrollView.showsVerticalScrollIndicator = false
        holeScrollView.showsHorizontalScrollIndicator = false
        holeScrollView.addSubview(numberStack)
        numberStack.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        for i in 2..<numberStack.arrangedSubviews.count {
            let view = numberStack.arrangedSubviews[i]
            let before = numberStack.arrangedSubviews[i-1]
            
            addSpacer(.vertical, in: holeScrollView, between: before, and: view).snp.makeConstraints { make in
                make.top.bottom.equalToSuperview()
            }
        }
        
        firstView.addSubview(holeLabel)
        holeLabel.snp.makeConstraints { make in
            make.height.equalTo(64)
            make.width.equalTo(72)
            make.top.leading.equalToSuperview()
        }
        
        firstView.addSubview(holeScrollView)
        holeScrollView.snp.makeConstraints { make in
            make.top.trailing.equalToSuperview()
            make.height.equalToSuperview()
            make.leading.equalTo(holeLabel.snp.trailing).offset(1)
        }
        
        addSpacer(.vertical, in: firstView, between: holeLabel, and: holeScrollView).snp.makeConstraints { make in
            make.top.bottom.equalToSuperview()
        }
        
        self.addSubview(firstView)
        let sepa = UIView(frame: .zero)
        sepa.backgroundColor = #colorLiteral(red: 0.9450980392, green: 0.9450980392, blue: 0.9450980392, alpha: 1)
        firstView.addSubview(sepa)
        sepa.snp.makeConstraints { make in
            make.trailing.leading.equalToSuperview()
            make.height.equalTo(1)
            make.top.equalTo(holeLabel.snp.bottom)
        }
        
        firstView.snp.makeConstraints { make in
            make.top.trailing.leading.equalToSuperview()
            make.height.equalTo(64)
        }
    }
}

