import AVKit
import UIKit
import AVFoundation

// A custom view that'll play a video. Used for the background in the login screen.
@IBDesignable class VideoView: UIView {
    
    @IBInspectable var videoName: String = "" {
        didSet {
            playVideo()
        }
    }
    
    var player: AVPlayer!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        // Notification to restart the video
         NotificationCenter.default.addObserver(self, selector: #selector(self.playVideo), name: Notification.Name("backToFront"), object: nil)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        // Notification to restart the video
        NotificationCenter.default.addObserver(self, selector: #selector(self.playVideo), name: Notification.Name("backToFront"), object: nil)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        playVideo()
    }
    
    @objc fileprivate func playVideo() {
        

        let file = self.videoName.components(separatedBy: ".")
        
        guard let path = Bundle.main.path(forResource: file[0], ofType:file[1]) else {
            log.error("Video file \(file.joined(separator: ".")) not found")
            return
        }
        
        self.player = AVPlayer(url: URL(fileURLWithPath: path))
        let playerLayer = AVPlayerLayer(player: self.player)
        playerLayer.videoGravity = AVLayerVideoGravityResizeAspectFill
        playerLayer.frame = self.bounds
        self.layer.addSublayer(playerLayer)
        self.play()
    
        NotificationCenter.default.addObserver(forName: .AVPlayerItemDidPlayToEndTime, object: self.player.currentItem, queue: .main) { _ in
            self.restart()
        }
    
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.restart), name: Notification.Name("playVideos"), object: nil)
    }
    
    @objc func restart() {
       // Only play the video when the login screen is actually visible to prevent the cutting of music.
        if (!AuthenticationManager.shared.isLoggedin) {
            self.player.seek(to: kCMTimeZero)
            self.player.play()
        }
        
    }
    
    func play() {
        // Only play the video when the login screen is actually visible to prevent the cutting of music.
        self.player.play()
    }
}



