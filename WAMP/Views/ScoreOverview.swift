import UIKit
import SnapKit

class ScoreOverview: UIView, UIScrollViewDelegate {
    var match: Match!
    
    var holeRow: UIView = UIView(frame: .zero) // The top row of the overview that houses the "HOLE" label & values. This view NEVER scrolls verically. The values do scroll horizontally, while the label always stays in place.
    
    var par_hcpRow: UIView = UIView(frame: .zero) // The row that houses both the "PAR" and "hcp" labels & values. The entire "row" scrolls vertically behind the `holeRow` (see above). The values do scroll horizontally, while the label always stays in place horizontally.
    
    var userRows: UIView = UIView(frame: .zero) // The part of the overview that houses the names of the users and the score.
    
    var verticalScrollView: UIScrollView = UIScrollView(frame: .zero) // The scroll view that enables horizontal scrolling in case of a 4BBB match.
    
    var holeScrollView: UIScrollView!
    var parScrollView: UIScrollView!
    var hcpScrollView: UIScrollView!
    var scoreScrollView: UIScrollView!
    
    // The computed amount of holes for the match currently being shown
    fileprivate var holeCount: Int {
        // The scores are initialized in `setup()` which calls `match.initializeHoles()` so this should be safe
        return match.players[0].scores!.count
    }
    
    func setup(_ match: Match) {
        self.match = match
        verticalScrollView.delegate = self
        if let match = match as? ConceptMatch, match.players[0].scores == nil {
            match.initializeHoles()
        }
        
        makeHoleRow()
        makeParhcp()
        createStatistics()
        
        
        self.addSubview(self.holeRow)
        self.holeRow.snp.makeConstraints { make in
            make.trailing.leading.top.equalToSuperview()
            make.height.equalTo(40)
        }
        
        self.verticalScrollView.addSubview(self.par_hcpRow)
        par_hcpRow.snp.makeConstraints { make in
            make.trailing.leading.top.equalToSuperview()
            make.height.equalTo(80)
            make.width.equalToSuperview()
        }
        
        self.verticalScrollView.addSubview(self.userRows)
        self.userRows.snp.makeConstraints { make in
            make.trailing.equalToSuperview()
            make.top.equalTo(par_hcpRow.snp.bottom)
            make.width.equalToSuperview()
            make.height.greaterThanOrEqualTo(100)
        }
        
        
        
        self.addSubview(self.verticalScrollView)
        
        self.verticalScrollView.snp.makeConstraints { make in
            make.leading.trailing.bottom.equalToSuperview()
            make.width.equalToSuperview()
            make.top.equalTo(self.holeRow.snp.bottom)
        }
        
        addSpacer(.horizontal, in: self, between: holeRow, and: verticalScrollView).snp.makeConstraints { make in
            make.trailing.equalToSuperview()
            make.leading.equalToSuperview().offset(120)
        }
        
        addSpacer(.horizontal, in: self, between: parScrollView, and: hcpScrollView).snp.makeConstraints { make in
            make.trailing.equalToSuperview()
            make.leading.equalToSuperview().offset(120)
        }
        
        let sepa = UIView(frame: .zero)
        sepa.backgroundColor = UIColor(hex: "000000", alpha: 0.05)
        verticalScrollView.addSubview(sepa)
        sepa.snp.makeConstraints { make in
            make.width.equalTo(1)
            make.top.bottom.equalTo(userRows)
            make.leading.equalTo(userRows.snp.trailing)
        }
        
        scoreScrollView.contentSize.height = CGFloat(55 * (match.players.count + 1))
        makeScoreIndicator()
    }
    
    private func makeHoleRow() {
        let separator = UIView(frame: .zero) // Starting with the separator so I can then position the title label based on that.
        separator.backgroundColor = UIColor(hex: "000000")
        separator.alpha = 0.05
        holeRow.addSubview(separator)
        separator.snp.makeConstraints { make in
            make.width.equalTo(1)
            make.top.bottom.equalToSuperview()
            make.leading.equalToSuperview().offset(120)
        }
        
        let holeLabel = makeTitleLabel("HOLE", alignment: .left, active: true)
        holeRow.addSubview(holeLabel)
        holeLabel.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(10)
            make.trailing.equalTo(separator.snp.leading).offset(-13)
        }
        
        holeScrollView = UIScrollView(frame: .zero)
        holeScrollView.delegate = self
        holeScrollView.showsHorizontalScrollIndicator = false
        holeScrollView.showsVerticalScrollIndicator = false
        holeRow.addSubview(holeScrollView)
        holeScrollView.snp.makeConstraints { make in
            make.top.bottom.trailing.equalToSuperview()
            make.leading.equalTo(separator.snp.trailing)
        }
        
        var holes : [UILabel] = []
        
        for hole in self.holeCount {
            let label = makeTitleLabel("\(hole + 1)", alignment: .center, active: true)
            
            holes.append(label)
            
            label.snp.makeConstraints { make in
                make.width.equalTo(30)
                make.height.equalTo(40)
            }
        }
        
        let holeValueStack = UIStackView(arrangedSubviews: holes)
        holeValueStack.axis = .horizontal
        holeValueStack.distribution = .fillEqually
        holeScrollView.addSubview(holeValueStack)
        holeValueStack.snp.makeConstraints { make in
            make.height.equalTo(40)
            make.edges.equalToSuperview()
        }
        
        
        
        for i in 1..<holeValueStack.arrangedSubviews.count {
            let view = holeValueStack.arrangedSubviews[i]
            let before = holeValueStack.arrangedSubviews[i-1]
            
            addSpacer(.vertical, in: holeScrollView, between: before, and: view).snp.makeConstraints { make in
                make.top.bottom.equalToSuperview()
            }
        }
    }
    
    private func makeParhcp() {
        let separator = UIView(frame: .zero) // Starting with the separator so I can then position the title label based on that.
        separator.backgroundColor = UIColor(hex: "000000")
        separator.alpha = 0.05
        par_hcpRow.addSubview(separator)
        separator.snp.makeConstraints { make in
            make.width.equalTo(1)
            make.height.equalToSuperview()
            make.top.equalToSuperview()
            make.leading.equalToSuperview().offset(120)
        }
        
        let parLabel = makeTitleLabel("PAR", alignment: .left)
        par_hcpRow.addSubview(parLabel)
        parLabel.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(10)
            make.trailing.equalTo(separator.snp.leading).offset(-13)
        }
        
        let hcpLabel = makeTitleLabel("hcp", alignment: .left)
        par_hcpRow.addSubview(hcpLabel)
        hcpLabel.snp.makeConstraints { make in
            make.top.equalTo(parLabel.snp.bottom).offset(20)
            make.trailing.equalTo(separator.snp.leading).offset(-13)
        }
        
        parScrollView = UIScrollView(frame: .zero)
        parScrollView.delegate = self
        parScrollView.showsHorizontalScrollIndicator = false
        parScrollView.showsVerticalScrollIndicator = false
        par_hcpRow.addSubview(parScrollView)
        parScrollView.snp.makeConstraints { make in
            make.top.trailing.equalToSuperview()
            make.height.equalTo(40)
            make.leading.equalTo(separator.snp.trailing)
        }
        
        
        hcpScrollView = UIScrollView(frame: .zero)
        hcpScrollView.delegate = self
        hcpScrollView.showsHorizontalScrollIndicator = false
        hcpScrollView.showsVerticalScrollIndicator = false
        par_hcpRow.addSubview(hcpScrollView)
        hcpScrollView.snp.makeConstraints { make in
            make.bottom.trailing.equalToSuperview()
            make.height.equalTo(40)
            make.leading.equalTo(separator.snp.trailing)
        }
        
        var pars : [UILabel] = []
        var hcps : [UILabel] = []
        
        for holeIndex in 0..<self.holeCount {
            let hole: Hole? = match.players[0].tee?.holes[holeIndex]
            
            
            let parLabel = makeTitleLabel("\(hole?.par ?? 0)", alignment: .center)
            pars.append(parLabel)
            
            parLabel.snp.makeConstraints { make in
                make.width.equalTo(30)
                make.height.equalTo(40)
            }
            
            let hcpLabel = parLabel.duplicated()
            hcpLabel.text = "\(hole?.strokeIndex ?? 0)"
            hcps.append(hcpLabel)
            
            hcpLabel.snp.makeConstraints { make in
                make.width.equalTo(30)
                make.height.equalTo(40)
            }
        }
        
        let parValueStack = UIStackView(arrangedSubviews: pars)
        parValueStack.axis = .horizontal
        parValueStack.distribution = .fillEqually
        parScrollView.addSubview(parValueStack)
        parValueStack.snp.makeConstraints { make in
            make.height.equalTo(40)
            make.edges.equalToSuperview()
        }
        
        let hcpValueStack = UIStackView(arrangedSubviews: hcps)
        hcpValueStack.axis = .horizontal
        hcpValueStack.distribution = .fillEqually
        hcpScrollView.addSubview(hcpValueStack)
        hcpValueStack.snp.makeConstraints { make in
            make.height.equalTo(40)
            make.edges.equalToSuperview()
        }
        
        log.error(parValueStack.arrangedSubviews.count)
        
        for i in 1..<parValueStack.arrangedSubviews.count {
            let view = parValueStack.arrangedSubviews[i]
            let before = parValueStack.arrangedSubviews[i-1]
            
            addSpacer(.vertical, in: parScrollView, between: before, and: view).snp.makeConstraints { make in
                make.top.bottom.equalToSuperview()
            }
        }
        
        for i in 1..<hcpValueStack.arrangedSubviews.count {
            let view = hcpValueStack.arrangedSubviews[i]
            let before = hcpValueStack.arrangedSubviews[i-1]
            
            addSpacer(.vertical, in: hcpScrollView, between: before, and: view).snp.makeConstraints { make in
                make.top.bottom.equalToSuperview()
            }
        }
    }
    
    
    
    private func createStatistics() {
        var playerViews: [UIView] = []
        var scoreViews: [UIStackView] = []
        let holeSummary = match.summarizeHoles()
        var buttonsStack: UIStackView!
        
        for (_, player) in match.players.enumerated() {
            let playerView = UIView(frame: .zero)
            
            let label = UILabel(frame: .zero)
            label.textColor = .black
            label.font = .medium(14)
            var lastname = player.profile.lastName
            if (player.profile.lastName.count > 9) {
                lastname = String(player.profile.lastName.prefix(6)) + "..."
            }
            label.text = player.profile.firstName[0] + ". " + lastname
            
            playerView.addSubview(label)
            
            let sep = UIView(frame: .zero)
            sep.backgroundColor = .black
            sep.alpha = 0.05
            playerView.addSubview(sep)
            sep.snp.makeConstraints { make in
                make.top.bottom.equalToSuperview()
                make.width.equalTo(1)
                make.trailing.equalToSuperview().offset(1)
            }
            
            label.snp.makeConstraints { make in
                make.centerY.equalToSuperview()
                make.leading.equalToSuperview().offset(16)
            }
            
            playerView.snp.makeConstraints { make in
                make.height.equalTo(55)
                make.width.equalTo(120)
            }
            playerViews.append(playerView)
            
            var buttons: [UIView] = []
            
            for (holeIndex, holeScore) in player.scores!.enumerated() {
                let title: String
                
                if let score = holeScore, score > 99 {
                    title = "l"
                } else if let score = holeScore, score < 0 {
                    title = "w"
                } else if let score = holeScore, score == 0 {
                    title = "s"
                } else if let score = holeScore {
                    title = "\(score)"
                } else {
                    title = "-"
                }
                
                var isWinning = false
                if match.players.count == 2 {
                    isWinning = holeSummary![holeIndex] == player.team.holeResultValue
                } else {
                    isWinning = match.isHoleWinner(for: holeIndex, and: player)
                }
                
                let color = isWinning ? player.team.color : #colorLiteral(red: 0.7490196078, green: 0.7490196078, blue: 0.7490196078, alpha: 1)
                let button = createScoreButton(with: title, and: color)
                let back = UIView(frame: .zero)
                back.addSubview(button)
                button.snp.makeConstraints { make in
                    make.center.equalToSuperview()
                }
                
                back.snp.makeConstraints { make in
                    make.height.equalTo(55)
                    make.width.equalTo(30)
                }
                
                buttons.append(back)
            }
            
            buttonsStack = UIStackView(arrangedSubviews: buttons)
            buttonsStack.axis = .horizontal
            
            
            
            buttonsStack.distribution = .fillEqually
            scoreViews.append(buttonsStack)
        }
        
        let playerStack = UIStackView(arrangedSubviews: playerViews)
        playerStack.axis = .vertical
        
        
        playerStack.distribution = .fillEqually
        
        userRows.addSubview(playerStack)
        playerStack.backgroundColor = .blue
        playerStack.snp.makeConstraints { make in
            make.width.equalTo(120)
            make.height.equalToSuperview()
            make.top.bottom.leading.equalToSuperview()
        }
        
        let separator = UIView(frame: .zero)
        separator.backgroundColor = .black
        separator.alpha = 0.05
        self.userRows.addSubview(separator)
        separator.snp.makeConstraints { make in
            make.height.equalTo(1)
            make.leading.trailing.equalToSuperview()
            make.top.equalToSuperview()
        }
        
        for i in 1..<playerStack.arrangedSubviews.count {
            let view = playerStack.arrangedSubviews[i]
            let before = playerStack.arrangedSubviews[i-1]
            
            addSpacer(.horizontal, in: userRows, between: before, and: view).snp.makeConstraints { make in
                make.trailing.leading.equalToSuperview()
            }
        }
        
        let scoreStack = UIStackView(arrangedSubviews: scoreViews)
        scoreStack.axis = .vertical
        scoreStack.distribution = .fillEqually
        
        
        
        scoreStack.backgroundColor = .blue
        
        scoreScrollView = UIScrollView(frame: .zero)
        scoreScrollView.delegate = self
        scoreScrollView.showsHorizontalScrollIndicator = false
        scoreScrollView.showsVerticalScrollIndicator = false
        scoreScrollView.addSubview(scoreStack)
        scoreStack.snp.makeConstraints { make in
            make.edges.height.equalToSuperview()
        }
        userRows.addSubview(scoreScrollView)
        scoreScrollView.clipsToBounds = true
        
        scoreScrollView.snp.makeConstraints { make in
            make.top.bottom.trailing.equalToSuperview()
            make.height.equalToSuperview()
            make.leading.equalTo(playerStack.snp.trailing).offset(1)
        }
        
        for i in 1..<scoreStack.arrangedSubviews.count {
            let view = scoreStack.arrangedSubviews[i]
            let before = scoreStack.arrangedSubviews[i-1]
            
            addSpacer(.horizontal, in: scoreScrollView, between: before, and: view).snp.makeConstraints { make in
                make.trailing.leading.equalToSuperview()
            }
        }
        
        
        
        
        
        for i in 1..<buttonsStack.arrangedSubviews.count {
            let view = buttonsStack.arrangedSubviews[i]
            let before = buttonsStack.arrangedSubviews[i-1]
            
            addSpacer(.vertical, in: scoreScrollView, between: before, and: view).snp.makeConstraints { make in
                make.top.bottom.equalToSuperview()
            }
        }
    }
    
    private func createScoreButton(with title: String, and color: UIColor) -> UIView {
        let button = UIView(frame: .zero)
        button.snp.makeConstraints { make in
            make.height.width.equalTo(24)
        }
        
        button.layer.cornerRadius = 12
        button.clipsToBounds = true
        button.backgroundColor = color
        
        let label = UILabel(frame: .zero)
        label.font = .medium(14)
        label.textColor = .white
        label.text = title
        
        button.addSubview(label)
        label.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.centerY.equalToSuperview()
        }
        
        return button
    }
    
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let scrollableViews = [hcpScrollView, parScrollView, scoreScrollView, holeScrollView]
        scrollableViews.forEach { if $0 != scrollView {
            var scrollBounds = $0?.bounds
            scrollBounds?.origin = scrollView.contentOffset
            $0?.bounds = scrollBounds!
            } }
    }
    
    
    
    /**
     * This just creates UILabels, so we don't have to keep copy & pasting that code.
     */
    private func makeTitleLabel(_ text: String, alignment: NSTextAlignment = .center, active: Bool = false) -> UILabel {
        let label = UILabel(frame: .zero)
        label.text = text
        label.textColor = active ? .black : UIColor(hex: "BFBFBF")
        label.font = .medium(14)
        label.textAlignment = alignment
        
        return label
    }
    
    private func makeScoreIndicator() {
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: 74, height: 16))
        let indicator = UIView(frame: .zero)
        indicator.layer.cornerRadius = 4
        indicator.backgroundColor = self.getWinningColor()
        self.addSubview(indicator)
        
        indicator.snp.makeConstraints { make in
            make.width.equalTo(74)
            make.height.equalTo(self.getHeight())
            make.trailing.equalToSuperview().offset(10)
            make.top.equalToSuperview().offset(self.getTopLocation())
        }
        
        label.font = .bold(16)
        label.textColor = .white
        label.textAlignment = .center
        label.text = match.score.rendered
        indicator.addSubview(label)
        
        label.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.centerX.equalToSuperview().offset(-5)
            make.width.equalToSuperview()
            make.height.equalTo(16)
        }
    }
    
    private func getHeight() -> Int {
        if match.winningTeam == nil {
            return (match.players.count == 4) ? 220 : 110
        }
        
        return (match.players.count == 4) ? 110 : 55
    }
    
    private func getWinningColor() -> UIColor {
        if match.winningTeam == nil {
            return UIColor(hex: "BFBFBF")
        }
        
        return (match.winningTeam?.color)!
    }
    
    private func getTopLocation() -> Int {
        if match.winningTeam == nil {
            return 121
        } else if match.winningTeam == .blue {
            return 121
        } else if match.winningTeam == .red {
            if match.players.count == 2 {
                return 175
            }
            
            return 223
        }
        
        return 121
    }
}

