import UIKit
import SnapKit

@IBDesignable class RoundedShadowRect : UIView {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup() {
        let imageView = UIImageView(image: UIImage(named: "RoundedShadowRect"))
        insertSubview(imageView, at: 0)
        
        
        imageView.snp.makeConstraints { make in
            make.edges.equalToSuperview().inset(UIEdgeInsets(top: -14, left: -14, bottom: -14, right: -14)) // the shadow is 10 blur + 10 spread, 5 Y
        }
    }
}

