//
//  WAMPButtonGrid.swift
//  WAMP
//


import UIKit
import SnapKit

/// The button grid used to enter Golf Scores. Shows numbers and some additional buttons.
/// The buttons are aligned with stack views, the outermost stack view being a vertical one
/// and then with horizontal stack views for each row.

@IBDesignable final class WAMPButtonGrid: UIView {
    
    enum Mode {
        case numberPad
        case holeSelection
    }
    
    let mode: Mode
    var verticalStackView: UIStackView! = nil
    weak var delegate: WAMPButtonGridDelegate? = nil
    
    override init(frame: CGRect) {
        mode = .numberPad
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        mode = .numberPad
        super.init(coder: aDecoder)
        setup()
    }
    
    var buttonColor: UIColor = .white
    
    init(mode: Mode = .numberPad, buttonColor: UIColor? = nil) {
        self.mode = mode
        super.init(frame: CGRect.zero)
        
        if let buttonColor = buttonColor {
            self.buttonColor = buttonColor
        }
        
        setup()
    }
    
    private func delegateCall(_ button: ButtonGridButton) -> (() -> Void) {
        return { [weak self] in
            guard let `self` = self else { return }
            self.delegate?.buttonGrid(self, didSelectButton: button)
        }
    }
    
    func setup() {
        switch mode {
        case .numberPad:
            let cancelButton = makeButton(.cancel, color: #colorLiteral(red: 0.8901960784, green: 0.9019607843, blue: 0.9294117647, alpha: 1), title: "")
            cancelButton.setImage(UIImage(named: "pijl_down"), for: .normal)
            
            let clearButton = makeButton(.clear, color: #colorLiteral(red: 0.8901960784, green: 0.9019607843, blue: 0.9294117647, alpha: 1), font: .medium(20), title: "")
            clearButton.setImage(UIImage(named: "Delete"), for: .normal)
            
            let winHoleButton = makeButton(.winHole, color: .white, font: .medium(14), title: "WIN", constrainWidth: false)
            let squareHoleButton = makeButton(.squareHole, color: .white, font: .medium(14), title: "SQUARE", constrainWidth: false)
            let loseHoleButton = makeButton(.loseHole, color: .white, font: .medium(14), title: "LOSE", constrainWidth: false)
            
            let numberRow = (1...3).map { makeButton(.number($0), color: .white, font: .medium(25), title: "\($0)") }
            let numberRow2 = (4...6).map { makeButton(.number($0), color: .white, font: .medium(25), title: "\($0)") }
            let numberRow3 = (7...9).map { makeButton(.number($0), color: .white, font: .medium(25), title: "\($0)") }
            
            let zero = makeButton(.number(0), color: .white, font: .medium(25), title: "0")
            let bottomRow = [
                cancelButton,
                zero,
                clearButton
            ]
            
            let topRow = [
                winHoleButton,
                squareHoleButton,
                loseHoleButton,
                ]
            
            // We will store all horizontal stack views here for creating them in the vertical stack view later
            let stackViews = [
                topRow,
                numberRow,
                numberRow2,
                numberRow3,
                bottomRow
                ].map(makeHorizontalStackView)
            
            
            
            
            
            createVerticalStackView(stackViews)
            let spaceRows = [
                topRow,
                numberRow,
                numberRow2,
                numberRow3,
                ]
            
            for row in spaceRows {
                let spacer = UIView(frame: .zero)
                
                spacer.backgroundColor = UIColor(hex: "E3E6ED")
                addSubview(spacer)
                spacer.snp.makeConstraints { make in
                    make.width.equalTo(UIScreen.main.bounds.size.width)
                    make.height.equalTo(1)
                    make.top.equalTo(row[0].snp.bottom)
                    make.trailing.leading.equalToSuperview()
                }
            }
            
            for i in (1...3) {
                let spacer = UIView(frame: .zero)
                spacer.backgroundColor = UIColor(hex: "E3E6ED")
                addSubview(spacer)
                spacer.snp.makeConstraints { make in
                    make.width.equalTo(1)
                    make.top.equalToSuperview()
                    make.bottom.equalToSuperview()
                    make.leading.equalTo((UIScreen.main.bounds.size.width / 3) * CGFloat(i))
                }
            }
            
            //            let spacer = UIView(frame: .zero)
            //            spacer.backgroundColor = UIColor(hex: "E3E6ED", with: 1.0)
            //            addSubview(spacer)
            //            spacer.snp.makeConstraints { make in
            //                make.width.equalTo(1)
            //                make.height.equalTo(54)
            //                make.top.equalToSuperview()
            //                make.centerX.equalToSuperview()
            //            }
            //
            let rows = [numberRow, numberRow2, numberRow3, bottomRow]
            
            for row in rows {
                for button in row {
                    button.snp.makeConstraints { make in
                        make.width.equalTo(UIScreen.main.bounds.size.width / 3)
                        make.height.equalTo(54)
                    }
                }
            }
            
            winHoleButton.snp.makeConstraints { make in
                //                make.leading.equalTo(numberRow1[1])
                make.width.equalTo(UIScreen.main.bounds.size.width)
                make.height.equalTo(54)
            }
            
            squareHoleButton.snp.makeConstraints { make in
                make.size.equalTo(winHoleButton)
            }
            
            loseHoleButton.snp.makeConstraints { make in
                make.size.equalTo(winHoleButton)
            }
            
        case .holeSelection:
            var stackViews = [UIStackView]()
            
            // these buttons will be centerX-aligned for equal spacing
            var firstRowSecondButton: UIButton!
            var lastRowSecondButton: UIButton!
            
            for range in [1...5, 6...10, 11...15, 16...19] {
                let buttons = range.map { makeButton(.number($0), color: buttonColor, font: .bold(20), title: "\($0)") }
                
                if firstRowSecondButton == nil {
                    firstRowSecondButton = buttons[2]
                }
                lastRowSecondButton = buttons[2]
                
                stackViews.append(makeHorizontalStackView(buttons))
            }
            
            stackViews.last!.addArrangedSubview(makeButton(.clear, color: #colorLiteral(red: 0.166390866, green: 0.5775913596, blue: 1, alpha: 1), font: .bold(20), title: "C"))
            
            createVerticalStackView(stackViews)
            
            lastRowSecondButton.snp.makeConstraints { make in
                make.centerX.equalTo(firstRowSecondButton)
            }
            
        }
    }
    
    private func makeButton(_ identifier: ButtonGridButton, color: UIColor, font: UIFont? = nil, title: String, constrainWidth: Bool = true) -> UIButton {
        let button = UIButton()
        button.tintColor = color
        button.backgroundColor = color
        button.setTitleColor(.black, for: .normal)
        button.setTitle(title, for: .normal)
        button.titleLabel?.textAlignment = .center
        button.add(event: .touchUpInside, action: delegateCall(identifier))
        
        if let font = font {
            button.titleLabel!.font = font
        }
        
        button.snp.makeConstraints { make in
            make.height.equalTo(54)
            
            if constrainWidth {
                make.width.equalTo(UIScreen.main.bounds.size.width / 3)
            }
        }
        
        return button
    }
    
    /// Creates & configures the vertical stack view, with the given views. Called once during setup.
    private func createVerticalStackView(_ views: [UIView]) {
        verticalStackView = UIStackView(arrangedSubviews: views)
        verticalStackView.alignment = .fill
        verticalStackView.axis = .vertical
        //        verticalStackView.spacing = 8
        verticalStackView.distribution = .fillEqually
        addSubview(verticalStackView)
        
        // Constraints
        verticalStackView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
    
    /// Creates & configures a horizontal button row stack view. It is not added to the view hierarchy or vertical stack view.
    private func makeHorizontalStackView(_ views: [UIView]) -> UIStackView {
        let stackView = UIStackView(arrangedSubviews: views)
        stackView.alignment = .fill
        stackView.axis = .horizontal
        stackView.distribution = .fillEqually
        stackView.snp.makeConstraints { make in
            make.height.equalTo(54)
        }
        
        return stackView
    }
    
}

enum ButtonGridButton : Equatable {
    case clear
    case cancel
    case winHole
    case loseHole
    case squareHole
    case number(Int)
    
    static func ==(lhs: ButtonGridButton, rhs: ButtonGridButton) -> Bool {
        switch (lhs, rhs) {
        case (.clear, .clear), (.cancel, .cancel), (.winHole, .winHole), (.squareHole, .squareHole), (.loseHole, .loseHole):
            return true
        case (.number(let n1), .number(let n2)) where n1 == n2:
            return true
        default:
            return false
        }
    }
}

protocol WAMPButtonGridDelegate : class {
    func buttonGrid(_ buttonGrid: WAMPButtonGrid, didSelectButton button: ButtonGridButton)
}

