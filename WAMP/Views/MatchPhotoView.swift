
import UIKit
import SnapKit


/// Used in the match photo view
class PhotoCountIndicator : UIStackView {
    init() {
        super.init(frame: .zero)
        
        spacing = 4
    }
    
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var count: Int = 0 {
        didSet {
            update()
        }
    }
    
    var currentPage: Int = 0 {
        didSet {
            update()
        }
    }
    
    func update() {
        while arrangedSubviews.count < count {
            let view = UIView()
            view.layer.cornerRadius = 2
            view.layer.masksToBounds = false
            view.layer.shadowOffset = CGSize(width: 0, height: 0)
            view.layer.shadowRadius = 4
            view.layer.shadowOpacity = 0.5
            view.layer.shadowColor = UIColor.black.cgColor
            view.layer.shouldRasterize = true
            view.layer.rasterizationScale = UIScreen.main.scale
            view.snp.makeConstraints { make in
                make.size.equalTo(4).priority(750)
            }
            view.isOpaque = false
            addArrangedSubview(view)
        }
        
        for (i, view) in arrangedSubviews.enumerated() {
            if count > i {
                view.isHidden = false
                view.backgroundColor = i == currentPage ? #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1) : #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.5)
            } else {
                view.isHidden = true
            }
            
        }
    }
}

class MatchPhotoView : UIView, UIScrollViewDelegate {
    let scrollView = UIScrollView(frame: .zero)
    let stackView = UIStackView(arrangedSubviews: [])
    var imageViews = [UIImageView]()
    let photoCountIndicator = PhotoCountIndicator()
    
    init() {
        super.init(frame: .zero)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageWidth = scrollView.frame.size.width
        let fractionalPage = scrollView.contentOffset.x / pageWidth
        let page = Int(fractionalPage.rounded())
        
        photoCountIndicator.currentPage = page
    }
    
    func setup() {
        scrollView.isPagingEnabled = true
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.alwaysBounceHorizontal = true
        scrollView.delegate = self
        
        addSubview(scrollView)
        scrollView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        scrollView.addSubview(stackView)
        stackView.axis = .horizontal
        stackView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        addSubview(photoCountIndicator)
        photoCountIndicator.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.bottom.equalTo(scrollView).offset(-10)
        }
    }
    
    func load(_ match: Match) {
        
        
        if match.photoCount > 1 {
            photoCountIndicator.count = match.photoCount
            photoCountIndicator.isHidden = false
        } else {
            photoCountIndicator.isHidden = true
        }
        scrollView.setContentOffset(.zero, animated: false)
        
        for index in 0..<match.photoCount {
            let imageView: UIImageView
            if index < imageViews.count {
                imageView = imageViews[index]
            } else {
                imageView = UIImageView()
                imageView.contentMode = .scaleAspectFill
                imageView.clipsToBounds = true
                stackView.addArrangedSubview(imageView)
                imageView.snp.makeConstraints { make in
                    make.size.equalTo(self)
                }
                imageViews.append(imageView)
            }
            
            if index < match.photoCount {
                if let photo = match.getPhoto(index: index) {
                    imageView.setPhoto(photo)
                    imageView.isHidden = false
                }
            }
        }
        
    
        
        // hide other image views
        for imageView in imageViews.suffix(from: match.photoCount) {
            imageView.isHidden = true
            imageView.image = nil
        }
    }
}

