import UIKit

// Pill button with shadow
@IBDesignable class WampButton: UIButton {
    
    // Mark - Overrides
    override var isHighlighted: Bool {
        didSet {
            UIView.animate(withDuration: 0.1) {
                self.layer.shadowOpacity = self.isHighlighted ? 0 : 0.15
            }
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.layer.cornerRadius = self.frame.height / 2
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 0.5, height: 3.2)
        self.layer.shadowOpacity = 0.15
        self.layer.masksToBounds = false
        self.layer.shadowRadius = 7.0
        self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: self.frame.height / 2).cgPath
    }
    
    override var backgroundColor: UIColor? {
        didSet {
            if backgroundColor != nil && backgroundColor!.cgColor.alpha == 0 {
                backgroundColor = oldValue
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    // Mark - Custom methods
    
    @objc func removeShadow(gesture: UILongPressGestureRecognizer) {
        if gesture.state == .recognized {
            UIView.animate(withDuration: 0.1, animations: {
                self.layer.shadowOpacity = 0
            })
        } else if gesture.state == .ended {
            UIView.animate(withDuration: 0.1, animations: {
                self.layer.shadowOpacity = 0.15
            })
        }
    }
    
    @objc func makeShadow() {
        UIView.animate(withDuration: 0.1, animations: {
            self.layer.shadowOpacity = 0.15
        })
    }
    
    
}

