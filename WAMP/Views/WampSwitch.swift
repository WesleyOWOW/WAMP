import UIKit
import SnapKit

@IBDesignable class WampSwitch: UIControl {
    var switch_on = UIImage(named: "switch_on")
    var switch_off = UIImage(named: "switch_off")
    var thumbSize : CGSize = .zero
    var back = UIColor(hex: "D9D9D9")
    var isOn = true
    var image: UIImageView!
    var animationDuration: Double = 0.2
    fileprivate var thumbView = UIView(frame: .zero)
    fileprivate var onPoint = CGPoint.zero
    fileprivate var offPoint = CGPoint.zero
    fileprivate var isAnimating = false
    fileprivate var isSetup = false
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
        
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupUI()
    }
    
    func setupUI() {
        self.clear()
        self.clipsToBounds = false
        self.thumbView.backgroundColor = self.isOn ? UIColor(hex: "007BFF") : .white
        self.thumbView.isUserInteractionEnabled = false
        self.addSubview(self.thumbView)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if !self.isAnimating, !self.isSetup {
            self.layer.cornerRadius = self.bounds.size.height / 2
            self.backgroundColor = self.back
            
            let thumbSize = self.thumbSize != .zero ? self.thumbSize : CGSize(width:
                self.bounds.size.height - 4, height: self.bounds.height - 4)
            
            let yPostition = (self.bounds.size.height - thumbSize.height) / 2
            
            self.onPoint = CGPoint(x: self.bounds.size.width - thumbSize.width - 2, y: yPostition)
            self.offPoint = CGPoint(x: 2, y: yPostition)
            
            self.thumbView.frame = CGRect(origin: self.isOn ? self.onPoint : self.offPoint, size: thumbSize)
            
            self.thumbView.layer.cornerRadius = thumbSize.height / 2
            
            image = UIImageView(image: self.isOn ? switch_on : switch_off)
            thumbView.addSubview(image)
            
            image.snp.makeConstraints { make in
                make.size.equalTo(14)
                make.center.equalToSuperview()
            }
            
            self.isSetup = true
        }
        
    }
    
    override func beginTracking(_ touch: UITouch, with event: UIEvent?) -> Bool {
        super.beginTracking(touch, with: event)
        self.animate()
        return true
    }
    
    private func animate() {
        self.isOn = !self.isOn
        print("privacy:", self.isOn)
        self.isAnimating = true
        
        UIView.transition(with: image,
                          duration: self.animationDuration,
                          options: .transitionCrossDissolve,
                          animations: { self.image.image = self.isOn ? self.switch_on : self.switch_off },
                          completion: nil)
        
        
        UIView.animate(withDuration: self.animationDuration, animations: {
            self.thumbView.backgroundColor = self.isOn ? UIColor(hex: "007BFF") : .white
            self.thumbView.frame.origin.x = self.isOn ? self.onPoint.x : self.offPoint.x
            
        }, completion: { _ in
            self.isAnimating = false
            self.sendActions(for: UIControlEvents.valueChanged)
        })
    }
    
    private func clear() {
        for view in self.subviews {
            view.removeFromSuperview()
        }
    }
}

