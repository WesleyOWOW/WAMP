//
//  FSAlbumViewCell.swift
//  Fusuma
//
//  Created by Yuta Akizuki on 2015/11/14.
//  Copyright © 2015年 ytakzk. All rights reserved.
//

import UIKit
import Photos

final class FSAlbumViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    
    var selectedLayer = CALayer()
    
    var image: UIImage? {
        
        didSet {
            
            self.imageView.image = image            
        }
    }
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        self.isSelected = false
        
        selectedLayer.backgroundColor = UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 0.5).cgColor
    }
    
    override var isSelected : Bool {
        
        didSet {

            if selectedLayer.superlayer == self.layer {
                self.layer.borderWidth = 0
                selectedLayer.removeFromSuperlayer()
            }
            
            if isSelected {
                self.layer.borderWidth = 2
                self.layer.borderColor = #colorLiteral(red: 0, green: 0.5794699788, blue: 0.998126924, alpha: 1)
                selectedLayer.frame = self.bounds
                self.layer.addSublayer(selectedLayer)
            }
        }
    }
    
}
