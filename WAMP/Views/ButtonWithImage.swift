import UIKit

class ButtonWithImage: UIButton {
    var highlight: ClickThroughView!
    var setup = false
    
    override open var isHighlighted: Bool {
        didSet {
            
            UIView.animate(withDuration: 0.1) {
                self.highlight.backgroundColor = self.isHighlighted ? UIColor(hex: "DCDCDC") : UIColor(hex: "FCFCFC")
            }
            
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        titleLabel?.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        imageView?.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        titleLabel?.snp.makeConstraints { make in
            make.trailing.equalToSuperview()
        }
        
        if (!setup) {
            highlight = ClickThroughView(frame: .zero)
            highlight.backgroundColor = UIColor(hex: "FCFCFC")
            self.addSubview(highlight)
            self.sendSubview(toBack: highlight)
            
            highlight.snp.makeConstraints { make in
                make.leading.equalToSuperview().offset(-16)
                make.trailing.equalToSuperview().offset(16)
                make.bottom.equalToSuperview().offset(6)
                make.top.equalToSuperview().offset(-6)
            }
            
            setup = true
        }
    }
}
