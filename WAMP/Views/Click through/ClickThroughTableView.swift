import UIKit

class ClickThroughTableView : UITableView {
    /// Allow for tapping through the table view at inset areas at the top
    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        let val = point.y > 0 ? super.point(inside: point, with: event) : false
        return val
    }
}

