//
//  LockableScrollView.swift
//  WAMP
//


import UIKit

/// A scroll view that supports a mode where it only scrolls horizontally and expands vertically
class LockableScrollView : UIScrollView {
    
    @IBInspectable var scrollsVertically = true {
        didSet {
            self.update()
        }
    }
    
    private var heightConstraint: NSLayoutConstraint? = nil
    
    private func update() {
        guard !scrollsVertically else {
            // Remove the constraint if it exists
            if let constraint = heightConstraint {
                constraint.isActive = false
                self.heightConstraint = nil
            }
            return
        }
        
        // Get the constraint or create it and set the constant
        if let heightConstraint = heightConstraint {
            heightConstraint.constant = contentSize.height
        } else {
            heightConstraint = NSLayoutConstraint(item: self,
                                                  attribute: .height,
                                                  relatedBy: .equal,
                                                  toItem: nil,
                                                  attribute: .notAnAttribute,
                                                  multiplier: 1.0,
                                                  constant: contentSize.height)
            
            addConstraint(heightConstraint!)
        }
    }
    
    override var contentSize: CGSize {
        didSet {
            guard oldValue.height != contentSize.height else {
                return
            }
            
            self.update()
        }
    }
    
}

