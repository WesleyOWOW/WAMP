import UIKit
import SnapKit
import Alamofire
import SwiftyJSON

struct Comment {
    var id: String
    var user: UserProtocol
    var body: String
    var date: Date
    
    static func post(_ text: String, on match: APIMatch, onCompletion: @escaping (Result<Comment>) -> (Void)) {
        let body: Parameters = ["comment": text]
     
            Alamofire.request(api_url + "/matches/\(match.id)/comments",
                method: .post,
                parameters: body,
                encoding: JSONEncoding.default,
                headers: AuthenticationManager.shared.headers
                ).validate().responseJSON { response in
                    switch response.result {
                    case .success(let value):
                        log.info("comment success: \(value)")
                        let json = JSON(value)
                        let comment = Comment(id: json["id"].stringValue, user: User.me, body: text, date: Date())
                        
                        // this changes match values
                        //                    Broadcaster.notify(Refreshable.self) { $0.refresh(entity: .match) }
                        
                        onCompletion(.success(comment))
                    case .failure(let error):
                        log.error("comment error: \(error)")
                        onCompletion(.failure(error))
                    }
            
        }
        
        

    }
    
    static func get(for match: APIMatch, completion: @escaping (Result<[Comment]>) -> (Void)) {
        Alamofire.request(api_url + "/matches/\(match.id)/comments",
            headers: AuthenticationManager.shared.headers)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .success(let value):
                    completion(.success(parse(JSON(value))))
                case .failure(let error):
                    completion(.failure(error))
                }
        }
    }
    
    static func parse(_ json: JSON) -> [Comment] {
        return json.flatMap { (_, json) in
            guard let user = Player(json: json["user"]), let date = apiDateFormatter.date(from: json["created_at"].stringValue) else {
                log.error("invalid comment json \(json)")
                return nil
            }
            
            return Comment(id: json["id"].stringValue, user: user, body: json["body"].stringValue, date: date)
        }
    }
    
    func delete(onComplete handler: @escaping (Result<Bool>) -> ()) {
        Alamofire.request(api_url + "/comments/\(self.id)",
            method: .delete,
            headers: AuthenticationManager.shared.headers)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .success(_):
                    handler(.success(true))
                case .failure(let error):
                    log.error("Could not delete comment because of error: \(error)")
                    handler(.failure(error))
                }
        }
    }
}

class CommentView: UIView {
    let avatar = WAMPAvatarSmall(frame: .zero)
    let nameLabel = UILabel(frame: .zero)
    let commentLabel = UILabel(frame: .zero)
    let timeLabel = UILabel(frame: .zero)
   
    override init(frame: CGRect) {
        super.init(frame: frame)
         setUp()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setUp()
    }
    
    func setUp() {
        let stack = UIStackView(arrangedSubviews: [nameLabel, commentLabel])
        stack.axis = .vertical
        stack.spacing = 5
        
        avatar.layer.cornerRadius = 20
        avatar.clipsToBounds = true
        
        nameLabel.font = .medium(14)
        
        commentLabel.font = .medium(14)
        commentLabel.numberOfLines = 0
        commentLabel.textColor = UIColor(hex: "8C8C8C")
        
        timeLabel.font = .medium(14)
        timeLabel.textColor = UIColor(hex: "8C8C8C")
        
        self.addSubview(avatar)
        avatar.snp.makeConstraints { make in
            make.size.equalTo(40)
            make.top.leading.equalToSuperview()
        }
        
        self.addSubview(stack)
        stack.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(2)
            make.leading.equalTo(avatar.snp.trailing).offset(16)
            make.trailing.equalToSuperview().inset(16)
        }
        
        //        self.addSubview(commentLabel)
        //        commentLabel.snp.makeConstraints { make in
        //            make.top.equalTo(nameLabel.snp.bottom).offset(5)
        //            make.leading.equalTo(avatar.snp.trailing).offset(16)
        //            make.trailing.equalToSuperview().inset(16)
        //        }
        
        self.addSubview(timeLabel)
        timeLabel.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(2)
            make.trailing.equalToSuperview()
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
    }
    
    
}
