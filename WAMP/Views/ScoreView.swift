import UIKit
import Actions


fileprivate func playerStack() -> UIStackView {
    let stack = UIStackView(arrangedSubviews: [])
    stack.axis = .vertical
    stack.spacing = 3
    return stack
}

protocol ProfileNavigationHandler : class {
    func openProfile(for id: String)
    func showShareSheet(match: APIMatch, matchImage: UIImage)
    func showOptionsSheet(match: APIMatch)
    func displayError(_ text: String)
    func showComments(match: APIMatch)
    func showDetails(match: APIMatch)
    func openProfileHandler(left: Bool, for match: Match)
}

class ScoreView: UIView {
    weak var profileNavigationHandler: ProfileNavigationHandler?
    var match: Match!
    
    private let leftPlayerStack = playerStack()
    private let rightPlayerStack = playerStack()
    
    var leftWinnerBack = UIView(frame: .zero)
    var rightWinnerBack = UIView(frame: .zero)
    var squareWinnerBack = UIView(frame: .zero)
    
    var leftScore = UILabel(frame: .zero)
    var rightScore = UILabel(frame: .zero)
    var squareScore = UILabel(frame: .zero)
    
    var canMove = true
    
    func setup() {
        leftWinnerBack.backgroundColor = #colorLiteral(red: 0, green: 0.5728718638, blue: 1, alpha: 1)
        leftWinnerBack.isHidden = true
        
        rightWinnerBack.backgroundColor = #colorLiteral(red: 1, green: 0.3030309081, blue: 0.3337643743, alpha: 1)
        rightWinnerBack.isHidden = true
        
        squareWinnerBack.backgroundColor = UIColor(hex: "BFBFBF")
        squareWinnerBack.isHidden = true
        
        leftScore.font = .bold(22)
        leftScore.isHidden = true
        leftScore.textAlignment = .center
        leftScore.textColor = .white
        
        rightScore = leftScore.duplicated()
        rightScore.isHidden = true
        
        squareScore = rightScore.duplicated()
        squareScore.isHidden = true
        
        leftPlayerStack.clipsToBounds = true
        rightPlayerStack.clipsToBounds = true
        
        [leftWinnerBack, rightWinnerBack, squareWinnerBack, leftScore, rightScore, squareScore, leftPlayerStack, rightPlayerStack].forEach(addSubview)
        
        leftWinnerBack.snp.makeConstraints { make in
            make.right.equalTo(self.snp.centerX)
            make.top.left.equalToSuperview()
            make.height.equalTo(40)
        }
        
        rightWinnerBack.snp.makeConstraints { make in
            make.width.equalTo(leftWinnerBack)
            make.top.right.equalToSuperview()
            make.height.equalTo(40)
        }
        
        squareWinnerBack.snp.makeConstraints { make in
            make.width.equalToSuperview()
            make.top.right.left.equalToSuperview()
            make.height.equalTo(40)
        }
        
        let separator = UIView(frame: .zero)
        separator.backgroundColor = UIColor.black.withAlphaComponent(0.1)
        self.addSubview(separator)
        
        separator.snp.makeConstraints { make in
            make.width.equalTo(1)
            make.centerX.equalToSuperview()
            
            make.bottom.equalToSuperview()
            make.top.equalToSuperview().offset(40)
        }
        
        let separator2 = UIView(frame: .zero)
        separator2.backgroundColor = UIColor.black.withAlphaComponent(0.1)
        self.addSubview(separator2)
        
        separator2.snp.makeConstraints { make in
            make.height.equalTo(1)
            make.width.equalToSuperview().multipliedBy(2)
            make.top.equalTo(leftWinnerBack.snp.bottom)
        }
        
        let separator4 = UIView(frame: .zero)
        separator4.backgroundColor = UIColor.black.withAlphaComponent(0.1)
        self.addSubview(separator4)
        
        separator4.snp.makeConstraints { make in
            make.height.equalTo(1)
            make.width.equalToSuperview().multipliedBy(2)
            make.top.equalTo(self.snp.bottom)
        }
        
        
        leftScore.snp.makeConstraints { make in
            make.center.equalTo(leftWinnerBack)
        }
        
        rightScore.snp.makeConstraints { make in
            make.center.equalTo(rightWinnerBack)
        }
        
        squareScore.snp.makeConstraints { make in
            make.center.equalTo(squareWinnerBack)
        }
        
        leftPlayerStack.snp.makeConstraints { make in
            make.top.equalTo(52)
//            make.left.equalToSuperview().offset(16)
            make.right.equalTo(self.snp.centerX)
        }
        
        rightPlayerStack.snp.makeConstraints { make in
            make.top.equalTo(52)
//            make.right.equalToSuperview().offset(-16)
            make.left.equalTo(self.snp.centerX)
        }
    }
    
    public func load(_ match: Match) {
        self.match = match
        
        
        if match.players.count == 2 {
            leftPlayerStack.snp.makeConstraints { make in
                make.height.equalTo(18)
            }
            
            rightPlayerStack.snp.makeConstraints { make in
                make.height.equalTo(18)
            }
        } else {
            leftPlayerStack.snp.makeConstraints { make in
                make.height.equalTo(35)
            }
            
            rightPlayerStack.snp.makeConstraints { make in
                make.height.equalTo(35)
            }
        }
        
//        self.frame.size.height = CGFloat(height)
        
        leftWinnerBack.isVisible = match.winningTeam == match.team1
        rightWinnerBack.isVisible = match.winningTeam == match.team2
        
        [leftScore, rightScore, squareScore].forEach({ $0.text = match.score.rendered })
        

        
        if match.winningTeam == match.team1 || match.winningTeam == .blue {
            leftWinnerBack.isHidden = false
            leftScore.isHidden = false
            
            rightWinnerBack.isHidden = true
            rightScore.isHidden = true
            squareWinnerBack.isHidden = true
            squareScore.isHidden = true
        } else if match.winningTeam == match.team2 || match.winningTeam == .red {
            rightWinnerBack.isHidden = false
            rightScore.isHidden = false
            
            leftWinnerBack.isHidden = true
            leftScore.isHidden = true
            squareWinnerBack.isHidden = true
            squareScore.isHidden = true
        } else {
            squareWinnerBack.isHidden = false
            squareScore.isHidden = false
            
            leftWinnerBack.isHidden = true
            leftScore.isHidden = true
            rightWinnerBack.isHidden = true
            rightScore.isHidden = true
        }
        
        // create / update player views
        for i in 0..<match.players.count {
            let player = match.players[i]
            let isLeftTeam = i < match.players.count / 2
            let stack = isLeftTeam ? leftPlayerStack : rightPlayerStack
            let profile = player.profile
            
            let playerLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.frame.width / 2, height: 12))
            playerLabel.font = .medium(14)
            playerLabel.textAlignment = .center
            playerLabel.textColor = .black
            playerLabel.text = profile.firstName[0] + ". " + profile.lastName
            stack.addArrangedSubview(playerLabel)
            if canMove {
                if match.players.count == 2 {
                    
                    playerLabel.addAction { [unowned self] in
                        self.showProfile(index: i)
                    }
                    
                } else {
                    
                    playerLabel.addAction { [unowned self] in
                        let superview = self.superview?.superview?.superview?.superview as! FeedItem
                        superview.openProfileHandler(left: isLeftTeam, for: match)
                        
                    }
                }
            }
           
            
            if isLeftTeam {
                stack.snp.makeConstraints { make in
                    make.left.equalTo(self.snp.left)
                    make.right.equalTo(self.snp.centerX)
                }
            } else {
                stack.snp.makeConstraints { make in
                    make.right.equalTo(self.snp.right)
                    make.left.equalTo(self.snp.centerX)
                }
            }
        }
    }
    
    
    func showProfile(index: Int) {
        let superview = self.superview?.superview?.superview?.superview as! FeedItem
        superview.openProfile(for: match.players[index].profile.id)
       
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    init() {
        super.init(frame: .zero)
        setup()
    }
    
//    override func prepareForInterfaceBuilder() {
//        let match = ConceptMatch(.single, players: [fakeUsers.random, fakeUsers.random])
//        self.load(match)
//    }
}
