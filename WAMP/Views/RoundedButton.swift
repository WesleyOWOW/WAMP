//
//  RoundedButton.swift
//  WAMP
//


import UIKit

/// Used for various button throughout the app
/// These buttons are circular when their frame is square
@IBDesignable final class RoundedButton: UIButton {
    
    /// The border color. `nil` means "use the title color"
    @IBInspectable var WAMPBorderColor: UIColor? = nil
    
    /// The border width. Defaults to 1.
    @IBInspectable var WAMPBorderWidth: CGFloat = 1 {
        didSet {
            self.setNeedsDisplay()
        }
    }
    
    /// The color with which the inside of the button is filled. Acts as a sort of background color.
    @IBInspectable var roundedFillColor: UIColor? = nil {
        didSet {
            self.setNeedsDisplay()
        }
    }
    
    /// When set to true, the activity animation will be rendered
    var renderActivityAnimation = false
    
    /// The progress of the activity animation, in percents. 1 means the gap is at the same position as 0, e.g. a full circle has been made
    var activityAnimationProgress: CGFloat = 0
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setup()
    }
    
    init() {
        super.init(frame: CGRect.zero)
        self.setup()
    }
    
    override func draw(_ rect: CGRect) {
        let context = UIGraphicsGetCurrentContext()!
        context.saveGState()
        
        // We don't use a different highlighted border color
        let borderState = state == .highlighted ? .normal : state
        
        // Draw the border/fill:
        if let borderColor = WAMPBorderColor ?? self.titleColor(for: borderState) {
            let borderWidth = WAMPBorderWidth
            borderColor.setStroke()
            
            // Inset the bounds of the view by half of the line width, because the stroke is drawn centered on the path
            let pathRect = self.bounds.insetBy(dx: borderWidth/2, dy: borderWidth/2)
            let path = UIBezierPath(roundedRect: pathRect, cornerRadius: self.bounds.height / 2)
            path.lineWidth = borderWidth
            
            if let fill = roundedFillColor ?? (isHighlighted || isSelected ? borderColor : nil) {
                fill.setFill()
                path.fill()
            }
            
            // Render a gap when the renderActivityAnimation needs to be drawn
            if renderActivityAnimation {

            }
            
            path.stroke()
        } else {
            log.error("RoundedButton did not find a border color")
        }
        
        context.restoreGState()
    }
    
    func setup() {
        if let titleLabel = titleLabel {
            titleLabel.font = .medium(14)
            //            titleLabel.setCharacterSpacing(1.6)
            titleLabel.frame = self.bounds
        } else {
            assertionFailure("RoundedButton did not have titleLabel during setup")
        }
    }
    
    override func prepareForInterfaceBuilder() {
        self.setup()
    }
    
    override var isHighlighted: Bool {
        didSet {
            if isHighlighted == oldValue {
                return
            }
            print("HIGHLIGHT")
            
            // ensure a readable highlighted color
 
            
            self.setNeedsDisplay()
        }
    }
    
}

