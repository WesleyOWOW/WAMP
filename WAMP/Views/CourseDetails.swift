import UIKit
import SnapKit

enum CourseType {
    case eighteen
    case nine
}


class CourseDetails : UIView, UIScrollViewDelegate {
    var course: Course?
    var holeScrollView: UIScrollView!
    var hasMade = false
    var scrollviews: [UIScrollView] = []
    init(course: Course, frame: CGRect) {
        super.init(frame: frame)
        self.translatesAutoresizingMaskIntoConstraints = false
        self.course = course
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if hasMade == false {
            if self.course != nil {
                self.make()
            }
            
            hasMade = true
        }
    }
    
    func make() {
        makeFirstRow()
        
        var tees: [UIView] = []
        for tee in course!.tees {
            let teeview = makeTee(tee)
            tees.append(teeview)
            teeview.snp.makeConstraints { make in
                make.height.equalTo(128)
            }
        }
        
        let teeStack = UIStackView(arrangedSubviews: tees)
        teeStack.axis = .vertical
        teeStack.backgroundColor = .purple
        self.addSubview(teeStack)
        teeStack.snp.makeConstraints { make in
            make.trailing.leading.bottom.equalToSuperview()
            make.top.equalToSuperview().offset(41)
        }
    }
    
    func makeFirstRow() {
        var firstRow : [UIView] = []
        let firstView: UIView = UIView(frame: .zero)
        let holeLabel = UILabel(frame: .zero)
        holeLabel.font = .medium(14)
        holeLabel.text = "HOLE"
        holeLabel.textAlignment = .center
        firstRow.append(holeLabel)
        
        
        var i = 1
        for _ in course!.tees[0].holes {
            let number = holeLabel.duplicated()
            number.text = "\(i)"
            number.snp.makeConstraints { make in
                make.size.equalTo(40)
            }
            
            firstRow.append(number)
            
            i += 1
        }
        
        let numberStack = UIStackView(arrangedSubviews: firstRow)
        numberStack.axis = .horizontal
        
        self.holeScrollView = UIScrollView(frame: .zero)
        holeScrollView.showsVerticalScrollIndicator = false
        holeScrollView.showsHorizontalScrollIndicator = false
        holeScrollView.addSubview(numberStack)
        scrollviews.append(holeScrollView)
        holeScrollView.delegate = self
        numberStack.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        for i in 2..<numberStack.arrangedSubviews.count {
            let view = numberStack.arrangedSubviews[i]
            let before = numberStack.arrangedSubviews[i-1]
            
            addSpacer(.vertical, in: holeScrollView, between: before, and: view).snp.makeConstraints { make in
                make.top.bottom.equalToSuperview()
            }
        }
        
        firstView.addSubview(holeLabel)
        holeLabel.snp.makeConstraints { make in
            make.height.equalTo(40)
            make.width.equalTo(72)
            make.top.leading.equalToSuperview()
        }
        
        firstView.addSubview(holeScrollView)
        holeScrollView.snp.makeConstraints { make in
            make.top.trailing.equalToSuperview()
            make.height.equalToSuperview()
            make.leading.equalTo(holeLabel.snp.trailing).offset(1)
        }
        
        addSpacer(.vertical, in: firstView, between: holeLabel, and: holeScrollView).snp.makeConstraints { make in
            make.top.bottom.equalToSuperview()
        }
        
        self.addSubview(firstView)
        let sepa = UIView(frame: .zero)
        sepa.backgroundColor = #colorLiteral(red: 0.9450980392, green: 0.9450980392, blue: 0.9450980392, alpha: 1)
        firstView.addSubview(sepa)
        sepa.snp.makeConstraints { make in
            make.trailing.leading.equalToSuperview()
            make.height.equalTo(1)
            make.top.equalTo(holeLabel.snp.bottom)
        }
        
        firstView.snp.makeConstraints { make in
            make.top.trailing.leading.equalToSuperview()
            make.height.equalTo(40)
        }
    }
    
    private func makeTee(_ tee: Tee) -> UIView {
        let teeView = UIView(frame: .zero)
        let titleView = UIView(frame: .zero)
        
        let nameLabel = UILabel(frame: .zero)
        nameLabel.font = .medium(14)
        nameLabel.text = tee.name
        nameLabel.textAlignment = .left
        
        // MARK - rate
        let rateValue = nameLabel.duplicated()
        rateValue.textColor = #colorLiteral(red: 0.7675911784, green: 0.7676092982, blue: 0.7675995827, alpha: 1)
        if let rating = tee.rating {
            rateValue.text = "\(rating)"
        } else if tee.rating == nil {
            rateValue.text = "Unknown"
        } else {
            rateValue.text = "\(String(describing: tee.rating))"
        
        }
        
        rateValue.textAlignment = .right
        
        titleView.addSubview(rateValue)
        rateValue.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.trailing.equalToSuperview().inset(16)
        }
        
        let rateLabel = nameLabel.duplicated()
        rateLabel.textColor = .black
        rateLabel.text = "Rate"
        
        titleView.addSubview(rateLabel)
        rateLabel.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.trailing.equalTo(rateValue.snp.leading).offset(-4)
        }
        
        // MARK - Slope
        
        let slopeValue = nameLabel.duplicated()
        slopeValue.textColor = #colorLiteral(red: 0.7675911784, green: 0.7676092982, blue: 0.7675995827, alpha: 1)
        if let slope = tee.slope {
            slopeValue.text = "\(slope)"
        } else if tee.slope == nil {
            slopeValue.text = "Unknown"
        } else {
        
            slopeValue.text = "\(String(describing: tee.slope))"
        }
        
        slopeValue.textAlignment = .right
        
        titleView.addSubview(slopeValue)
        slopeValue.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.trailing.equalTo(rateLabel.snp.leading).offset(-20)
        }
        
        let slopeLabel = nameLabel.duplicated()
        slopeLabel.textColor = .black
        slopeLabel.text = "Slope"
        
        titleView.addSubview(slopeLabel)
        slopeLabel.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.trailing.equalTo(slopeValue.snp.leading).offset(-4)
        }
        
        // MARK - rest
        
        titleView.addSubview(nameLabel)
        nameLabel.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.leading.equalToSuperview().offset(16)
        }
        
        teeView.addSubview(titleView)
        titleView.snp.makeConstraints { make in
            make.leading.top.trailing.equalToSuperview()
            make.height.equalTo(50)
        }
        
        teeView.snp.makeConstraints { make in
            make.height.equalTo(128)
        }
        
        let sepa = UIView(frame: .zero)
        sepa.backgroundColor = #colorLiteral(red: 0.9450980392, green: 0.9450980392, blue: 0.9450980392, alpha: 1)
        teeView.addSubview(sepa)
        sepa.snp.makeConstraints { make in
            make.trailing.leading.equalToSuperview()
            make.height.equalTo(1)
            make.top.equalTo(titleView.snp.bottom)
        }
        
        
        let parLabel = slopeValue.duplicated()
        parLabel.baselineAdjustment = .alignCenters
        parLabel.textAlignment = .left
        parLabel.text = "PAR"
        
        let hcpLabel = parLabel.duplicated()
        hcpLabel.text = "hcp"
        
        
        
        
        let labelsStack = UIStackView(arrangedSubviews: [parLabel, hcpLabel])
        labelsStack.axis = .vertical
        
        parLabel.snp.makeConstraints { make in
            make.height.equalTo(40)
            
        }
        
        hcpLabel.snp.makeConstraints { make in
            make.height.equalTo(40)
            
        }
        
        teeView.addSubview(labelsStack)
        labelsStack.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(16)
            make.top.equalTo(titleView.snp.bottom).offset(1)
            make.width.equalTo(56)
        }
        labelsStack.backgroundColor = .green
        let separator2 = UIView(frame: .zero)
        separator2.backgroundColor = #colorLiteral(red: 0.9450980392, green: 0.9450980392, blue: 0.9450980392, alpha: 1)
        teeView.addSubview(separator2)
        separator2.snp.makeConstraints { make in
            make.top.bottom.equalTo(labelsStack)
            make.width.equalTo(1)
            make.leading.equalTo(labelsStack.snp.trailing)
        }
        var verticalStacks: [UIStackView] = []
        for hole in tee.holes {
            let holePar = parLabel.duplicated()
            holePar.textAlignment = .center
            holePar.textColor = #colorLiteral(red: 0, green: 0.5353117585, blue: 1, alpha: 1)
            if let par = hole.par {
                holePar.text = "\(par)"
            }
            holePar.snp.makeConstraints { make in
                make.size.equalTo(40)
            }
            
            let holehcp = holePar.duplicated()
            if let hcp = hole.strokeIndex {
                holePar.text = "\(hcp)"
            }
            
            holehcp.snp.makeConstraints { make in
                make.size.equalTo(40)
            }
            
            let vStack = UIStackView(arrangedSubviews: [holePar, holehcp])
            vStack.axis = .vertical
            vStack.spacing = 1
            verticalStacks.append(vStack)
        }
        
        let horizontalStack = UIStackView(arrangedSubviews: verticalStacks)
        horizontalStack.axis = .horizontal
        horizontalStack.spacing = 1
        
        
        let scrollv = UIScrollView(frame: .zero)
        scrollv.addSubview(horizontalStack)
        scrollviews.append(scrollv)
        scrollv.delegate = self
        horizontalStack.snp.makeConstraints { make in
            make.edges.equalToSuperview()
            make.height.equalTo(81)
        }
        scrollv.showsVerticalScrollIndicator = false
        scrollv.showsHorizontalScrollIndicator = false
        for i in 1..<horizontalStack.arrangedSubviews.count {
            let view = horizontalStack.arrangedSubviews[i]
            let before = horizontalStack.arrangedSubviews[i-1]
            
            addSpacer(.vertical, in: scrollv, between: before, and: view).snp.makeConstraints { make in
                make.top.bottom.equalToSuperview()
            }
        }
        
        
        
        teeView.addSubview(scrollv)
        scrollv.snp.makeConstraints { make in
            make.height.equalTo(81)
            make.top.equalTo(labelsStack)
            make.leading.equalTo(labelsStack.snp.trailing).offset(1)
            make.trailing.equalToSuperview()
        }
        
        let separator3 = UIView(frame: .zero)
        separator3.backgroundColor = #colorLiteral(red: 0.9450980392, green: 0.9450980392, blue: 0.9450980392, alpha: 1)
        teeView.addSubview(separator3)
        separator3.snp.makeConstraints { make in
            make.trailing.leading.equalToSuperview()
            make.height.equalTo(1)
            make.top.equalTo(scrollv).offset(40)
        }
        
        let separator = UIView(frame: .zero)
        separator.backgroundColor = #colorLiteral(red: 0.9450980392, green: 0.9450980392, blue: 0.9450980392, alpha: 1)
        teeView.addSubview(separator)
        separator.snp.makeConstraints { make in
            make.trailing.leading.equalToSuperview()
            make.height.equalTo(1)
            make.top.equalTo(scrollv.snp.bottom)
        }
        
        return teeView
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        self.scrollviews.forEach { if $0 != scrollView {
            var scrollBounds = $0.bounds
            scrollBounds.origin = scrollView.contentOffset
            $0.bounds = scrollBounds
            } }
    }
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        
    }
    
    
}

