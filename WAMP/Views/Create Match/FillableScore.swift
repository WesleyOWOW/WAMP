//import UIKit
//import SnapKit
//import RxSwift
//
//
//import Foundation
//
//extension Collection {
//    /// Does not crash when accessing an invalid index
//    subscript (safe index: Index) -> Iterator.Element? {
//        return indices.contains(index) ? self[index] : nil
//    }
//}
//
//
//class FillableScore: UIView, UIScrollViewDelegate {
//    fileprivate let disposeBag = DisposeBag()
//    fileprivate let selectedIndex = Variable<(player: Int, hole: Int)?>(nil)
//    var holeRow: UIView = UIView(frame: .zero) // The top row of the overview that houses the "HOLE" label & values. This view NEVER scrolls verically. The values do scroll horizontally, while the label always stays in place.
//     private var teamScoreButtons = [[RoundedButton]]()
//    var par_hcpRow: UIView = UIView(frame: .zero) // The row that houses both the "PAR" and "hcp" labels & values. The entire "row" scrolls vertically behind the `holeRow` (see above). The values do scroll horizontally, while the label always stays in place horizontally.
//
//    var userRows: UIView = UIView(frame: .zero) // The part of the overview that houses the names of the users and the score.
//
//    var verticalScrollView: UIScrollView = UIScrollView(frame: .zero) // The scroll view that enables horizontal scrolling in case of a 4BBB match.
//
//    var holeScrollView: UIScrollView!
//    var parScrollView: UIScrollView!
//    var hcpScrollView: UIScrollView!
//    var scoreScrollView: UIScrollView!
//
//    // The computed amount of holes for the match currently being shown
//    fileprivate var holeCount: Int {
//        // The scores are initialized in `setup()` which calls `match.initializeHoles()` so this should be safe
//        return (CreateMatchViewModel.shared.match?.course?.tees[0].holes.count)!
//    }
//    var selectedScore: Int? {
//        get {
//            guard let (player, hole) = selectedIndex.value else {
//                assertionFailure("No selected index")
//                return nil
//            }
//            return CreateMatchViewModel.shared.match?.players[player].scores![hole]
//        }
//        set {
//            guard let match = CreateMatchViewModel.shared.match as? ConceptMatch else {
//                assertionFailure("Tried to edit a match that is not a concept match")
//                return
//            }
//
//            guard let (player, hole) = selectedIndex.value else {
//                assertionFailure("No selected index")
//                return
//            }
//
//            CreateMatchViewModel.shared.match?.players[player].scores![hole] = newValue
//        }
//    }
//    var isSetup = false
//
//    var masterScrollView: UIScrollView = UIScrollView(frame: .zero)
//
//    override init(frame: CGRect) {
//        super.init(frame: frame)
//
//    }
//
//    required init?(coder aDecoder: NSCoder) {
//        super.init(coder: aDecoder)
//    }
//
//    override func layoutSubviews() {
//        super.layoutSubviews()
//        self.backgroundColor = .clear
//        if !isSetup {
//            setup()
//            isSetup = true
//        }
//
//    }
//
//     private let horizontallyScrollingStack = UIStackView(arrangedSubviews: [])
//
//    func setup() {
//        verticalScrollView.delegate = self
//
//        makeHoleRow()
//        makeParhcp()
//
//        self.addSubview(self.holeRow)
//        self.holeRow.snp.makeConstraints { make in
//            make.trailing.leading.top.equalToSuperview()
//            make.height.equalTo(40)
//        }
//
//        self.verticalScrollView.addSubview(self.par_hcpRow)
//        par_hcpRow.snp.makeConstraints { make in
//            make.trailing.leading.top.equalToSuperview()
//            make.height.equalTo(80)
//            make.width.equalToSuperview()
//        }
//
//        self.addSubview(self.verticalScrollView)
//        self.verticalScrollView.snp.makeConstraints { make in
//            make.leading.trailing.bottom.equalToSuperview()
//            make.width.equalToSuperview()
//            make.height.greaterThanOrEqualTo(80)
//            make.top.equalTo(self.holeRow.snp.bottom)
//        }
//
//        addSpacer(.horizontal, in: self, between: holeRow, and: verticalScrollView).snp.makeConstraints { make in
//            make.trailing.equalToSuperview()
//            make.leading.equalToSuperview().offset(120)
//        }
//
//        addSpacer(.horizontal, in: self, between: parScrollView, and: hcpScrollView).snp.makeConstraints { make in
//            make.trailing.equalToSuperview()
//            make.leading.equalToSuperview().offset(120)
//        }
//
//    }
//
//    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        let scrollableViews = [hcpScrollView, parScrollView, scoreScrollView]
//        scrollableViews.forEach { if $0 != scrollView {
//            var scrollBounds = $0?.bounds
//            scrollBounds?.origin = scrollView.contentOffset
//            $0?.bounds = scrollBounds!
//            } }
//    }
//
//
//    private func makeHoleRow() {
//        let separator = UIView(frame: .zero) // Starting with the separator so I can then position the title label based on that.
//        separator.backgroundColor = UIColor(hex: "000000")
//        separator.alpha = 0.05
//        holeRow.addSubview(separator)
//        separator.snp.makeConstraints { make in
//            make.width.equalTo(1)
//            make.top.bottom.equalToSuperview()
//            make.leading.equalToSuperview().offset(120)
//        }
//
//        let holeLabel = makeTitleLabel("HOLE", alignment: .left, active: true)
//        holeRow.addSubview(holeLabel)
//        holeLabel.snp.makeConstraints { make in
//            make.top.equalToSuperview().offset(10)
//            make.trailing.equalTo(separator.snp.leading).offset(-13)
//        }
//
//        holeScrollView = UIScrollView(frame: .zero)
//        holeScrollView.showsHorizontalScrollIndicator = false
//        holeScrollView.showsVerticalScrollIndicator = false
//        holeRow.addSubview(holeScrollView)
//        holeScrollView.snp.makeConstraints { make in
//            make.top.bottom.trailing.equalToSuperview()
//            make.leading.equalTo(separator.snp.trailing)
//        }
//
//        var holes : [UILabel] = []
//
//        for hole in self.holeCount {
//            let label = makeTitleLabel("\(hole + 1)", alignment: .center, active: true)
//
//            holes.append(label)
//
//            label.snp.makeConstraints { make in
//                make.width.equalTo(30)
//                make.height.equalTo(40)
//            }
//        }
//
//        let holeValueStack = UIStackView(arrangedSubviews: holes)
//        holeValueStack.axis = .horizontal
//        holeValueStack.distribution = .fillEqually
//        holeScrollView.addSubview(holeValueStack)
//        holeValueStack.snp.makeConstraints { make in
//            make.height.equalTo(40)
//            make.edges.equalToSuperview()
//        }
//
//
//
//        for i in 1..<holeValueStack.arrangedSubviews.count {
//            let view = holeValueStack.arrangedSubviews[i]
//            let before = holeValueStack.arrangedSubviews[i-1]
//
//            addSpacer(.vertical, in: holeScrollView, between: before, and: view).snp.makeConstraints { make in
//                make.top.bottom.equalToSuperview()
//            }
//        }
//    }
//
//    private func makeParhcp() {
//        let separator = UIView(frame: .zero) // Starting with the separator so I can then position the title label based on that.
//        separator.backgroundColor = UIColor(hex: "000000")
//        separator.alpha = 0.05
//        par_hcpRow.addSubview(separator)
//        separator.snp.makeConstraints { make in
//            make.width.equalTo(1)
//            make.height.equalToSuperview()
//            make.top.equalToSuperview()
//            make.leading.equalToSuperview().offset(120)
//        }
//
//        let parLabel = makeTitleLabel("PAR", alignment: .left)
//        par_hcpRow.addSubview(parLabel)
//        parLabel.snp.makeConstraints { make in
//            make.top.equalToSuperview().offset(10)
//            make.trailing.equalTo(separator.snp.leading).offset(-13)
//        }
//
//        let hcpLabel = makeTitleLabel("HCP", alignment: .left)
//        par_hcpRow.addSubview(hcpLabel)
//        hcpLabel.snp.makeConstraints { make in
//            make.top.equalTo(parLabel.snp.bottom).offset(20)
//            make.trailing.equalTo(separator.snp.leading).offset(-13)
//        }
//
//        parScrollView = UIScrollView(frame: .zero)
//        parScrollView.delegate = self
//        parScrollView.showsHorizontalScrollIndicator = false
//        parScrollView.showsVerticalScrollIndicator = false
//        par_hcpRow.addSubview(parScrollView)
//        parScrollView.snp.makeConstraints { make in
//            make.top.trailing.equalToSuperview()
//            make.height.equalTo(40)
//            make.leading.equalTo(separator.snp.trailing)
//        }
//
//
//        hcpScrollView = UIScrollView(frame: .zero)
//        hcpScrollView.delegate = self
//        hcpScrollView.showsHorizontalScrollIndicator = false
//        hcpScrollView.showsVerticalScrollIndicator = false
//        par_hcpRow.addSubview(hcpScrollView)
//        hcpScrollView.snp.makeConstraints { make in
//            make.bottom.trailing.equalToSuperview()
//            make.height.equalTo(40)
//            make.leading.equalTo(separator.snp.trailing)
//        }
//
//        var pars : [UILabel] = []
//        var hcps : [UILabel] = []
//
//        for holeIndex in 0..<self.holeCount {
//            let hole: Hole? = CreateMatchViewModel.shared.match?.players[0].tee?.holes[safe: holeIndex]
//            log.error(hole)
//
//            let parLabel = makeTitleLabel("\(hole?.par ?? 0)", alignment: .center)
//            pars.append(parLabel)
//
//            parLabel.snp.makeConstraints { make in
//                make.width.equalTo(30)
//                make.height.equalTo(40)
//            }
//
//            let hcpLabel = parLabel.duplicated()
//            hcpLabel.text = "\(hole?.strokeIndex ?? 0)"
//            hcps.append(hcpLabel)
//
//            hcpLabel.snp.makeConstraints { make in
//                make.width.equalTo(30)
//                make.height.equalTo(40)
//            }
//        }
//
//        let parValueStack = UIStackView(arrangedSubviews: pars)
//        parValueStack.axis = .horizontal
//        parValueStack.distribution = .fillEqually
//        parScrollView.addSubview(parValueStack)
//        parValueStack.snp.makeConstraints { make in
//            make.height.equalTo(40)
//            make.edges.equalToSuperview()
//        }
//
//        let hcpValueStack = UIStackView(arrangedSubviews: hcps)
//        hcpValueStack.axis = .horizontal
//        hcpValueStack.distribution = .fillEqually
//        hcpScrollView.addSubview(hcpValueStack)
//        hcpValueStack.snp.makeConstraints { make in
//            make.height.equalTo(40)
//            make.edges.equalToSuperview()
//        }
//
//        log.error(parValueStack.arrangedSubviews.count)
//
//        for i in 1..<parValueStack.arrangedSubviews.count {
//            let view = parValueStack.arrangedSubviews[i]
//            let before = parValueStack.arrangedSubviews[i-1]
//
//            addSpacer(.vertical, in: parScrollView, between: before, and: view).snp.makeConstraints { make in
//                make.top.bottom.equalToSuperview()
//            }
//        }
//
//        for i in 1..<hcpValueStack.arrangedSubviews.count {
//            let view = hcpValueStack.arrangedSubviews[i]
//            let before = hcpValueStack.arrangedSubviews[i-1]
//
//            addSpacer(.vertical, in: hcpScrollView, between: before, and: view).snp.makeConstraints { make in
//                make.top.bottom.equalToSuperview()
//            }
//        }
//    }
//
//
//
//    private func createStatistics() {
//        var playerViews: [UIView] = []
//        var scoreViews: [UIStackView] = []
//        var buttonsStack: UIStackView!
//         let handicapMarkHolesPerPlayer = CreateMatchViewModel.shared.match?.holesWithAssignedStrokePerPlayer()
//        for (index, player) in (CreateMatchViewModel.shared.match?.players.enumerated())! {
//            let playerView = UIView(frame: .zero)
//
//            let label = UILabel(frame: .zero)
//            label.textColor = .black
//            label.font = .medium(14)
//            var lastname = player.profile.lastName
//            if (player.profile.lastName.count > 9) {
//                lastname = String(player.profile.lastName.characters.prefix(9)) + "..."
//            }
//            label.text = player.profile.firstName[0] + ". " + lastname
//
//            playerView.addSubview(label)
//
//            let sep = UIView(frame: .zero)
//            sep.backgroundColor = .black
//            sep.alpha = 0.05
//            playerView.addSubview(sep)
//            sep.snp.makeConstraints { make in
//                make.top.bottom.equalToSuperview()
//                make.width.equalTo(1)
//                make.trailing.equalToSuperview().offset(1)
//            }
//
//            label.snp.makeConstraints { make in
//                make.centerY.equalToSuperview()
//                make.leading.equalToSuperview().offset(16)
//            }
//
//            playerView.snp.makeConstraints { make in
//                make.height.equalTo(55)
//                make.width.equalTo(120)
//            }
//            playerViews.append(playerView)
//
//            for holeIndex in 0..<holeCount {
//                var buttons: [UIView] = []
//
//                var teamButtons = [RoundedButton]()
//                var strokeMarkerSetsToConstrain = [(button: RoundedButton, marker: UIView)]() // keep them here because they need to have a common parent view
//
//
//                let isTeam1 = index < ((CreateMatchViewModel.shared.match?.players.count)! / 2)
//                let playerTeam = isTeam1 ? CreateMatchViewModel.shared.match?.team1 : CreateMatchViewModel.shared.match?.team2
//                let handicapMarkHoles = handicapMarkHolesPerPlayer![index]
//
//                let teamButton = RoundedButton()
//                teamButton.WAMPBorderColor =  playerTeam?.color
//                teamButton.setTitleColor(playerTeam?.color, for: .normal)
//                teamButton.setTitleColor(.white, for: .selected)
//                teamButton.titleLabel?.font = .medium(16)
//                teamButton.isUserInteractionEnabled = true
//
//                if CreateMatchViewModel.shared.match is ConceptMatch {
//                    teamButton.add(event: .touchUpInside) { [weak self] in
//                        guard let `self` = self else { return }
//
//                        self.selectedIndex.value = (player: index, hole: holeIndex)
//                    }
//
//                    // add stroke marker
////                    if let markHole = handicapMarkHoles.first(where: { $0.index == holeIndex }) {
////                        // We use the view tag to indicate the stroke index for the view
////                        for stroke in 0..<markHole.strokes {
////                            let strokeMarker = UIView()
////                            strokeMarker.backgroundColor = playerTeam?.color
////                            strokeMarker.layer.cornerRadius = strokeMarkerDiameter / 2
////                            strokeMarker.tag = stroke
////                            scrollView.addSubview(strokeMarker)
////                            strokeMarkerSetsToConstrain.append((teamButton, strokeMarker))
////                        }
////                    }
//                }
//
//                teamButtons.append(teamButton)
//
//                // Store avatar alignment targets for every first hole
////                if holeIndex == 0 {
////                    avatarAlignmentTargets.append(teamButton.snp.centerY)
////                }
//
//
//                teamScoreButtons.append(teamButtons)
//
//            // Setup team button constraints
//            for button in teamButtons {
//                button.snp.makeConstraints { make in
//                    make.width.height.equalTo(48)
//                }
//            }
//
//            // Setup vertical stack
//            let verticalStack = UIStackView(arrangedSubviews: teamButtons)
//            verticalStack.axis = .vertical
//            verticalStack.spacing = (64 - 48)
//            horizontallyScrollingStack.addArrangedSubview(verticalStack)
//
//
//            buttonsStack = UIStackView(arrangedSubviews: buttons)
//            buttonsStack.axis = .horizontal
//
//
//
//            buttonsStack.distribution = .fillEqually
//            scoreViews.append(buttonsStack)
//            }
//        }
//
//        let playerStack = UIStackView(arrangedSubviews: playerViews)
//        playerStack.axis = .vertical
//
//
//        playerStack.distribution = .fillEqually
//
//        userRows.addSubview(playerStack)
//        playerStack.backgroundColor = .blue
//        playerStack.snp.makeConstraints { make in
//            make.width.equalTo(120)
//            make.height.equalToSuperview()
//            make.top.bottom.leading.equalToSuperview()
//        }
//
//        let separator = UIView(frame: .zero)
//        separator.backgroundColor = .black
//        separator.alpha = 0.05
//        self.userRows.addSubview(separator)
//        separator.snp.makeConstraints { make in
//            make.height.equalTo(1)
//            make.leading.trailing.equalToSuperview()
//            make.top.equalToSuperview()
//        }
//
//        for i in 1..<playerStack.arrangedSubviews.count {
//            let view = playerStack.arrangedSubviews[i]
//            let before = playerStack.arrangedSubviews[i-1]
//
//            addSpacer(.horizontal, in: userRows, between: before, and: view).snp.makeConstraints { make in
//                make.trailing.leading.equalToSuperview()
//            }
//        }
//
//        let scoreStack = UIStackView(arrangedSubviews: scoreViews)
//        scoreStack.axis = .vertical
//        scoreStack.distribution = .fillEqually
//
//
//
//        scoreStack.backgroundColor = .blue
//
//        scoreScrollView = UIScrollView(frame: .zero)
//        scoreScrollView.delegate = self
//        scoreScrollView.showsHorizontalScrollIndicator = false
//        scoreScrollView.showsVerticalScrollIndicator = false
//        scoreScrollView.addSubview(scoreStack)
//        scoreStack.snp.makeConstraints { make in
//            make.edges.height.equalToSuperview()
//        }
//        userRows.addSubview(scoreScrollView)
//        scoreScrollView.clipsToBounds = true
//
//        scoreScrollView.snp.makeConstraints { make in
//            make.top.bottom.trailing.equalToSuperview()
//            make.height.equalToSuperview()
//            make.leading.equalTo(playerStack.snp.trailing).offset(1)
//        }
//
//        for i in 1..<scoreStack.arrangedSubviews.count {
//            let view = scoreStack.arrangedSubviews[i]
//            let before = scoreStack.arrangedSubviews[i-1]
//
//            addSpacer(.horizontal, in: scoreScrollView, between: before, and: view).snp.makeConstraints { make in
//                make.trailing.leading.equalToSuperview()
//            }
//        }
//
//
//
//
//
//        for i in 1..<buttonsStack.arrangedSubviews.count {
//            let view = buttonsStack.arrangedSubviews[i]
//            let before = buttonsStack.arrangedSubviews[i-1]
//
//            addSpacer(.vertical, in: scoreScrollView, between: before, and: view).snp.makeConstraints { make in
//                make.top.bottom.equalToSuperview()
//            }
//        }
//    }
//
//
//
//    private func makeTitleLabel(_ text: String, alignment: NSTextAlignment = .center, active: Bool = false) -> UILabel {
//        let label = UILabel(frame: .zero)
//        label.text = text
//        label.textColor = active ? .black : UIColor(hex: "BFBFBF")
//        label.font = .medium(14)
//        label.textAlignment = alignment
//
//        return label
//    }
//
//    private func makeScoreIndicator() {
//        let label = UILabel(frame: CGRect(x: 0, y: 0, width: 74, height: 16))
//        let indicator = UIView(frame: .zero)
//        indicator.layer.cornerRadius = 4
////        indicator.backgroundColor = self.getWinningColor()
//        self.addSubview(indicator)
////
////        indicator.snp.makeConstraints { make in
////            make.width.equalTo(74)
////            make.height.equalTo(self.getHeight())
////            make.trailing.equalToSuperview().offset(10)
////            make.top.equalToSuperview().offset(self.getTopLocation())
////        }
//
//        label.font = .bold(16)
//        label.textColor = .white
//        label.textAlignment = .center
//        label.text = CreateMatchViewModel.shared.match?.score.rendered
//        indicator.addSubview(label)
//
//        label.snp.makeConstraints { make in
//            make.centerY.equalToSuperview()
//            make.centerX.equalToSuperview().offset(-5)
//            make.width.equalToSuperview()
//            make.height.equalTo(16)
//        }
//    }
//
//
//    private func createScoreButton(with title: String, and color: UIColor) -> UIView {
//        let button = UIView(frame: .zero)
//        button.snp.makeConstraints { make in
//            make.height.width.equalTo(24)
//        }
//
//        button.layer.cornerRadius = 12
//        button.clipsToBounds = true
//        button.backgroundColor = color
//
//        let label = UILabel(frame: .zero)
//        label.font = .medium(14)
//        label.textColor = .white
//        label.text = title
//
//        button.addSubview(label)
//        label.snp.makeConstraints { make in
//            make.centerX.equalToSuperview()
//            make.centerY.equalToSuperview()
//        }
//
//        return button
//    }
//
//
//}
//
//
//class CircleScoreIndicator : UIView {
//    init() {
//        super.init(frame: .zero)
//        self.isOpaque = false
//    }
//
//    required init?(coder aDecoder: NSCoder) {
//        super.init(coder: aDecoder)
//        self.isOpaque = false
//    }
//
//    lazy var label: UILabel = {
//        let label = UILabel(frame: .zero)
//        label.textColor = .black
//        label.font = .medium(16)
//        label.textAlignment = .center
//        self.addSubview(label)
//        label.snp.makeConstraints { make in
//            make.leading.trailing.bottom.equalToSuperview()
//            make.top.equalToSuperview().offset(1)
//        }
//        return label
//    }()
//
//    var text: String {
//        get {
//            return label.text ?? ""
//        }
//        set {
//            label.text = newValue
//        }
//    }
//    var fillColor: UIColor = .white {
//        didSet {
//            self.setNeedsDisplay()
//        }
//    }
//
//    override func draw(_ rect: CGRect) {
//        let circle = UIBezierPath(ovalIn: bounds)
//        fillColor.setFill()
//        circle.fill()
//    }
//}
//
//class ChangingScoreIndicator : UIView {
//    let label = UILabel(frame: .zero)
//
//
//    var rectangle: UIView!
//
//    init() {
//        super.init(frame: CGRect(x: 0, y: 0, width: 1, height: 1))
//
//        addSubview(label)
//        label.font = .bold(16)
//        label.textColor = .white
//        label.textAlignment = .center
//        label.snp.makeConstraints { make in
//            make.centerY.equalToSuperview()
//            make.leading.equalToSuperview().offset(10)
//            make.trailing.equalToSuperview()
//        }
//
//        self.backgroundColor = .clear
//        self.isOpaque = false
//    }
//
//    var team: Team? {
//        didSet {
//            self.tintColor = team?.color
//            self.setNeedsDisplay()
//        }
//    }
//
//    var score: MatchplayScore? {
//        didSet {
//            update()
//        }
//    }
//
//    func update() {
//        label.text = score?.rendered
//    }
//
//
//    override func draw(_ rect: CGRect) {
//        rectangle = UIView(frame: CGRect(x: 10, y: 0, width: self.frame.width + 10, height: self.frame.height))
//        rectangle.layer.cornerRadius = 8
//        rectangle.backgroundColor = tintColor
//        self.addSubview(rectangle)
//        sendSubview(toBack: rectangle)
//        bringSubview(toFront: label)
//    }
//
//    required init?(coder aDecoder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }
//}
//
