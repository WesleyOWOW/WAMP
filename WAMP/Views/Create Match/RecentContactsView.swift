//
//  RecentContactsView.swift
//  WAMP
//

/// The recent contacts view that shows a horizontal bar of contacts in the PickOpponentViewController

import UIKit
import SnapKit
import SwiftyUserDefaults
import RxSwift

class RecentContactItem : UIView {
    
    let avatar = ChoosePlayerAvatar(frame: .zero)
    let label = UILabel(frame: .zero)
    
    init() {
        super.init(frame: .zero)
        
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setup()
    }
    
    private func setup() {
        addSubview(avatar)
        addSubview(label)
        
        label.numberOfLines = 1
        label.lineBreakMode = .byTruncatingTail
        label.font = .medium(10)
        label.textAlignment = .center
        
        avatar.imageView.backgroundColor = #colorLiteral(red: 0.6666069627, green: 0.6667048931, blue: 0.6665855646, alpha: 1)
        
        avatar.snp.makeConstraints { make in
            make.size.equalTo(56)
            make.top.centerX.equalToSuperview()
        }
        
        label.snp.makeConstraints { make in
            make.top.equalTo(avatar.snp.bottom).offset(6)
            make.left.right.bottom.equalToSuperview()
        }
        
        self.snp.makeConstraints { make in
            make.width.equalTo(80)
        }
    }
    
    func load(avatarURL: URL, name: String) {
        avatar.setAvatarURL(avatarURL)
        label.text = name
    }
    
    func loadGuest() {
        label.text = "Create guest"
        avatar.setPhoto(UIImage(named: "addGuestButton"))
        avatar.contentMode = .center
    }
}

class RecentContactsView : UIView {
    
    let scrollView = UIScrollView(frame: .zero)
    var stack: UIStackView!
    let disposeBag = DisposeBag()
    
    init() {
        super.init(frame: .zero)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    var onAddGuest: (() -> Void)?
    var onSelectContact: ((UserProtocol) -> Void)?
    
    // to prevent double contact loading
    var isLoading = false
    
    private func setup() {
        for sv in self.subviews {
            sv.removeFromSuperview()
        }
        
        addSubview(scrollView)
        
        // Scroll view configuration
        scrollView.showsVerticalScrollIndicator = false
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        // Load the contacts
        var views = [UIView]()
        let guest = RecentContactItem()
        guest.loadGuest()
        guest.addAction { [unowned self] in
            self.onAddGuest?()
        }
        views.append(guest)
        
        for recent in RecentContactsManager.fetch() {
            let item = RecentContactItem()
            item.load(avatarURL: recent.avatarURL, name: recent.name)
            item.addAction { [unowned self] in
                self.isLoading = true
                User.fetch(id: recent.id) { result in
                    self.isLoading = false
                    
                    switch result {
                    case .success(let user):
                        log.info("Successfully downloaded recent contact")
                        self.onSelectContact?(user)
                    case .failure(let error):
                        log.error("Could not fetch user from recent contact")
                        log.error(error)
                    }
                }
            }
            views.append(item)
        }
        
        // Stack view configuration
        stack = UIStackView(arrangedSubviews: views)
        scrollView.addSubview(stack)
        stack.axis = .horizontal
        
        stack.snp.makeConstraints { make in
            make.centerY.equalTo(self)
            make.left.right.equalToSuperview().inset(UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16))
            make.bottom.equalToSuperview()
        }
    }
}

