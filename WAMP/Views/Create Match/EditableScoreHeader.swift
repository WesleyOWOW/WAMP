import UIKit
import SnapKit
import SwiftyAnimate
fileprivate extension UIView {
    var bounceAnimation: Animate {
        return Animate()
            .then(animation: scale(duration: 0.1, x: 1.1, y: 1.0))
            .then(animation: scale(duration: 0.1, x: 1.0, y: 1.0))
    }
}

fileprivate func playerStack() -> UIStackView {
    let stack = UIStackView(arrangedSubviews: [])
    stack.axis = .vertical
    stack.spacing = 3
    return stack
}

@IBDesignable class EditableScoreHeader : UIView {
    
    var match: Match!
    
    
    private let scoreLabelLeft: UILabel = {
        let label = UILabel()
        label.font = .bold(22)
        label.textAlignment = .center
        label.textColor = .white
        return label
    }()
    
    private let scoreLabelMiddle: UILabel = {
        let label = UILabel()
        label.font = .bold(22)
        label.text = "AS"
        label.textAlignment = .center
        label.textColor = .white
        return label
    }()
    
    private let scoreLabelRight: UILabel = {
        let label = UILabel()
        label.font = .bold(22)
        label.textAlignment = .center
        label.textColor = .white
        return label
    }()
    
    
    fileprivate let leftWinnerBackground: UIView = {
        let field = UIView()
        field.clipsToBounds = true
        field.backgroundColor = .white
        return field
    }()
    
    fileprivate let squareWinnerBackground: UIView = {
        let field = UIView()
        
        field.backgroundColor = UIColor(hex: "BFBFBF")
        return field
    }()
    
    
    fileprivate let rightWinnerBackground: UIView = {
        let field = UIView()
        field.clipsToBounds = true
        field.backgroundColor = .white
        return field
    }()
    
    
    private let leftPlayers: UILabel = {
        let label = UILabel(frame: .zero)
        label.font = .medium(14)
        label.textAlignment = .center
        label.textColor = .black
        label.numberOfLines = 0
        return label
    }()
    
    private let rightPlayers: UILabel = {
        let label = UILabel(frame: .zero)
        label.font = .medium(14)
        label.textAlignment = .center
        label.textColor = .black
        label.numberOfLines = 0
        return label
    }()
    
    func setup(match: Match) {
        self.match = match
        var height = 80
        
        if (match.players.count == 4) {
            height = 106
        }
        
        self.frame.size.height = CGFloat(height)
        self.layer.cornerRadius = 8
        self.clipsToBounds = true
        [leftWinnerBackground, rightWinnerBackground].forEach(addSubview)
        [scoreLabelLeft, leftPlayers].forEach(leftWinnerBackground.addSubview)
        [scoreLabelRight, rightPlayers].forEach(rightWinnerBackground.addSubview)
        
        let separator2 = UIView(frame: .zero)
        separator2.backgroundColor = .black
        separator2.alpha = 0.05
        addSubview(separator2)
        
        separator2.snp.makeConstraints { make in
            make.width.equalTo(1)
            make.top.bottom.equalToSuperview()
            make.center.equalToSuperview()
        }
        
        addSubview(squareWinnerBackground)
        squareWinnerBackground.addSubview(scoreLabelMiddle)
        
        let tapZone = UIView(frame: .zero)
        self.addSubview(tapZone)
        self.bringSubview(toFront: tapZone)
        tapZone.snp.makeConstraints { make in
            make.edges.equalToSuperview()
            if self.match.players.count == 4 {
                make.height.greaterThanOrEqualTo(104)
            } else {
                make.height.greaterThanOrEqualTo(80)
            }
            
            make.width.greaterThanOrEqualTo(350)
        }
        //        tapZone.backgroundColor = .yellow
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleTap(gesture:)))
        tapZone.addGestureRecognizer(tapGestureRecognizer)
        
        self.clipsToBounds = true //
        self.layer.masksToBounds = true //
        
        leftWinnerBackground.snp.makeConstraints { make in
            make.width.equalTo((UIScreen.main.bounds.width - 32) / 2)
            make.top.left.bottom.equalToSuperview()
        }
        
        rightWinnerBackground.snp.makeConstraints { make in
           make.width.equalTo((UIScreen.main.bounds.width - 32) / 2)
            make.top.right.bottom.equalToSuperview()
        }
        
        squareWinnerBackground.snp.makeConstraints { make in
            make.trailing.leading.top.equalToSuperview()
            
            if self.match.players.count == 4 {
                make.height.equalTo(52)
            } else {
                make.height.greaterThanOrEqualTo(40)
            }
            
        }
        
        scoreLabelLeft.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.top.equalToSuperview().offset(10)
        }
        
        scoreLabelRight.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.top.equalToSuperview().offset(10)
            
        }
        
        scoreLabelMiddle.snp.makeConstraints { make in
            make.trailing.leading.equalToSuperview()
            make.centerY.equalToSuperview()
            make.height.equalTo(22)
            
        }
        
        let separator = UIView(frame: .zero)
        separator.backgroundColor = UIColor(hex: "000000")
        separator.alpha = 0.05
        addSubview(separator)
        separator.snp.makeConstraints { make in
            make.height.equalTo(1)
            make.trailing.leading.equalToSuperview()
            make.center.equalToSuperview()
        }
        
        
        
        
        
        for (index, player) in match.players.enumerated() {
            let profile = player.profile
            let isLeftTeam = index < match.players.count / 2
            print(index, player, isLeftTeam)
            if isLeftTeam {
                if leftPlayers.text == "" {
                    leftPlayers.text = profile.firstName[0] + ". " + profile.lastName
                } else {
                    leftPlayers.text = (leftPlayers.text ?? "") + "\n" + profile.firstName[0] + ". " + profile.lastName
                }
            } else {
                if rightPlayers.text == "" {
                    rightPlayers.text = profile.firstName[0] + ". " + profile.lastName
                } else {
                    rightPlayers.text = (rightPlayers.text ?? "") + "\n" + profile.firstName[0] + ". " + profile.lastName
                }
            }
            
            
        }
        
        if match.players.count == 4 {
            let attributedStringLeft = NSMutableAttributedString(string: leftPlayers.text!)
            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.lineSpacing = 5
            paragraphStyle.alignment = .center
            attributedStringLeft.addAttribute(NSParagraphStyleAttributeName, value:paragraphStyle, range:NSMakeRange(0, attributedStringLeft.length))
            leftPlayers.attributedText = attributedStringLeft;
            
            
            
            let attributedStringRight = NSMutableAttributedString(string: rightPlayers.text!)
            attributedStringRight.addAttribute(NSParagraphStyleAttributeName, value:paragraphStyle, range:NSMakeRange(0, attributedStringRight.length))
            rightPlayers.attributedText = attributedStringRight;
        }
        
        rightPlayers.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.bottom.equalToSuperview().offset(-10)
            make.width.equalToSuperview()
            make.height.greaterThanOrEqualTo(18)
        }
        
        
        
        leftPlayers.snp.makeConstraints { make in
            make.centerX.equalTo(leftWinnerBackground.snp.centerX)
            make.centerY.equalTo(rightPlayers)
            make.width.equalToSuperview()
            make.height.greaterThanOrEqualTo(18)
        }
        
        
        print(leftPlayers, rightPlayers)
        self.bringSubview(toFront: tapZone)
    }
    
    @objc private func handleTap(gesture: UIGestureRecognizer) {
        print("tap")
        let point = gesture.location(in: self)
        
        if leftWinnerBackground.frame.contains(point) {
            UIView.animate(withDuration: 0.1,
                           animations: {
                            self.scoreLabelLeft.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
            },
                           completion: { _ in
                            UIView.animate(withDuration: 0.1) {
                                self.scoreLabelLeft.transform = CGAffineTransform.identity
                            }
            })
            
            addScore(.left)
        } else if rightWinnerBackground.frame.contains(point) {
            UIView.animate(withDuration: 0.1,
                           animations: {
                            self.scoreLabelRight.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
            },
                           completion: { _ in
                            UIView.animate(withDuration: 0.1) {
                                self.scoreLabelRight.transform = CGAffineTransform.identity
                            }
            })
            addScore(.right)
        }
        
        //load
        
        if match.winningTeam == .blue {
            squareWinnerBackground.isHidden = true
            leftWinnerBackground.backgroundColor = UIColor(hex: "007BFF")
            leftPlayers.textColor = .white
            rightPlayers.textColor = .black
        } else if match.winningTeam == .red {
            squareWinnerBackground.isHidden = true
            rightWinnerBackground.backgroundColor = UIColor(hex: "FF3344")
            rightPlayers.textColor = .white
            leftPlayers.textColor = .black
        } else {
            squareWinnerBackground.isHidden = false
            leftWinnerBackground.backgroundColor = UIColor(hex: "FFFFFF")
            rightWinnerBackground.backgroundColor = UIColor(hex: "FFFFFF")
            rightPlayers.textColor = .black
            leftPlayers.textColor = .black
        }
    }
    
    private var score: MatchplayScore {
        get {
            return match.score
        }
        set {
            guard let match = CreateMatchViewModel.shared.match as? ConceptMatch else {
                assertionFailure()
                return
            }
            
            match.score = newValue
            match.winningTeam = self.currentTeam
            
            //
            
            if match.winningTeam == .blue {
                self.scoreLabelLeft.text = match.score.rendered
                self.scoreLabelRight.text = ""
            } else if match.winningTeam == .red {
                self.scoreLabelRight.text = match.score.rendered
                self.scoreLabelLeft.text = ""
            } else {
                self.scoreLabelLeft.text = ""
                self.scoreLabelRight.text = ""
                
            }
            
        }
    }
    
    enum Side { case left, right }
    
    var currentSide: Side = .left
    var currentTeam: Team? {
        get {
            if score.isSquare {
                return nil
            } else if currentSide == .left {
                return .blue
            } else {
                return .red
            }
        }
        set {
            if .blue == newValue {
                currentSide = .left
            } else {
                currentSide = .right
            }
        }
    }
    
    private func addScore(_ side: Side, animated: Bool = true) {
        // Update the score
        if side == currentSide {
            // Get the next score
            var nextScoreIndex = (availableScores.index(of: self.score) ?? 0) + 1
            if nextScoreIndex >= availableScores.count {
                nextScoreIndex = 0
            }
            
            let nextScore = availableScores[nextScoreIndex]
            self.score = nextScore
        } else {
            // The tapped side is different from the current side.
            // If the score is not allSquare, we should substract one from the score.
            let index = availableScores.index(of: self.score) ?? 0
            
            if index == 0 {
                currentSide = side
                self.score = availableScores[1]
            } else {
                self.score = availableScores[index-1]
            }
        }
    }
    
}

fileprivate let availableScores = [
    MatchplayScore(0,0), // AS
    MatchplayScore(1,0), // 1UP
    MatchplayScore(2,0), // 2UP
    MatchplayScore(2,1), // 2&1
    MatchplayScore(3,1), // 3&1
    MatchplayScore(3,2), // 3&2
    MatchplayScore(4,2), // 4&2
    MatchplayScore(4,3), // 4&3
    MatchplayScore(5,3), // 5&3
    MatchplayScore(5,4), // 5&4
    MatchplayScore(6,4), // 6&4
    MatchplayScore(6,5), // 6&5
    MatchplayScore(7,5), // 7&5
    MatchplayScore(7,6), // 7&6
    MatchplayScore(8,6), // 8&6
    MatchplayScore(8,7), // 8&7
    MatchplayScore(9,7), // 9&7
    MatchplayScore(9,8), // 9&8
    MatchplayScore(10,8) // 10&8
]
