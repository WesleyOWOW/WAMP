//
//  ScoreCardEditor.swift
//  WAMP
//

/// Implements the score card editor

import UIKit
import RxSwift
import RxCocoa
import SnapKit

extension Collection {
    /// Does not crash when accessing an invalid index
    subscript (safe index: Index) -> Iterator.Element? {
        return indices.contains(index) ? self[index] : nil
    }
}

protocol ScoreCardEditorDelegate : class {
    func scoreCardEditorKeyboardDidAppear()
    func scoreCardEditorKeyboardDidDisappear()
    func scoreCardEditorDidAddScore()
}

fileprivate let inactiveLabelColor = #colorLiteral(red: 0.6666069627, green: 0.6667048931, blue: 0.6665855646, alpha: 1)
fileprivate let activeLabelColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
fileprivate let strokeMarkerDiameter: CGFloat = 6

class CircleScoreIndicator : UIView {
    init() {
        super.init(frame: .zero)
        self.isOpaque = false
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.isOpaque = false
       
    }
    
    lazy var label: UILabel = {
        let label = UILabel(frame: .zero)
        label.textColor = .black
        label.font = .medium(16)
        label.textAlignment = .center
        self.addSubview(label)
        label.snp.makeConstraints { make in
            make.leading.trailing.bottom.equalToSuperview()
            make.top.equalToSuperview().offset(1)
        }
        return label
    }()
    
    var text: String {
        get {
            return label.text ?? ""
        }
        set {
            label.text = newValue
        }
    }
    var fillColor: UIColor = .white {
        didSet {
            self.setNeedsDisplay()
        }
    }
    
    override func draw(_ rect: CGRect) {
        let circle = UIBezierPath(ovalIn: bounds)
        fillColor.setFill()
        circle.fill()
    }
}

class FinalScoreIndicator : UIView {
    let label = UILabel(frame: .zero)
    
    var conceptMode = false { // when set to true, all scores are displayed as "0UP"
        didSet {
            update()
            
            label.font = conceptMode ? .bold(12) : .bold(22)
        }
    }
    
    var r: UIView?
    
    init() {
        super.init(frame: CGRect(x: 0, y: 0, width: 1, height: 1))
        
        addSubview(label)
        label.font = .bold(12)
        label.textColor = .white
        label.textAlignment = .center
        label.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.leading.equalToSuperview().offset(10)
            make.trailing.equalToSuperview()
        }
        
        self.backgroundColor = .clear
        self.isOpaque = false
        
    }
    
    var team: Team? {
        didSet {
            self.tintColor = UIColor(hex: "BFBFBF")
            self.r?.setNeedsDisplay()
        }
    }
    
    var score: MatchplayScore? {
        didSet {
            update()
        }
    }
    
    func update() {
        label.text = score?.nonfinalRendering
    }
    
    override func draw(_ rect: CGRect) {
        r = UIView(frame: CGRect(x: 10, y: 0, width: self.frame.width + 10, height: self.frame.height))
        r?.layer.cornerRadius = 8
        r?.backgroundColor = UIColor(hex: "BFBFBF")
        self.addSubview(r!)
        sendSubview(toBack: r!)
        bringSubview(toFront: label)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

@IBDesignable
class ScoreCardEditor : UIView {
    
    weak var delegate: ScoreCardEditorDelegate?
    
    @IBInspectable var buttonSize: CGFloat = 48
    @IBInspectable var totalRowHeight: CGFloat = 64
    @IBInspectable var totalRowWidth: CGFloat = 64
    @IBInspectable var readOnlyMode: Bool = false // affects the design, spacing
    
    var buttonHorizontalMarginPerSide: CGFloat { return (totalRowWidth - buttonSize) / 2 }
    var buttonVerticalMarginPerSide: CGFloat { return (totalRowHeight - buttonSize) / 2 }
    
    fileprivate let disposeBag = DisposeBag()
    
    /// The currently selected button item
    /// The items are 0-indexed, so the maximum possible value for hole is 17 which really just means hole 18
     let selectedIndex = Variable<(player: Int, hole: Int)?>(nil)
    var backgr = UIView(frame: .zero)
    /// A "shortcut" to the selected score item inside the match
    var selectedScore: Int? {
        get {
            guard let (player, hole) = selectedIndex.value else {
                selectedIndex.value = (player: 0, hole: 0)
                return nil
            }
            return CreateMatchViewModel.shared.match?.players[player].scores![hole]
        }
        set {
            guard let match = CreateMatchViewModel.shared.match as? ConceptMatch else {
                assertionFailure("Tried to edit a match that is not a concept match")
                return
            }
            
            guard let (player, hole) = selectedIndex.value else {
                assertionFailure("No selected index")
                return
            }
            
            CreateMatchViewModel.shared.match?.players[player].scores![hole] = newValue
        }
    }
    
    private let scrollView = LockableScrollView(frame: .zero)
    private let scrollViewContainer = UIView(frame: .zero)
    private let horizontallyScrollingStack = UIStackView(arrangedSubviews: [])
    
    private var holeTitle: UILabel!
    private var parTitle: UILabel?
    private var handicapTitle: UILabel?
    
    private var finalScoreIndicator = FinalScoreIndicator()
    private var finalScoreIndicatorHeightConstraint: Constraint?
    
    private let columnHeaderClippingView = UIView(frame: .zero)
    private let avatarViewClippingView = UIView(frame: .zero)
    private let separatorClippingView = UIView(frame: .zero)
    private let equalToScrollViewClippingView = UntappableView(frame: .zero)
    private let scrollViewInnerTopAnchor = UIView(frame: .zero) // anchored to the top of the scroll view, scrolling with the view, and above the content inset (so with offset 0,0 at frame.minY and frame.minX) - width/height is 1x1. does not work if it is a UILayoutGuide so that's why it is a UIView
    
    private lazy var keyboard: WAMPButtonGrid = {
        let grid = WAMPButtonGrid(buttonColor: .black)
        grid.delegate = self
        return grid
    }()
    
    /// First dimension = team (vertical), second is hole (horizontal)
    private var teamScoreButtons = [[RoundedButton]]()
    
    var match: Match!
    
    @IBInspectable var scrollsVertically: Bool {
        get {
            return scrollView.scrollsVertically
        }
        set {
            scrollView.scrollsVertically = newValue
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        if !isSetup {
            setup(hideFullCardAlignmentTarget: nil)
            isSetup = true
        }
    }
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        if !isSetup {
            setup(hideFullCardAlignmentTarget: nil)
            isSetup = true
        }
    }
    
    /// The computed amount of holes for the match currently being edited
    fileprivate var holeCount: Int {
        // The scores are initialized in `setup()` which calls `CreateMatchViewModel.shared.match?.initializeHoles()` so this should be safe
        return (CreateMatchViewModel.shared.match?.course?.tees[0].holes.count)!
    }
    
    /// The default scroll view inset
    private var scrollViewInset: UIEdgeInsets!
    
    var isSetup = false
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        
    }
    
    /// Sets up the Score Card Editor
    /// - parameter hideFullCardAlignmentTarget: Alignment target that is used to vertically pin the "Hide full card" button with an optional constraint
    func setup(hideFullCardAlignmentTarget: ConstraintRelatableTarget?) {
        
        scrollViewInset = UIEdgeInsets(top: buttonVerticalMarginPerSide + 80, left: buttonHorizontalMarginPerSide, bottom: buttonVerticalMarginPerSide, right: 56 /* 16 + 40 for the final score overlay */)
        
        // If needed, initialize the match holes, because this is the score card editor
        if let match = CreateMatchViewModel.shared.match as? ConceptMatch, CreateMatchViewModel.shared.match?.players[0].scores == nil {
            CreateMatchViewModel.shared.match?.initializeHoles()
        }
        
        // Add the scroll view anchor
        scrollView.addSubview(scrollViewInnerTopAnchor)
        
        // Create the left hand side title stack
        var titleViews: [UILabel]
        
        holeTitle = makeTitleLabel("HOLE", alignment: .left, active: true)
        if CreateMatchViewModel.shared.match?.course != nil {
            parTitle = makeTitleLabel("PAR", alignment: .left)
            handicapTitle = makeTitleLabel("HCP", alignment: .left)
            
            titleViews = [holeTitle, parTitle!, handicapTitle!]
        } else {
            titleViews = [holeTitle]
        }
        
        // Separator clipping view
        separatorClippingView.clipsToBounds = true
        addSubview(separatorClippingView) // it is important that this is behind the other views
        
        let rowTitlesStack = UIStackView(arrangedSubviews: titleViews)
        rowTitlesStack.axis = .vertical
        rowTitlesStack.spacing = readOnlyMode ? 16 : 24
        rowTitlesStack.distribution = .fillEqually
        addSubview(rowTitlesStack)
        
        // Column header clipping view
        addSubview(columnHeaderClippingView)
        columnHeaderClippingView.clipsToBounds = true
        
        // Create spacer
        let spacerView = UIView(frame: .zero)
        spacerView.backgroundColor = spacerColor
        addSubview(spacerView)
        spacerView.snp.makeConstraints { make in
            make.height.equalTo(1)
            make.left.right.equalToSuperview()
            make.top.equalTo(titleViews.last!.snp.bottom).offset(12)
        }
        
        // Configure the scroll view
        addSubview(scrollViewContainer)
        scrollViewContainer.addSubview(scrollView)
        scrollView.contentInset = scrollViewInset
        scrollView.showsVerticalScrollIndicator = false
        scrollView.showsHorizontalScrollIndicator = false
        scrollViewContainer.clipsToBounds = true
//        scrollView.contentInset = UIEdgeInsetsMake(80, 0, 0, 0)
        scrollViewContainer.snp.makeConstraints { make in
            make.trailing.equalToSuperview()
            make.top.equalTo(spacerView.snp.bottom).offset(-80)
            
            make.bottom.equalToSuperview()
            make.leading.equalTo(self).offset(120)
        }
        scrollView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        // Configure the avatar clipping view
        addSubview(avatarViewClippingView)
        avatarViewClippingView.clipsToBounds = true
        avatarViewClippingView.isUserInteractionEnabled = false
        avatarViewClippingView.snp.makeConstraints { make in
            make.top.bottom.equalTo(scrollView)
            make.leading.trailing.equalTo(self)
        }
        
        // Configure horizontally scrolling stack
        scrollView.addSubview(horizontallyScrollingStack)
        
        backgr.backgroundColor = .white
        scrollView.addSubview(backgr)
        backgr.snp.makeConstraints { make in
            make.height.equalTo(horizontallyScrollingStack)
            make.trailing.leading.top.bottom.equalToSuperview()
        }
        scrollView.sendSubview(toBack: backgr)
        scrollView.bringSubview(toFront: horizontallyScrollingStack)
        horizontallyScrollingStack.spacing = (totalRowWidth - buttonSize)
        horizontallyScrollingStack.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        
        
        
        // Equal to scroll view clipping view
        addSubview(equalToScrollViewClippingView)
        equalToScrollViewClippingView.clipsToBounds = true
        
        makeHoleEntries()
        setupRx()
        
        // Constraints setup
        scrollViewInnerTopAnchor.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(-scrollView.contentInset.top)
            make.left.equalToSuperview().offset(-scrollView.contentInset.left)
            make.size.equalTo(1)
        }
        
        rowTitlesStack.snp.makeConstraints { make in
            make.leading.equalTo(self.snp.leadingMargin).offset(14)
            make.top.equalToSuperview().offset(10)
            make.trailing.equalTo(scrollView.snp.leading).offset(-14)
        }
        
        columnHeaderClippingView.snp.makeConstraints { make in
            make.leading.equalTo(scrollView)
            make.trailing.equalToSuperview()
            make.top.bottom.equalTo(rowTitlesStack)
        }
        
        separatorClippingView.snp.makeConstraints { make in
            make.leading.trailing.equalTo(scrollView)
            make.top.bottom.equalToSuperview()
        }
        
        equalToScrollViewClippingView.snp.makeConstraints { make in
            make.edges.equalTo(scrollView)
        }
        
        equalToScrollViewClippingView.addSubview(finalScoreIndicator)
        finalScoreIndicator.conceptMode = match is ConceptMatch
        finalScoreIndicator.snp.makeConstraints { make in
            make.trailing.equalTo(self)
            finalScoreIndicatorHeightConstraint = make.top.equalTo(scrollViewInnerTopAnchor).offset(80).constraint
            make.height.equalTo(CreateMatchViewModel.shared.match?.players.count == 4 ? 64 * 2 : 64)
            make.width.equalTo(74)
        }
        
        if let parTitle = parTitle, let handicapTitle = handicapTitle {
            let spacers = [
                addSpacer(.horizontal, in: separatorClippingView, between: holeTitle, and: parTitle),
                addSpacer(.horizontal, in: separatorClippingView, between: parTitle, and: handicapTitle)
            ]
            
            for spacer in spacers {
                spacer.snp.makeConstraints { make in
                    make.leading.equalTo(horizontallyScrollingStack.snp.leading).offset(-((totalRowWidth-buttonSize) / 2))
                    make.trailing.equalToSuperview()
                }
            }
        }
        
        updateButtons()
        
    }
    
    private func setFinalScoreIndicatorPlayerIndex(_ index: Int) {
        assert(finalScoreIndicatorHeightConstraint != nil)
        
        finalScoreIndicatorHeightConstraint?.update(offset: (CGFloat(index) * totalRowHeight) + 80)
    }
    
    /// Creates a new title label with given text and color
    private func makeTitleLabel(_ text: String, alignment: NSTextAlignment = .center, active: Bool = false) -> UILabel {
        let label = UILabel(frame: .zero)
        label.text = text
        label.textColor = active ? activeLabelColor : inactiveLabelColor
        label.font =  .medium(14)
        
        label.textAlignment = alignment
        
        return label
    }
    
    /// Caled from setup to create the hole columns
    private func makeHoleEntries() {
        var avatarAlignmentTargets = [ConstraintRelatableTarget]()
        
        let handicapMarkHolesPerPlayer = CreateMatchViewModel.shared.match?.holesWithAssignedStrokePerPlayer()
        
        for holeIndex in 0..<holeCount {
            let hole: Hole? = CreateMatchViewModel.shared.match?.players[0].tee?.holes[safe: holeIndex]
            
            // Title Labels
            let labelViews: [UILabel]
            let holeLabel = makeTitleLabel("\(holeIndex+1)", active: true)
            var parLabel: UILabel? = nil
            var handicapLabel: UILabel? = nil
            if let hole = hole {
                parLabel = makeTitleLabel("\(hole.par ?? 0)")
                handicapLabel = makeTitleLabel("\(hole.strokeIndex ?? 0)")
                labelViews = [holeLabel, parLabel!, handicapLabel!]
            } else {
                labelViews = [holeLabel]
            }
            
            labelViews.forEach(columnHeaderClippingView.addSubview)
            
            var teamButtons = [RoundedButton]()
            var strokeMarkerSetsToConstrain = [(button: RoundedButton, marker: UIView)]() // keep them here because they need to have a common parent view
            
            for (playerIndex, _) in (CreateMatchViewModel.shared.match?.players.enumerated())! {
                let isTeam1 = playerIndex < ((CreateMatchViewModel.shared.match?.players.count)! / 2)
                let playerTeam = isTeam1 ? CreateMatchViewModel.shared.match?.team1 : CreateMatchViewModel.shared.match?.team2
                let handicapMarkHoles = handicapMarkHolesPerPlayer![playerIndex]
                
                let teamButton = RoundedButton()
                teamButton.WAMPBorderColor = playerTeam?.color
                teamButton.setTitleColor(playerTeam?.color, for: .normal)
                teamButton.setTitleColor(.white, for: .selected)
                teamButton.titleLabel?.font = .medium(16)
                teamButton.isUserInteractionEnabled = true
                
                
                    teamButton.addAction {
                        log.debug("action pressed")
                        self.selectedIndex.value = (player: playerIndex, hole: holeIndex)
                    }
                    
                    teamButton.add(event: .touchUpInside) { [weak self] in
                        log.debug("Event pressed")
                        self?.selectedIndex.value = (player: playerIndex, hole: holeIndex)
                    }
                    
                    // add stroke marker
                    if let markHole = handicapMarkHoles.first(where: { $0.index == holeIndex }) {
                        // We use the view tag to indicate the stroke index for the view
                        for stroke in 0..<markHole.strokes {
                            let strokeMarker = UIView()
                            strokeMarker.backgroundColor = playerTeam?.color
                            strokeMarker.layer.cornerRadius = strokeMarkerDiameter / 2
                            strokeMarker.tag = stroke
                            scrollView.addSubview(strokeMarker)
                            strokeMarkerSetsToConstrain.append((teamButton, strokeMarker))
                        }
                    }
                
                
                teamButtons.append(teamButton)
                
                // Store avatar alignment targets for every first hole
                if holeIndex == 0 {
                    avatarAlignmentTargets.append(teamButton.snp.centerY)
                }
            }
            
            teamScoreButtons.append(teamButtons)
            
            // Setup team button constraints
            for button in teamButtons {
                button.backgroundColor = .white
                button.snp.makeConstraints { make in
                    make.width.height.equalTo(buttonSize)
                }
            }
            
            // Setup vertical stack
            let verticalStack = UIStackView(arrangedSubviews: teamButtons)
            
            verticalStack.axis = .vertical
            
            verticalStack.spacing = (totalRowHeight - buttonSize)
            horizontallyScrollingStack.addArrangedSubview(verticalStack)
            
            if holeIndex == 0 {
                for i in 1..<verticalStack.arrangedSubviews.count {
                    let view = verticalStack.arrangedSubviews[i]
                    let before = verticalStack.arrangedSubviews[i-1]

                    let spacer = addSpacer(.horizontal, in: verticalStack, between: before, and: view)

                    spacer.snp.makeConstraints { make in
                        make.leading.trailing.equalToSuperview()
                    }

                    spacer.backgroundColor = UIColor(hex: "efefef")
                }
            }
            
            // Add separator
            addSpacer(.vertical, in: separatorClippingView).snp.makeConstraints { make in
                make.centerX.equalTo(verticalStack.snp.leading).offset(-(horizontallyScrollingStack.spacing/2)).priority(500)
                make.leading.greaterThanOrEqualTo(separatorClippingView.snp.leading)
                make.top.bottom.equalToSuperview()
            }
            
            // Setup marker constraints
            for (teamButton, strokeMarker) in strokeMarkerSetsToConstrain {
                // The tag of a marker indicates the stroke number it is marking
                strokeMarker.snp.makeConstraints { make in
                    make.size.equalTo(strokeMarkerDiameter)
                    make.centerY.equalTo(teamButton.snp.top)
                    
                    // Place the first stroke marker on the left side, the other on the right
                    if strokeMarker.tag == 0 {
                        make.centerX.equalTo(teamButton.snp.left)
                    } else {
                        make.centerX.equalTo(teamButton.snp.right)
                    }
                }
            }
            
            // Setup label constraints
            holeLabel.snp.makeConstraints { make in
                make.centerY.equalTo(holeTitle)
                make.centerX.equalTo(verticalStack)
            }
            
            if let parLabel = parLabel {
                parLabel.snp.makeConstraints { make in
                    make.centerY.equalTo(parTitle!)
                    make.centerX.equalTo(verticalStack)
                }
            }
            
            if let handicapLabel = handicapLabel {
                handicapLabel.snp.makeConstraints { make in
                    make.centerY.equalTo(handicapTitle!)
                    make.centerX.equalTo(verticalStack)
                }
            }
        }
        
        layoutSubviews()
        
        for (index, player) in (CreateMatchViewModel.shared.match?.players.enumerated())! {
            makeAvatarView(player: player, verticalAlignmentTarget: avatarAlignmentTargets[index])
        }
    }
    
    /// Creates the avatar view (on the left hand side of the superview) for the given player
    /// The `verticalAlignmentTarget`
    private func makeAvatarView(player: MatchPlayer, verticalAlignmentTarget: ConstraintRelatableTarget) {
        let firstNameLabel = UILabel(frame: .zero)
        firstNameLabel.font = .medium(14)
        var lastname = player.profile.lastName
        if (player.profile.lastName.count > 9) {
            lastname = String(player.profile.lastName.characters.prefix(6)) + "..."
        }
        firstNameLabel.text = player.profile.firstName[0] + ". " + lastname
        
        let lastNameLabel = UILabel(frame: .zero)
        lastNameLabel.font = .medium(14)
        lastNameLabel.text = player.profile.lastName
        
        let nameLabels = [firstNameLabel]
        for label in nameLabels {
            label.textAlignment = .left
            label.setContentHuggingPriority(230, for: .horizontal)
            label.lineBreakMode = .byClipping
            label.textColor = .black
        }
        
        let nameStack = UIStackView(arrangedSubviews: nameLabels)
        nameStack.axis = .vertical
        nameStack.spacing = 0
        
        nameStack.backgroundColor = .white
        // The layout guide represents the full area that could be used by the name stack
        let layoutGuide = UILayoutGuide()
        
        avatarViewClippingView.addLayoutGuide(layoutGuide)
        avatarViewClippingView.addSubview(nameStack)
        
        layoutGuide.snp.makeConstraints { make in
            make.leading.equalTo(self.snp.leading)
            make.centerY.equalTo(verticalAlignmentTarget)
            make.height.equalTo(totalRowHeight)
            make.trailing.greaterThanOrEqualTo(scrollView.snp.leading)
            make.trailing.equalTo(horizontallyScrollingStack.snp.leading).offset(-((totalRowWidth-buttonSize) / 2)).priority(750) // allow overscrolling to see truncated names
        }
        
        nameStack.snp.makeConstraints { make in
            make.leading.equalTo(layoutGuide).offset(16)
            make.centerY.equalTo(layoutGuide)
            make.trailing.equalTo(layoutGuide)
        }
        
        let sepa = UIView(frame: .zero)
        sepa.backgroundColor = #colorLiteral(red: 0.9450980392, green: 0.9450980392, blue: 0.9450980392, alpha: 1)
        avatarViewClippingView.addSubview(sepa)
        sepa.snp.makeConstraints { make in
            make.bottom.equalTo(nameStack)
            make.leading.equalTo(nameStack).offset(-16)
            make.width.equalTo(self.bounds.size.width)
            make.height.equalTo(1)
        }
        
//        avatarViewClippingView.backgroundColor = .white
        
        var bg = UIView(frame: .zero)
        bg.backgroundColor = .white
        avatarViewClippingView.addSubview(bg)
        bg.snp.makeConstraints { make in
            make.width.edges.equalTo(nameStack)
            make.height.equalTo(totalRowHeight)
//            make.bottom.leading.equalToSuperview()
//            make.height.equalTo(40 * (CreateMatchViewModel.shared.match?.players.count)!)
        }
       
        avatarViewClippingView.sendSubview(toBack: bg)
        
        // Stroke count indicator
        if player.strokes > 0 {
            let strokeCountIndicator = CircleScoreIndicator()
            strokeCountIndicator.fillColor = player.team.color
            strokeCountIndicator.label.textColor = .white
            strokeCountIndicator.label.font = .medium(10)
            strokeCountIndicator.text = "\(player.strokes)"
            
            avatarViewClippingView.addSubview(strokeCountIndicator)
            
            strokeCountIndicator.snp.makeConstraints { make in
                make.centerX.equalTo(layoutGuide.snp.right).offset(-16)
                make.top.equalTo(layoutGuide).offset(8)
                make.size.equalTo(18)
            }
        }
    }
    
    private func setupRx() {
        // Button selection
        Observable.zip(selectedIndex.asObservable(),
                       selectedIndex.asObservable().skip(1)) { ($0, $1) }
            .bind { [weak self] (oldValue, newValue) in
                guard let `self` = self else { return }
                
                // Hide the old button selection
                if let oldValue = oldValue {
                    let oldButton = self.teamScoreButtons[oldValue.hole][oldValue.player]
                    oldButton.WAMPBorderWidth = 1
                    oldButton.backgroundColor = .white
                }
                
                // Show the new button selection
                if let newValue = newValue {
                    let newButton = self.teamScoreButtons[newValue.hole][newValue.player]
                    newButton.WAMPBorderWidth = 1
                    //                    newButton.backgroundColor = newButton.borderColor?.withAlphaComponent(0.2)
                    
                    newButton.backgroundColor = UIColor(hex: "007BFF").withAlphaComponent(0.2)
                    
                    if CreateMatchViewModel.shared.match?.players.count == 4 {
                        if newValue.player == 2 || newValue.player == 3 {
                            newButton.backgroundColor = UIColor(hex: "FF3344").withAlphaComponent(0.2)
                            
                        }
                    } else {
                        if newValue.player == 1 {
                            newButton.backgroundColor = UIColor(hex: "FF3344").withAlphaComponent(0.2)
                            
                        }
                    }
                    //                    newButton.borderColor = .purple
                    newButton.clipsToBounds = true
                    newButton.layer.cornerRadius = self.buttonSize / 2
                    
                    
                    // Make sure this button is visible by scrolling.
                    var frameInScrollview = newButton.convert(newButton.bounds, to: self.scrollView)
//                    frameInScrollview.size.height = 0
                    frameInScrollview.origin.y = 80
                    print("autoscroll", frameInScrollview)
                    self.scrollView.scrollRectToVisible(frameInScrollview, animated: true)
                   
                    
                    
                    self.displayKeyboard()
                } else {
                    // No new value so hide the keyboard if it is visible
                    self.hideScoreEntryKeyboard()
                }
            }.addDisposableTo(disposeBag)
    }
    
    /// Displays the hole entry keyboard above everything else, like the normal keyboard does
    var keyboardIsVisible = false {
        didSet {
            guard keyboardIsVisible != oldValue else {
                return
            }
            
            if keyboardIsVisible {
                delegate?.scoreCardEditorKeyboardDidAppear()
            } else {
                delegate?.scoreCardEditorKeyboardDidDisappear()
            }
        }
    }
    
    
    lazy var keyboardContainer: UIView = {
        let view = UIView()
        
        view.backgroundColor = .clear
        view.addSubview(self.keyboard)
        self.keyboard.snp.makeConstraints { make in
            make.left.right.equalToSuperview().inset(16)
            make.top.bottom.equalToSuperview()
        }
        
        return view
    }()
    
    private var keyboardBottomConstraint: Constraint?
    
    /// Displays the score entry keyboard
    func displayKeyboard(animated: Bool = true) {
        guard !keyboardIsVisible else { return }
        
        let currentWindow = UIApplication.shared.keyWindow!
        currentWindow.addSubview(keyboardContainer)
        
        var keyboardYInitialConstraint: Constraint! = nil
        keyboardContainer.snp.makeConstraints { make in
            make.height.equalTo(271)
            make.bottom.equalToSuperview()
            make.left.equalToSuperview().offset(-16)
            make.right.equalToSuperview().offset(16)
            //            keyboardYInitialConstraint = make.top.equalTo(currentWindow.snp.bottom).constraint
        }
        
        //        currentWindow.layoutSubviews()
        UIView.animate(withDuration: animated ? 0.3 : 0) {
            //            keyboardYInitialConstraint.deactivate()
            self.keyboardContainer.snp.makeConstraints { make in
                self.keyboardBottomConstraint = make.bottom.equalTo(currentWindow).constraint
            }
            //            currentWindow.layoutSubviews()
            
            // Set this in the animation block, so constraint changes in didSet happen animated
            self.keyboardIsVisible = true
        }
    }
    
    /// True if the keyboard is in the process of being hidden. Used by `hideScoreEntryKeyboard` internally.
    private var keyboardIsHiding = false
    
    /// Hides the score entry keyboard
    func hideScoreEntryKeyboard(animated: Bool = true) {
        guard keyboardIsVisible && !keyboardIsHiding else { return }
        keyboardIsHiding = true
        
        let currentWindow = UIApplication.shared.keyWindow!
        
        UIView.animate(withDuration: animated ? 0.3 : 0, animations: {
            self.keyboardBottomConstraint?.deactivate()
            
            self.keyboardBottomConstraint = nil
            self.keyboardContainer.snp.makeConstraints { make in
                make.top.equalTo(currentWindow.snp.bottom)
            }
            
            // Reset the inset to the original value
            self.scrollView.contentInset = self.scrollViewInset
            
            // Set this in the animation block, so constraint changes in didSet happen animated
            self.keyboardIsVisible = false
            
            //            currentWindow.layoutSubviews()
        }, completion: { _ in
            self.keyboardContainer.removeFromSuperview()
            self.keyboardIsHiding = false
        })
    }
    
    /// Selects the next score (starting top left, moving to bottom right)
    func selectNextScore() {
        guard var (player, hole) = selectedIndex.value else {
            selectedIndex.value = (player: 0, hole: 0)
            return
        }
        
        if player < (CreateMatchViewModel.shared.match?.players.count)!-1 {
            player += 1
        } else if hole < holeCount-1 {
            hole += 1
            player = 0
        } else {
            selectedIndex.value = nil
            return
        }
        
        selectedIndex.value = (player, hole)
        
    }
    
    /// Selects the first player on the next hole
    func selectNextHole() {
        guard var (player, hole) = selectedIndex.value else {
            selectedIndex.value = (player: 0, hole: 0)
            return
        }
        
        if hole < holeCount - 1 {
            hole += 1
            player = 0
        } else {
            selectedIndex.value = nil
            return
        }
        
        selectedIndex.value = (player, hole)
    }
    
    /// Updates the values on the buttons according to the match values.
    func updateButtons() {
        let holeSummary = CreateMatchViewModel.shared.match?.summarizeHoles()
        
        for (playerIndex, player) in (CreateMatchViewModel.shared.match?.players.enumerated())! {
            for (holeIndex, holeScore) in player.scores!.enumerated() {
                let button = teamScoreButtons[holeIndex][playerIndex]
                
               
                
                // Update the rounded fill color of the button
                if let holeSummary = holeSummary {
                    
                    let isWinning = holeSummary[holeIndex] == player.team.holeResultValue
                    
                    let isWinner = CreateMatchViewModel.shared.match?.isHoleWinner(for: holeIndex, and: player)
                    
                    print("CURRENT WINNER: ", holeIndex, isWinner!)
                    button.isSelected = isWinner!
                    
                    // give the button the gray background for square
                    if readOnlyMode && holeSummary[holeIndex] == .square {
                        button.roundedFillColor = #colorLiteral(red: 0.8312992454, green: 0.8314195275, blue: 0.8312730193, alpha: 1)
                    }
                }
                
                let title: String
                if let score = holeScore, score > 99 {
                    title = buttonSize < 40 ? "l" : "lost"
                    button.isSelected = false
                } else if let score = holeScore, score < 0 {
                    title = buttonSize < 40 ? "w" : "won"
                    button.isSelected = true
                } else if let score = holeScore, score == 0 {
                    title = buttonSize < 40 ? "s" : "AS"
                    button.isSelected = false
                } else if let score = holeScore {
                    title = "\(score)"
                } else {
                    title = readOnlyMode ? "-" : ""
                }
                button.setTitle(title, for: .normal)
            }
            

        }
        
        if let score = CreateMatchViewModel.shared.match?.calculateScore() {
            finalScoreIndicator.isHidden = false
//            finalScoreIndicator.team = score.winningTeam
            finalScoreIndicator.score = score.score
            
            if let r = finalScoreIndicator.r {
//                print("Winning color", score.winningTeam, score.winningTeam == Team.red, score.winningTeam == Team.blue)
//
                var color : UIColor = UIColor(hex: "BFBFBF")
                if score.winningTeam == Team.red {
                    print("Winning color is red")
                    color = Team.red.color
                } else if score.winningTeam == Team.blue {
                    print("Winning color is blue")
                    color = Team.blue.color
                }
                 print("Winning color is ", color)

                r.backgroundColor = color
                r.tintColor = color
                finalScoreIndicator.tintColor = color
                
//                finalScoreIndicator.team = score.winningTeam
                
            }
            
            if score.winningTeam == .neutral {
                finalScoreIndicator.snp.makeConstraints { make in
                    make.height.equalTo(256)
                }
            }
            
            setFinalScoreIndicatorPlayerIndex(score.winningTeam == Team.red ? ((CreateMatchViewModel.shared.match?.players.count)! / 2) : 0)
        } else {
            finalScoreIndicator.isHidden = true
        }
        
    }
    
    // MARK: - Initializer Crap
    init() {
        super.init(frame: .zero)
    }

  
}

extension ScoreCardEditor : WAMPButtonGridDelegate {
    
    func buttonGrid(_ buttonGrid: WAMPButtonGrid, didSelectButton button: ButtonGridButton) {
        defer {
            updateButtons()
            delegate?.scoreCardEditorDidAddScore()
        }
        
        switch button {
        case .cancel:
            self.hideScoreEntryKeyboard()
            self.selectedIndex.value = nil
        case .number(let value):
            if selectedScore == nil {
                // Currently, there is no score value, so we should set the value of the button on this hole.
                selectedScore = value
                
                if value != 1 {
                    self.selectNextScore()
                }
            } else if let currentScore = selectedScore {
                // There currently is a score value, so we should append the value to it if it currently is a low score
                if currentScore < 10 && currentScore > 0 {
                    selectedScore = currentScore * 10 + value
                    self.selectNextScore()
                } else {
                    // Don't increase it to three number scores! 99 is bad enough
                    selectedScore = value
                }
            }
        case .clear:
            selectedScore = nil
        case .winHole, .squareHole, .loseHole:
            selectedScore = -1
            
            let selectedIndex = self.selectedIndex.value!
            let selectedPlayer = CreateMatchViewModel.shared.match?.players[selectedIndex.player]
            
            for playerIndex in 0..<(CreateMatchViewModel.shared.match?.players.count)! {
                let player = CreateMatchViewModel.shared.match?.players[playerIndex]
                print("button", button, button == .winHole, button == .loseHole)
                if button == .winHole {
                    
                    print("Color teams.", player?.team, selectedPlayer?.team)
                    if player?.team == selectedPlayer?.team {
                        CreateMatchViewModel.shared.match?.players[playerIndex].scores![selectedIndex.hole] = -1
                    } else {
                        CreateMatchViewModel.shared.match?.players[playerIndex].scores![selectedIndex.hole] = 10_000
                    }
                } else if button == .loseHole {
                    print("'loseHole", player?.team == selectedPlayer?.team)
                    if player?.team == selectedPlayer?.team {
                        CreateMatchViewModel.shared.match?.players[playerIndex].scores![selectedIndex.hole] = 10_000
                    } else {
                        CreateMatchViewModel.shared.match?.players[playerIndex].scores![selectedIndex.hole] = -1
                    }
                } else { // square hole
                    CreateMatchViewModel.shared.match?.players[playerIndex].scores![selectedIndex.hole] = 0
                }
            }
            
            
            selectNextHole()
        }
    }
    
}


