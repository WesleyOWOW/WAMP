import UIKit
import SnapKit
// Decided to write this programatically, as a norval UIView (NOT a UITableViewCell) because of reusability.

final class ReactionCountIndicator : UIView {
    
    enum Mode {
        case comment
        case flag
    }
    
    let countLabel = UILabel(frame: .zero)
    let icon = UIImageView(frame: .zero)
    
    var mode: Mode
    
    init(type: Mode) {
        self.mode = type
        super.init(frame: .zero)
        
        [countLabel, icon].forEach(self.addSubview)
        
        // --- count label setup ---
        countLabel.font = .medium(12)
        countLabel.text = "0"
        countLabel.textColor = #colorLiteral(red: 0.7489532232, green: 0.7490621805, blue: 0.7489292622, alpha: 1)
        
        // --- icon setup ---
        icon.contentMode = .center
        switch type {
        case .comment:
            icon.image = UIImage(named: "CommentIcon")
        case .flag:
            icon.image = UIImage(named: "FlagUnselected")

        }
        icon.tintColor = .black
        
        // --- constraints setup ---
        icon.snp.makeConstraints { make in
            make.left.top.bottom.equalToSuperview()
            make.size.equalTo(32)
        }
        
        countLabel.snp.makeConstraints { make in
            make.left.equalTo(icon.snp.right)
            make.centerY.equalToSuperview()
            make.right.equalToSuperview()
            make.width.greaterThanOrEqualTo(20)
        }
    }
    
    var highlighted: Bool = false {
        didSet {
            icon.tintColor = UIColor(hex: highlighted ? "000000" : "000000")
            if mode == .flag {
                icon.image = UIImage(named: highlighted ? "FlagBlue" : "FlagUnselected")
            } else if mode == .comment {
                icon.image = UIImage(named: highlighted ? "CommentIconSelectedBlue" : "CommentIcon")
            }
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

class FeedItem: UIView {
    var match: APIMatch!
    var header: ScoreView!
    var stack: UIStackView!
    var background: RoundedShadowRect = RoundedShadowRect(frame: .zero)
    let photoView = MatchPhotoView()
    let flagIndicator = ReactionCountIndicator(type: .flag)
    let commentIndicator = ReactionCountIndicator(type: .comment)
    var parentVC: WampViewController!
    var isJudge = false
    lazy var repostButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setImage(UIImage(named: "repostIcon"), for: .normal)
        button.addAction {
//            self.share()
        }
        button.snp.makeConstraints { make in
            make.size.equalTo(32)
        }
        return button
    }()
    
    lazy var shareOptionsButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setImage(UIImage(named: "ShareOptionsIcon"), for: .normal)
        button.addAction { [unowned self] in
            self.showOptionsSheet(match: self.match)
        }
        button.snp.makeConstraints { make in
            make.size.equalTo(32).priority(999)
        }
        return button
    }()
    
    func setup() {
        var items: [UIView] = []
        self.header = ScoreView(frame: .zero)
        self.header.load(match)
        items.append(self.header)
        self.header.snp.makeConstraints { make in
            if (self.match.players.count == 4) {
                make.height.equalTo(96)
            } else {
                make.height.equalTo(80)
            }
        }
        
        if let summary = match.summarizeHoles(), summary.count > 0 {
            let holeSummary = UIView(frame: .zero)
            let holePreview = MiniGameHolesSummaryView()
            holePreview.setup(values: summary)
            
           
            
           
            
            holeSummary.addSubview(holePreview)
            holePreview.snp.makeConstraints { make in
                make.center.equalToSuperview()
                make.height.equalTo(8)
                
            }
            
            holeSummary.snp.makeConstraints { make in
                make.height.equalTo(40)
            }
            
            
            
            holeSummary.addAction {
                self.parentVC.showDetails(match: self.match)
            }
            items.append(holeSummary)
        }
        
        photoView.load(match)
        items.append(photoView)
        
        self.addSubview(background)
        background.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        let texts = UIView(frame: .zero)
        let courseLabel = UILabel(frame: .zero)
        if let course = match.course {
            courseLabel.text = "\(course.name)"
            if course.name.count > 20 {
                courseLabel.text = "\(course.name.prefix(20))..."
            }
        }
        
        courseLabel.font = .bold(14)
        courseLabel.textColor = UIColor(hex: "BFBFBF")
        
        let dateLabel = courseLabel.duplicated()
        if match.date != nil {
            dateLabel.text = match.dateString
        } else {
            dateLabel.text = ""
        }
        
        texts.addSubview(courseLabel)
        courseLabel.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.left.equalToSuperview().offset(16)
        }
        
        texts.addSubview(dateLabel)
        dateLabel.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.right.equalToSuperview().offset(-16)
        }
        
        texts.snp.makeConstraints { make in
            make.height.equalTo(40)
        }
        
        items.append(texts)
        
       
        if match.text != "" {
            let comment = PaddingLabel(frame: .zero)
            comment.bottomInset = 0
            comment.topInset = 0
            comment.leftInset = 16
            comment.rightInset = 16
            comment.text = match.text
            comment.numberOfLines = 0
            comment.font = .medium(14)
            comment.textColor = UIColor(hex: "1A1A1A")
            items.append(comment)
        
        }
        
        let separatorView = UIView(frame: .zero)
        let separator = UIView(frame: .zero)
        separator.backgroundColor = UIColor.black.withAlphaComponent(0.1)
        separatorView.addSubview(separator)
        separator.snp.makeConstraints { make in
            make.height.equalTo(1)
            make.trailing.leading.equalToSuperview()
            if match.text != "" {
                make.centerY.equalToSuperview()
            } else {
                make.top.equalToSuperview()
            }
        }
        
        separatorView.snp.makeConstraints { make in
            if match.text != "" {
                make.height.equalTo(33)
            } else {
                make.height.equalTo(17)
            }
        }
        
        items.append(separatorView)
        // commentIndicator, repostButton
        
        if !isJudge {
            let shareView = UIView(frame: .zero)
            var shareBarStack: UIStackView!
            if  match.canDelete {
                 shareBarStack = UIStackView(arrangedSubviews: [flagIndicator, commentIndicator, UIView(frame: .zero), shareOptionsButton])
            } else {
                 shareBarStack = UIStackView(arrangedSubviews: [flagIndicator, commentIndicator, UIView(frame: .zero)])
            }
           
            shareBarStack.axis = .horizontal
            shareBarStack.spacing = 10
            shareView.addSubview(shareBarStack)
            
            flagIndicator.addAction { [unowned self] in
                self.likeMatch()
            }
            
            commentIndicator.addAction { [unowned self] in
                print("show comments")
                self.showComments()
            }
            
            
            commentIndicator.countLabel.text = "\(match.commentCount)"
            commentIndicator.highlighted = match.commented
            
            flagIndicator.countLabel.text = "\(match.likeCount)"
            flagIndicator.highlighted = match.liked
            
            shareBarStack.snp.makeConstraints { make in
                make.top.equalToSuperview()
                make.trailing.leading.equalToSuperview().inset(16)
                
            }
           items.append(shareView)
            
            shareView.snp.makeConstraints { make in
                make.height.equalTo(40)
            }
        }
        
        
     
        
        stack = UIStackView(arrangedSubviews: items)
        stack.axis = .vertical
        
        let clippable = UIView(frame: .zero)
        clippable.addSubview(stack)
        clippable.layer.cornerRadius = 8
        clippable.clipsToBounds = true
        stack.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        background.addSubview(clippable)
        clippable.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        photoView.snp.makeConstraints { make in
            make.width.equalToSuperview()
            make.height.equalTo(self.snp.width)
        }
        

        
    }
    
    func likeMatch() {
        guard let match = self.match else { return }
        
        if match.liked {
            self.flagIndicator.countLabel.text = "\(match.likeCount - 1)"
            self.flagIndicator.highlighted = false
        } else {
            self.flagIndicator.countLabel.text = "\(match.likeCount + 1)"
            self.flagIndicator.highlighted = true
        }
        
        match.toggleLike() { result in
            guard self.match === match else { return } // cell could be reused by now
            guard result.error == nil else { return }
            
            
        }
    }
    
    func showComments() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "comment") as! CommentViewController
        
        vc.match = self.match
//        if let p = self.parentVC.parent as? BaseViewController {
//            p.setTab(vc, showTabBar: false)
//        }
//        let controller = StoryboardScene.Main.instantiateComments()
//        controller.match = self.match
        self.parentVC.navigationController!.pushViewController(vc, animated: true)
    }
    
    func openProfileHandler(left: Bool, for match: Match) {
        var players = [match.players[0], match.players[1]]
        
        if !left {
            players = [match.players[2], match.players[3]]
        }
        
        self.parentVC.showPlayerPicker(players: players)
    }
    
    func openProfile(for id: String) {
        User.fetch(id: id) { result in
            switch result {
            case .success(let user):
                
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "profile") as! ProfileViewController
                    vc.profile = user
                    BaseViewController.shared.setTab(vc)
//                    vc.showBack = true
                    
                
            case .failure(_):
                // TODO: Errors for other cases
                self.displayError("That user is a guest, and has no profile")
            }
        }
    }
    
    func showOptionsSheet(match: APIMatch) {
        let controller = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)

        if match.canDelete {
            let deleteAction = UIAlertAction(title: "Delete", style: .destructive) { _ in
                match.delete() { result in
                    if let error = result.error {
                        self.displayError("\((error as NSError).localizedDescription)")
                    }
                }
            }
            controller.addAction(deleteAction)
        }

        let cancelAction = UIAlertAction(title: "Cancel", style: .default)
        controller.addAction(cancelAction)

        self.parentVC.present(controller, animated: true)
    }
    
    func displayError(_ text: String) {
        let controller = UIAlertController(title: "An error has occurred", message: text, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default)
        controller.addAction(action)
        
        self.parentVC.present(controller, animated: true)
    }
}


