//
//  LoadingAnimation.swift
//  WAMP
//
//  Created by Wesley Peeters on 06-03-18.
//  Copyright © 2018 WAMP - We Are Matchplay. All rights reserved.
//

import UIKit
import Lottie
import SnapKit

class LoadingAnimation: UIView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        let anim = LOTAnimationView(name: "logo anim")
        self.addSubview(anim)
        anim.loopAnimation = true
        anim.play()
        anim.snp.makeConstraints { make in
            make.center.equalToSuperview()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        let anim = LOTAnimationView(name: "logo anim")
        self.addSubview(anim)
        anim.loopAnimation = true
        anim.play()
        anim.snp.makeConstraints { make in
            make.center.equalToSuperview()
        }
    }
}
