import UIKit
import RxSwift

class WampViewController: UIViewController, UITextFieldDelegate {
    var hidesKeyboardOnTap = true
    @IBInspectable var navigationWrap: Bool = false
    var viewIsDark: Bool = false
    let disposeBag = DisposeBag()
    
    override public var prefersStatusBarHidden : Bool {
        return false
    }
    
    
    // Mark - Overrides
    override var preferredStatusBarStyle: UIStatusBarStyle {
        if viewIsDark {
            return .lightContent
        } else {
            return .default
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addSubview(effectOverlay)
        effectOverlay.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        effectOverlay.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if hidesKeyboardOnTap {
            let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
            view.addGestureRecognizer(tap)
        }
    }
    
    // Mark - Custom functions
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func makeViewDark() {
        viewIsDark = true
        UIView.animate(withDuration: 0.1) {
            self.setNeedsStatusBarAppearanceUpdate()
        }
    }
    
    func makeViewLight() {
        viewIsDark = false
        UIView.animate(withDuration: 0.1) {
            self.setNeedsStatusBarAppearanceUpdate()
        }
    }
    
    func displayError(_ text: String) {
        let controller = UIAlertController(title: "An error has occurred", message: text, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default)
        controller.addAction(action)
        
        self.present(controller, animated: true)
    }
    
    
    // MARK: - Overlays
    let effectOverlay = UIVisualEffectView(effect: nil)
    func present(overlay: UIViewController) {
        self.addChildViewController(overlay)
        self.view.addSubview(overlay.view)
        overlay.view.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        overlay.didMove(toParentViewController: self)
    }
    
    func remove(overlay: UIViewController) {
        overlay.willMove(toParentViewController: nil)
        overlay.removeFromParentViewController()
        overlay.view.removeFromSuperview()
    }
    
    func showBlurOverlay() {
        self.view.bringSubview(toFront: effectOverlay)
        effectOverlay.isHidden = false
        
        UIView.animate(withDuration: 0.2) {
            self.effectOverlay.effect = UIBlurEffect(style: .regular)
        }
    }
    
    func hideBlurOverlay() {
        UIView.animate(withDuration: 0.2, animations: {
            self.effectOverlay.effect = nil
        }) { _ in
            self.effectOverlay.isHidden = true
        }
    }
    
    // MARK: - Showing match detail overlay
    var overlayViewController: UIViewController?
    
    /// The view controller that should be used for presenting overlays
    var overlayTargetViewController: WampViewController {
        if let WampViewController = self.navigationController?.visibleViewController as? WampViewController {
            return WampViewController
        } else {
            return self
        }
    }
    
    // TODO: This should probably not be on WampViewController
    func showDetails(match: APIMatch) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let overlay = storyboard.instantiateViewController(withIdentifier: "MatchDetailOverlay") as? MatchDetailOverlayViewController
        overlay?.match = match
        
        overlayTargetViewController.showBlurOverlay()
        overlayTargetViewController.present(overlay: overlay!)
        
        overlayViewController = overlay
        overlay?.closeButton.addAction {
            self.hideDetails()
        }
        
        overlay?.backgroundTapTarget.addAction {
            self.hideDetails()
        }
    }
    
    // TODO: This should probably not be on WampViewController
    func showPlayerPicker(players: [MatchPlayer]) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "choosePlayerPopup") as? ChoosePlayerPopupViewController
        
        vc?.setUp(players: players, parent: self)
        
        overlayTargetViewController.showBlurOverlay()
        overlayTargetViewController.present(overlay: vc!)
        
        overlayViewController = vc
        
        vc!.backgroundTapTarget.addAction {
            self.hideDetails()
        }
        
        vc!.closeButton.addAction {
            self.hideDetails()
        }
    }
    
    func hideDetails() {
        overlayTargetViewController.hideBlurOverlay()
        
        guard let overlay = overlayViewController else {
            return
        }
        
        overlayTargetViewController.remove(overlay: overlay)
    }
    
    func openProfile(for id: String) {
        User.fetch(id: id) { result in
            switch result {
            case .success(let user):
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "profile") as! ProfileViewController
                vc.profile = user
                BaseViewController.shared.setTab(vc)
                //                    vc.showBack = true
                
                
            case .failure(_):
                // TODO: Errors for other cases
                self.displayError("That user is a guest, and has no profile")
            }
        }
    }

}


/// The base class for view controllers in the app. It handles generic stuff.
///
/// ## Keyboard and text fields
///
/// WAMPViewController by default hides the keyboard when tapping outside a text field.
/// It also provides tab-like support for selecting the next text field.
/// To use this feature, every text field must have a tag value of > 0.
/// When pressing return, WAMPViewController looks for the text field with `currentTag + 1`.
/// This also allows for groups of text fields: you could use a group from 1 to 4, and from 20 to 26,
/// for example.
///
/// For the keyboard-related functions to work, set the view controller as the text field delegate.
///
/// ## Other
///
/// For information on other functions of this class, refer to the individual comments below.
@IBDesignable class WAMPViewController: UIViewController, UITextFieldDelegate {
    
    /// When true, a tap gesture recognizer is added to dismiss the keyboard when tapping the view
    /// Note that this value is not dynamically changable, and must be set before `viewDidLoad`
    @IBInspectable var hidesKeyboardOnTap: Bool = true
    
    /// When true, WAMPNavigationController will wrap this view controller in a NavigationWrapperViewController
    @IBInspectable var navigationWrap: Bool = false
    
    /// Dispose bag, for use by subclasses
    let disposeBag = DisposeBag()
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if hidesKeyboardOnTap {
            // Add a tap-outside-text-field-dismisses-keyboard gesture recognizer
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(WAMPViewController.dismissKeyboard))
            tapGesture.cancelsTouchesInView = false
            self.view.addGestureRecognizer(tapGesture)
        }
        
//        self.hideNavigationBar = true
        
        // Effect overlay
        view.addSubview(effectOverlay)
        effectOverlay.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        effectOverlay.isHidden = true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        /// Handle navigation from photo manageable view controllers to photo managing view controllers
//        if let photoManager = segue.destinationRoot as? PhotoManaging, let photoManageable = self as? PhotoManageable {
//            photoManager.photoManageable = photoManageable
//        }
//
        super.prepare(for: segue, sender: sender)
    }
    
    /// Dismisses the keyboard
        func dismissKeyboard() {
            self.view.endEditing(true)
        }
    
    /// `WAMPViewController` facilitates "tabbing" to the text field with the next tab index (tag)
    /// See the doc comment at the top of the class for more info
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.tag > 0, let nextField = self.view.viewWithTag(textField.tag+1) as? UITextField {
            nextField.becomeFirstResponder()
            return false
        } else {
            textField.resignFirstResponder()
            return true
        }
    }
    
    /// Indicates that the view will appear wrapped in the given navigation wrapper view controller
    /// This is a customization point for subclasses to adjust settings on the navigation wrapper
    func viewWillAppearWrapped(_ navigationWrapper: NavigationWrapperViewController, animated: Bool) {
        // Default implementation - do nothing
    }
    
    // MARK: - Overlays
    let effectOverlay = UIVisualEffectView(effect: nil)
    func present(overlay: UIViewController) {
        //        self.addChildViewController(overlay)
        self.view.addSubview(overlay.view)
        overlay.view.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        //        overlay.didMove(toParentViewController: self)
        
    }
    
    func remove(overlay: UIViewController) {
        overlay.willMove(toParentViewController: nil)
        overlay.removeFromParentViewController()
        overlay.view.removeFromSuperview()
    }
    
    func showBlurOverlay() {
        self.view.bringSubview(toFront: effectOverlay)
        effectOverlay.isHidden = false
        if  self is ProfileViewController {
            let s = self as! ProfileViewController
            //            log.error("parent profileVC")
            //            s.feedViewController.view.isHidden = true
            //            s.tabsView.isHidden = true
            //            s.bgd.isHidden = true
            s.view.addSubview(effectOverlay)
        }
        UIView.animate(withDuration: 0.2) {
            self.effectOverlay.effect = UIBlurEffect(style: .regular)
            self.view.bringSubview(toFront: self.effectOverlay)
            self.view.bringSubview(toFront: self.effectOverlay)
        }
    }
    
    func hideBlurOverlay() {
        UIView.animate(withDuration: 0.2, animations: {
            self.effectOverlay.effect = nil
            if let perant = self.parent as? ProfileViewController {
                perant.feedViewController.view.isHidden = false
//                perant.tabBar.isHidden = false
            }
        }) { _ in
            self.effectOverlay.isHidden = true
        }
    }
    
    // MARK: - Showing match detail overlay
    var overlayViewController: UIViewController?
    
    /// The view controller that should be used for presenting overlays
    var overlayTargetViewController: WAMPViewController {
        if let WAMPController = self.navigationController?.visibleViewController as? WAMPViewController {
            
            
            return WAMPController
            
            
        } else {
            return self
        }
    }
    

    
    // TODO: This should probably not be on WAMPViewController
    func showPlayerPicker(players: [MatchPlayer]) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "choosePlayerPopup") as? ChoosePlayerPopupViewController
        
        vc?.setUp(players: players, parent: BaseViewController.shared)
        
        overlayTargetViewController.showBlurOverlay()
        overlayTargetViewController.present(overlay: vc!)
        
        overlayViewController = vc
        
        vc!.backgroundTapTarget.addAction {
            self.hideDetails()
        }
        
        vc!.closeButton.addAction {
            self.hideDetails()
        }
    }
    
    func hideDetails() {
        overlayTargetViewController.hideBlurOverlay()
        
        guard let overlay = overlayViewController else {
            return
        }
        
        overlayTargetViewController.remove(overlay: overlay)
    }
    
}
