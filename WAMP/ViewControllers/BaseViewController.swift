import UIKit
import Alamofire
/**
 * Handles the initial navigation in the app. Takes care of e.g. the tab bar and all of the child views
 * related to that.
 */

/// A protocol that contains some configuration options for tab items
/// Also used by WAMPNavigationController
protocol WAMPTabConfiguration {
    var displayTitleBar: Bool { get }
    var goBack: Bool { get }
    var tabTitle: String { get }
}

extension WAMPTabConfiguration {
    // default implementations
    var displayTitleBar: Bool { return false }
    var tabTitle: String { return "" }
    
    var goBack: Bool { return true }
}

class BaseViewController: WampViewController, NewGameDelegate {
    var didLoadTabs = false
    var lastActive: [UIViewController] = []
    static var shared: BaseViewController!
    
    /// The currently active tab
    var activeViewController: UIViewController?
    var feedViewController: FeedViewController!
    var profileViewController: ProfileViewController!
    var searchViewController: SearchViewController!
    var notificationsViewController: NotificationsViewController!
    
    override var prefersStatusBarHidden: Bool { return false }
    
    // Mark - Outlets
    @IBOutlet var profileButton: UIButton!
    @IBOutlet var searchButton: UIButton!
    @IBOutlet var feedButton: UIButton!
    @IBOutlet var gameButton: UIButton!
    @IBOutlet var notificationsButton: UIButton!
    @IBOutlet weak var contentView: UIView!
    
    @IBOutlet weak var tabBarBack: UIView!
    @IBOutlet weak var tabBar: UIView!
    // MARK - Overrides
    
    override func viewDidLoad() {
        super.viewDidLoad()
         NotificationCenter.default.addObserver(self, selector: #selector(self.checkNotifications), name: Notification.Name("refreshNotifications"), object: nil)
        self.hidesKeyboardOnTap = false
        BaseViewController.shared = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // If there is one or more notification, make the notifications flag red.
        notificationsButton.isSelected = UIApplication.shared.applicationIconBadgeNumber > 0
        
        // Update the state of the create game button, to indicate if we have a saved match
//        gameButton.isSelected = ConceptMatch.hasSavedMatch
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if !AuthenticationManager.shared.isLoggedin {
            self.performSegue(withIdentifier: "ShowAuthentication", sender: nil)
        } else if !didLoadTabs {
            User.me.updateLocal { result in
                switch result {
                case .failure(let error):
                    log.error("Error updating local user: \(error)")
                    if case Alamofire.AFError.responseValidationFailed(let reason) = error,
                        case Alamofire.AFError.ResponseValidationFailureReason.unacceptableStatusCode(code: 401) = reason {
                        log.info("Representing login")
                        self.performSegue(withIdentifier: "ShowAuthentication", sender: nil)
                    } else {
                        self.loadTabs()
                    }
                case .success:
                    return self.loadTabs()
                }
            }
        }
    }
    
    
    // MARK - Custom methods
    
    func create() {
        print("CREATE")
        self.performSegue(withIdentifier: "createMatch", sender: nil)
    }
    
    
    
    func loadTabs() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        feedViewController = storyboard.instantiateViewController(withIdentifier: "feed") as! FeedViewController
        profileViewController = storyboard.instantiateViewController(withIdentifier: "profile") as! ProfileViewController
        searchViewController = storyboard.instantiateViewController(withIdentifier: "search") as! SearchViewController
        
         notificationsViewController = storyboard.instantiateViewController(withIdentifier: "notifications") as! NotificationsViewController
        
//        profileViewController = StoryboardScene.Main.instantiateProfileView()
//
//        feedViewController.allowPlaceholder = true
//        feedViewController.master = self
//        searchViewController.setup(searchField: searchField)
        searchViewController.onSelect = { user in
             let vc = storyboard.instantiateViewController(withIdentifier: "profile") as! ProfileViewController
            vc.profile = user
            self.setTab(vc)
        }
//
        setTab(feedViewController)
        didLoadTabs = true
    }
    

    @IBAction func CreateMatch(_ sender: Any) {
        self.performSegue(withIdentifier: "newGameOverlay", sender: nil)
    }
    
    /// Updates the states of the tab buttons according to the current view controller
    func updateButtonStates() {
        feedButton.isSelected = activeViewController is FeedViewController
        notificationsButton.isSelected = activeViewController is NotificationsViewController
        searchButton.isSelected = activeViewController is SearchViewController
        
        if activeViewController is ProfileViewController && activeViewController == profileViewController {
            profileButton.isSelected = true
        } else {
            profileButton.isSelected = false
        }
        
        
//        profileButton.isSelected = activeViewController == profileViewController
        
//
        
        
        if UIApplication.shared.applicationIconBadgeNumber == 0 {
            notificationsButton.imageView?.tintColor = .black
            notificationsButton.setImage(UIImage(named: "NotificationsTabIcon"), for: .normal)
        } else {
            notificationsButton.setImage(UIImage(named: "FlagBlue"), for: .normal)
            notificationsButton.imageView?.tintColor = UIColor(hex: "FF3344")
        }
        
    }
    
    @objc func checkNotifications() {
        if UIApplication.shared.applicationIconBadgeNumber == 0 {
            notificationsButton.imageView?.tintColor = .black
            notificationsButton.setImage(UIImage(named: "NotificationsTabIcon"), for: .normal)
        } else {
            notificationsButton.setImage(UIImage(named: "FlagBlue"), for: .normal)
            notificationsButton.imageView?.tintColor = UIColor(hex: "FF3344")
        }
    }
    
    func setTab(_ vc: UIViewController, showTabBar: Bool = true) {
        
        if let activeVc = activeViewController {
            // Remove the currently active view controller
            if lastActive.last != activeVc {
                lastActive.append(activeVc)
            }
           
            activeVc.view.removeFromSuperview()
            activeVc.didMove(toParentViewController: nil)
        }
        
        addChildViewController(vc)
        contentView.addSubview(vc.view)
        vc.view.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        vc.view.layoutMargins = contentView.layoutMargins
        vc.didMove(toParentViewController: self)
        
        activeViewController = vc
        
        self.tabBar.isVisible = showTabBar
        self.tabBarBack.isVisible = showTabBar
        
        updateButtonStates()
        setNeedsStatusBarAppearanceUpdate()
        
    }
    
    func goBack() {
        if !(lastActive.count-1 < 0) && !lastActive.isEmpty {
            let last = lastActive.last
            lastActive.remove(at: lastActive.count-1)
            setTab(last!)
        }
    }

    @IBAction func showProfile(_ sender: Any) {
        if profileViewController != nil {
            User.me.updateLocal(callback: {_ in})
            profileViewController.profile = User.me
            if profileViewController.feedViewController != nil {
                 profileViewController.feedViewController.table.contentOffset.y = -296
            }
            setTab(profileViewController, showTabBar: true)
        }
    }
    
    @IBAction func showFeed(_ sender: Any) {
        if feedViewController != nil && (feedViewController.matches.count > 0 || activeViewController != feedViewController ){
            feedViewController.table.scrollToTop(animated: true)
            setTab(feedViewController, showTabBar: true)
        }
//        feedViewController.table.scrollRectToVisible(CGRect(x: 0, y: 0, width: 1, height: 1), animated: true)
       
    }
    
    @IBAction func showSearch(_ sender: Any) {
        if searchViewController !=  nil {
            setTab(searchViewController, showTabBar: true)
        }
        
    }
    
    @IBAction func showNotifications(_ sender: Any) {
        if notificationsViewController != nil {
            setTab(notificationsViewController, showTabBar: true)
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        if let dest = segue.destinationRoot as? NewGameOverlayViewController {
            dest.transitioningDelegate = self
            dest.modalPresentationStyle = .custom
            dest.delegate = self
        }
    }
    
    let circularTransition = CircularTransition()
}

extension BaseViewController: UIViewControllerTransitioningDelegate {
    
    var circleStartingPoint: CGPoint {
        return self.view.convert(gameButton.center, from: gameButton.superview!)
    }
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        circularTransition.transitionMode = .present
        circularTransition.startingPoint = circleStartingPoint
        circularTransition.circleColor = #colorLiteral(red: 0, green: 0.4823529412, blue: 1, alpha: 1)
        
        return circularTransition
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        circularTransition.transitionMode = .dismiss
        circularTransition.startingPoint = circleStartingPoint
        circularTransition.circleColor = #colorLiteral(red: 0, green: 0.4823529412, blue: 1, alpha: 1)
        
        return circularTransition
    }
    
}
