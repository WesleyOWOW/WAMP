import UIKit
import SnapKit
import RxSwift
import RxKeyboard

class SearchViewController: WampViewController {
    weak var searchField: UITextField!
    let viewModel = SearchViewModel()
    var cancelButtonConstraint: Constraint!
    var tableView: UITableView = UITableView(frame: .zero)
    var onSelect: ((User) -> ()) = { _ in }
    var results = [Player]() {
        didSet {
            tableView.reloadData()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.hidesKeyboardOnTap = false
        super.viewWillAppear(animated)
        rebindSearchResults()
        tableView.scrollRectToVisible(CGRect(x: 0, y: 0, width: 1, height: 1), animated: true)
    
        RxKeyboard.instance.visibleHeight.drive(onNext: { keyboardVisibleHeight in
                self.tableView.contentInset.bottom = keyboardVisibleHeight
                if keyboardVisibleHeight < 3 {
                     self.tableView.contentInset.bottom = 48
                }
            }).disposed(by: disposeBag)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.addSubview(tableView)
        tableView.snp.makeConstraints { make in
            make.trailing.leading.bottom.equalToSuperview()
            make.top.equalToSuperview().offset(25)
        }
        
        let header = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 60))
        let text = UILabel()
        text.font = .bold(32)
        text.textColor = .black
        text.numberOfLines = 0
        header.addSubview(text)
        text.text = "Search players"
        
        tableView.tableHeaderView = header
        header.backgroundColor = UIColor(hex: "FCFCFC")
        text.snp.makeConstraints { make in
            make.right.equalToSuperview()
            make.left.equalToSuperview().offset(16)
            make.top.equalToSuperview().offset(28)
            
        }
        
        tableView.contentInset = UIEdgeInsets(top: -20, left: 0, bottom: 48, right: 0)
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 68
        tableView.separatorStyle = .none
        tableView.allowsSelection = true
        
        
        tableView.register(SearchResultCell.self, forCellReuseIdentifier: SearchResultCell.identifier)
        
        tableView.delegate = self
        tableView.dataSource = self
    }

    var searchResultsDisposeBag = DisposeBag()
    func rebindSearchResults() {
        searchResultsDisposeBag = DisposeBag()
        viewModel.searchResults.bind { [weak self] results in
            
            self?.results = results
            }.disposed(by: searchResultsDisposeBag)
    }
}

extension SearchViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        var follow: UITableViewRowAction!
        
        if !results[indexPath.row].following {
            follow = UITableViewRowAction(style: .normal, title: "Follow") { (action, indexPath) in
                self.results[indexPath.row].toggleFollow(callback: { _ in })
                self.tableView.setEditing(false, animated: true)
                
            }
            
            follow.backgroundColor = UIColor(hex: "007BFF")
            
        } else {
            follow = UITableViewRowAction(style: .normal, title: "Unfollow") { (action, indexPath) in
                self.results[indexPath.row].toggleFollow(callback: {_ in})
                self.tableView.setEditing(false, animated: true)
            }
            
            follow.backgroundColor = UIColor(hex: "FF3344")
        }
        
        return [follow]
    }
    

    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let searchView = UIView()
        
        let searchBack = UIView(frame: .zero)
        searchBack.backgroundColor = UIColor(hex: "F2F2F2")
        searchBack.layer.cornerRadius = 5
        searchBack.clipsToBounds = true
        searchView.backgroundColor = UIColor(hex: "FCFCFC")
        searchView.addSubview(searchBack)
        searchBack.snp.makeConstraints { make in
            make.height.equalTo(32)
            make.trailing.leading.bottom.equalToSuperview().inset(16)
            
        }
        
        let searchImage = UIImageView(image: UIImage(named: "searchIcon"))
        searchBack.addSubview(searchImage)
        searchImage.snp.makeConstraints { make in
            make.size.equalTo(15)
            make.centerY.equalToSuperview()
            make.leading.equalToSuperview().offset(16)
        }
        
        
        searchField = UITextField(frame: .zero)
        searchField.placeholder = "Search for players"
        searchField.font = .medium(16)
        
        searchBack.addSubview(searchField)
        searchField.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.leading.equalTo(searchImage.snp.trailing).offset(12)
        }
        
        let sep = UIView(frame: .zero)
        sep.backgroundColor = .black
        sep.alpha = 0.05
        
        searchView.addSubview(sep)
        sep.snp.makeConstraints { make in
            make.leading.trailing.bottom.equalToSuperview()
            make.height.equalTo(1)
        }
        
        searchField.rx.text
            .orEmpty
            .bind(to: viewModel.searchText)
            .disposed(by: disposeBag)
        
        return searchView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 84
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return results.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let result = results[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: SearchResultCell.identifier, for: indexPath) as! SearchResultCell
        cell.configure(result)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let result = results[indexPath.row]
        print("selected row")
//        if let indexPath = self.tableView.indexPathForSelectedRow {
//            self.tableView.deselectRow(at: indexPath, animated: false)
//        }
        
        self.select(user: result)
    }
    
    /// Displays the profile page for the given search result
    func select(user: Player) {
        // fetch the user
        User.fetch(id: user.id) { [weak self] result in
            if let user = result.value {
                self?.onSelect(user)
            }
        }
    }
    
    
}


class SearchResultCell : UITableViewCell {
    
    static let identifier = "searchresultcell"
    
    let avatar: WAMPAvatarSmall = {
        let avatar = WAMPAvatarSmall()
        
        return avatar
    }()
    
    let verifiedIcon: UIImageView = UIImageView(image: UIImage(named: "verified user"))
    
    let usernameLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.font = .medium(14)
        return label
    }()
    let realNameLabel: UILabel = {
        let label = UILabel()
        label.textColor = #colorLiteral(red: 0.6666070223, green: 0.6667048931, blue: 0.6665856242, alpha: 1)
        label.font = .medium(14)
        return label
    }()
    let handicapLabel: UILabel = {
        let label = UILabel()
        label.textColor = #colorLiteral(red: 0, green: 0.4801899791, blue: 1, alpha: 1)
        label.font = .medium(12)
        return label
    }()
    let bottomSpacer: UIView = {
        let view = UIView()
        view.backgroundColor = #colorLiteral(red: 0.8665904999, green: 0.8667154908, blue: 0.8665630221, alpha: 1)
        return view
    }()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        contentView.backgroundColor = UIColor(hex: "FCFCFC")
        addSubview(verifiedIcon)
        let nameStack = UIStackView(arrangedSubviews: [usernameLabel, realNameLabel])
        let handicapContainer = UIView()
        handicapContainer.addSubview(handicapLabel)
        let horizontalStack = UIStackView(arrangedSubviews: [avatar, nameStack, handicapContainer])
        [horizontalStack, bottomSpacer].forEach(self.addSubview)
        
        nameStack.axis = .vertical
        nameStack.alignment = .leading
        nameStack.setContentHuggingPriority(260, for: .horizontal)
        
        horizontalStack.axis = .horizontal
        horizontalStack.spacing = 12
        horizontalStack.alignment = .center
        
        // Constraints
        horizontalStack.snp.makeConstraints { make in
            make.edges.equalToSuperview()
                .inset(UIEdgeInsets(top: 10, left: 16, bottom: 10, right: 16))
        }
        
        avatar.snp.makeConstraints { make in
            make.size.equalTo(48).priority(999)
        }
        
        handicapLabel.snp.makeConstraints { make in
            make.firstBaseline.equalTo(usernameLabel.snp.firstBaseline)
            make.left.top.right.equalToSuperview()
            make.bottom.lessThanOrEqualToSuperview()
        }
        
        bottomSpacer.snp.makeConstraints { make in
            make.height.equalTo(0.5)
            make.leading.trailing.bottom.equalToSuperview()
        }
        
        verifiedIcon.snp.makeConstraints { make in
            make.height.width.equalTo(16)
            make.top.equalTo(usernameLabel.snp.top).offset(-3)
            make.left.equalTo(usernameLabel.snp.right).offset(6)
        }
        
        // Highlight color
        let view = UIView()
        view.backgroundColor = UIColor(hex: "D9D9D9")
        selectedBackgroundView = view
        
    }
    
    func configure(_ user: Player) {
        usernameLabel.text = user.username
        handicapLabel.text = user.renderHandicap(includeSuffix: true)
        
        
        self.verifiedIcon.isHidden = !user.verified
        
        
        var realNameText = user.firstName + " " + user.lastName
        if user.id == User.me.id {
            realNameText += " • That's you"
        } else if user.isFollowing {
            realNameText += " • Following"
        }
        
        realNameLabel.text = realNameText
        
        
        avatar.setProfile(user)
    }
    
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
