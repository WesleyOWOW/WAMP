import UIKit
import Alamofire
import SwiftyJSON
import PullToRefresh
import AlamofireImage
import SwiftRichString

let commentStyle = Style("comment", {
    $0.font = FontAttribute("Gordita-Medium", size: 14) // font + size
    $0.color = .black // text color
})

let timeStyle = Style("time", {
    $0.font = FontAttribute("Gordita-Medium", size: 12) // font + size
    $0.color = UIColor(hex: "BFBFBF") // text color
})

let usernameStyle = Style("username", {
    $0.font = FontAttribute("Gordita-Bold", size: 14) // font + size
    $0.color = .black // text color
})


class NotificationsViewController: WampViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var tableView: UITableView!
    var header: UIView!
    @IBOutlet weak var smallHeader: UIView!
    @IBOutlet weak var placeholderView: UILabel!
    enum Section : Int {
        case matchesToJudge = 0
        case notifications = 1
        
        init(_ section: Int) {
            guard let value = Section(rawValue: section) else {
                fatalError("Invalid table view section \(section)")
            }
            
            self = value
        }
    }
    
    var notifications = [JSON]() {
        didSet {
            tableView.reloadData()
            updatePlaceholderHiddenStatus()
        }
    }
    
    var matches = [APIMatch]() {
        didSet {
            log.info("Got \(matches.count) matches")
            tableView.reloadData()
            updatePlaceholderHiddenStatus()
            
            UIApplication.shared.applicationIconBadgeNumber = matches.count
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if scrollView.contentOffset.y > 48 {
            UIView.animate(withDuration: 0.1) {
                self.smallHeader.alpha = 1
            }
        } else {
            UIView.animate(withDuration: 0.1) {
                self.smallHeader.alpha = 0
            }
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadData()
        self.hidesKeyboardOnTap = false
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 60
        tableView.dataSource = self
        tableView.allowsMultipleSelection = true
        tableView.delegate = self
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 48, right: 0)
        header = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 64))
        let text = UILabel()
        text.font = .bold(32)
        text.textColor = .black
        text.numberOfLines = 0
        header.backgroundColor = UIColor(hex: "FCFCFC")
        header.addSubview(text)
        text.text = "Notifications"
        
        tableView.tableHeaderView = header
        
        text.snp.makeConstraints { make in
            make.right.equalToSuperview()
            make.left.equalToSuperview().offset(16)
            make.top.equalToSuperview()
            make.bottom.equalToSuperview().offset(16)
        }
        
        let refresher = PullToRefresh()
        tableView.addPullToRefresh(refresher) {
            self.loadData()
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.refresh), name: Notification.Name("refreshNotifications"), object: nil)
    }
    
    @objc func refresh() {
        self.notifications = []
        self.matches = []
        tableView.reloadData()
        loadData()
        tableView.reloadData()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.notifications = []
        self.loadData()
        tableView.reloadData()
    }
    
    
    func updatePlaceholderHiddenStatus() {
        self.placeholderView.isHidden = (!didFinishLoading) || self.notifications.count > 0 || self.matches.count > 0
    }
    
    deinit {
        tableView.removeAllPullToRefresh()
    }
    
    var didFinishLoading = false
    func loadData() {
        Alamofire.request(api_url + "/users/me/notifications/all",
                          method: .get,
                          headers: AuthenticationManager.shared.headers)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .failure(let error):
                    self.displayError("Error loading notifications: \((error as NSError).localizedDescription)")
                case .success(let notifications):
                    let json = JSON(notifications)
                    self.notifications = json.arrayValue
                    self.didFinishLoading = true
                }
        }
        
        Alamofire.request(api_url + "/users/me/verifications",
                          method: .get,
                          headers: AuthenticationManager.shared.headers)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .failure(let error):
                    self.displayError("Could not fetch unverified matches: \((error as NSError).localizedDescription)")
                case .success(let matchesToVerify):
                    let json = JSON(matchesToVerify)
                    self.matches = []
                    for (_, match) in json {
                        
                        APIMatch.load(json: match) { result in
                            switch result {
                            case .failure(let error):
                                self.displayError("Could not load unverified match: \((error as NSError).localizedDescription)")
                            case .success(let match):
                                self.matches.append(match)
                            }
                        }
                    }
                }
        }
        
        self.tableView.endAllRefreshing()
    }
    
    func presentJudgeMatchScreen(with match: APIMatch) {
//        let vc = StoryboardScene.Main.instantiateJudgeMatch()
//        vc.match = match
//        print(match)
//        self.navigationController!.pushViewController(vc, animated: true)
    }
    
    func followBack(notification: JSON) {
        let userId = notification["data"]["user_id"]
        let player = Player(json: notification["created_by"])
        
        let url = api_url + "/users/" + userId.stringValue + ((player?.isFollowing)! ? "/unfollow" : "/follow")
        
        log.verbose("toggling follow using url: \(url)")
        
        Alamofire.request(url,
                          method: .get,
                          headers: AuthenticationManager.shared.headers)
            .validate()
            .responseJSON { response in
                log.verbose("follow toggle status code: \(response.response!.statusCode)")
                switch response.result {
                case .failure(let error):
                    log.warning("Error while following:", error.localizedDescription)
                case .success(_):
                    player?.isFollowing = !(player?.isFollowing)!
                }
        }
    }
    
    // MARK: - Table View Data Source
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch Section(section) {
        case .matchesToJudge: return matches.count
        case .notifications: return notifications.count
        }
    } 
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationCell", for: indexPath) as! NotificationTableViewCell
        cell.selectionStyle = .none
        cell.contentView.backgroundColor = UIColor(hex: "D9D9D9")
        switch Section(indexPath.section) {
        case .matchesToJudge:
            let match = matches[indexPath.row]
            cell.load(.matchToJudge(match))
            cell.contentButton.removeActions(for: .touchUpInside)
            cell.contentButton.addAction(events: [.touchUpInside]) { [unowned self] in
                cell.contentView.backgroundColor = UIColor(hex: "D9D9D9")
                
                let vc = JudgeMatchViewController(match: match)
                  BaseViewController.shared.setTab(vc, showTabBar: false)
            }
            cell.addAction { [unowned self] in
                cell.contentView.backgroundColor = UIColor(hex: "D9D9D9")
                
                let vc = JudgeMatchViewController(match: match)
                BaseViewController.shared.setTab(vc, showTabBar: false)
            }
        case .notifications:
            cell.load(.notification(notifications[indexPath.row]))
            cell.contentButton.removeActions(for: .touchUpOutside)
            cell.contentButton.addAction(events: [.touchUpInside]) { [unowned self] in
                self.followBack(notification: self.notifications[indexPath.row])
            }
            
            
            if (String(describing: notifications[indexPath.row]["type"]).contains("NewLikeOnPost") || String(describing: notifications[indexPath.row]["type"]).contains("NewCommentOnPost") || String(describing: notifications[indexPath.row]["type"]).contains("NewMatchPlayed")) {
                cell.addAction {
                    cell.contentView.backgroundColor = UIColor(hex: "D9D9D9")
                    self.showLoading(text: "Loading match...")
                    Alamofire.request(api_url + "/matches/\(self.notifications[indexPath.row]["data"]["match_id"])", method: .get, encoding: JSONEncoding.default, headers: AuthenticationManager.shared.headers).validate().responseJSON {  response in
                        switch response.result {
                        case .failure(let error):
                            log.error("Error getting match: \(error)")
                        case .success(let data):
                            let json = JSON(data)
                            
                            APIMatch.load(json: json) { result in
                                switch result {
                                case .failure(let error):
                                    
                                    log.error("Error parsing match: \(error)")
                                case .success(let match):
                                    self.dismiss(animated: true, completion: nil)
                                    let vc = SingleMatchViewController(match: match)
                                    BaseViewController.shared.setTab(vc, showTabBar: false)
                                    
                                }
                            }
                        }
                    }
                }
            }
        }
        
        return cell
    }
    
    func showLoading(text: String) {
        let alert = UIAlertController(title: nil, message: text, preferredStyle: .alert)
        
        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        loadingIndicator.startAnimating();
        
        alert.view.addSubview(loadingIndicator)
        present(alert, animated: true, completion: nil)
    }
    
    // MARK: - Table View Delegate
    var isBusy = false
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
//        print("didSelect", indexPath.section == Section.matchesToJudge.rawValue,  indexPath.section == Section.notifications.rawValue && (String(describing: notifications[indexPath.row]["type"]).contains("NewLikeOnPost") || String(describing: notifications[indexPath.row]["type"]).contains("NewCommentOnPost") || String(describing: notifications[indexPath.row]["type"]).contains("NewMatchPlayed")))
//        tableView.deselectRow(at: indexPath, animated: true)
        
        guard !isBusy else { return }
        isBusy = true
        
        if indexPath.section == Section.notifications.rawValue && (String(describing: notifications[indexPath.row]["type"]).contains("NewLikeOnPost") || String(describing: notifications[indexPath.row]["type"]).contains("NewCommentOnPost") || String(describing: notifications[indexPath.row]["type"]).contains("NewMatchPlayed")) {
            showLoading(text: "Loading match...")
            Alamofire.request(api_url + "/matches/\(self.notifications[indexPath.row]["data"]["match_id"])", method: .get, encoding: JSONEncoding.default, headers: AuthenticationManager.shared.headers).validate().responseJSON {  response in
                switch response.result {
                case .failure(let error):
                    log.error("Error getting match: \(error)")
                case .success(let data):
                    let json = JSON(data)
                    
                    APIMatch.load(json: json) { result in
                        switch result {
                        case .failure(let error):
                            log.error("Error parsing match: \(error)")
                        case .success(let match):
                            self.dismiss(animated: true, completion: nil)
                            let vc = SingleMatchViewController(match: match)
                            BaseViewController.shared.setTab(vc, showTabBar: false)
                            
                        }
                    }
                }
            }
        } else if indexPath.section == Section.matchesToJudge.rawValue {
             showLoading(text: "Loading match...")
            Alamofire.request(api_url + "/matches/\(notifications[indexPath.row]["data"]["match_id"])", method: .get, encoding: JSONEncoding.default, headers: AuthenticationManager.shared.headers).validate().responseJSON {  response in
                switch response.result {
                case .failure(let error):
                    log.error("Error getting match: \(error)")
                case .success(let data):
                    let json = JSON(data)
                    
                    APIMatch.load(json: json) { result in
                        switch result {
                        case .failure(let error):
                            log.error("Error parsing match: \(error)")
                        case .success(let match):
                             self.dismiss(animated: true, completion: nil)
                            let vc = JudgeMatchViewController(match: match)
                              BaseViewController.shared.setTab(vc, showTabBar: false)
                        
                        }
                    }
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
//        if indexPath.section == Section.notifications.rawValue {
//            let read = UITableViewRowAction(style: .normal, title: "Mark as read") { (action, indexPath) in
//                let cell = tableView.cellForRow(at: indexPath) as? NotificationTableViewCell
//                self.notifications.remove(at: indexPath.row)
////                self.tableView.deleteRows(at: [indexPath], with: .automatic)
//                Alamofire.request(api_url + "/users/me/notifications/" + (cell?.id)!, method: .put, encoding: JSONEncoding.default, headers: AuthenticationManager.shared.headers)
//            }
//
//            read.backgroundColor = UIColor(hex: "007BFF")
//
//            return [read]
//        }
       
        return []
    }
    
    
//    func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [UITableViewRowAction]?
//    {
////
////        let delete = UITableViewRowAction(style: .normal, title: "Mark as read"){(UITableViewRowAction,NSIndexPath) -> Void in
////
////            print("What u want while Pressed delete")
////        }
//////        let edit = UITableViewRowAction(style: UITableViewRowActionStyle.Normal, title: "EDIT"){(UITableViewRowAction,NSIndexPath) -> Void in
//////
//////            print("What u want while Pressed Edit")
//////        }
//////
//////        edit.backgroundColor = UIColor.blackColor()
////        return [delete,edit]
//    }
    
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let cell =  tableView.cellForRow(at: indexPath)
        cell?.contentView.backgroundColor = UIColor(hex: "FCFCFC")
    }
}

class NotificationTableViewCell: UITableViewCell {
    enum Item {
        case notification(JSON)
        case matchToJudge(APIMatch)
    }
    
    @IBOutlet var timeLabel: UILabel!
    @IBOutlet var avatar: WAMPAvatarSmall!
    @IBOutlet var contentLabel: UILabel!
    @IBOutlet var contentButton: UIButton!
    @IBOutlet weak var matchImage: UIImageView!
    var id: String!
    
    func load(_ item: Item) {
        let profile: UserProtocol?
        let createdAtString: String
        let body: String
        
        self.contentView.backgroundColor = UIColor(hex: "FCFCFC")
        contentButton.layer.shadowColor = UIColor.black.cgColor
        contentButton.layer.shadowOffset = CGSize(width: 0.5, height: 1.8)
        contentButton.layer.shadowOpacity = 0.1
        contentButton.backgroundColor = UIColor.white
        contentButton.layer.masksToBounds = false
        contentButton.layer.shadowRadius = 5.0
        contentButton.clipsToBounds = false
        contentButton.layer.shadowPath = UIBezierPath(roundedRect: contentButton.bounds, cornerRadius: 16).cgPath
        self.contentView.backgroundColor = UIColor(hex: "fcfcfc")
        self.backgroundColor = UIColor(hex: "fcfcfc")
        switch item {
        
        case .notification(let notification):
            self.id = notification["id"].stringValue
            profile = Player(json: notification["created_by"])
            createdAtString = notification["created_at"].stringValue
            let date = apiDateFormatter.date(from: createdAtString)
            
            body = "<comment>\(notification["data"]["body"].stringValue)</comment> <time>\(timeAgoSince(date!))</time>"
            contentButton.setTitleColor(UIColor(hex: "007BFF"), for: .normal)
            contentButton.isHidden = true
            matchImage.isHidden = true
            if "\(notification["type"])" == "UserIsNowFollowingYou" {
                contentButton.isHidden = false
                if let player = profile as? Player, player.isFollowing {
                    self.contentButton.setTitle("Unfollow", for: .normal)
                    self.contentButton.setTitleColor(#colorLiteral(red: 1, green: 0.3030309081, blue: 0.3337643743, alpha: 1), for: .normal)
                } else {
                    self.contentButton.setTitle("Follow", for: .normal)
                    self.contentButton.setTitleColor(#colorLiteral(red: 0, green: 0.5728718638, blue: 1, alpha: 1), for: .normal)
                }
                
                contentButton.addAction {
                    self.changeFollowButton(notification: notification)
                }
            } else if String(describing: notification["type"]).contains("NewLikeOnPost") || String(describing: notification["type"]).contains("NewCommentOnPost") || String(describing: notification["type"]).contains("NewMatchPlayed") {
                matchImage.isHidden = false
                let id = notification["data"]["match_id"]
                Alamofire.request(api_url + "/matches/\(id)", method: .get, encoding: JSONEncoding.default, headers: AuthenticationManager.shared.headers).validate().responseJSON {  response in
                    switch response.result {
                    case .failure(let error):
                        log.error("Error getting match photos: \(error)")
                    case .success(let data):
                        
                        
                        let json = JSON(data)
                        
                        
                        Alamofire.request(json["photos"][0]["url"].stringValue).responseImage { response in
                            if let image = response.result.value {
                                self.matchImage.image = image
                            }
                        }
                        
                    }
                }
            }
            
            
            
        case .matchToJudge(let match):
            profile = match.players[0].profile
            self.id = "\(match.id)"
            createdAtString = match.originalJSON["created_at"].stringValue
            let date = apiDateFormatter.date(from: createdAtString)
            
            
            
            body = "<comment>posted a matchplay with you involved.</comment> <time>\(timeAgoSince(date!))</time>"
           
            contentButton.isHidden = false
            matchImage.isHidden = true
            contentButton.setTitleColor(UIColor(hex: "36B37E"), for: .normal)
            contentButton.setTitle("Judge", for: .normal)
            
        }
        
        avatar.setProfile(profile)
        
        
        let username = profile?.username ?? "???"
        let bodyString = "<username>\(username)</username> " + body
        let parser = MarkupString(source: bodyString, styles: [commentStyle,timeStyle, usernameStyle])!
        contentLabel.attributedText = parser.render(withStyles: [commentStyle,timeStyle, usernameStyle])
        contentLabel.numberOfLines = 0
        contentLabel.frame.size.width = 200
    }
    
    func changeFollowButton(notification: JSON) {
        var url = api_url + "/users/"
        let userId = notification["data"]["user_id"]
        let player = Player(json: notification["created_by"])
        
        if self.contentButton.currentTitle == "Follow" {
            url = api_url + "/users/" + userId.stringValue + "/follow"
            self.contentButton.setTitle("Unfollow", for: .normal)
            self.contentButton.setTitleColor(#colorLiteral(red: 1, green: 0.3030309081, blue: 0.3337643743, alpha: 1), for: .normal)
        } else {
            url = api_url + "/users/" + userId.stringValue + "/unfollow"
            self.contentButton.setTitle("Follow", for: .normal)
            self.contentButton.setTitleColor(#colorLiteral(red: 0, green: 0.5728718638, blue: 1, alpha: 1), for: .normal)
        }
        
        
        
        
        
        log.verbose("toggling follow using url: \(url)")
        
        Alamofire.request(url,
                          method: .get,
                          headers: AuthenticationManager.shared.headers)
            .validate()
            .responseJSON { response in
                log.verbose("follow toggle status code: \(response.response!.statusCode)")
                switch response.result {
                case .failure(let error):
                    log.warning("Error while following:", error.localizedDescription)
                case .success(_):
                    player?.isFollowing = !(player?.isFollowing)!
                }
        }
        
        
    }
    
}
