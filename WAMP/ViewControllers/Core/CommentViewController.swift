import UIKit
import RxSwift
import RxKeyboard

class CommentViewController: WampViewController, UITextViewDelegate {
    var match: APIMatch!
    var header: UIView!
    var comments: [Comment] = []
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var smallHeader: UILabel!
    @IBOutlet weak var textViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var commentButton: UIButton!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var bottomSpaceConstraint: NSLayoutConstraint!
    var isDeleting = false // to prevent double deletion requests
    @IBOutlet weak var containerHeightConstraint: NSLayoutConstraint!
    var isPosting = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        textView.delegate = self
        tableView.allowsSelection = false
        tableView.dataSource = self
        tableView.register(CommentTableViewCell.self, forCellReuseIdentifier: CommentTableViewCell.identifier)
        tableView.estimatedSectionHeaderHeight = 70
        tableView.sectionHeaderHeight =   (match.text != "") ?UITableViewAutomaticDimension : 0
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 150

        
        header = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 48))
        let text = UILabel()
        header.backgroundColor = UIColor(hex: "FCFCFC")
        text.font = .bold(32)
        text.textColor = .black
        text.numberOfLines = 0
        header.addSubview(text)
        text.text = "Comments"
        
        tableView.tableHeaderView = header
        
        text.snp.makeConstraints { make in
            make.right.equalToSuperview()
            make.left.equalToSuperview().offset(16)
            make.top.equalToSuperview()
            make.bottom.equalToSuperview().offset(16)
        }
        
        RxKeyboard.instance.visibleHeight
            .drive(onNext: { height in
//                self.tableView.contentInset.bottom = height
                self.bottomSpaceConstraint.constant = height
                UIView.animate(withDuration: 0) {
                    self.view.layoutSubviews()
                }
            })
            .disposed(by: disposeBag)
        
        textView.rx.text
            .orEmpty
            .map{$0.count > 0}
            .bind(to: commentButton.rx.isEnabled)
            .disposed(by: disposeBag)
    }

    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            self.postComment(self)
            return false
        }
        return true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        reload()
    }
    
    func reload() {
        Comment.get(for: self.match) { result in
            switch result {
            case .success(let comments):
                self.comments = comments
                self.tableView.reloadData()
            case .failure(let error):
                self.displayError("Error loading comments: \((error as NSError).localizedDescription)")
            }
        }
    }
    
    @IBAction func backButtonPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    func textViewDidChange(_ textView: UITextView) {
        if textView.text.count > 500 {
             self.displayError("please make sure your comment stays under 500 characters.")
        }
        
        else {
            let size = textView.sizeThatFits(CGSize(width: textView.frame.size.width, height: CGFloat.greatestFiniteMagnitude))
            if size.height != textViewHeightConstraint.constant && size.height > textView.frame.size.height{
                textViewHeightConstraint.constant = size.height
                containerHeightConstraint.constant = size.height + 32
                textView.setContentOffset(CGPoint.zero, animated: false)
            }
        }
    }
    
    @IBAction func postComment(_ sender: Any) {
        guard let text = textView.text, text.count > 0, !isPosting else {
            return
        }
        self.showLoading()
        
        textView.resignFirstResponder()
        
        isPosting = true
        
        Comment.post(text, on: match) { result in
            self.isPosting = false
            switch result {
            case .success(let comment):
                self.textView.text = ""
                self.commentButton.isEnabled = false
                self.comments.append(comment)
                self.tableView.insertRows(at: [IndexPath(row: self.comments.count-1, section: 0)], with: .automatic)
                self.tableView.scrollToBottom(animated: true)
                self.textViewHeightConstraint.constant = 28
                self.containerHeightConstraint.constant = 28 + 32
                self.dismiss(animated: false, completion: nil)
                NotificationCenter.default.post(name: Notification.Name("refreshFeed"), object: nil)
            case .failure(let error):
                self.displayError("Could not post comment: \((error as NSError).localizedDescription)")
            }
        }
    }
    
    func showLoading() {
        let alert = UIAlertController(title: nil, message: "Posting comment...", preferredStyle: .alert)
        
        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        loadingIndicator.startAnimating();
        
        alert.view.addSubview(loadingIndicator)
        present(alert, animated: true, completion: nil)
    }
    
}

extension CommentViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(self.comments.count)
        return self.comments.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        print(self.comments[indexPath.row], indexPath.row)
        let cell = tableView.dequeueReusableCell(withIdentifier: CommentTableViewCell.identifier, for: indexPath) as! CommentTableViewCell
        cell.comment = self.comments[indexPath.row]
        print(cell)
        return cell

    }
    
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        return tableView.cellForRow(at: indexPath)!
//    }
//
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y > 48 {
            UIView.animate(withDuration: 0.1) {
                self.smallHeader.alpha = 1
            }
        } else {
            UIView.animate(withDuration: 0.1) {
                self.smallHeader.alpha = 0
            }
        }
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        let comment = comments[indexPath.row]
        
        // Delete for own comments, otherwise none
        return comment.user.isMe ? .delete : .none
    }
    


    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        guard editingStyle == .delete, !isDeleting else {
            return
        }
        
        isDeleting = true
        
        let comment = self.comments[indexPath.row]
        comment.delete() { result in
            self.isDeleting = false
            
            switch result {
            case .success:
                self.comments.remove(at: indexPath.row)
                tableView.deleteRows(at: [indexPath], with: .automatic)
            case .failure:
                self.displayError("Could not delete comment")
            }
        }
    }
    
   
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = UIView()
        if match.text != "" {
            let nameLabel: UILabel = UILabel(frame: .zero)
            let avatar: WAMPAvatarSmall = WAMPAvatarSmall()
            let commentLabel: UILabel = UILabel(frame: .zero)
            let timeLabel: UILabel = UILabel(frame: .zero)
            
            
            
            avatar.setProfile(match.players.first?.profile)
            nameLabel.text = match.players.first?.profile.username
            commentLabel.text = match.text
            commentLabel.numberOfLines = 0
            if let date = match.date {
                timeLabel.text = timeAgoSince(date)
            }
            
            
            nameLabel.font = .medium(14)
            commentLabel.font = .medium(14)
            timeLabel.font = .medium(12)
            
            nameLabel.textColor = .black
            commentLabel.textColor = UIColor(hex: "8C8C8C")
            timeLabel.textColor = UIColor(hex: "BFBFBF")
            
            
            
            header.addSubview(avatar)
            avatar.snp.makeConstraints { make in
                make.height.equalTo(40)
                make.top.equalToSuperview().offset(16)
                make.leading.equalToSuperview().offset(32)
            }
            //
            header.addSubview(nameLabel)
            nameLabel.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(18)
                make.leading.equalTo(avatar.snp.trailing).offset(32)
            }
            
            header.addSubview(timeLabel)
            timeLabel.snp.makeConstraints { make in
                make.centerY.equalTo(nameLabel)
                make.trailing.equalToSuperview().offset(-16)
            }
            //
            header.addSubview(commentLabel)
            commentLabel.snp.makeConstraints { make in
                make.top.equalTo(nameLabel.snp.bottom).offset(2)
                make.leading.equalToSuperview().offset(66)
                make.trailing.equalToSuperview().inset(16)
                make.bottom.equalToSuperview().offset(-24)
            }
            //
            
            //
            let separator = UIView(frame: .zero)
            separator.backgroundColor = .black
            separator.alpha = 0.1
            header.addSubview(separator)
            separator.snp.makeConstraints { make in
                make.trailing.leading.bottom.equalToSuperview()
                make.height.equalTo(1)
                
            }
        }
        
        
        return header
    }
}


class CommentTableViewCell: UITableViewCell {
    static let identifier = "wamp.CommentTableViewCell"
    var comment: Comment! { didSet { setup() } }
    
    var nameLabel: UILabel = UILabel(frame: .zero)
    var avatar: WAMPAvatarSmall = WAMPAvatarSmall()
    var commentLabel: UILabel = UILabel(frame: .zero)
    var timeLabel: UILabel = UILabel(frame: .zero)
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setup() {
        self.avatar.setProfile(comment.user)
        self.nameLabel.text = comment.user.username
        self.commentLabel.text = comment.body
        self.commentLabel.numberOfLines = 0
        self.timeLabel.text = timeAgoSince(comment.date)
        
        self.nameLabel.font = .medium(14)
        self.commentLabel.font = .medium(14)
        self.timeLabel.font = .medium(12)
        
        self.nameLabel.textColor = .black
        self.commentLabel.textColor = UIColor(hex: "8C8C8C")
        self.timeLabel.textColor = UIColor(hex: "BFBFBF")
        self.contentView.backgroundColor = UIColor(hex: "fcfcfc")
        self.backgroundColor = UIColor(hex: "fcfcfc")
        
        
        
        self.contentView.addSubview(avatar)
        avatar.snp.makeConstraints { make in
            make.height.equalTo(40)
            make.top.equalToSuperview().offset(16)
            make.leading.equalToSuperview().offset(32)
        }
        
        self.contentView.addSubview(nameLabel)
        nameLabel.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(18)
            make.leading.equalTo(avatar.snp.trailing).offset(32)
        }
        
        self.contentView.addSubview(timeLabel)
        timeLabel.snp.makeConstraints { make in
            make.centerY.equalTo(nameLabel)
            make.trailing.equalToSuperview().offset(-16)
        }
        //
        self.contentView.addSubview(commentLabel)
        commentLabel.snp.makeConstraints { make in
            make.top.equalTo(nameLabel.snp.bottom).offset(2)
            make.leading.equalToSuperview().offset(66)
            make.trailing.equalToSuperview().inset(16)
            make.bottom.equalToSuperview().offset(-16)
        }
        //
        
        //
        let separator = UIView(frame: .zero)
        separator.backgroundColor = .black
        separator.alpha = 0.1
        self.contentView.addSubview(separator)
        separator.snp.makeConstraints { make in
            make.trailing.leading.equalToSuperview().inset(16)
            make.height.equalTo(1)
            make.bottom.equalToSuperview()
        }
    }
}
