import UIKit
import Alamofire
import SwiftyJSON
import PullToRefresh

let feedQueue = DispatchQueue(label: "wamp.feedQueue", qos: .userInteractive, attributes: .concurrent)

let headerTexts = [
    "Looks like we have\na winner! 👊",
    "Who are you going\nto beat today? 🏆",
    "Feel like winning\ntoday? 🏅",
    "I can smell victory\nfrom here! 💩",
    "There's nothing\n  like winning! ⛳"
]

class FeedViewController: WampViewController, ScrollDelegateForwarding {
    // Mark - Variables
    var matches = [APIMatch]() { didSet { checkForPlaceholder() } }
    var requestStartIndex = 0
    var apiUrl = "/matches"
    var isFetchingItems = false
    var header: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
    var hasRefreshed = false
    var canPullToRefresh = true
    var isProfile = false
    var delegate: FeedDelgate?
    weak var forwardedScrollDelegate: UIScrollViewDelegate?
    
    // Mark - Outlets
    @IBOutlet weak var placeholderView: UIView!
    @IBOutlet var table: ClickThroughTableView! {
        didSet {
            self.table.delegate = self
            self.table.dataSource = self
        }
    }
    
    
    
    @IBOutlet weak var statusBarBack: UIView!
    
    // Mark - Overrides
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hidesKeyboardOnTap = false
        self.reload()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.reload), name: Notification.Name("refreshFeed"), object: nil)
       
        table.register(FeedTableViewCell.self, forCellReuseIdentifier: FeedTableViewCell.identifier)
        table.register(RandomTextTableViewCell.self, forCellReuseIdentifier: RandomTextTableViewCell.identifier)
        table.rowHeight = UITableViewAutomaticDimension
        table.estimatedRowHeight = 150
        table.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 100, right: 0)
        table.allowsSelection = false
        table.showsVerticalScrollIndicator = false
        table.showsHorizontalScrollIndicator = false
        
        
    
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.table.isHidden = true
        if canPullToRefresh {
            table.removeAllPullToRefresh()
            let refresher = PullToRefresh()
            table.addPullToRefresh(refresher) {
                self.reload()
            }
        }
        
        statusBarBack.isHidden = isProfile
        
        if !isProfile {
            header.removeFromSuperview()
            header = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 132))
            let text = UILabel()
            text.font = .bold(32)
            text.textColor = .black
            text.numberOfLines = 0
            
            text.text = headerTexts[Int.random(min: 0, max: 4)]
            header.addSubview(text)
            self.view.addSubview(header)
            self.view.sendSubview(toBack: header)
            header.snp.makeConstraints { make in
                make.leading.equalToSuperview()
                make.top.equalToSuperview().offset(20)
            }
            
            table.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 132))
            
            text.snp.makeConstraints { make in
                make.right.equalToSuperview()
                make.left.equalToSuperview().offset(16)
                make.top.equalToSuperview().offset(35)
            }
        }
        
        checkForPlaceholder()
    }
    
    // Mark - Custom methods
    func reload() {
        requestStartIndex = 0
        checkForPlaceholder()
        loadMoreFeedItems()
    }
    
    deinit {
        table.removeAllPullToRefresh()
    }
}

extension FeedViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let double = String(format: "%.0f", Double(matches.count / 10))
        let columnsToAdd = (Int(double))
        
        return matches.count + columnsToAdd!
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let double = String(format: "%.0f", Double(indexPath.row / 10))
        let columnsToAdd = (Int(double))
        if (indexPath.row - columnsToAdd!) <= matches.count && (indexPath.row % 10 != 0 || indexPath.row == 0) {
            let match = matches[indexPath.row - columnsToAdd!]
            let cell = tableView.dequeueReusableCell(withIdentifier: FeedTableViewCell.identifier, for: indexPath) as! FeedTableViewCell
            
            cell.setNeedsLayout()
            cell.configure(match, parent: self)
            cell.parentVC = BaseViewController.shared
            
            if indexPath.row == matches.count {
                loadMoreFeedItems()
            }
            return cell
            
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: RandomTextTableViewCell.identifier, for: indexPath) as! RandomTextTableViewCell
            
            cell.setNeedsLayout()
            cell.setUp()
            //
            
            if indexPath.row == matches.count {
                loadMoreFeedItems()
            }
            return cell
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.forwardedScrollDelegate?.scrollViewDidScroll?(scrollView)
        if (scrollView.contentOffset.y < 0) {
            header.frame.origin.y = -scrollView.contentOffset.y
        }
    }
    

}

// Mark - loading matches
extension FeedViewController {
    func checkForPlaceholder() {
        if let delegate = self.delegate, !self.matches.isEmpty {
            delegate.hasMatches(true)
        }
        
        log.verbose("Checking if we should show the placeholder.")
        Alamofire.request(api_url + "/users/me/matches/any", method: .get, headers: AuthenticationManager.shared.headers).validate().responseJSON { response in
            guard let value = response.value else { return }
            
            let json = JSON(value)

            if self.matches.count > 0 {
                self.header.isHidden = self.isProfile
                self.placeholderView.isHidden = true
                 self.table.isHidden = false
            } else if json["has_matches"] == false {
                self.placeholderView.isHidden = self.isProfile
                self.table.isHidden = true
                self.header.isHidden = true
            } else {
                self.table.isHidden = false
                self.header.isHidden = self.isProfile
                self.placeholderView.isHidden = true
            }
        }
    }
    
    func loadMoreFeedItems() {
        guard !isFetchingItems else { return }
        isFetchingItems = true
        
        feedQueue.async {
            
            let fetchCount = 25
            let urlParams = [
                "start":"\(self.requestStartIndex)",
                "end":"\(self.requestStartIndex + fetchCount)",
            ]
            
            // Fetch Request
            Alamofire.request(api_url + self.apiUrl, method: .get, parameters: urlParams, headers: AuthenticationManager.shared.headers)
                .validate(statusCode: 200..<300)
                .responseJSON(queue: feedQueue) { response in
                    guard let value = response.value else {
                        let error = response.error! as NSError
                        self.isFetchingItems = false
                        if error.localizedDescription.contains("timed out") {
                            self.loadMoreFeedItems()
                            return
                        } else {
                            log.error("Error fetching matches: \(error)")
                            self.displayError("Error loading feed: \(error.localizedDescription)")
                            self.table.endAllRefreshing()
                            return
                        }
                    }
                    
                    let json = JSON(value)
                    var matches: [APIMatch] = []
                    
                    func whenFinished() {
                        log.info("Parsed \(matches.count) matches from \(json.count) json matches")
                        
                        DispatchQueue.main.sync {
                            if self.requestStartIndex == 0 {
                                self.matches = matches
                                self.table.reloadData()
                            } else if matches.count > 0 {
                                UIView.setAnimationsEnabled(false)
                                self.table.beginUpdates()
                                let startIndex = self.matches.endIndex - (self.hasRefreshed ? 0 : 1)
                                self.matches += matches
                                let double = String(format: "%.0f", Double(matches.count / 10))
                                let columnsToAdd = (Int(double))!
                                self.table.insertRows(at: (startIndex..<((self.matches.count)+columnsToAdd)).map { IndexPath(row: $0, section: 0)}, with: .none)
                                self.table.endUpdates()
                                UIView.setAnimationsEnabled(true)
                                self.hasRefreshed = true
                            }
                            self.table.endAllRefreshing()
//
//
                            
                            self.table.isHidden = !(self.matches.count > 0)
                        }
                        
                        self.requestStartIndex += fetchCount
                        self.isFetchingItems = false
                    }
                    
                    var matchesToParse = json.array ?? []
                    func parseNextMatch() {
                        guard matchesToParse.count > 0 else {
                            whenFinished()
                            return
                        }
                        
                        let nextJSON = matchesToParse.removeFirst()
                        
                        APIMatch.load(json: nextJSON) { result in
                            switch result {
                            case .failure(let error):
                                log.error("Error parsing match: \(error)")
                            case .success(let match):
                                print("Added match to table")
                                matches.append(match)
                                DispatchQueue.main.async {
                                NotificationCenter.default.post(name: Notification.Name("refreshFeed"), object: nil)
                                self.delegate?.hasMatches(true)
                                }
                            }
                            parseNextMatch()
                        }
                    }
                    parseNextMatch()
            }
        }
    }
    
    
}
