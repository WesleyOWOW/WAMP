import UIKit
import SnapKit
import NYTPhotoViewer
import Alamofire

protocol ScrollDelegateForwarding : class {
    weak var forwardedScrollDelegate: UIScrollViewDelegate? { get set }
}

protocol FeedDelgate {
    func hasMatches(_ forSure: Bool)
}


class ProfileViewController: WampViewController, UIScrollViewDelegate {
    // Mark - Custom variables
    var profile: UserProtocol!
    var feedViewController: FeedViewController!
    let statsViewController = ProfileStatsViewController()
    let compareViewController = ProfileStatsViewController()
    var tabCenterConstraint: Constraint!
    var isFeed = false
    
    var myProfilePlaceholder: UIImageView?
    var yourProfilePlaceholder: UIImageView?
    
    // Mark - Outlets
    @IBOutlet weak var avatar: WAMPAvatarProfile!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var usernameHpcLabel: UILabel!
    @IBOutlet weak var followButton: WampButton!
    @IBOutlet weak var compareButton: UIButton!
    @IBOutlet weak var statsButton: UIButton!
    @IBOutlet weak var followersLabel: UILabel!
    @IBOutlet weak var followingLabel: UILabel!
    @IBOutlet weak var matchesLabel: UILabel!
    @IBOutlet weak var settingsButton: UIButton!
    @IBOutlet weak var feedButton: UIButton!
    @IBOutlet weak var tabIndicator: UIView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var collapsedNameLabel: UILabel!
    @IBOutlet weak var collapsedHeader: UIView!
    @IBOutlet weak var backButton: UIButton!
    
    
    // Mark - Constraints
    @IBOutlet weak var followButtonHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var statisticsTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var tabBarPositionConstraint: NSLayoutConstraint!
    
    // Mark - Overides
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(self.refreshFeed), name: Notification.Name("refreshFeed"), object: nil)
        backButton.isHidden = true
        self.hidesKeyboardOnTap = true
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        feedViewController = storyboard.instantiateViewController(withIdentifier: "feed") as! FeedViewController
        feedViewController.isProfile = true
        feedViewController.canPullToRefresh = false
        
        followersLabel.addAction {
            self.showFollowers()
        }
        
        followingLabel.addAction {
            self.showFollowing()
        }
        avatar.addAction {
            print("Photo tapped")
            self.enlargeProfilePicture()
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.refresh), name: Notification.Name("refreshSettings"), object: nil)
    }
    
    @objc func refresh() {
        
        self.avatar.setPhoto(nil)
        self.avatar.setProfile(self.profile)
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        if backButton.isVisible {
            BaseViewController.shared.goBack()
        }
    }
    
    @IBAction func settingsButtonTapped(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "settings") as! SettingsViewController
        
        BaseViewController.shared.navigationController?.pushViewController(vc, animated: true)
    }
    
    var isFollowing = false
    
    @IBAction func followTapped() {
        guard let user = profile as? User else {
            // make sure we're not pushing 2 follow requests...
            return
        }
        isFollowing = true
        
        if !self.profile.following {
            self.followersLabel.text = "\(Int(self.followersLabel.text!)! + 1)"
            self.followButton.setTitle("Unfollow", for: .normal)
            self.followButton.setTitleColor(UIColor(hex: "FF3344"), for: .normal)
        } else {
            self.followersLabel.text = "\(Int(self.followersLabel.text!)! - 1)"
            self.followButton.setTitle("Follow", for: .normal)
            self.followButton.setTitleColor(UIColor(hex: "007BFF"), for: .normal)
        }
        
        user.toggleFollow { result in
            switch result {
            case .success:
                
                self.isFollowing = false
            case .failure(let error):
                let alert = UIAlertController(title: "An error has occured while (un)following", message: "\((error as NSError).localizedDescription)", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alert, animated: true)
                self.isFollowing = false
            }
        }
        
        
    }
    
    func makePlaceholders() {
        if feedViewController.matches.isEmpty && isFeed {
            print ("")
            if let myPlaceholder = self.myProfilePlaceholder {
                myPlaceholder.removeFromSuperview()
            }
            
            if let yourProfilePlaceholder = self.yourProfilePlaceholder {
                yourProfilePlaceholder.removeFromSuperview()
            }
            if profile.isMe {
                self.myProfilePlaceholder = UIImageView(image: UIImage(named: "MyProfilePlaceholder"))
                self.contentView.addSubview(myProfilePlaceholder!)
                
                myProfilePlaceholder!.snp.makeConstraints { make in
                    make.height.equalTo(311)
                    make.width.equalTo(267)
                    make.leading.equalToSuperview().offset(((UIScreen.main.bounds.width - 267) / 2) + 30)
                    
//                    make.centerY.equalToSuperview()
                    make.top.equalToSuperview().offset(16)
                }
            } else {
                self.yourProfilePlaceholder = UIImageView(image: UIImage(named: "YourProfilePlaceholder"))
                self.contentView.addSubview(yourProfilePlaceholder!)
                
                yourProfilePlaceholder!.snp.makeConstraints { make in
                    make.height.equalTo(170)
                    make.width.equalTo(172)
                    make.leading.equalToSuperview().offset((UIScreen.main.bounds.width - 172) / 2)
                    
                    //                    make.centerY.equalToSuperview()
                    make.top.equalToSuperview().offset(16)
                }
            }
        }
        
    }
    
    

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(false)
        
        avatar.setPhoto(nil) // Reset the image, just to be sure
        avatar.setProfile(profile)
        avatar.layer.cornerRadius = 32
        avatar.clipsToBounds = true 
        avatar.layer.masksToBounds = true
        
        followButton.isHidden = profile.isMe
        followButtonHeightConstraint.constant = (profile.isMe) ? 0 : 32
        statisticsTopConstraint.constant = (profile.isMe) ? 0 : 22
        
        if profile.following {
            self.followButton.setTitle("Unfollow", for: .normal)
            self.followButton.setTitleColor(UIColor(hex: "FF3344"), for: .normal)
        } else {
            self.followButton.setTitle("Follow", for: .normal)
            self.followButton.setTitleColor(UIColor(hex: "007BFF"), for: .normal)
        }
        
        compareButton.isHidden = profile.isMe
        
        nameLabel.text = profile.firstName + " " + profile.lastName
        collapsedNameLabel.text = profile.firstName + " " + profile.lastName
        usernameHpcLabel.text = profile.username + " — " + profile.handicap.renderAsHandicap(includeSuffix: true)
        
        followersLabel.text = "\(profile.followerCount)"
        followingLabel.text = "\(profile.followingCount)"
        matchesLabel.text = "\(profile.matchCount)"
        
        settingsButton.isVisible = profile.isMe
        
        tabIndicator.snp.makeConstraints { make in
            tabCenterConstraint = make.centerX.equalTo(feedButton).constraint
        }
        
        tabBarPositionConstraint.constant = (profile.isMe ? 184 : 244)
        
        feedViewController.apiUrl = "/users/\(profile.id)/matches"
        statsViewController.profile = profile
        compareViewController.compareMode = true
        compareViewController.profile = profile
        
        for vc in [feedViewController, compareViewController, statsViewController] as [WampViewController & ScrollDelegateForwarding] {
            self.view.addSubview((vc.view)!)
            
            
            vc.forwardedScrollDelegate = self
            vc.view.snp.makeConstraints { make in
                make.edges.equalToSuperview()
            }
            
            addChildViewController(vc)
            vc.view.backgroundColor = .clear
//            vc.table.backgroundColor = .clear
        
        }
        self.view.bringSubview(toFront: collapsedHeader)
        feedViewController.table.contentInset = UIEdgeInsetsMake((profile.isMe ? 271 : 326), 0, 100, 0)
        compareViewController.tableView.contentInset = UIEdgeInsetsMake((profile.isMe ? 271 : 326), 0, 100, 0)
        statsViewController.tableView.contentInset = UIEdgeInsetsMake((profile.isMe ? 271 : 326), 0, 100, 0)
        
        compareViewController.tableView.backgroundColor = .clear
        statsViewController.tableView.backgroundColor = .clear
        feedViewController.table.removeAllPullToRefresh()
        isFeed = true
        self.hasMatches()
        
        compareViewController.view.isHidden = true
        statsViewController.view.isHidden = true
        statsViewController.tableView.setContentOffset(statsViewController.tableView.contentOffset, animated: false)
        compareViewController.tableView.setContentOffset(compareViewController.tableView.contentOffset, animated: false)
        view.layoutSubviews()
        
    }
    
    @objc func refreshFeed() {
        print ("Refresh feed")
        if isFeed {
            print("refresh feed and is feed")
            hasMatches(true)
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        print(-scrollView.contentOffset.y)
//
        if ((-scrollView.contentOffset.y) - 56) > 48 && ((-scrollView.contentOffset.y) - 20) < (profile.isMe ? 271 : 326) {
            tabBarPositionConstraint.constant = ((-scrollView.contentOffset.y) - 92)
        } else if ((-scrollView.contentOffset.y) - 20) > (profile.isMe ? 271 : 326) {
            tabBarPositionConstraint.constant = (profile.isMe ? 271 : 326) - 72
        }


        if ((-scrollView.contentOffset.y) - 56) > 48 && ((-scrollView.contentOffset.y) - 56) < (profile.isMe ? 271 : 326) {


            if collapsedHeader.alpha != 0 {
                UIView.animate(withDuration: 0.1) {
                    self.collapsedHeader.alpha = 0
                }
            }
        } else {
            if collapsedHeader.alpha != 1 {
                UIView.animate(withDuration: 0.1) {
                    self.collapsedHeader.alpha = 1
                }
            }
        }

        view.layoutSubviews()
    }
    
    func profilePictureTapHandler() {
        
        self.enlargeProfilePicture()
        
    }
    
    var isFetchingPhoto = false
    func enlargeProfilePicture() {
        guard !isFetchingPhoto else { return }
        isFetchingPhoto = true
        
        class ProfilePicturePhoto : NSObject, NYTPhoto {
            
            let image: UIImage?
            let imageData: Data? = nil
            let placeholderImage: UIImage? = nil
            var attributedCaptionTitle: NSAttributedString? = nil
            var attributedCaptionCredit: NSAttributedString? = nil
            var attributedCaptionSummary: NSAttributedString? = nil
            
            init(_ image: UIImage) {
                self.image = image
            }
            
        }
        
        Alamofire.request(profile.fullsizeAvatarURL, headers: AuthenticationManager.shared.headers).responseImage { response in
            self.isFetchingPhoto = false
            
            switch response.result {
            case .success(let image):
                let photosVc = NYTPhotosViewController(photos: [ProfilePicturePhoto(image)])
                self.present(photosVc, animated: true)
            case .failure(let error):
                self.displayError("Could not download photo for viewing")
                log.error("Error while downloading profile photo for viewing: \(error)")
            }
        }
        
    }

}

extension ProfileViewController: FeedDelgate {
    
    func hasMatches(_ forSure: Bool = false) {
        
        print("hasMatches", feedViewController.matches.count > 0, isFeed)
        if forSure || feedViewController.matches.count > 0 && isFeed {
            if let myPlaceholder = self.myProfilePlaceholder {
                myPlaceholder.removeFromSuperview()
            }
            
            if let yourProfilePlaceholder = self.yourProfilePlaceholder {
                yourProfilePlaceholder.removeFromSuperview()
            }
            feedViewController.view.isHidden = false
        } else {
            makePlaceholders()
            feedViewController.view.isHidden = true
        }
    }
        
    func showFollowing() {
        profile.fetchFollowing { result in
            if let following = result.value {
                log.info("Showing following list with \(following.count) users")
                
                let vc = PersonListViewController(nibName: nil, bundle: nil)
                vc.data = following
                vc.tabTitle = "Following"
                
               self.present(vc, animated: true, completion: nil)
                //                self.navigationController!.pushViewController(, animated: true)
            }
        }
    }
    
    func showFollowers() {
        profile.fetchFollowers { result in
            guard let followers = result.value else { return }
            
            log.info("Showing follower list with \(followers.count) users")
            
            let vc = PersonListViewController(nibName: nil, bundle: nil)
            vc.data = followers
            vc.tabTitle = "Followers"
              self.present(vc, animated: true, completion: nil)
        }
    }
    
    @IBAction func activateFeedTab() {
        UIView.animate(withDuration: 0.3) {
            self.tabCenterConstraint.deactivate()
            self.tabIndicator.snp.remakeConstraints { make in
                self.tabCenterConstraint = make.centerX.equalTo(self.feedButton).constraint
            }
            
            self.view.layoutIfNeeded()
        }
        isFeed = true
        self.hasMatches()
       
        compareViewController.view.isHidden = true
        statsViewController.view.isHidden = true
        statsViewController.tableView.setContentOffset(statsViewController.tableView.contentOffset, animated: false)
        compareViewController.tableView.setContentOffset(compareViewController.tableView.contentOffset, animated: false)
//        feedViewController.table.setContentOffset(compareViewController.tableView.contentOffset, animated: false)
//        feedViewController.table.scrollRectToVisible(CGRect(x: 0, y: 0, width: 1, height: 1), animated: false)
//        hasMatches()
    }
    
    @IBAction func activateCompareTab() {
        UIView.animate(withDuration: 0.3) {
            self.tabCenterConstraint.deactivate()
            self.tabIndicator.snp.remakeConstraints { make in
                self.tabCenterConstraint = make.centerX.equalTo(self.compareButton).constraint
            }
            
            self.view.layoutIfNeeded()
        }
        
        isFeed = false
        if let myPlaceholder = self.myProfilePlaceholder {
            myPlaceholder.removeFromSuperview()
        }
        
        if let yourProfilePlaceholder = self.yourProfilePlaceholder {
            yourProfilePlaceholder.removeFromSuperview()
        }
        
        feedViewController.view.isHidden = true
        compareViewController.view.isHidden = false
        statsViewController.view.isHidden = true
        statsViewController.tableView.setContentOffset(statsViewController.tableView.contentOffset, animated: false)
        compareViewController.tableView.setContentOffset(compareViewController.tableView.contentOffset, animated: false)
        
    }
    
    @IBAction func activateStatsTab() {
        UIView.animate(withDuration: 0.3) {
            self.tabCenterConstraint.deactivate()
            self.tabIndicator.snp.remakeConstraints { make in
                self.tabCenterConstraint = make.centerX.equalTo(self.statsButton).constraint
            }
            
            self.view.layoutIfNeeded()
        }
        
        isFeed = false
        if let myPlaceholder = self.myProfilePlaceholder {
            myPlaceholder.removeFromSuperview()
        }
        
        if let yourProfilePlaceholder = self.yourProfilePlaceholder {
            yourProfilePlaceholder.removeFromSuperview()
        }
        
        feedViewController.view.isHidden = true
        compareViewController.view.isHidden = true
        statsViewController.view.isHidden = false
        statsViewController.tableView.setContentOffset(statsViewController.tableView.contentOffset, animated: false)
        compareViewController.tableView.setContentOffset(compareViewController.tableView.contentOffset, animated: false)
        
    }
}
