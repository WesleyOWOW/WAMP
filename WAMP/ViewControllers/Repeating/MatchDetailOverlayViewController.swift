import UIKit

class MatchDetailOverlayViewController : WampViewController {
    
    @IBOutlet weak var closeButton: WampButton!
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    @IBOutlet var holeSummary: MiniGameHolesSummaryView!
    @IBOutlet var scoreCard: ScoreOverview!
    @IBOutlet var backgroundTapTarget: UIView!
    
    var match: Match!
    
    var didSetup = false
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        guard !didSetup else { return }
        defer { didSetup = true }
        
//        scoreCard.totalRowHeight = match.players.count == 2 ? 112 : 56
        
        if match.players.count == 4 {
            heightConstraint.constant = 400
        }
        
        holeSummary.setup(values: match.summarizeHoles()!)
        scoreCard.setup(match)
    }
    
}
