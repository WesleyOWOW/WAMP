import UIKit
import Photos

// Mark - Comparables
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    
    switch (lhs, rhs) {
    case let (l?, r?):
        return l < r
    case (nil, _?):
        return true
    default:
        return false
    }
}

fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    
    switch (lhs, rhs) {
    case let (l?, r?):
        return l > r
    default:
        return rhs < lhs
    }
}

// Mark - Enumerations
@objc public enum CameraMode: Int {
    case camera
    case library
    
    static var all: [CameraMode] {
        return [.camera, .library]
    }
}

// Mark - Structs
public struct ImageMetadata {
    public let mediaType: PHAssetMediaType
    public let pixelWidth: Int
    public let pixelHeight: Int
    public let creationDate: Date?
    public let modificationDate: Date?
    public let location: CLLocation?
    public let duration: TimeInterval
    public let isFavourite: Bool
    public let isHidden: Bool
    public let asset: PHAsset
}

class CameraViewController: WampViewController {
    
    // Mark - Variables
    fileprivate var mode: CameraMode = .camera
    public var availableModes: [CameraMode] = [.library, .camera]
    public var cameraPosition = AVCaptureDevicePosition.back
    fileprivate var hasGalleryPermission: Bool {
        return PHPhotoLibrary.authorizationStatus() == .authorized
    }
    
    var allowMultipleSelection = false
    
    // Mark - Outlets
    @IBOutlet weak var libraryView: UIView!
    @IBOutlet weak var cameraView: UIView!
    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var cameraButton: UIButton!
    @IBOutlet weak var libraryButton: UIButton!
    
    lazy var albumView  = FSAlbumView.instance()
    lazy var cameraViewI = FSCameraView.instance()

    @IBOutlet weak var tabView: UIView!
    
    
    // Mark - Overrides
    override func viewDidLoad() {
        super.viewDidLoad()
        
        cameraViewI.delegate = self
        albumView.delegate  = self
        
        self.cameraView.addSubview(cameraViewI)
        self.libraryView.addSubview(albumView)
        
        
        changeMode(.camera, isForced: true)
        
    
        cameraViewI.croppedAspectRatioConstraint = NSLayoutConstraint(
            item: cameraViewI.previewViewContainer,
            attribute: NSLayoutAttribute.height,
            relatedBy: NSLayoutRelation.equal,
            toItem: cameraViewI.previewViewContainer,
            attribute: NSLayoutAttribute.width,
            multiplier: 1,
            constant: 0)
        cameraViewI.fullAspectRatioConstraint.isActive     = false
        cameraViewI.croppedAspectRatioConstraint?.isActive = true
        cameraViewI.initialCaptureDevicePosition = cameraPosition
    }

    override public func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        albumView.frame = CGRect(origin: CGPoint.zero, size: libraryView.frame.size)
        albumView.layoutIfNeeded()
        albumView.initialize()
        cameraViewI.frame = CGRect(origin: CGPoint.zero, size: cameraView.frame.size)
        cameraViewI.layoutIfNeeded()
        cameraViewI.initialize()
    }
    
    public override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.stopAll()
    }
    
    override public var prefersStatusBarHidden : Bool {
        return false
    }
    
    @IBAction func libraryButtonPressed(_ sender: UIButton) {
        changeMode(CameraMode.library)
    }
    
    @IBAction func photoButtonPressed(_ sender: UIButton) {
        changeMode(CameraMode.camera)
    }
    
    func doneButtonPressed() {
        allowMultipleSelection ? fusumaDidFinishInMultipleMode() : fusumaDidFinishInSingleMode()
    }
    
    private func fusumaDidFinishInSingleMode() {
        
        guard let view = albumView.imageCropView else { return }
        

            
        let normalizedX = view.contentOffset.x / view.contentSize.width
        let normalizedY = view.contentOffset.y / view.contentSize.height
        
        let normalizedWidth  = view.frame.width / view.contentSize.width
        let normalizedHeight = view.frame.height / view.contentSize.height
        
        let cropRect = CGRect(x: normalizedX, y: normalizedY,
                              width: normalizedWidth, height: normalizedHeight)
        
        requestImage(with: self.albumView.phAsset, cropRect: cropRect) { (asset, image) in
            
//            self.delegate?.fusumaImageSelected(image, source: self.mode)
            
            self.dismiss(animated: true) {
                
//                self.delegate?.fusumaDismissedWithImage(image, source: self.mode)
            }
            
          
//            self.delegate?.fusumaImageSelected(image, source: self.mode, metaData: metaData)
        }
        
    }

    private func requestImage(with asset: PHAsset, cropRect: CGRect, completion: @escaping (PHAsset, UIImage) -> Void) {
        
        DispatchQueue.global(qos: .default).async(execute: {
            
            let options = PHImageRequestOptions()
            options.deliveryMode = .highQualityFormat
            options.isNetworkAccessAllowed = true
            options.normalizedCropRect = cropRect
            options.resizeMode = .exact
            
            let targetWidth  = floor(CGFloat(asset.pixelWidth) * cropRect.width)
            let targetHeight = floor(CGFloat(asset.pixelHeight) * cropRect.height)
            let dimensionW   = max(min(targetHeight, targetWidth), 1024 * UIScreen.main.scale)
            let dimensionH   = dimensionW * 1
            
            let targetSize   = CGSize(width: dimensionW, height: dimensionH)
            
            PHImageManager.default().requestImage(
                for: asset, targetSize: targetSize,
                contentMode: .aspectFill, options: options) { result, info in
                    
                    guard let result = result else { return }
                    
                    DispatchQueue.main.async(execute: {
                        
                        completion(asset, result)
                    })
            }
        })
    }
    
    private func fusumaDidFinishInMultipleMode() {
        
        guard let view = albumView.imageCropView else { return }
        
        let normalizedX = view.contentOffset.x / view.contentSize.width
        let normalizedY = view.contentOffset.y / view.contentSize.height
        
        let normalizedWidth  = view.frame.width / view.contentSize.width
        let normalizedHeight = view.frame.height / view.contentSize.height
        
        let cropRect = CGRect(x: normalizedX, y: normalizedY,
                              width: normalizedWidth, height: normalizedHeight)
        
        var images = [UIImage]()
        
        for asset in albumView.selectedAssets {
            
            requestImage(with: asset, cropRect: cropRect) { asset, result in
                
                images.append(result)
                
                if asset == self.albumView.selectedAssets.last {
                    
                    self.dismiss(animated: true) {
                        
//                        self.delegate?.fusumaMultipleImageSelected(images, source: self.mode)
                    }
                }
            }
        }
    }
    
    func stopAll() {
        
        
        
        if availableModes.contains(.camera) {
            
            self.cameraViewI.stopCamera()
        }
    }
    
    func changeMode(_ mode: CameraMode, isForced: Bool = false) {
        
        if !isForced && self.mode == mode { return }
        
        switch self.mode {
            
        case .camera:
            
            self.cameraViewI.stopCamera()
            
            
            
        default:
            
            break
        }
        
        self.mode = mode
        
        dishighlightButtons()
        updateDoneButtonVisibility()
        
        switch mode {
            
        case .library:
            
            
            highlightButton(libraryButton)
            self.view.bringSubview(toFront: libraryView)
            
        case .camera:
            
            
            highlightButton(cameraButton)
            self.view.bringSubview(toFront: cameraView)
            cameraViewI.startCamera()
            
        }
        
        self.view.bringSubview(toFront: tabView)
    }
    
    func updateDoneButtonVisibility() {
        
        if !hasGalleryPermission {
            self.doneButton.isHidden = true
            return
        }
        
        switch self.mode {
            
        case .library:
            self.doneButton.isHidden = false
            
        default:
            self.doneButton.isHidden = true
        }
    }
    
    func dishighlightButtons() {
        
        cameraButton.setTitleColor(#colorLiteral(red: 0.6178864241, green: 0.6179012656, blue: 0.6178932786, alpha: 1), for: .normal)
        
        if let libraryButton = libraryButton {
            
            libraryButton.setTitleColor(#colorLiteral(red: 0.6178864241, green: 0.6179012656, blue: 0.6178932786, alpha: 1), for: .normal)
        }
        
        
    }
    
    func highlightButton(_ button: UIButton) {
        
        button.setTitleColor(#colorLiteral(red: 0, green: 0.5794699788, blue: 0.998126924, alpha: 1), for: .normal)
    }
    
    func getTabButton(mode: CameraMode) -> UIButton {
        
        switch mode {
            
        case .library:
            
            return libraryButton
            
        case .camera:
            
            return cameraButton
            
            
        }
    }
}

extension CameraViewController: FSAlbumViewDelegate, FSCameraViewDelegate, FSVideoCameraViewDelegate {
    
    public func getCropHeightRatio() -> CGFloat {
        
        return 1
    }
    
    // MARK: FSCameraViewDelegate
    func cameraShotFinished(_ image: UIImage) {
        
//        delegate?.fusumaImageSelected(image, source: mode)
        
        self.dismiss(animated: true) {
            
//            self.delegate?.fusumaDismissedWithImage(image, source: self.mode)
        }
    }
    
    public func albumViewCameraRollAuthorized() {
        
        // in the case that we're just coming back from granting photo gallery permissions
        // ensure the done button is visible if it should be
        self.updateDoneButtonVisibility()
    }
    
    // MARK: FSAlbumViewDelegate
    public func albumViewCameraRollUnauthorized() {
        
        self.updateDoneButtonVisibility()
//        delegate?.fusumaCameraRollUnauthorized()
    }
    
    func videoFinished(withFileURL fileURL: URL) {
        
//        delegate?.fusumaVideoCompleted(withFileURL: fileURL)
//        self.doDismiss(completion: nil)
    }
    
}
