import UIKit
import Alamofire
import AlamofireImage

class ChoosePlayerPopupViewController: WampViewController {

    @IBOutlet weak var closeButton: WampButton!
    @IBOutlet weak var backgroundTapTarget: UIView!
    @IBOutlet weak var topPlayer: UIView!
    @IBOutlet weak var bottomPlayer: UIView!
    
    @IBOutlet weak var playerOne_photo: UIImageView!
    @IBOutlet weak var playerOne_handicap: UILabel!
    @IBOutlet weak var playerOne_username: UILabel!
    @IBOutlet weak var playerOne_fullname: UILabel!
    
    @IBOutlet weak var playerTwo_photo: UIImageView!
    @IBOutlet weak var playerTwo_handicap: UILabel!
    @IBOutlet weak var playerTwo_username: UILabel!
    @IBOutlet weak var playerTwo_fullname: UILabel!
    
    var players: [MatchPlayer]!
    var parentviewController: WampViewController!
    
    static private let imageDownloader = ImageDownloader(
        configuration: ImageDownloader.defaultURLSessionConfiguration(),
        downloadPrioritization: .lifo,
        maximumActiveDownloads: 4,
        imageCache: AutoPurgingImageCache()
    )
    
    func setUp(players: [MatchPlayer], parent: WampViewController) {
        self.players = players
        self.parentviewController = parent
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        for player in [topPlayer, bottomPlayer] {
            player?.layer.shadowColor = UIColor.black.cgColor
            player?.layer.shadowOffset = CGSize(width: 0.5, height: 1.2)
            player?.layer.shadowOpacity = 0.08
            player?.layer.masksToBounds = false
            player?.layer.shadowRadius = 5.0
            player?.layer.cornerRadius = 8
            player?.clipsToBounds = false
            player?.layer.shadowPath = UIBezierPath(roundedRect: (player?.bounds)!, cornerRadius: 8).cgPath
        }
        
        topPlayer.addAction {
            self.parentviewController.hideDetails()
            self.parentviewController.openProfile(for: self.players[0].profile.id)
        }
        
        bottomPlayer.addAction {
            self.parentviewController.hideDetails()
            self.parentviewController.openProfile(for: self.players[1].profile.id)
        }
        
        /// MARK: Player 1.
        var urlRequest = URLRequest(url: players[0].profile.avatarURL)
        urlRequest.setValue(AuthenticationManager.shared.headers["Authorization"], forHTTPHeaderField: "Authorization")
        
        ChoosePlayerPopupViewController.imageDownloader.download(urlRequest) { response in
            self.playerOne_photo.image = response.result.value
        }
        
        playerOne_handicap.text = players[0].profile.handicap.renderAsHandicap(includeSuffix: true)
        playerOne_username.text = players[0].profile.username
        playerOne_fullname.text = "\(players[0].profile.firstName) \(players[0].profile.lastName)"
        
        /// MARK: Player 2.
        var urlRequest2 = URLRequest(url: players[1].profile.avatarURL)
        urlRequest2.setValue(AuthenticationManager.shared.headers["Authorization"], forHTTPHeaderField: "Authorization")
        
        ChoosePlayerPopupViewController.imageDownloader.download(urlRequest2) { response in
            self.playerTwo_photo.image = response.result.value
        }
        
        playerTwo_handicap.text = players[1].profile.handicap.renderAsHandicap(includeSuffix: true)
        playerTwo_username.text = players[1].profile.username
        playerTwo_fullname.text = "\(players[1].profile.firstName) \(players[1].profile.lastName)"
    }
}
