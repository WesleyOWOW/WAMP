//
//  NavigationWrapperViewController.swift
//  WAMP
//


import UIKit
import SnapKit

class NavigationWrapperViewController : WAMPViewController {
    @IBOutlet var topBar: UIView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var contentView: UIView!
    @IBOutlet var rightButton: UIButton!
    @IBOutlet var bottomSpacer: UIView!
    @IBOutlet weak var topLabel: UILabel!
    var offset: CGFloat = 0 {
        didSet {
            scrollTitle()
        }
    }
    @IBOutlet weak var backButton: UIButton!
    var containedViewController: UIViewController!
    
    @IBOutlet weak var titleConstraint: NSLayoutConstraint!
    static func `for`(_ vc: UIViewController) -> NavigationWrapperViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let cvc = storyboard.instantiateViewController(withIdentifier: "NavigationWrapper") as! NavigationWrapperViewController
        
        cvc.containedViewController = vc
        
        return cvc
    }
    var currentConstant: CGFloat!
    func scrollTitle() {
        
     
        if (71 - self.offset) > 5 && (71 - self.offset) < 76 {
            self.titleConstraint.constant = 71 - self.offset
            
            print("scroll", (71 - self.offset))
            if ((71 - self.offset) < 20) {
                
                print("scroll true")
                topLabel.isHidden = false
            } else {
                 print("scroll false")
                topLabel.isHidden = true
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        rightButton.setTitle("", for: .normal)
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let split = UIView(frame: .zero)
        topBar.addSubview(split)
        split.snp.makeConstraints { make in
            make.leading.top.trailing.equalToSuperview()
            make.height.equalTo(56)
        }
        split.backgroundColor = UIColor(hex: "FCFCFC", alpha: 1.0)
        self.topBar.bringSubview(toFront: rightButton)
        self.topBar.bringSubview(toFront: backButton)
        self.topBar.bringSubview(toFront: topLabel)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let config = containedViewController as? WAMPTabConfiguration
        topBar.isVisible = config?.displayTitleBar ?? false
        titleLabel.text = config?.tabTitle ?? ""
        topLabel.text = config?.tabTitle ?? ""
        topLabel.isHidden = true
        // here we really add the contained view controller as child view controller
        addChildViewController(containedViewController)
        contentView.addSubview(containedViewController.view)
        containedViewController.view.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        containedViewController.didMove(toParentViewController: self)
        
        if let vc = containedViewController as? WAMPViewController {
            vc.viewWillAppearWrapped(self, animated: animated)
        }
        
    }
    
    @IBAction func backButtonTapped() {
        if let config = containedViewController as? WAMPTabConfiguration, config.goBack {
            if let override = containedViewController as? NavigationWrappedBackButtonOverriding {
                override.backButtonTapped()
            } else {
                self.navigationController!.popViewController(animated: true)
            }
        } else {
            if let override = containedViewController as? NavigationWrappedBackButtonOverriding {
                override.backButtonTapped()
            } else {
                self.navigationController!.popViewController(animated: true)
            }
        }
    }
}

protocol NavigationWrappedBackButtonOverriding {
    func backButtonTapped()
}

extension UIViewController {
    var navigationWrapped: NavigationWrapperViewController {
        return NavigationWrapperViewController.for(self)
    }
}
