import UIKit
import SnapKit
import RxSwift

class SingleMatchViewController: WampViewController, UITableViewDataSource {
    
    let tableView = UITableView(frame: .zero, style: .plain)
    let match: APIMatch

    var showBack = true
    
    init(match: APIMatch) {
        self.match = match
        super.init(nibName: nil, bundle: nil)
      
        self.hidesKeyboardOnTap = false
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // Sets up the single match view controller
    override func loadView() {
        self.view = UIView()
        self.view.backgroundColor = UIColor(hex: "FCFCFC")
        
        var header: UIView!
        
        tableView.dataSource = self
        tableView.allowsSelection = false
        tableView.separatorStyle = .none
        tableView.register(FeedTableViewCell.self, forCellReuseIdentifier: FeedTableViewCell.identifier)
        tableView.contentInset = UIEdgeInsetsMake(0, 0, 100, 0)
        view.addSubview(tableView)
        tableView.snp.makeConstraints { make in
            make.trailing.leading.bottom.top.equalToSuperview()
        
        }
        
        if !showBack {
            let closeButton = UIButton(frame: .zero)
            closeButton.setImage(UIImage(named: "Close Black"), for: .normal)
        } else {
            
            header = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 64))
            let backButton = UIButton(frame: .zero)
            backButton.setImage(UIImage(named: "Backward arrow black"), for: .normal)
            backButton.addAction {
                print("goBack")
                BaseViewController.shared.goBack()
                
            }
            
            header.addSubview(backButton)
            backButton.snp.makeConstraints { make in
                make.size.equalTo(32)
                make.top.leading.equalToSuperview().offset(16)
            }
            
            tableView.tableHeaderView = header
            
        }
    }
    
    // MARK: - Table View Data Source
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: FeedTableViewCell.identifier, for: indexPath) as! FeedTableViewCell
        cell.configure(match, parent: self)
        
        return cell
    }
    
}

