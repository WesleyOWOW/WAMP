import UIKit
import SnapKit
import RxSwift
import RxKeyboard

fileprivate class PersonTableViewCell : UITableViewCell {
    
    static let identifier = "PersonTableViewCell"
    
    var table: PersonTableViewCell!
    let avatarView = WAMPAvatarSmall()
    let usernameLabel = UILabel(frame: .zero)
    let realnameHandicapLabel = UILabel(frame: .zero)
    let followButton = WampButton(frame: CGRect(x: 0, y: 0, width: 80, height: 32))
    let separatorView = UIView()
    var user: User!
    let disposeBag = DisposeBag()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        contentView.backgroundColor = #colorLiteral(red: 0.9881382585, green: 0.9883032441, blue: 0.9881165624, alpha: 1)
        
        // Add subviews
        [avatarView, usernameLabel, realnameHandicapLabel, followButton, separatorView].forEach(contentView.addSubview)
        
        // --- avatar setup ---
        avatarView.snp.makeConstraints { make in
            make.size.equalTo(40)
            make.leading.equalToSuperview().offset(16)
            make.centerY.equalToSuperview()
        }
        
        // --- username label setup ---
        usernameLabel.font = .medium(14)
        usernameLabel.textColor = .black
        usernameLabel.snp.makeConstraints { make in
            make.bottom.equalTo(self.contentView.snp.centerY).offset(-2)
            make.leading.equalTo(self.avatarView.snp.trailing).offset(12)
            make.trailing.lessThanOrEqualTo(self.followButton.snp.leading).offset(-12)
        }
        
        
        // --- real name / handicap label setup ---
        realnameHandicapLabel.font = .medium(14)
        realnameHandicapLabel.textColor = #colorLiteral(red: 0.5489020944, green: 0.5489976406, blue: 0.5488895178, alpha: 1)
        realnameHandicapLabel.snp.makeConstraints { make in
            make.top.equalTo(self.contentView.snp.centerY).offset(2)
            make.leading.equalTo(self.usernameLabel)
            make.trailing.equalTo(self.usernameLabel)
        }
        
        
        
        
        
        // --- follow button setup ---
        followButton.setTitle("Follow", for: .normal)
        followButton.titleLabel?.font = .medium(14)
        followButton.backgroundColor = .white
        followButton.setTitleColor(#colorLiteral(red: 0, green: 0.4823529412, blue: 1, alpha: 1), for: .normal)
        
        followButton.layer.cornerRadius = 16
        followButton.removeActions(for: .touchUpInside)
       
        followButton.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.trailing.equalToSuperview().offset(-16)
            make.width.equalTo(80)
            make.height.equalTo(32)
        }
        
        // --- separator view ---
        separatorView.backgroundColor = #colorLiteral(red: 0.9136453271, green: 0.9137768149, blue: 0.9136165977, alpha: 1)
        separatorView.snp.makeConstraints { make in
            make.height.equalTo(1)
            make.leading.trailing.equalToSuperview().inset(16)
            make.bottom.equalToSuperview()
        }
        
        // Highlight color
        let highlightView = UIView()
        highlightView.backgroundColor = #colorLiteral(red: 0.8656281663, green: 0.870160251, blue: 0.870160251, alpha: 1)
        selectedBackgroundView = highlightView
    }
    
    func configure(_ profile: UserProtocol) {
        usernameLabel.text = profile.username
        realnameHandicapLabel.text = "\(profile.firstName) \(profile.lastName)"
        avatarView.setProfile(profile)
        
        //        usernameLabel.addAction {
        //            table.follow(profile: <#T##User#>)
        //        }
        
        followButton.isEnabled = !profile.following && profile is User
        followButton.isSelected = profile.following
        if profile.following {
            followButton.setTitleColor(#colorLiteral(red: 1, green: 0.2, blue: 0.2666666667, alpha: 1), for: .normal)
            followButton.setTitle("Unfollow", for: .normal)
        }
        print(followButton.isSelected, profile.following)
        
        self.user = profile as? User
        
        followButton.gestureRecognizers?.forEach(followButton.removeGestureRecognizer)
        if let user = profile as? User {
            
            followButton.addAction { [unowned self] in
                print("followButtonClicked")
                self.follow(profile: user)
            }
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func follow(profile: User) {
        if self.followButton.isSelected {
            followButton.titleLabel?.font = .medium(14)
            followButton.setTitle("Follow", for: .normal)
            followButton.setTitleColor(#colorLiteral(red: 0, green: 0.4823529412, blue: 1, alpha: 1), for: .normal)
            self.followButton.isSelected = false
            profile.toggleFollow(callback: { _ in })
        } else {
            self.followButton.isSelected = true
            followButton.setTitleColor(#colorLiteral(red: 1, green: 0.2, blue: 0.2666666667, alpha: 1), for: .normal)
            followButton.setTitle("Unfollow", for: .normal)
            profile.toggleFollow(callback: { _ in })
        }
    }
}

class PersonListViewController : WampViewController, UITableViewDataSource, UITableViewDelegate {
    
    var goBack: Bool = false
    var searchResult: [UserProtocol] = []
    var displayTitleBar: Bool = true
    var tabTitle: String = "People" { didSet { updateText() } }
    var searchField: UITextField!
    var searchBack: UIView!
    let tableView = UITableView(frame: .zero, style: .plain)
    var header: UIView!
    var text: UILabel!
    var data = [UserProtocol]() {
        didSet {
            reload()
        }
    }
    
    func updateText() {
        self.text.text = self.tabTitle
    }
    
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        
        let bgColor = #colorLiteral(red: 0.9882352941, green: 0.9882352941, blue: 0.9882352941, alpha: 1)
        view.backgroundColor = bgColor
        tableView.backgroundColor = bgColor
        
        view.addSubview(tableView)
        
        
        tableView.register(PersonTableViewCell.self, forCellReuseIdentifier: PersonTableViewCell.identifier)
        tableView.rowHeight = 70
        tableView.dataSource = self
        tableView.delegate = self
        tableView.separatorStyle = .none
        
        
        header = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 48))
        text = UILabel()
        header.backgroundColor = UIColor(hex: "FCFCFC")
        text.font = .bold(32)
        text.textColor = .black
        text.numberOfLines = 0
        header.addSubview(text)
        text.text = tabTitle
        
        tableView.tableHeaderView = header
        
        text.snp.makeConstraints { make in
            make.right.equalToSuperview()
            make.left.equalToSuperview().offset(16)
            make.top.equalToSuperview()
            make.bottom.equalToSuperview().offset(16)
        }
        
        tableView.allowsSelection = true
      
        let separator = UIView(frame: .zero)
        separator.backgroundColor = UIColor(hex: "000000", alpha: 0.05)
        
//        self.view.addSubview(separator)
//        separator.snp.makeConstraints { make in
//            make.trailing.leading.equalToSuperview()
//            make.height.equalTo(1)            make.top.equalTo(searchBack.snp.bottom).offset(16)
//        }
//        
        tableView.snp.makeConstraints { make in
            make.trailing.leading.bottom.top.equalToSuperview()
//            make.top.equalToSuperview().offset(48)
            
        }
        
        let smallTab = UIView(frame: .zero)
//        let effectOverlay = UIVisualEffectView(effect: nil)
//        effectOverlay.effect = UIBlurEffect(style: .regular)
//        smallTab.addSubview(effectOverlay)
//        effectOverlay.snp.makeConstraints { make in
//            make.edges.equalToSuperview()
//        }
        
        smallTab.backgroundColor = UIColor(hex: "FCFCFC")
        
        let backButton = UIButton(frame: .zero)
        backButton.setImage(UIImage(named: "Backward arrow black"), for: .normal)
        backButton.addAction {
            self.dismiss(animated: true, completion: nil)
        }
        
        smallTab.addSubview(backButton)
        backButton.snp.makeConstraints { make in
            make.size.equalTo(32)
            make.centerY.equalToSuperview().offset(16)
            make.leading.equalToSuperview().offset(16)
            
        }
        
        self.view.addSubview(smallTab)
        smallTab.snp.makeConstraints { make in
            make.top.trailing.leading.equalToSuperview()
            make.height.equalTo(64)
        }
        
        RxKeyboard.instance.visibleHeight
            .drive(onNext: { keyboardVisibleHeight in
                self.tableView.contentInset.bottom = keyboardVisibleHeight
                if keyboardVisibleHeight < 3 {
                    self.tableView.contentInset.bottom = 48
                }
            })
            .disposed(by: disposeBag)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.hidesKeyboardOnTap = false
        super.viewWillAppear(animated)
        tableView.contentInset = UIEdgeInsetsMake(44, 0, 100, 0)
        
        tableView.scrollToTop(animated: false)
        tableView.contentOffset.y = -64
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        var follow: UITableViewRowAction!
        let cell = tableView.cellForRow(at: indexPath) as! PersonTableViewCell
        if !cell.user.following {
            follow = UITableViewRowAction(style: .normal, title: "Follow") { (action, indexPath) in
                cell.user.toggleFollow(callback: { _ in })
                self.tableView.setEditing(false, animated: true)
                
            }
            
            follow.backgroundColor = UIColor(hex: "007BFF")
        } else {
            follow = UITableViewRowAction(style: .normal, title: "Unfollow") { (action, indexPath) in
                cell.user.toggleFollow(callback: {_ in})
                self.tableView.setEditing(false, animated: true)
            }
            
            follow.backgroundColor = UIColor(hex: "FF3344")
        }
        
        return [follow]
    }
    
    
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let searchView = UIView()
        
        let searchBack = UIView(frame: .zero)
        searchBack.backgroundColor = UIColor(hex: "F2F2F2")
        searchBack.layer.cornerRadius = 5
        searchBack.clipsToBounds = true
        searchView.backgroundColor = UIColor(hex: "FCFCFC")
        searchView.addSubview(searchBack)
        searchBack.snp.makeConstraints { make in
            make.height.equalTo(32)
            make.edges.equalToSuperview().inset(16)
        }
        
        let searchImage = UIImageView(image: UIImage(named: "searchIcon"))
        searchBack.addSubview(searchImage)
        searchImage.snp.makeConstraints { make in
            make.size.equalTo(12)
            make.centerY.equalToSuperview()
            make.leading.equalToSuperview().offset(16)
        }
        
        searchField = UITextField(frame: .zero)
        searchField.placeholder = "Search"
        searchField.font = .medium(16)
        searchField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        searchBack.addSubview(searchField)
        searchField.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.leading.equalTo(searchImage.snp.trailing).offset(12)
        }
        
    
        
        return searchView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 64
    }
    
    func reload() {
        searchResult = data
        self.tableView.reloadData()
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        print(scrollView.contentOffset.y)
        //        wrapper.offset = scrollView.contentOffset.y
    }
    
    func textFieldDidChange(_ textField: UITextField) {
        self.searchResult = []
        if textField.text == "" {
            self.reload()
        } else {
            self.tableView.reloadData()
            self.searchResult = self.data.filter { $0.firstName.lowercased().contains(textField.text!.lowercased()) }
            self.tableView.reloadData()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchResult.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: PersonTableViewCell.identifier, for: indexPath) as! PersonTableViewCell
        
        if ( indexPath.row < (self.searchResult.count)) {
            cell.configure(searchResult[indexPath.row])
        }
        cell.followButton.isHidden = true
//
//
//        cell.followButton.addAction {
//            cell.follow(profile: cell.user)
//        }
//
//        cell.addAction {
//             let storyboard = UIStoryboard(name: "Main", bundle: nil)
//            let vc = storyboard.instantiateViewController(withIdentifier: "profile") as! ProfileViewController
//            vc.profile = cell.user
//            BaseViewController.shared.setTab(vc)
//        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("DIDSELECT")
        let cell = tableView.cellForRow(at: indexPath) as! PersonTableViewCell
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "profile") as! ProfileViewController
        vc.profile = cell.user
        BaseViewController.shared.setTab(vc)
        self.dismiss(animated: true, completion: nil)
        tableView.deselectRow(at: indexPath, animated: false)
    }
}

