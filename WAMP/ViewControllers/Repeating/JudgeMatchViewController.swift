import UIKit
import SnapKit
import RxSwift
import Alamofire

class JudgeMatchViewController : WampViewController, UITableViewDataSource {
    
    let tableView = UITableView(frame: .zero, style: .plain)
    let match: APIMatch
    
    var showBack = true
    
    init(match: APIMatch) {
        self.match = match
        super.init(nibName: nil, bundle: nil)
        
        self.hidesKeyboardOnTap = false
        
        
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    enum Action {
        case approvePost
        case approveHide
        case reportIncorrect
        
        var urlPortion: String {
            switch self {
            case .approvePost: return "verify"
            case .approveHide: return "hide"
            case .reportIncorrect: return "report-incorrect"
            }
        }
    }
    
    // Sets up the single match view controller
    override func loadView() {
        self.view = UIView()
        self.view.backgroundColor = UIColor(hex: "FCFCFC")
        
        var smallTab = UIView(frame: .zero)
        let effectOverlay = UIVisualEffectView(effect: nil)
        effectOverlay.effect = UIBlurEffect(style: .regular)
        smallTab.addSubview(effectOverlay)
        effectOverlay.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        let backButton = UIButton(frame: .zero)
        backButton.setImage(UIImage(named: "Backward arrow black"), for: .normal)
        backButton.addAction {
            BaseViewController.shared.goBack()
        }
        
        smallTab.addSubview(backButton)
        backButton.snp.makeConstraints { make in
            make.size.equalTo(32)
            make.centerY.equalToSuperview()
            make.leading.equalToSuperview().offset(16)
            
        }
        
        
        var header: UIView!
        
        tableView.dataSource = self
        tableView.allowsSelection = false
        tableView.separatorStyle = .none
        tableView.register(FeedTableViewCell.self, forCellReuseIdentifier: FeedTableViewCell.identifier)
        tableView.contentInset = UIEdgeInsetsMake(56, 0, 100, 0)
        view.addSubview(tableView)
        tableView.snp.makeConstraints { make in
            make.trailing.leading.bottom.top.equalToSuperview()
            
        }
        
        self.view.addSubview(smallTab)
        smallTab.snp.makeConstraints { make in
            make.top.trailing.leading.equalToSuperview()
            make.height.equalTo(64)
        }
        
       
            
        header = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 64))
        let text = UILabel()
        text.font = .bold(32)
        text.textColor = .black
        text.numberOfLines = 0
        header.addSubview(text)
        text.text = "Judge match"
        
        tableView.tableHeaderView = header
        
        text.snp.makeConstraints { make in
            make.right.centerY.equalToSuperview()
            make.left.equalToSuperview().offset(16)
            
        }
        
        let judgeButton = UIButton(frame: .zero)
        judgeButton.backgroundColor = UIColor(hex: "36B37E")
        judgeButton.titleLabel?.font = .bold(16)
        judgeButton.setTitleColor(.white, for: .normal)
        judgeButton.setTitle("Judge matchplay", for: .normal)
        
        judgeButton.addAction {
            let controller = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            
            func action(_ title: String, _ action: Action? = nil, style: UIAlertActionStyle = .default) -> UIAlertAction {
                return UIAlertAction(title: title, style: style, handler: { [unowned self] _ in
                    if let action = action {
                        self.handle(action)
                    }
                })
            }
            
            [
                action("Approve & Post", .approvePost),
                action("Approve & Hide", .approveHide),
                action("Report Incorrect Match", .reportIncorrect, style: .destructive),
                action("Cancel", style: .cancel)
                ].forEach(controller.addAction)
            
            self.present(controller, animated: true)
            
        }
        
        view.addSubview(judgeButton)
        judgeButton.snp.makeConstraints { make in
            make.height.equalTo(48)
            make.trailing.leading.equalToSuperview()
            if #available(iOS 11, *) {
                make.bottom.equalTo(self.view.safeAreaLayoutGuide.snp.bottom)
            } else {
                make.bottom.equalToSuperview()
            }
        }
        
        
        if #available(iOS 11, *) {
            let buttonExtended = UIView(frame: .zero)
            buttonExtended.backgroundColor = UIColor(hex: "36B37E")
            view.addSubview(buttonExtended)
            buttonExtended.snp.makeConstraints { make in
                make.trailing.leading.equalToSuperview()
                make.top.equalTo(self.view.safeAreaLayoutGuide.snp.bottom)
                make.bottom.equalToSuperview()
            }
        }
        
     
        
        
    }
    
    func showLoading(text: String) {
        let alert = UIAlertController(title: nil, message: text, preferredStyle: .alert)
        
        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        loadingIndicator.startAnimating();
        
        alert.view.addSubview(loadingIndicator)
        present(alert, animated: true, completion: nil)
    }
    
    func handle(_ action: Action) {
        showLoading(text: "Judging match...")
        
        
        Alamofire.request("\(api_url)/matches/\(match.id)/\(action.urlPortion)",
            headers: AuthenticationManager.shared.headers)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .success:
                    self.dismiss(animated: true, completion: nil)
                    NotificationCenter.default.post(name: Notification.Name("refreshNotifications"), object: nil)
                    BaseViewController.shared.notificationsViewController.refresh()
                    BaseViewController.shared.goBack()
                // TODO: reload notifications
                case .failure(let error):
                    self.displayError("Could not \(action.urlPortion) match")
                    log.error(error)
                }
        }
    }
    
    // MARK: - Table View Data Source
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: FeedTableViewCell.identifier, for: indexPath) as! FeedTableViewCell
        
        cell.item?.isJudge = true
        cell.configure(match, parent: self, true)
        return cell
    }
    
    
    
}


