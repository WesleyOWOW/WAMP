//
//  LaunchViewController.swift
//  WAMP
//
//  Created by Wesley Peeters on 06-03-18.
//  Copyright © 2018 WAMP - We Are Matchplay. All rights reserved.
//

import UIKit
import Lottie
import SnapKit

class LaunchViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        let anim = LOTAnimationView(name: "logo anim")
        self.view.addSubview(anim)
        anim.loopAnimation = true
        anim.play()
        anim.snp.makeConstraints { make in
            make.center.equalToSuperview()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
