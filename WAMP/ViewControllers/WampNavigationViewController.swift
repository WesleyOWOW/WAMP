//
//  WampNavigationViewController.swift
//  WAMP
//
//  Created by Wesley Peeters on 02/03/2018.
//  Copyright © 2018 WAMP - We Are Matchplay. All rights reserved.
//

import UIKit

class WampNavigationViewController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        if let vc = viewController as? WAMPViewController, vc.navigationWrap {
            super.pushViewController(vc.navigationWrapped, animated: animated)
        } else {
            super.pushViewController(viewController, animated: animated)
        }
    }
}
