//
//  ResetPasswordViewController.swift
//  WAMP
//
//  Created by Wesley Peeters on 29/01/2018.
//  Copyright © 2018 Robbert Brandsma. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxKeyboard
import Alamofire
import SwiftyJSON

class ResetPasswordViewController: WampViewController {
    @IBOutlet weak var errorHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var nextButtonBottomConstaint: NSLayoutConstraint!
    @IBOutlet weak var input: UITextField!
    @IBOutlet weak var nextButton: WampButton!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var backButton: UIButton!
    var emailValid: Bool = false
    var email = Variable<String>("")
    var emailIsValid : Observable<Bool> {
        return email.asObservable().map { email in
            let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
            let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
            self.emailValid = emailTest.evaluate(with: email)
            return emailTest.evaluate(with: email)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.input.delegate = self
        self.input.becomeFirstResponder()
        setupRX()
    }

    @IBAction func backButtonPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func nextButtonPressed(_ sender: Any) {
       
        
        if emailValid {
            Alamofire.request("\(api_url)/password/email", method: .post, parameters: ["email" : self.email.value], encoding: JSONEncoding.default, headers: [:]).validate().responseJSON { response in
                    switch response.result {
                    case .failure(_):
                        if let value = response.data {
                            let json = JSON(value)
                            log.debug("Error - password reset error thrown: \(json)")
                            self.errorLabel.text = "Oops, this email address isn't valid"
                            self.errorHeightConstraint.constant = 160
                            self.makeViewDark()
                            self.backButton.setImage(UIImage(named: "Backward arrow white"), for: .normal, animated: true)
                            UIView.animate(withDuration: 0.2) {
                                self.view.layoutIfNeeded()
                            }
                            
                            DispatchQueue.main.asyncAfter(deadline: .now() + 5.0) {
                                self.errorHeightConstraint.constant = 0
                                self.makeViewLight()
                                self.backButton.setImage(UIImage(named: "Backward arrow black"), for: .normal, animated: true)
                                UIView.animate(withDuration: 0.2) {
                                    self.view.layoutIfNeeded()
                                }
                            }
                        }
                      
                    case .success(_):
                        self.performSegue(withIdentifier: "emailReset", sender: nil)
                        
                    }
            }
        }
    }
    
    func setupRX() {
        self.input.rx.text.orEmpty.bind(to: self.email).disposed(by: disposeBag)
        _ = self.emailIsValid.asObservable().bind { [weak self] emailIsValid in
            self?.emailValid = emailIsValid
            }.disposed(by: disposeBag)
       
        
        RxKeyboard.instance.visibleHeight.drive(onNext: { [weak self] visibleHeight in
            self?.nextButtonBottomConstaint.constant = visibleHeight + 20
            self?.view.setNeedsLayout()
            UIView.animate(withDuration: 0.1) {
                self?.view.layoutIfNeeded()
            }
        }).disposed(by: disposeBag)
    }
    
    func textFieldShouldReturn(_ textField: UITextField!) -> Bool {
        textField.resignFirstResponder()
        if self.nextButton.isEnabled {
            self.nextButtonPressed(self)
        }
        
        return true
    }
}
