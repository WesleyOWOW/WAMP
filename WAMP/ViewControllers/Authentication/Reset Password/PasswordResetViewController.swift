//
//  PasswordResetViewController.swift
//  WAMP
//
//  Created by Wesley Peeters on 29/01/2018.
//  Copyright © 2018 Robbert Brandsma. All rights reserved.
//

import UIKit

class PasswordResetViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func closeButtonPressed(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func openMailPressed(_ sender: Any) {
        let mailURL = URL(string: "message://")!
        if UIApplication.shared.canOpenURL(mailURL) {
            UIApplication.shared.open(mailURL, options: [:], completionHandler: nil)
        }
    }
    
}
