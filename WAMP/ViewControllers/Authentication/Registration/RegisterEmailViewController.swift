//
//  RegisterEmailViewController.swift
//  We Are Matchplay
//
//  Created by Wesley Peeters on 15/12/2017.
//  Copyright © 2017 OWOW. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxKeyboard

class RegisterEmailViewController: WampViewController {
    enum errorType {
        case invalidEmail
        case takenEmail
        case none
    }
    
    @IBOutlet weak var errorHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var nextButtonBottomConstaint: NSLayoutConstraint!
    @IBOutlet weak var input: UITextField!
    @IBOutlet weak var nextButton: WampButton!
    var error: errorType = .none
    
    @IBOutlet weak var errorLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.input.delegate = self
        setupRX()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.input.becomeFirstResponder()
        self.input.text = RegisterViewModel.shared.email.value
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func backButtonPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func nextButtonPressed(_ sender: Any) {
        
        let taken = !RegisterViewModel.shared.emmailValid
        
        if taken {
            self.errorLabel.text = "Oops, this email address isn't valid"
            self.errorHeightConstraint.constant = 160
            self.makeViewDark()
            self.backButton.setImage(UIImage(named: "Backward arrow white"), for: .normal, animated: true)
            UIView.animate(withDuration: 0.2) {
                self.view.layoutIfNeeded()
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 5.0) {
                self.errorHeightConstraint.constant = 0
                self.makeViewLight()
                self.backButton.setImage(UIImage(named: "Backward arrow black"), for: .normal, animated: true)
                UIView.animate(withDuration: 0.2) {
                    self.view.layoutIfNeeded()
                }
            }
        } else {
            _ = AuthenticationManager.shared.emailIsTaken(email: input.text!) { result in
                switch result {
                case .success:
                    self.performSegue(withIdentifier: "nextStep", sender: nil)
                case .failure:
                    self.errorLabel.text = "Oops, this email address is already taken"
                    self.errorHeightConstraint.constant = 160
                    self.makeViewDark()
                    self.backButton.setImage(UIImage(named: "Backward arrow white"), for: .normal, animated: true)
                    UIView.animate(withDuration: 0.2) {
                        self.view.layoutIfNeeded()
                    }
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 5.0) {
                        self.errorHeightConstraint.constant = 0
                        self.makeViewLight()
                        self.backButton.setImage(UIImage(named: "Backward arrow black"), for: .normal, animated: true)
                        UIView.animate(withDuration: 0.2) {
                            self.view.layoutIfNeeded()
                        }
                    }
                }
            }
        }
        
       

    }
    
    func setupRX() {
        self.input.rx.text.orEmpty.bind(to: RegisterViewModel.shared.email).disposed(by: disposeBag)
        
        _ = RegisterViewModel.shared.emailIsValid.asObservable().bind { [weak self] emailIsValid in
            self?.nextButton.isEnabled = true
        }.disposed(by: disposeBag)
        
        RxKeyboard.instance.visibleHeight.drive(onNext: { [weak self] visibleHeight in
            self?.nextButtonBottomConstaint.constant = visibleHeight + 16
//            self?.nextButton.frame.origin.y = ((self?.view.frame.height)! - willShowVisibleHeight) - 71.
            
            self?.view.setNeedsLayout()
            UIView.animate(withDuration: 0.1) {
                self?.view.layoutIfNeeded()
            }
        }).disposed(by: disposeBag)
    }
    
    func textFieldShouldReturn(_ textField: UITextField!) -> Bool {
        textField.resignFirstResponder()
        if self.nextButton.isEnabled {
           self.nextButtonPressed(self)
        }
        
        return true
    }
}

