//
//  RegisterHandicapViewController.swift
//  We Are Matchplay
//
//  Created by Wesley Peeters on 18/12/2017.
//  Copyright © 2017 OWOW. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxKeyboard

class RegisterHandicapViewController: WampViewController {
    
    @IBOutlet weak var input: UITextField!
    @IBOutlet weak var nextButton: WampButton!
    @IBOutlet weak var profilePicture: UIImageView!
    @IBOutlet weak var nextButtonBottomConstaint: NSLayoutConstraint!
    @IBOutlet weak var backButton: UIButton!
    
    @IBOutlet weak var errorHeightConstraint: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupRX()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.profilePicture.image = WampCameraViewModel.shared.currentPhoto.value?.image
          self.input.becomeFirstResponder()
        self.input.delegate = self
        self.profilePicture.layer.cornerRadius = 28
        profilePicture.clipsToBounds = true
        self.input.text = RegisterViewModel.shared.handicap.value
        self.input.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func textFieldDidChange(_ textField: UITextField) {
        if let intValue = Double(textField.text!), intValue > 54.00 {
            textField.text = "54"
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if string.isEmpty {
            return true
        }
        
        let countdots = textField.text!.components(separatedBy: ".").count - 1
        
        if countdots > 0 && (string == "." || string == ",")
        {
            return false
        }
        
        let decimals = textField.text!.components(separatedBy: ".")
        
        if decimals.count > 1, decimals[1].count > 0 {
            return false
        }
        
        
        
        let aSet = NSCharacterSet(charactersIn:"0123456789,.").inverted
        let compSepByCharInSet = string.components(separatedBy: aSet)
        let numberFiltered = compSepByCharInSet.joined(separator: "")
        if string == "," {
            textField.text! += "."
            return false
        }
        return string == numberFiltered
        
    }
    
    
    @IBAction func backButtonPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func nextButtonPressed(_ sender: Any) {
        if self.input.text != "" || self.input.text != " " {
            self.performSegue(withIdentifier: "nextStep", sender: nil)
        } else {
            self.errorHeightConstraint.constant = 160
            self.makeViewDark()
            self.backButton.setImage(UIImage(named: "Backward arrow white"), for: .normal, animated: true)
            UIView.animate(withDuration: 0.2) {
                self.view.layoutIfNeeded()
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 5.0) {
                self.errorHeightConstraint.constant = 0
                self.makeViewLight()
                self.backButton.setImage(UIImage(named: "Backward arrow black"), for: .normal, animated: true)
                UIView.animate(withDuration: 0.2) {
                    self.view.layoutIfNeeded()
                }
            }
        }
        
    }
    
    func setupRX() {
        self.input.rx.text.orEmpty.bind(to: RegisterViewModel.shared.handicap).disposed(by: disposeBag)
        
        RxKeyboard.instance.visibleHeight.drive(onNext: { [weak self] visibleHeight in
            self?.nextButtonBottomConstaint.constant = visibleHeight + 16
            //            self?.nextButton.frame.origin.y = ((self?.view.frame.height)! - willShowVisibleHeight) - 71.
            
            self?.view.setNeedsLayout()
            UIView.animate(withDuration: 0.1) {
                self?.view.layoutIfNeeded()
            }
        }).disposed(by: disposeBag)
    }
}



