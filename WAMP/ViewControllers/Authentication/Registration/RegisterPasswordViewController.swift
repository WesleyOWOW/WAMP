//
//  RegisterPasswordViewController.swift
//  We Are Matchplay
//
//  Created by Wesley Peeters on 16/12/2017.
//  Copyright © 2017 OWOW. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxKeyboard

class RegisterPasswordViewController: WampViewController {
    enum errorType {
        case passwordTooShort
        case none
    }
    
    @IBOutlet weak var errorHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var nextButtonBottomConstaint: NSLayoutConstraint!
    @IBOutlet weak var input: UITextField!
    @IBOutlet weak var nextButton: WampButton!
    var error: errorType = .passwordTooShort
    var isValid = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.input.delegate = self
        RxKeyboard.instance.visibleHeight.drive(onNext: { [weak self] visibleHeight in
            self?.nextButtonBottomConstaint.constant = visibleHeight + 16
            
            
            self?.view.setNeedsLayout()
            UIView.animate(withDuration: 0.1) {
                self?.view.layoutIfNeeded()
            }
        }).disposed(by: disposeBag)
        setupRX()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.input.becomeFirstResponder()
        self.input.text = RegisterViewModel.shared.password.value
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func backButtonPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func nextButtonPressed(_ sender: Any) {
        if isValid {
            self.performSegue(withIdentifier: "nextStep", sender: nil)
        } else {
            self.errorHeightConstraint.constant = 160
            self.makeViewDark()
            self.backButton.setImage(UIImage(named: "Backward arrow white"), for: .normal, animated: true)
            UIView.animate(withDuration: 0.2) {
                self.view.layoutIfNeeded()
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 5.0) {
                self.errorHeightConstraint.constant = 0
                self.makeViewLight()
                self.backButton.setImage(UIImage(named: "Backward arrow black"), for: .normal, animated: true)
                UIView.animate(withDuration: 0.2) {
                    self.view.layoutIfNeeded()
                }
            }
        }
    }
    
    func setupRX() {
        self.input.rx.text.orEmpty.bind(to: RegisterViewModel.shared.password).disposed(by: disposeBag)
        
        _ = RegisterViewModel.shared.passwordIsValid.asObservable().bind { [weak self] passwordIsValid in
            self?.isValid = passwordIsValid
        }.disposed(by: disposeBag)
        
       
    }
}

extension RegisterPasswordViewController {
    func textFieldShouldReturn(_ textField: UITextField!) -> Bool {
        textField.resignFirstResponder()
        if self.nextButton.isEnabled {
           self.nextButtonPressed(self)
        }
        
        return true
    }
}

