import UIKit
import SnapKit

class RegisterHomeCourseCheckViewController: WampViewController, UIScrollViewDelegate {
    var details: CourseDetails!
    var course: Course!
    @IBOutlet weak var buttonStack: UIStackView!
    @IBOutlet weak var backButton: UIButton!

    var hasmade = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hidesKeyboardOnTap = false
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.course = RegisterViewModel.shared.homeCourse
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.view.bringSubview(toFront: backButton)
        if !hasmade {
            make()
            self.hasmade = true
        }
        
    }
    
    func make() {
        let scrollview = UIScrollView(frame: .zero)
        
        
        
        let courseName = UILabel(frame: .zero)
        courseName.font = .bold(14)
        courseName.textColor = .black
        courseName.text = course.name
        
        let courseAddress = UILabel(frame: .zero)
        courseAddress.font = .medium(14)
        courseAddress.textColor = #colorLiteral(red: 0.7675911784, green: 0.7676092982, blue: 0.7675995827, alpha: 1)
        courseAddress.text = "\(course.city ?? "Unknown"), \(course.country ?? "Unknown")"
        
        let stack = UIStackView(arrangedSubviews: [courseName, courseAddress])
        stack.axis = .vertical
        stack.spacing = 5
        scrollview.addSubview(stack)
        stack.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(16)
            make.top.equalToSuperview().offset(5    )
        }
        
        let contentView = RoundedShadowRect(frame: .zero)
        scrollview.addSubview(contentView)
        contentView.snp.makeConstraints { make in
            make.leading.trailing.bottom.equalToSuperview().inset(16)
            make.top.equalTo(stack.snp.bottom).offset(16)
            make.width.equalToSuperview().offset(-32)
        }
        
        
        self.details = CourseDetails(course: self.course, frame: .zero)
        contentView.addSubview(self.details)
        self.details.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        self.view.addSubview(scrollview)
        scrollview.snp.makeConstraints { make in
            make.trailing.leading.bottom.equalToSuperview()
            make.top.equalTo(self.buttonStack.snp.bottom)
        }
    }
    
    @IBAction func backButtonPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func reportIncorrect(_ sender: Any) {
        let alert = UIAlertController(title: "Thanks!", message: "Thanks for reporting this course as incorrect! We'll take a look at it!", preferredStyle: .alert)
        self.present(alert, animated: true)
        alert.addAction(UIAlertAction(title: "You're welcome!", style: .default))
    }
    
    @IBAction func nextButtonPressed(_ sender: Any) {
        RegisterViewModel.shared.trySubmit() { result in
            switch result {
            case .success:
                self.performSegue(withIdentifier: "nextStep", sender: nil)
                
            case .failure:
                let (message, title): (String, String)
                title = "Whoops!"
                message = "An unknown error occured while creating your account"
                
                let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "I'll fix it", style: .default))
                self.present(alert, animated: true)
            }
        }
       
        
    }
}
