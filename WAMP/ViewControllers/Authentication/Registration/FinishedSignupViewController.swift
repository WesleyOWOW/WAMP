//
//  FinishedSignupViewController.swift
//  We Are Matchplay
//
//  Created by Alex Pedersen on 12/12/2017.
//  Copyright © 2017 OWOW. All rights reserved.
//

import UIKit
import SAConfettiView

class FinishedSignupViewController: WampViewController {
    var finishButton: UIButton!
    @IBOutlet weak var letsGoButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        makeViewDark()


        
        self.finishButton = UIButton(frame: self.letsGoButton.frame)
        finishButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.letsgo)))
        
        let confetti = SAConfettiView(frame: self.view.bounds)
        confetti.type = .Confetti
        self.view.addSubview(confetti)
        self.view.addSubview(finishButton)
        confetti.startConfetti()
        self.view.bringSubview(toFront: self.letsGoButton)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func letsgo() {
//        self.dismiss(animated: true, completion: nil)
        WampCameraViewModel.shared.reset()
    }
    
    @IBAction func letsGoButtonPressed(_ sender: Any) {
        self.performSegue(withIdentifier: "nestStep", sender: nil)
        WampCameraViewModel.shared.reset()
    }
}
