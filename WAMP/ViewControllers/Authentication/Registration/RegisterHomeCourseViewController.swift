import UIKit
import RxSwift
import RxCocoa
import RxKeyboard
import Alamofire
import CoreLocation



class RegisterHomeCourseViewController: WampViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var noLocation: UIView!
    var courses: [Course] = []
    var locationManager: CLLocationManager = CLLocationManager()
    var currentLocation : CLLocation!
    var search: String?
    var searchTextField: UITextField!
    var tableView: UITableView = UITableView()
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
         self.hidesKeyboardOnTap = false
        locationManager.requestAlwaysAuthorization()
        if CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways || CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
            updateLocation()
        } else {
            tableView.separatorStyle = .none
            noLocation.isHidden = false
        }
        
        self.view.addSubview(tableView)
        tableView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        self.view.sendSubview(toBack: tableView)
        tableView.register(CourseTableViewCell.self, forCellReuseIdentifier: CourseTableViewCell.identifier)
        tableView.contentInset = UIEdgeInsetsMake(48, 0, 0, 0)
        let header = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 48))
        let text = UILabel()
        header.backgroundColor = UIColor(hex: "FCFCFC")
        text.font = .bold(32)
        text.textColor = .black
        text.numberOfLines = 0
        header.addSubview(text)
        text.text = "Homecourse"
        
        tableView.tableHeaderView = header
        
        text.snp.makeConstraints { make in
            make.right.equalToSuperview()
            make.left.equalToSuperview().offset(16)
            make.top.equalToSuperview()
            make.bottom.equalToSuperview().offset(16)
        }
       
       
    }
    
    func updateLocation() {
        locationManager.requestAlwaysAuthorization()
        self.currentLocation = self.locationManager.location
        self.refreshLocation()
        self.update()
    }
    
    func refreshLocation() {
        locationManager.requestAlwaysAuthorization()
        
        if CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways {
            self.currentLocation = self.locationManager.location
        }
    }
    
    func update() {
        
        if let searchText = search, searchText.count > 0 {
            self.noLocation.isHidden = true
            _ = Course.find(query: searchText, callback: self.makeClosestCourses)
        } else if let currentLocation = currentLocation {
             if CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways || CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
                self.noLocation.isHidden = true
            _ = Course.findNearby(latitude: currentLocation.coordinate.latitude, longitude: currentLocation.coordinate.longitude, callback: self.makeClosestCourses)
                
             } else {
                courses = []
                self.noLocation.isHidden = false
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tableView.delegate = self
        self.tableView.dataSource = self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    
    @IBAction func backButtonPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func setupRX() {
        _ = self.searchTextField.rx.text.orEmpty.throttle(0.3, scheduler: MainScheduler.instance).distinctUntilChanged().bind { [unowned self] text in
            self.search = nil
            
            if text.count > 2 {
                self.search = text
            }
            
            self.update()
        }
    }
    
    func makeClosestCourses(result: Result<[Course]>) {
        switch result {
        case .success(let courses):
            self.courses = courses
            
            self.tableView.reloadData()
        case .failure(_):
            return
        }
    }
    

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let searchView = UIView()

        let searchBack = UIView(frame: .zero)
        searchBack.backgroundColor = UIColor(hex: "F2F2F2")
        searchBack.layer.cornerRadius = 5
        searchBack.clipsToBounds = true
        searchView.backgroundColor = UIColor(hex: "FCFCFC")
        searchView.addSubview(searchBack)
        searchBack.snp.makeConstraints { make in
            make.height.equalTo(32)
            make.edges.equalToSuperview().inset(16)
        }

        let searchImage = UIImageView(image: UIImage(named: "searchIcon"))
        searchBack.addSubview(searchImage)
        searchImage.snp.makeConstraints { make in
            make.size.equalTo(15)
            make.centerY.equalToSuperview()
            make.leading.equalToSuperview().offset(16)
        }

        searchTextField = UITextField(frame: .zero)
        searchTextField.placeholder = "Search for Courses"
        searchTextField.font = .medium(16)

        searchBack.addSubview(searchTextField)
        searchTextField.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.leading.equalTo(searchImage.snp.trailing).offset(12)
        }

        setupRX()

        return searchView
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 64
    }
//
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let course = courses[indexPath.row]
        select(course)
        tableView.deselectRow(at: indexPath, animated: false)
    }
    
    
    @IBAction func openSettings(_ sender: Any) {
        if let appSettings = NSURL(string: UIApplicationOpenSettingsURLString) {
            UIApplication.shared.openURL(appSettings as URL)
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let course = self.courses[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: CourseTableViewCell.identifier, for: indexPath) as! CourseTableViewCell
        
         if (CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways || CLLocationManager.authorizationStatus() == .authorizedWhenInUse) && self.currentLocation != nil {
        cell.make(course, location: self.currentLocation.coordinate)
         } else {
            
            cell.make(course, location: nil)
        }
        return cell
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func select (_ course: Course) {
        print("select")
        RegisterViewModel.shared.homeCourse = course
        self.performSegue(withIdentifier: "nextStep", sender: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         if CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways || CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
            return courses.count
        }
        
        if courses.count > 0 {
            self.noLocation.isHidden = true
        } else {
            self.noLocation.isHidden = false
        }
        
        return courses.count
    }
}




