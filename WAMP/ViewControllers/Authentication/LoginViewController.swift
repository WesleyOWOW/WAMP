import UIKit
import RxCocoa
import RxSwift
import Alamofire
import SwiftyJSON
import RxKeyboard
import FacebookLogin

class LoginViewController: WampViewController {
    var viewModel: LoginViewModel = LoginViewModel()
    var canLogin: Bool = false
    
    // Mark - Input fields
    @IBOutlet weak var usernameInput: UITextField!
    @IBOutlet weak var passwordInput: UITextField!
    
    // Mark - Buttons
    @IBOutlet weak var loginButton: WampButton!
    @IBOutlet weak var facebookLoginButton: WampButton!
    
    // Mark - Constraints
    @IBOutlet weak var loginButtonBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var inputFieldsBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var ErrorHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var logoTopConstraint: NSLayoutConstraint!
    
    // Mark - Overrides
    override func viewDidLoad() {
        super.viewDidLoad()
        self.rxSwift()
        self.makeViewDark()
        self.usernameInput.delegate = self
        self.passwordInput.delegate = self
    }
    
    @IBAction func login(_ sender: Any) {
        if canLogin {
            [passwordInput, usernameInput].forEach({ $0?.resignFirstResponder() })
            viewModel.tryLogin() { result in
                switch result {
                case .success:
                    self.navigationController!.dismiss(animated: true, completion: nil)
                case .failure(let error):
                    
                    let alert = UIAlertController(title: "Whoops!",  message: "Those details weren't correct!", preferredStyle: .alert)
                    [UIAlertAction(title: "Close", style: .cancel)].forEach(alert.addAction)
                    self.present(alert, animated: true)
                }
            }
        } else {
            let alert = UIAlertController(title: "Whoops!",  message: "Please fill in your login details!", preferredStyle: .alert)
            [UIAlertAction(title: "Close", style: .cancel)].forEach(alert.addAction)
            self.present(alert, animated: true)
        }
    }
    
    func passwordWrong() {
        self.ErrorHeightConstraint.constant = 160
        UIView.animate(withDuration: 0.2) {
            self.view.layoutIfNeeded()
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 5.0) {
            self.ErrorHeightConstraint.constant = 0
            UIView.animate(withDuration: 0.2) {
                self.view.layoutIfNeeded()
            }
        }
        
        self.passwordInput.text = ""
        self.passwordInput.becomeFirstResponder()
    }
    
    
    @IBAction func btnFBLoginPressed(_ sender: AnyObject) {
        let loginManager = LoginManager()
        loginManager.logIn(readPermissions: [ .publicProfile, .email ], viewController: self) { loginResult in
            switch loginResult {
            case .failed(let error):
                print(error)
            case .cancelled:
                print("User cancelled login.")
            case .success(let grantedPermissions, let declinedPermissions, let accessToken):
                print("Logged in!")
                Alamofire.request(
                    api_url + "/socialite",
                    method: .post,
                    parameters: [
                        "driver": "facebook",
                        "token": accessToken.authenticationToken
                    ],
                    encoding: JSONEncoding.default)
                    .validate()
                    .responseJSON { response in
                        switch response.result {
                        case .failure(let error):
                            log.error("fb login error:", error.localizedDescription)
                        case .success(let value):
                            let json = JSON(value)
                            if json["social_id"] != nil {
                                let socialid = json["social_id"].stringValue
                                print("socialite", json)
                                RegisterViewModel.shared.email.value = json["email"].stringValue
                                let names = json["name"].stringValue.characters.split(separator: " ", maxSplits:1).map(String.init)
                                RegisterViewModel.shared.firstname.value = names[0]
                                RegisterViewModel.shared.lastname.value = names[1]
                                RegisterViewModel.shared.socialID = socialid
                                RegisterViewModel.shared.type = .facebook
                                Alamofire.request(json["avatar"].stringValue).responseImage { response in
                                    if let image = response.result.value {
                                        print("image", image)
                                        let photo = Photo(image: image)
                                        WampCameraViewModel.shared.currentPhoto.value = photo
                                        RegisterViewModel.shared.profilePicture.value = image
                                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "pickUsername") as! RegisterUsernameViewController
                                        let transition = CATransition()
                                        transition.duration = 0.5
                                        transition.type = kCATransitionPush
                                        transition.subtype = kCATransitionFromRight
                                        transition.timingFunction = CAMediaTimingFunction(name:kCAMediaTimingFunctionEaseInEaseOut)
                                        self.view.window!.layer.add(transition, forKey: kCATransition)
                                        self.present(vc, animated: false, completion: nil)
                                    }
                                }
                                
                            } else if json["api_token"] != nil {
                                AuthenticationManager.shared.token = json["api_token"].stringValue
                                keychain["token"] = json["api_token"].stringValue
                                log.info("Authentication token saved in keychain. Facebook login success!")
                                AuthenticationManager.shared.isLoggedin = true
                                self.viewModel.loginComplete.value = true
                                self.navigationController?.dismiss(animated: true, completion: nil)
                            }
                            
                        }
                }
            }
        }
    }
}

// Mark - UITextFieldDelegate
extension LoginViewController {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.placeholder = ""
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text == "" {
            if textField == usernameInput {
                textField.placeholder = "Email / Username"
            } else {
                textField.placeholder = "Password"
            }
        }
    }
}

// Mark - RxSwift
extension LoginViewController {
    func rxSwift() {
        self.usernameInput.rx.text.orEmpty.bind(to: self.viewModel.username).disposed(by: self.disposeBag)
        self.passwordInput.rx.text.orEmpty.bind(to: self.viewModel.password).disposed(by: self.disposeBag)
        
        self.viewModel.passwordWrong.asObservable().bind { [weak self] passwordWrong in
            if passwordWrong {
                self?.passwordWrong()
            }
        }.disposed(by: self.disposeBag)
        
        self.viewModel.canLogin.asObservable().bind { [weak self] canLogin in
            self?.loginButton.isEnabled = true
            self?.passwordInput.enablesReturnKeyAutomatically = true
            self?.canLogin = canLogin
        }.disposed(by: self.disposeBag)
        
        self.viewModel.loginComplete.asObservable().bind { [weak self] loginComplete in
            guard loginComplete else { return }
            self?.navigationController!.dismiss(animated: true, completion: nil)
        }.disposed(by: self.disposeBag)
        
        RxKeyboard.instance.visibleHeight.drive(onNext: { [weak self] visibleHeight in
            
            if visibleHeight > 1 {
                self?.logoTopConstraint.constant = 45
                self?.loginButtonBottomConstraint.constant = visibleHeight + 48
                self?.inputFieldsBottomConstraint.constant = visibleHeight + 180
            } else {
                self?.logoTopConstraint.constant = 103
                self?.loginButtonBottomConstraint.constant = 161
                self?.inputFieldsBottomConstraint.constant = 304
            }
            
    
            UIView.animate(withDuration: 0.1) {
                self?.view.layoutIfNeeded()
            }
        }).disposed(by: disposeBag)
    }
    
}
