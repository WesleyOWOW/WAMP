import UIKit
import SnapKit

enum CreationMode {
    case create
    case edit
}

class CourseCreationViewController: UIViewController {
    var mode: CreationMode = .edit
    var hasMade = false
    var course: Course!
    
    @IBOutlet weak var editHeightConstraints: NSLayoutConstraint!
    @IBOutlet weak var courseName: UILabel!
    @IBOutlet weak var courseLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if mode == .edit {
            self.course = RegisterViewModel.shared.homeCourse
            self.courseName.text = course.name
            self.courseLabel.text = "\(course.city ?? "Unknown"), \(course.country ?? "Unknown")"
        } else {
            editHeightConstraints.constant = 0
        }
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if !hasMade, let course = self.course, mode == .edit {
            make(course)
            self.hasMade = true
        }
    }
    
    @IBAction func backButtonPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func make(_ course: Course) {
        let scrollView = ClickThroughScrollView(frame: .zero)
        self.view.addSubview(scrollView)
        scrollView.snp.makeConstraints { make in
            make.edges.size.equalToSuperview()
        }
        
        let contentView = RoundedShadowRect(frame: .zero)
        scrollView.addSubview(contentView)
        contentView.snp.makeConstraints { make in
            make.trailing.leading.equalToSuperview().inset(16)
            make.top.equalToSuperview().offset(225)
            make.width.equalToSuperview().offset(-32)
        }
        
        let creator = CourseCreator(course: course, frame: .zero)
        contentView.addSubview(creator)
        creator.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }

}
