//
//  ChooseNameViewController.swift
//  WAMP
//
//  Created by Wesley Peeters on 14/01/2018.
//  Copyright © 2018 Robbert Brandsma. All rights reserved.
//

import UIKit

//
//  RegisterFirstNameViewController.swift
//  We Are Matchplay
//
//  Created by Wesley Peeters on 18/12/2017.
//  Copyright © 2017 OWOW. All rights reserved.
//


import UIKit
import RxSwift
import RxCocoa
import RxKeyboard

class ChooseName: WampViewController {
    
    @IBOutlet weak var backgroundTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var input: UITextField!
    @IBOutlet weak var nextButton: WampButton!
    @IBOutlet weak var nextButtonBottomConstaint: NSLayoutConstraint!
    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var errorHeightConstraint: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupRX()
        self.input.delegate = self
        self.input.becomeFirstResponder()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.input.becomeFirstResponder()
        self.input.text = CreateGuestViewModel.shared.name.value
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func backButtonPressed(_ sender: Any) {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func nextButtonPressed(_ sender: Any) {
        if self.input.text == "" {
            self.errorHeightConstraint.constant = 160
            self.makeViewDark()
            self.backButton.setImage(UIImage(named: "Backward arrow white"), for: .normal, animated: true)
            UIView.animate(withDuration: 0.2) {
                self.view.layoutIfNeeded()
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 5.0) {
                self.errorHeightConstraint.constant = 0
                self.makeViewLight()
                self.backButton.setImage(UIImage(named: "Backward arrow black"), for: .normal, animated: true)
                UIView.animate(withDuration: 0.2) {
                    self.view.layoutIfNeeded()
                }
            }
        } else {
             self.performSegue(withIdentifier: "nextStep", sender: nil)
        }
       
    }
    
    func setupRX() {
        self.input.rx.text.orEmpty.bind(to: CreateGuestViewModel.shared.name).disposed(by: disposeBag)

        
        RxKeyboard.instance.visibleHeight.drive(onNext: { [weak self] visibleHeight in
            self?.nextButtonBottomConstaint.constant = visibleHeight + 16
            
          
                self?.backgroundTopConstraint.constant = -visibleHeight + (visibleHeight / 1.3)
          
            
            self?.view.setNeedsLayout()
            UIView.animate(withDuration: 0.1) {
                self?.view.layoutIfNeeded()
            }
        }).disposed(by: disposeBag)
    }
}

extension ChooseName {
    func textFieldShouldReturn(_ textField: UITextField!) -> Bool {
        textField.resignFirstResponder()
        if self.nextButton.isEnabled {
            self.nextButtonPressed(self)
        }
        
        return true
    }
}

