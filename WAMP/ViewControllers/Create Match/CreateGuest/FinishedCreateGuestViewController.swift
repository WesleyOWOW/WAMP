//
//  FinishedCreateGuestViewController.swift
//  WAMP
//
//  Created by Wesley Peeters on 14/01/2018.
//  Copyright © 2018 Robbert Brandsma. All rights reserved.
//

import UIKit


class FinishedCreateGuestViewController: WampViewController {

    @IBOutlet weak var profilePicture: UIImageView!
    @IBOutlet weak var handicapLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var addPhotoButton: UIButton!
    
    private var onCreateGuest: ((Guest) -> ())?
    private var hasPhoto: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupRx()

        self.nameLabel.text = CreateGuestViewModel.shared.name.value
        self.handicapLabel.text = CreateGuestViewModel.shared.handicap.value
        
        addPhotoButton.addAction {
//            WampCameraViewModel.shared.amount = .single
//            self.performSegue(withIdentifier: "Camera", sender: nil)
        }
        
        addPhotoButton.isHidden = true

    }

    @IBAction func shareWhatsappButtonPressed(_ sender: Any) {
        let escapedText = "Hi there! I'm playing a match with WAMP. Download it here: https://itunes.apple.com/app/id1221831832".addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
        let whatsappURL = URL(string: "whatsapp://send?text=\(escapedText)")!
        if UIApplication.shared.canOpenURL(whatsappURL) {
            UIApplication.shared.open(whatsappURL)
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.view.bringSubview(toFront: addPhotoButton)
    }
    
    @IBAction func addPictureButtonPressed(_ sender: Any) {
        WampCameraViewModel.shared.amount = .single
        self.performSegue(withIdentifier: "Camera", sender: nil)
    }
    
    @IBAction func backButtonPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func saveButtonPressed(_ sender: Any) {
        var picture = self.profilePicture.image
        
        if picture == UIImage(named: "AddPhoto_blue") {
            picture = UIImage(named: "Avatar Placeholder")
        }
        let alert = UIAlertController(title: nil, message: "Creating guest user...", preferredStyle: .alert)
        
        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        loadingIndicator.startAnimating();
        
        alert.view.addSubview(loadingIndicator)
        present(alert, animated: true, completion: nil)
        
        Guest.create(firstName: CreateGuestViewModel.shared.name.value, lastName: "",
                     handicap: Double(CreateGuestViewModel.shared.handicap.value)!,
                     photo: self.profilePicture.image)
            .subscribe(onNext: { guest in
                self.dismiss(animated: true, completion: nil)
                self.onCreateGuest?(guest)
                CreateGuestViewModel.shared.guest.value = guest
               
                CreateGuestViewModel.shared.reset()
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute:
                    {
                        self.navigationController!.dismiss(animated: true, completion: nil)
                })
                
                
            },
                       onError: { error in
                    
                        let alert = UIAlertController(title: "An error has occured while creating the guest account", message: "\((error as NSError).localizedDescription)", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                        self.present(alert, animated: true)
            }).addDisposableTo(disposeBag)
    }
    
    private func setupRx() {
        _ = WampCameraViewModel.shared.currentPhoto.asObservable().bind { [weak self] currentPhoto in
                if currentPhoto != nil, currentPhoto?.image != nil {
                    CreateGuestViewModel.shared.profilePicture.value = currentPhoto?.image
                    self?.profilePicture.image = CreateGuestViewModel.shared.profilePicture.value!
                } else {
                    self?.profilePicture.image = UIImage(named: "Avatar Placeholder")
                }
//            WampCameraViewModel.shared.currentPhoto.value = nil
            
            }.disposed(by: disposeBag)
    }
}
