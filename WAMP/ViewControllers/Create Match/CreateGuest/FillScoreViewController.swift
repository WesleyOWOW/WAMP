import UIKit

class FillScoreViewController: WampViewController, ScoreCardEditorDelegate {

    
    @IBOutlet weak var heightConstant: NSLayoutConstraint!
    
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    @IBOutlet weak var scoreEditor: ScoreCardEditor!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hidesKeyboardOnTap = false
        scoreEditor.delegate = self
        if  CreateMatchViewModel.shared.match?.players.count == 4 {
            heightConstant.constant = 296
            topConstraint.constant = -35
        } else {
              topConstraint.constant = 20
             heightConstant.constant = 248
        }
        
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        scoreEditor.displayKeyboard()
        CreateMatchViewModel.shared.match?.initializeHoles()
        scoreEditor.selectedIndex.value = (player: 0, hole: 0)
        scoreEditor.updateButtons()
    }
    
    @IBAction func backButtonPressed(_ sender: Any) {
        scoreEditor.hideScoreEntryKeyboard()
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func closeButtonPressed(_ sender: Any) {
        scoreEditor.hideScoreEntryKeyboard()
        let alert = UIAlertController(title: "Close match?",
                                      message: "Are you sure you want to close this match? The match will be deleted.",
                                      preferredStyle: .alert)
        [
            UIAlertAction(title: "Close & delete match", style: .default) { _ in
                ConceptMatch.deleteSavedConcept()
                BaseViewController.shared.showFeed(self)
                self.navigationController!.popToRootViewController(animated: true)
            },

            UIAlertAction(title: "Cancel", style: .cancel) { _ in
                self.scoreEditor.displayKeyboard()
            }
            ].forEach(alert.addAction)
        
        self.present(alert, animated: true)
    }
    
    func scoreCardEditorKeyboardDidAppear() {
        //
    }
    
    func scoreCardEditorKeyboardDidDisappear() {
        //
    }
    
    func scoreCardEditorDidAddScore() {
        if let calculated =  CreateMatchViewModel.shared.match?.calculateScore() {
            CreateMatchViewModel.shared.match?.score = calculated.score
             CreateMatchViewModel.shared.match?.winningTeam = calculated.winningTeam
        }
        
        if !(CreateMatchViewModel.shared.match?.otherCanStillWin)! {
            scoreEditor.hideScoreEntryKeyboard()
            self.performSegue(withIdentifier: "endGame", sender: nil)
            
//            self.perform(segue: StoryboardSegue.CreateGame.gameFinished)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
