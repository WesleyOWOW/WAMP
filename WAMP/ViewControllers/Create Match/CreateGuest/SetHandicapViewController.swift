//
//  RegisterFirstNameViewController.swift
//  We Are Matchplay
//
//  Created by Wesley Peeters on 18/12/2017.
//  Copyright © 2017 OWOW. All rights reserved.
//


import UIKit
import RxSwift
import RxCocoa
import RxKeyboard

class SetHandicapViewController: WampViewController {
    
    @IBOutlet weak var input: UITextField! {
        didSet {
             self.input.delegate = self
        }
    }
    @IBOutlet weak var nextButton: WampButton!
    @IBOutlet weak var nextButtonBottomConstaint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupRX()
        self.input.becomeFirstResponder()
        self.input.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
    }
    
    func textFieldDidChange(_ textField: UITextField) {
        if let intValue = Double(textField.text!), intValue > 54.00 {
            textField.text = "54"
            CreateGuestViewModel.shared.handicap.value = "54"
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if string.isEmpty {
            return true
        }
        
        let countdots = textField.text!.components(separatedBy: ".").count - 1
        
        if countdots > 0 && (string == "." || string == ",")
        {
            return false
        }
        
        let decimals = textField.text!.components(separatedBy: ".")
        
        if decimals.count > 1, decimals[1].count > 0 {
            return false
        }
        
        
        
        let aSet = NSCharacterSet(charactersIn:"0123456789,.").inverted
        let compSepByCharInSet = string.components(separatedBy: aSet)
        let numberFiltered = compSepByCharInSet.joined(separator: "")
        if string == "," {
            textField.text! += "."
            return false
        }
        return string == numberFiltered
        
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        self.input.becomeFirstResponder()
        self.input.text = CreateGuestViewModel.shared.handicap.value
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func backButtonPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func nextButtonPressed(_ sender: Any) {
        self.performSegue(withIdentifier: "nextStep", sender: nil)
    }
    
    
    @IBOutlet weak var backgroundTopConstraint: NSLayoutConstraint!
    
    func setupRX() {
        self.input.rx.text.orEmpty.bind(to: CreateGuestViewModel.shared.handicap).disposed(by: disposeBag)
        RxKeyboard.instance.visibleHeight.drive(onNext: { [weak self] visibleHeight in
            self?.nextButtonBottomConstaint.constant = visibleHeight + 16
            
            
            self?.backgroundTopConstraint.constant = -visibleHeight + (visibleHeight / 1.3)
            
            
            self?.view.setNeedsLayout()
            UIView.animate(withDuration: 0.1) {
                self?.view.layoutIfNeeded()
            }
        }).disposed(by: disposeBag)
    }
}

extension SetHandicapViewController {
    func textFieldShouldReturn(_ textField: UITextField!) -> Bool {
        textField.resignFirstResponder()
        if self.nextButton.isEnabled {
            self.nextButtonPressed(self)
        }
        
        return true
    }
}


