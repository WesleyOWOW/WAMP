import UIKit
import SnapKit
import RxSwift
import SVProgressHUD
import RxKeyboard
import RxSwift
import DatePickerDialog
import UITextView_Placeholder

class PreplayedMatchPrepostPreviewViewController: CreateMatchBaseViewController, UITextViewDelegate {
    
    override var tabTitle: String { return "" }
    var match: ConceptMatch!
    
    var photos: [Photo] {
        get {
            return match.photos
        }
        set {
            match.photos = newValue
        }
    }
    
    var allowMultiplePhotos: Bool {
        return true
    }
    
    @IBOutlet var backgroundView: UIView!
    @IBOutlet var contentView: UIView!
    @IBOutlet var scrollView: UIScrollView!
    
    @IBOutlet weak var editPhotosButtonHeight: NSLayoutConstraint!
    
    @IBOutlet var scoreHeader: ScoreView!
    @IBOutlet var matchDateLabel: UILabel!
    @IBOutlet var hideDateButton: UIButton!
    @IBOutlet var editDateButton: UIButton!
    @IBOutlet var holeSummary: MiniGameHolesSummaryView!
    @IBOutlet var courseLabel: UILabel!
    @IBOutlet var photoView: MatchPhotoView!
    @IBOutlet var commentBox: UITextView! { didSet { commentBox.delegate = self } }
    
    @IBOutlet weak var editPhotosButton: UIView!
    
    @IBOutlet weak var bottom2: NSLayoutConstraint!
    @IBOutlet weak var bottom: NSLayoutConstraint!
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var holeSpacer: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.match =  CreateMatchViewModel.shared.match
        self.hidesKeyboardOnTap = false
        self.scrollView.contentInset = UIEdgeInsetsMake(64, 0, 0, 0)
        
        courseLabel.text = match.course?.name ?? "Unknown course"
        
        if let summary = match.summarizeHoles() {
            holeSummary.setup(values: summary)
        } else {
            topspacer.isHidden = true
            bottomspacer.isHidden = true
            holeSummary?.isHidden = true
            holeSpacer.isHidden = true
        }
        
        scoreHeader.canMove = false
        
        
        scoreHeader.load(match)
        
    
        
        updateDateLabel()
        
        editPhotosButton.addAction {
            self.editPhotos()
        }
        
        [addPhotosButton, addPhotosText].forEach( { $0.addAction { [unowned self] in
            self.addPhotos()
        }})
       
    
//        let view = UIView(frame: .zero)
//        view.backgroundColor = UIColor(hex: "000000", with: 0.05)
//        backgroundView.addSubview(view)
//        view.snp.makeConstraints { make in
//            make.leading.trailing.equalToSuperview()
//            make.height.equalTo(1)
//            make.top.equalTo(scoreHeader.snp.bottom)
//        }
        
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }

    @IBOutlet weak var addPhotosButton: UIButton!
    @IBOutlet weak var addPhotosText: UIButton!
    
    @IBAction func backButtonPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBOutlet weak var scoreHeaderHeight: NSLayoutConstraint!
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        RxKeyboard.instance.visibleHeight
            .drive(onNext: { height in
                print("keyboard", height)
                 self.scrollView.contentOffset.y += (height/2)+10
                 self.scrollView.contentInset = UIEdgeInsetsMake(64, 0, height, 0)
                print("offset", self.scrollView.contentInset)
            })
            .disposed(by: disposeBag)
    }
    
    @IBOutlet weak var topspacer: UIView!
    @IBOutlet weak var bottomspacer: UIView!
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        photos = WampCameraViewModel.shared.allPhotos.value
        if photos.count > 0 {
            photoView.isHidden = false
            editPhotosButtonHeight.constant = 56
        } else {
            photoView.isHidden = true
            editPhotosButtonHeight.constant = 0
        }
        
        photoView.load(match)
        if match.players.count == 4 {
            scoreHeaderHeight.constant = 96
            scoreHeader.frame.size.height = 96
            scoreHeader.snp.makeConstraints { make in
                make.height.equalTo(scoreHeader)
            }
        }
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
        
        RxKeyboard.instance.visibleHeight
            .drive(onNext: { keyboardVisibleHeight in
                self.scrollView.contentInset.bottom = keyboardVisibleHeight
            })
            .disposed(by: disposeBag)
    }
    
    @IBAction func toggleDateHidden() {
        if match.date == nil {
            match.date = Date()
            editDateButton.isHidden = false
        } else {
            match.date = nil
    
            editDateButton.isHidden = true
        }
        
        updateDateLabel()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.placeholder = ""
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        textView.placeholder = ""
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            textView.placeholder = "Add a comment"
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        if textView.text.count > 500 {
            self.displayError("please make sure your comment stays under 500 characters.")
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text == "" {
            textField.placeholder = "Add a comment"
        }
    }
    
    func updateDateLabel() {
        if let date = match.date {
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            formatter.dateStyle = .medium
            formatter.timeStyle = .none
            let formattedDate = formatter.string(from: date)
            self.matchDateLabel.text = formattedDate
            self.hideDateButton.setTitle("Hide", for: .normal)
        } else {
            self.matchDateLabel.text = "Date hidden"
            self.hideDateButton.setTitle("Show", for: .normal)
        }
    }
    
    @IBAction func addPhotos() {
        keychain["multiPic"] = "YES"
        WampCameraViewModel.shared.amount = .multiple
//        FeedViewModel.shared.pop = 3
        self.performSegue(withIdentifier: "camera", sender: nil)
        
    }
    
    
    @IBAction func post(_ sender: Any) {
        
        self.postMatch()
    }
    
    func editPhotos() {
//        FeedViewModel.shared.pop = 1
        self.performSegue(withIdentifier: "editPics", sender: nil)
        
    }
    
    
    
    
    func postMatch() {
        guard photos.count > 0 else {
            displayError("Take or select one or more pictures to post your match.", title: "Picture time!")
            return
        }
        
        match.text = commentBox.text ?? ""
        keychain["multiPic"] = "NO"
        let alert = UIAlertController(title: nil, message: "Posting match...", preferredStyle: .alert)
        
        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        loadingIndicator.startAnimating();
        
        alert.view.addSubview(loadingIndicator)
        present(alert, animated: true, completion: nil)
        
        match.post().subscribeOn(MainScheduler.instance).subscribe(
            onNext: { progress in
                
        },
            onError: { error in
                self.dismiss(animated: true, completion: nil)
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute:
                    {
                self.displayError("Connection to the WAMP servers was lost. Please try again.")
                })
               
                log.error("Post match error: \(error)")
        },
            onCompleted: {
                self.dismiss(animated: true, completion: nil)
                BaseViewController.shared.showFeed(self)
                BaseViewController.shared.feedViewController.reload()
                BaseViewController.shared.feedViewController.table.scrollToTop(animated: false)
               WampCameraViewModel.shared.reset()
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute:
                    {
            self.navigationController?.popToRootViewController(animated: true)
                })
        }
            )
            .addDisposableTo(disposeBag)
        
        
    }
    
    @IBAction func editDate() {
        DatePickerDialog().show(
            "Match date",
            doneButtonTitle: "Done",
            cancelButtonTitle: "Cancel",
            defaultDate: self.match.date ?? Date(),
            datePickerMode: .date
        ) { [unowned self] date in
            guard let date = date else {
                return
            }
            
            self.match.date = date
            self.updateDateLabel()
        }
    }
    
    override func viewWillAppearWrapped(_ navigationWrapper: NavigationWrapperViewController, animated: Bool) {
        super.viewWillAppearWrapped(navigationWrapper, animated: animated)
        
        navigationWrapper.rightButton.setTitle("Post", for: .normal)
        navigationWrapper.rightButton.addAction { [unowned self] in
            self.postMatch()
        }
        
        navigationWrapper.contentView.snp.makeConstraints { make in
//            make.top.equalToSuperview().offset(60).constraint
        }
    }
    
    func displayError(_ text: String, title: String = "An error has occurred") {
        let controller = UIAlertController(title:title, message: text, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default)
        controller.addAction(action)
        
        self.present(controller, animated: true)
    }
}
