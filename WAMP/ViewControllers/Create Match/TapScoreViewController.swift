//
//  TapScoreViewController.swift
//  WAMP
//
//  Created by Wesley Peeters on 02/03/2018.
//  Copyright © 2018 WAMP - We Are Matchplay. All rights reserved.
//

import UIKit

class TapScoreViewController: WampViewController {

    @IBOutlet weak var tapheader: EditableScoreHeader!
    override func viewDidLoad() {
        super.viewDidLoad()

        tapheader.setup(match: CreateMatchViewModel.shared.match!)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backButtonPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
