import UIKit

class SetTeeboxViewController: WampViewController {
    var match = CreateMatchViewModel.shared.match
    var currentSelection = 0
    var picker: UIPickerView!
    var toolBar: UIToolbar!
    var teeButtons: [UITextField] = []
    var isGoing: Bool = false
    @IBOutlet weak var contentView: RoundedShadowRect!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        picker = UIPickerView()
        picker.dataSource = self
        picker.delegate = self
        picker.backgroundColor = .white
        picker.showsSelectionIndicator = true
        toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 76/255, green: 217/255, blue: 100/255, alpha: 1)
        toolBar.sizeToFit()
        

        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donePicker))
        
        toolBar.setItems([spaceButton, cancelButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
         makePlayerRows()
    }
    
    @objc func donePicker (sender:UIBarButtonItem) {
        self.view.resignFirstResponder()
        teeButtons[currentSelection].resignFirstResponder()
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
       
    }
    
    @IBAction func nextButtonPressed(_ sender: Any) {
        guard !isGoing  else {
            return
        }
        
        self.performSegue(withIdentifier: "NEXT", sender: nil)
        
        self.isGoing = true
    }
    
    @IBAction func backButtonPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func makePlayerRows() {
        
        var playerViews: [UIView] = []
        for (index, player) in (CreateMatchViewModel.shared.match?.players.enumerated())! {
            let playerView = UIView(frame: .zero)
            
            let nameLabel = UILabel(frame: .zero)
            nameLabel.font = .bold(14)
            
            var lastname = player.profile.lastName
            if (player.profile.lastName.count > 9) {
                lastname = String(player.profile.lastName.prefix(6)) + "..."
            }
            nameLabel.text = player.profile.firstName[0] + ". " + lastname
            nameLabel.textAlignment = .left
            nameLabel.setContentHuggingPriority(230, for: .horizontal)
            nameLabel.lineBreakMode = .byClipping
            nameLabel.textColor = .white
            
        
            playerView.addSubview(nameLabel)
            nameLabel.snp.makeConstraints { make in
                make.bottom.top.equalToSuperview().inset(26)
                make.leading.equalToSuperview().offset(16)
            }
            
            // -- teebox buttons
            
            let teeboxButton = UITextField()
            teeboxButton.text = match?.course!.tees[0].name
            teeboxButton.font = .medium(14)
            teeboxButton.textColor = .black
            teeboxButton.contentHorizontalAlignment = .left
            teeboxButton.addAction {
                print("Selected b")
                self.currentSelection = index
                teeboxButton.becomeFirstResponder()
            }
            teeButtons.append(teeboxButton)
            teeboxButton.inputAccessoryView = toolBar
            teeboxButton.inputView = picker
            
            playerView.addSubview(teeboxButton)
            teeboxButton.snp.makeConstraints { make in
                make.bottom.top.equalToSuperview().inset(26)
                make.leading.equalToSuperview().offset(153)
            }
            
            let sep = UIView(frame: .zero)
            sep.backgroundColor = .black
            sep.alpha = 0.05
            
            playerView.addSubview(sep)
            sep.snp.makeConstraints { make in
                make.bottom.trailing.leading.equalToSuperview()
                make.height.equalTo(1)
            }
            
            playerViews.append(playerView)
        }
        
        let playerStack = UIStackView(arrangedSubviews: playerViews)
        playerStack.axis = .vertical
        playerStack.spacing = 0
        
        self.contentView.addSubview(playerStack)
        playerStack.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
    
   
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

}

extension SetTeeboxViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return (CreateMatchViewModel.shared.match?.course?.tees.count)!
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String! {
        let newValue =  CreateMatchViewModel.shared.match?.course?.tees[row]
        return newValue?.name
    }
    
    
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let newValue =  CreateMatchViewModel.shared.match?.course?.tees[row]
        teeButtons[currentSelection].text = newValue?.name
        CreateMatchViewModel.shared.match?.players[currentSelection].tee = newValue
        
//        if match.players.count == 2 {
//            match.players[playerIndex == 0 ? 1 : 0].strokes = 0
//        }
        
     
//        onSelect?(newValue)
    }
}

