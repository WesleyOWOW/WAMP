import UIKit

class SetStrokesViewController: WampViewController {
    
    var currentSelection = 0
    var picker: UIPickerView!
    var toolBar: UIToolbar!
      let componentItems = Array(0...36)
    var teeButtons: [UITextField] = []
    var isGoing = false
    @IBOutlet weak var contentView: RoundedShadowRect!
    override func viewDidLoad() {
        super.viewDidLoad()
        print("match", CreateMatchViewModel.shared.match)
        self.hidesKeyboardOnTap = false
        for (index, player) in (CreateMatchViewModel.shared.match?.players.enumerated())! {
            if player.tee == nil {
                CreateMatchViewModel.shared.match?.players[index].tee = CreateMatchViewModel.shared.match?.course!.tees[0]
            }
        }
        
         CreateMatchViewModel.shared.match?.adjustStrokesAccordingToTees()
        
        picker = UIPickerView()
        picker.dataSource = self
        picker.delegate = self
        picker.backgroundColor = .white
        picker.showsSelectionIndicator = true
        toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(hex: "007BFF")
        toolBar.sizeToFit()
        
        makePlayerRows()
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donePicker))
        
        
        toolBar.setItems([spaceButton, cancelButton], animated: false)
        toolBar.isUserInteractionEnabled = true
    }
    
    @objc func donePicker (sender:UIBarButtonItem) {
        self.view.resignFirstResponder()
        teeButtons[currentSelection].resignFirstResponder()
        
    }
    
    @IBAction func nextButtonPressed(_ sender: Any) {
        guard !isGoing  else {
            return
        }
        
        self.performSegue(withIdentifier: "NEXT", sender: nil)
        
        self.isGoing = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
        for (index, player) in (CreateMatchViewModel.shared.match?.players.enumerated())! {
            
            teeButtons[index].text = "\(player.strokes)"
            
        }
    }
    
    @IBAction func backButtonPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func makePlayerRows() {

        var playerViews: [UIView] = []
        for (index, player) in (CreateMatchViewModel.shared.match?.players.enumerated())! {
            let playerView = UIView(frame: .zero)
            
            let nameLabel = UILabel(frame: .zero)
            nameLabel.font = .bold(14)
            
            var lastname = player.profile.lastName
            if (player.profile.lastName.count > 9) {
                lastname = String(player.profile.lastName.prefix(6)) + "..."
            }
            nameLabel.text = player.profile.firstName[0] + ". " + lastname
            nameLabel.textAlignment = .left
            nameLabel.setContentHuggingPriority(230, for: .horizontal)
            nameLabel.lineBreakMode = .byClipping
            nameLabel.textColor = .white
            
            
            playerView.addSubview(nameLabel)
            nameLabel.snp.makeConstraints { make in
                make.bottom.top.equalToSuperview().inset(26)
                make.leading.equalToSuperview().offset(16)
            }
            
            // -- teebox buttons
            
            let hcpButton = UILabel()
            hcpButton.text =  player.profile.handicap.renderAsHandicap(includeSuffix: false)
            hcpButton.font = .medium(14)
            hcpButton.textColor =  UIColor(hex: "BFBFBF")
            
            playerView.addSubview(hcpButton)
            hcpButton.snp.makeConstraints { make in
                make.bottom.top.equalToSuperview().inset(26)
                make.leading.equalToSuperview().offset(153)
            }
            
            let ChcpButton = UILabel()
            ChcpButton.text =  "\(player.playingHandicap)"
            ChcpButton.font = .medium(14)
            ChcpButton.textColor = UIColor(hex: "BFBFBF")
            
            playerView.addSubview(ChcpButton)
            ChcpButton.snp.makeConstraints { make in
                make.bottom.top.equalToSuperview().inset(26)
                make.leading.equalToSuperview().offset(227)
            }
            
            
            let strokesButton = UITextField()
            strokesButton.text = "\(player.strokes)"
            strokesButton.font = .medium(14)
            strokesButton.textColor = .black
            strokesButton.contentHorizontalAlignment = .left
            strokesButton.addAction {
                print("Selected b")
                self.currentSelection = index
                strokesButton.becomeFirstResponder()
            }
            teeButtons.append(strokesButton)
            strokesButton.inputAccessoryView = toolBar
            strokesButton.inputView = picker
            
            playerView.addSubview(strokesButton)
            strokesButton.snp.makeConstraints { make in
                make.bottom.top.equalToSuperview().inset(26)
                make.leading.equalToSuperview().offset(302)
            }
            
            let sep = UIView(frame: .zero)
            sep.backgroundColor = .black
            sep.alpha = 0.05
            
            playerView.addSubview(sep)
            sep.snp.makeConstraints { make in
                make.bottom.trailing.leading.equalToSuperview()
                make.height.equalTo(1)
            }
            
            playerViews.append(playerView)
        }
        
        let playerStack = UIStackView(arrangedSubviews: playerViews)
        playerStack.axis = .vertical
        playerStack.spacing = 0
        
        self.contentView.addSubview(playerStack)
        playerStack.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        let sep = UIView(frame: .zero)
        sep.backgroundColor = .black
        sep.alpha = 0.05
        
        contentView.addSubview(sep)
        sep.snp.makeConstraints { make in
            make.bottom.top.equalToSuperview()
            make.width.equalTo(1)
            make.leading.equalToSuperview().offset(205)
        }
        
        let sep2 = UIView(frame: .zero)
        sep2.backgroundColor = .black
        sep2.alpha = 0.05
        
        contentView.addSubview(sep2)
        sep2.snp.makeConstraints { make in
            make.bottom.top.equalToSuperview()
            make.width.equalTo(1)
            make.leading.equalToSuperview().offset(278)
        }
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return false
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
}

extension SetStrokesViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return componentItems.count
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
         return "\(componentItems[row])"
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let newValue = componentItems[row]
        teeButtons[currentSelection].text = "\(newValue)"
        CreateMatchViewModel.shared.match?.players[currentSelection].strokes = newValue
        if  CreateMatchViewModel.shared.match?.players.count == 2 {
             CreateMatchViewModel.shared.match?.players[currentSelection == 0 ? 1 : 0].strokes = 0
            teeButtons[currentSelection == 0 ? 1 : 0].text = "0"
        }
    }
}


