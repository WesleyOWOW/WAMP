//
//  PickOpponentViewController.swift
//  WAMP
//

/// Part of the create game process, allows you to pick your opponents (and teammate)

import UIKit

import RxSwift
import RxCocoa
import RxKeyboard
import RxSwiftExt


class PickOpponentViewController: CreateMatchBaseViewController, UIScrollViewDelegate {
    
    @IBOutlet var scrollableView: ClickThroughView!
    @IBOutlet weak var teamTwo: UIStackView!
    let viewModel = PickOpponentViewModel()
    override var tabTitle: String {
        if matchType == .single {
            return "Select opponent"
        } else {
            return "Select players"
        }
    }
    override var displayTitleBar: Bool { return true }
    // Avatars
    @IBOutlet weak var facesView: UIView!
    @IBOutlet weak var playerAvatar: ChoosePlayerAvatar!
    @IBOutlet weak var teammateAvatar: ChoosePlayerAvatar!
    @IBOutlet weak var opponent1Avatar: ChoosePlayerAvatar!
    @IBOutlet weak var opponent2Avatar: ChoosePlayerAvatar!
    var wrapper: NavigationWrapperViewController!
    @IBOutlet weak var teamOne: UIStackView!
    // Avatar Containers
    @IBOutlet weak var playerAvatarContainer: UIStackView!
    @IBOutlet weak var teammateAvatarContainer: UIStackView!
    @IBOutlet weak var opponent1AvatarContainer: UIStackView!
    @IBOutlet weak var opponent2AvatarContainer: UIStackView!
    
    // Search
    
    @IBOutlet weak var searchView: UIView!
    
    // Constraints
    @IBOutlet weak var playerSelectorIndicatorAlignmentConstraint: NSLayoutConstraint!
    
    // Avatar labels
    @IBOutlet weak var playerNameLabel: UILabel!
    @IBOutlet weak var teammateNameLabel: UILabel!
    @IBOutlet weak var opponent1NameLabel: UILabel!
    @IBOutlet weak var opponent2NameLabel: UILabel!
    
    // Other views
    @IBOutlet weak var playerSelectorIndicatorView: UIView!
    @IBOutlet weak var searchResultsView: UIView!
    
    let searchViewController = CreateGameSearchViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        searchViewController.parentvc = self
        

        
        searchViewController.onAddGuest = { [unowned self] in
            self.performSegue(withIdentifier: "addGuest", sender: nil)
//            self.perform(segue: StoryboardSegue.CreateGame.addGuest)
        }
        
        searchViewController.onSelect = { [unowned self] contact in
            self.selectOpponent(contact)
        }
        
        let recognizer = UIPanGestureRecognizer(target: self, action: #selector(handleSwipe))
        
        //    searchViewController.tableView.addGestureRecognizer(recognizer)
        
        
        
        // Hide views if match type is single
        if matchType == .single {
            views(for: .teammate(1)).avatar.alpha = 0.0
            views(for: .opponent(1)).avatar.alpha = 0.0
            views(for: .teammate(1)).nameLabel.alpha = 0.0
            views(for: .opponent(1)).nameLabel.alpha = 0.0
            
            
            teamOne.addArrangedSubview(playerAvatarContainer)
            teamTwo.addArrangedSubview(opponent2AvatarContainer)
        } else {
            viewModel.selectedPlayer.value = .teammate(1)
        }
        
        self.setupRx()
        
        // Setup search view controller
        addChildViewController(searchViewController)
        searchResultsView.addSubview(searchViewController.view)
        
        searchViewController.view.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
//
        searchViewController.onSelect = self.selectOpponent
        searchViewController.view.backgroundColor = .red
        teamOne.removeArrangedSubview(teammateAvatar)
    }
    
    var cell: UITableViewCell!
    
   

    func handleSwipe(_ sender:UIPanGestureRecognizer){
        let translation = sender.translation(in: self.view)
        
        print("swipe da bitch", translation)
        searchViewController.tableView.contentOffset.y = translation.y
        sender.setTranslation(CGPoint.zero, in: self.view)
    }
    
    func setupRx() {
        // Indicator view
        viewModel.selectedPlayer.asObservable()
            .bind { [unowned self] in
                let views = self.views(for: $0)
                let color = self.color(for: $0)
                
                self.alignPlayerIndicator(to: views.avatar, color: color)
            }
            .addDisposableTo(disposeBag)
        
        RxKeyboard.instance.visibleHeight
            .drive(onNext: { keyboardVisibleHeight in
                self.searchViewController.tableView.contentInset.bottom = keyboardVisibleHeight
                
            })
            .disposed(by: disposeBag)
        
        configureAvatar(views(for: .opponent(0)), color: color(for: .opponent(0)), forObservable: viewModel.opponent1.asObservable(), placeholder: "Opponent")
        configureAvatar(views(for: .opponent(1)),  color: color(for: .opponent(1)), forObservable: viewModel.opponent2.asObservable(), placeholder: "Opponent")
        configureAvatar(views(for: .teammate(1)),  color: color(for: .teammate(0)), forObservable: viewModel.teammate.asObservable(), placeholder: "Teammate")
    }
    
    /// Select the given opponent
    func selectOpponent(_ contact: UserProtocol) {
        // Deduplicate
        
        print("Selected iser is", contact.id)
        
        if viewModel.teammate.value?.id == contact.id {
            viewModel.teammate.value = nil
        }
        
        if viewModel.opponent1.value?.id == contact.id {
            viewModel.opponent1.value = nil
        }
        
        if viewModel.opponent2.value?.id == contact.id {
            viewModel.opponent2.value = nil
        }
        
        // Set
        viewModel.selectedPlayerVariable.value = contact
        
        dismissKeyboard()

        
        selectNextPlayer()
    }

    func childTableDidScroll(distance: CGFloat) {

    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        log.debug(segue.destination)
        if segue.identifier == "addGuest" {
            _ = CreateGuestViewModel.shared.hasGuest.asObservable().bind { [weak self] hasGuest in
                if hasGuest {
                    if CreateGuestViewModel.shared.guest.value != nil {
                        self?.selectOpponent(CreateGuestViewModel.shared.guest.value!)
                        CreateGuestViewModel.shared.reset()
                    }
                    
                }
                
                }.disposed(by: disposeBag)
            
        }
    }
    
    func selectNextPlayer() {
        guard matchType == .double else {
            return
        }
        
        // view layout (teammate-opponent): 0 1 - 1 0
        switch viewModel.selectedPlayer.value {
        case .teammate(1): viewModel.selectedPlayer.value = .opponent(1)
        case .opponent(1): viewModel.selectedPlayer.value = .opponent(0)
        default: return
        }
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        // Set the CreateMatchBaseViewController values
        team1Players = [User.me, viewModel.teammate.value].flatMap{$0}
        team2Players = [viewModel.opponent1.value, viewModel.opponent2.value].flatMap{$0}
        
        print("SEGUE", identifier)
        if identifier == "selectCourse" {
            if (matchType == .single && team2Players.count == 1) || (team1Players.count == 2 && team2Players.count == 2) {
                print("players",  CreateMatchViewModel.shared.team1Players, team1Players,  CreateMatchViewModel.shared.team2Players, team2Players)
                CreateMatchViewModel.shared.team1Players = team1Players
                CreateMatchViewModel.shared.team2Players = team2Players
                return true
            } else {
                let message = matchType == .single ? "Select your opponent to continue." : "Select your teammate and opponents to continue."
                let alert = UIAlertController(title: message, message: nil, preferredStyle: .alert)
                let action = UIAlertAction(title: "OK", style: .default, handler: nil)
                alert.addAction(action)
                present(alert, animated: true)
                return false
            }
        }
        
        return super.shouldPerformSegue(withIdentifier: identifier, sender: sender)
    }
    
    /// Aligns the player indicator (the small bar) horizontally to a new view. Called from an observer.
    func alignPlayerIndicator(to target: UIView, color: UIColor) {
        self.view.layoutIfNeeded()
        
        UIView.animate(withDuration: 0.3) {
            self.playerSelectorIndicatorAlignmentConstraint.isActive = false
            self.playerSelectorIndicatorView.backgroundColor = color
            self.playerSelectorIndicatorAlignmentConstraint =
                NSLayoutConstraint(item: self.playerSelectorIndicatorView,
                                   attribute: .centerX,
                                   relatedBy: .equal,
                                   toItem: target,
                                   attribute: .centerX,
                                   multiplier: 1,
                                   constant: 0)
            
            self.view.addConstraint(self.playerSelectorIndicatorAlignmentConstraint)
            
            self.view.layoutIfNeeded()
        }
    }
    
    /// Called by the gesture recognizers on the avatars, to select a different player
    @IBAction func selectPlayer(_ sender: UIGestureRecognizer) {
        switch sender.view! {
        case teammateAvatarContainer:
            viewModel.selectedPlayer.value = .teammate(1)
        case opponent1AvatarContainer:
            viewModel.selectedPlayer.value = .opponent(0)
        case opponent2AvatarContainer:
            viewModel.selectedPlayer.value = .opponent(1)
        default:
            fatalError("selectPlayer sender is unknown view")
        }
    }
    
    typealias AvatarViews = (container: UIStackView, avatar: ChoosePlayerAvatar, nameLabel: UILabel)
    
    /// Returns the views associated with the given player identifier
    func views(for player: PickOpponentViewModel.PlayerIdentifier) -> AvatarViews {
        switch player {
        case .teammate(0):
            return (playerAvatarContainer, playerAvatar, playerNameLabel)
        case .teammate(1):
            return (teammateAvatarContainer, teammateAvatar, teammateNameLabel)
        case .opponent(0):
            return (opponent1AvatarContainer, opponent1Avatar, opponent1NameLabel)
        case .opponent(1):
            return (opponent2AvatarContainer, opponent2Avatar, opponent2NameLabel)
        default:
            fatalError("invalid arguments for views(for:...)")
        }
    }
    
    func color(for player: PickOpponentViewModel.PlayerIdentifier) -> UIColor {
        switch player {
        case .teammate(0):
            return UIColor(hex: "007BFF", alpha: 1.0)
        case .teammate(1):
            return UIColor(hex: "007BFF", alpha: 1.0)
        case .opponent(0):
            return UIColor(hex: "FF3344", alpha: 1.0)
        case .opponent(1):
            return UIColor(hex: "FF3344", alpha: 1.0)
        default:
            fatalError("invalid arguments for views(for:...)")
        }
    }
    
    /// Configures the given avatar views with the given observable, to update them when needed
    func configureAvatar(_ views: AvatarViews, color: UIColor, forObservable observable: Observable<UserProtocol?>, placeholder: String) {
        observable.bind { profile in
            if let profile = profile {
                views.nameLabel.text = profile.firstName + " " + profile.lastName
                views.avatar.setProfile(profile, color: color)
            } else {
                views.nameLabel.text = placeholder
                views.avatar.setProfile(nil, color: color)
            }
            }.addDisposableTo(disposeBag)
    }
    
    @IBAction func backButtonPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func nextButtonPressed(_ sender: Any) {
        team1Players = [User.me, viewModel.teammate.value].flatMap{$0}
        team2Players = [viewModel.opponent1.value, viewModel.opponent2.value].flatMap{$0}
        
        
        
            if (matchType == .single && team2Players.count == 1) || (team1Players.count == 2 && team2Players.count == 2) {
                print("players",  CreateMatchViewModel.shared.team1Players, team1Players,  CreateMatchViewModel.shared.team2Players, team2Players)
                CreateMatchViewModel.shared.team1Players = team1Players
                CreateMatchViewModel.shared.team2Players = team2Players
                self.performSegue(withIdentifier: "selectCourse", sender: nil)
            } else {
                let message = matchType == .single ? "Select your opponent to continue." : "Select your teammate and opponents to continue."
                let alert = UIAlertController(title: message, message: nil, preferredStyle: .alert)
                let action = UIAlertAction(title: "OK", style: .default, handler: nil)
                alert.addAction(action)
                present(alert, animated: true)
            }
        
    }
    
    override func viewWillAppearWrapped(_ navigationWrapper: NavigationWrapperViewController, animated: Bool) {
         playerAvatar.setProfile(User.me, color: color(for: .teammate(0)))
        //        navigationWrapper.bottomSpacer.isHidden = true
        self.facesView.snp.makeConstraints { make in
            make.height.equalTo(95)
        }
        self.wrapper = navigationWrapper
        navigationWrapper.rightButton.setTitle("Next", for: .normal)
        navigationWrapper.rightButton.addAction { [unowned self] in
//            guard self.shouldPerformSegue(withIdentifier: StoryboardSegue.CreateGame.selectClub.rawValue, sender: nil) else { return }
//
//            self.perform(segue: StoryboardSegue.CreateGame.selectClub)
        }
    }
}
