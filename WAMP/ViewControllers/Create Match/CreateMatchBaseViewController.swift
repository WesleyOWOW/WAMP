import UIKit

protocol ConceptMatchContaining : class {
    var match: ConceptMatch! { get set }
}

enum CreateMatchFlow {
    case postGame
    case calculateStrokes
}

class CreateMatchBaseViewController: WAMPViewController {
    
    var displayTitleBar: Bool { return true }
    var tabTitle: String { return "" }
    
    var matchType: MatchType!
    var flow: CreateMatchFlow!
    
    var team1Players: [UserProtocol] = [User.me]
    var team2Players = [UserProtocol]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.matchType = CreateMatchViewModel.shared.matchType
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.hidesKeyboardOnTap = false
        super.viewWillAppear(animated)
        
        log.info("\(type(of: self)) viewWillAppear(\(animated)) - ⛳️ Flow: \(flow)")
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? CreateMatchBaseViewController {
            destination.team1Players = team1Players
            destination.team2Players = team2Players
            destination.matchType = matchType
            destination.flow = flow
        }
        
        if let source = self as? ConceptMatchContaining, let destination = segue.destination as? ConceptMatchContaining {
            destination.match = source.match
        }
        
        super.prepare(for: segue, sender: sender)
    }
    
    
}

