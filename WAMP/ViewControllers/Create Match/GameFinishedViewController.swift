import UIKit
import SAConfettiView
class GameFinishedViewController: CreateMatchBaseViewController, ConceptMatchContaining {
    
    var match: ConceptMatch!
    
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var subtitleLabel: UILabel!
    @IBOutlet weak var continueButton: WampButton!
    @IBOutlet weak var backButton: UIButton!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        CreateGuestViewModel.shared.reset()
        
        if CreateMatchViewModel.shared.match?.winningTeam == CreateMatchViewModel.shared.match?.players[0].team {
            titleLabel.text = "You won \(CreateMatchViewModel.shared.match!.score.rendered)"
            subtitleLabel.text = wonTexts.random
            continueButton.backgroundColor = Team.blue.color
            continueButton.setTitle("Celebrate your victory", for: .normal)
            let confetti = SAConfettiView(frame: self.view.bounds)
            confetti.type = .Confetti
            self.view.addSubview(confetti)
            confetti.startConfetti()
            view.bringSubview(toFront: titleLabel)
            view.bringSubview(toFront: subtitleLabel)
            view.bringSubview(toFront: continueButton)
            view.bringSubview(toFront: backButton)
            view.backgroundColor = Team.blue.color
        } else if (CreateMatchViewModel.shared.match?.score.isSquare)! {
            titleLabel.text = "All square"
            subtitleLabel.text = squareTexts.random
            continueButton.backgroundColor = UIColor(hex: "9A9A9A")
        } else {
            titleLabel.text = "You lost \(CreateMatchViewModel.shared.match!.score.rendered)"
            continueButton.backgroundColor = Team.red.color
            continueButton.setTitle("Accept your defeat", for: .normal)
            subtitleLabel.text = lostTexts.random
            view.backgroundColor = Team.red.color
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func back() {
        // Present an alert, telling the user that going back erases the score card
        let alert = UIAlertController(title: "Are you sure?",
                                      message: "Going back will erase any previously entered scores",
                                      preferredStyle: .alert)
        alert.addAction(
            UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        )
        alert.addAction(
            UIAlertAction(title: "Go back", style: .destructive) { _ in
                self.navigationController?.popViewController(animated: true)
            }
        )

        self.present(alert, animated: true)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
}

let wonTexts = [
    "Congrats! You kicked his ass. 🙏",
    "Well Done! You're a winner ✨",
    "Winner 🍾",
    "Yeah! You beat him!",
    "🔥You're on fire🔥",
    "Get this man a medal 🏅",
    "Check your stats!",
    "So much winning 🎖"
]

let squareTexts = [
    "Booooooooooring 😕 Let's see who can win the bar 🍷",
    "You're both winners... and losers! 👯",
    "Hmmm... Feels like a day sailing with no wind... ⛵️"
]

let lostTexts = [
    "Go back to the range my friend! 🐶",
    "Try again! 🙈",
    "Don't forget to practise on the way home. 🏌️",
    "Maybe try pingpong as a sport 🏓",
    "Better luck next time... NOT ⛳️",
    "Go fishing! 🐳🐋",
    "Did your ball move in your downswing?",
    "First time on the course?",
    "Wanna try again?"
]

