import UIKit
import SnapKit

class  MultiplePicturesViewController: WampViewController  {
    @IBOutlet weak var addPhotoBackgroundView: UIView!
    @IBOutlet weak var addPhotoButton: UIButton!
    @IBOutlet weak var imagesStackView: UIStackView!
    @IBOutlet weak var bigPhotoView: UIView!
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var photoStackView: UIStackView!
    @IBOutlet weak var setCoverButton: UIButton!
    @IBOutlet weak var photoPreview: UIImageView!
    var activePhoto: Photo? = nil
    var holeSelectionButtonGrid: WAMPButtonGrid?
    var pop = 3
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        shadowView.layer.shadowColor = UIColor.black.cgColor
        shadowView.layer.shadowOffset = CGSize(width: 0.5, height: 1.2)
        shadowView.layer.shadowOpacity = 0.08
        shadowView.layer.masksToBounds = false
        shadowView.layer.shadowRadius = 5.0
        
        shadowView.clipsToBounds = false
        shadowView.layer.shadowPath = UIBezierPath(roundedRect: self.shadowView.bounds, cornerRadius: 8).cgPath
        self.navigationController?.isNavigationBarHidden = true
        setCoverButton.backgroundColor = .clear
        setCoverButton.layer.cornerRadius = 16
        setCoverButton.layer.borderWidth = 1
        setCoverButton.layer.borderColor = UIColor.white.cgColor
        setCoverButton.addAction {
            WampCameraViewModel.shared.makeCover()
            //            setCoverButton.isSelected = false
            //            setCoverButton.isHighlighted = false
            self.setCoverButton.layoutSubviews()
            self.setCoverButton.backgroundColor = .white
            self.setCoverButton.setTitleColor(#colorLiteral(red: 0, green: 0.5728718638, blue: 1, alpha: 1), for: .normal)
            self.setCoverButton.setTitle("Cover", for: .normal)
            
            self.photoStackView.removeArrangedSubviews()
            for image in WampCameraViewModel.shared.allPhotos.value {
                self.makeNewEntry(for: image)
            }
            self.setCoverButton.backgroundColor = .white
        }
        
        
        bigPhotoView.clipsToBounds = true
        
        addPhotoBackgroundView.layer.shadowColor = UIColor.black.cgColor
        addPhotoBackgroundView.layer.shadowOffset = CGSize(width: 0.5, height: 1.2)
        addPhotoBackgroundView.layer.shadowOpacity = 0.08
        addPhotoBackgroundView.layer.masksToBounds = false
        addPhotoBackgroundView.layer.shadowRadius = 5.0
        addPhotoBackgroundView.layer.cornerRadius = 4
        addPhotoBackgroundView.clipsToBounds = false
        addPhotoBackgroundView.layer.shadowPath = UIBezierPath(roundedRect: addPhotoBackgroundView.bounds, cornerRadius: 4).cgPath
        photoStackView.layoutMargins =  UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 0)
        photoStackView.isLayoutMarginsRelativeArrangement = true
        
        for image in WampCameraViewModel.shared.allPhotos.value {
            self.makeNewEntry(for: image)
        }
        
        if WampCameraViewModel.shared.currentPhoto.value != nil {
            photoPreview.image = WampCameraViewModel.shared.currentPhoto.value!.image
            self.activePhoto = WampCameraViewModel.shared.currentPhoto.value
        }
    }
    
    @IBAction func addPhotoButtonPressed(_ sender: Any) {
        //        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func chooseHoleButtonpressed(_ sender: Any) {
        let selector = HoleSelector(for: WampCameraViewModel.shared.currentPhoto.value!)
        self.view.addSubview(selector)
        selector.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        //        self.makeViewDark()
    }
    
    func resetImages() {
        photoStackView.removeArrangedSubviews()
        for image in WampCameraViewModel.shared.allPhotos.value {
            
            self.makeNewEntry(for: image)
        }
    }
    
    
    @IBAction func trashButtonPressed(_ sender: Any) {
        WampCameraViewModel.shared.trash()
        photoStackView.removeArrangedSubviews()
        for image in WampCameraViewModel.shared.allPhotos.value {
            self.makeNewEntry(for: image)
        }
        
        if WampCameraViewModel.shared.currentPhoto.value != nil {
            photoPreview.image = WampCameraViewModel.shared.currentPhoto.value?.image!
            
            self.activePhoto = WampCameraViewModel.shared.currentPhoto.value
        }
        
        if WampCameraViewModel.shared.allPhotos.value.count == 0 {
            self.performSegue(withIdentifier: "showCamera", sender: nil)
        }
    }
    
    
    
    @IBAction func confirmButtonPressed(_ sender: Any) {
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController]
        
        //        if (FeedViewModel.shared.pop == 1 ) {
        //            self.navigationController?.popViewController(animated: true)
        //        } else {
        //             self.navigationController!.popToViewController(viewControllers[viewControllers.count - FeedViewModel.shared.pop], animated: true)
        //        }
        
        self.performSegue(withIdentifier: "ToPreplay", sender: nil)
        
    }
    
    private func makeNewEntry(for photo: Photo) {
        let backview = UIView(frame: CGRect(x: 0, y: 0, width: 104, height: 134))
        
        
        
        
        
        let imageView = UIImageView(frame: .zero)
        
        
        
        imageView.image = photo.image
        
        let tap = ActivatePhotoTapGestureRecognizer(target: self, action: #selector(self.makeActive))
        tap.photo = photo
        backview.addGestureRecognizer(tap)
        
        backview.addSubview(imageView)
        
        
        let shadow = UIView(frame: CGRect(x: 0, y: 0, width: 104, height: 104))
        shadow.layer.shadowColor = UIColor.black.cgColor
        shadow.layer.shadowOffset = CGSize(width: 0.5, height: 1.2)
        shadow.layer.shadowOpacity = 0.08
        shadow.layer.shadowRadius = 5.0
        shadow.layer.cornerRadius = 8
        shadow.layer.shadowPath = UIBezierPath(roundedRect: imageView.bounds, cornerRadius: 8).cgPath
        
        let label = UILabel(frame: .zero)
        if let photo = photo.hole, photo != nil {
            label.text = "Hole \(photo)"
        }
        
        label.textAlignment = .center
        label.font = .medium(12)
        label.textColor = .black
        backview.addSubview(label)
        backview.addSubview(shadow)
        
        
        imageView.snp.makeConstraints { make in
            make.leading.top.equalToSuperview()
            make.width.height.equalTo(104)
        }
        
        
        
        shadow.snp.makeConstraints { make in
            make.edges.equalTo(imageView)
        }
        backview.sendSubview(toBack: shadow)
        
        
        label.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.top.equalTo(imageView.snp.bottom).offset(6)
        }
        
        //        backview.backgroundColor = .purple
        
        self.photoStackView.addArrangedSubview(backview)
        
        
        backview.snp.makeConstraints { make in
            make.width.equalTo(104)
            make.height.equalTo(134)
        }
    }
    
    @objc private func makeActive(sender: ActivatePhotoTapGestureRecognizer) {
        self.photoPreview.image = sender.photo.image!
        WampCameraViewModel.shared.currentPhoto.value = sender.photo
        
        if WampCameraViewModel.shared.allPhotos.value.first == sender.photo {
            setCoverButton.backgroundColor = .white
            setCoverButton.setTitleColor(#colorLiteral(red: 0, green: 0.5728718638, blue: 1, alpha: 1), for: .normal)
            setCoverButton.setTitle("Cover", for: .normal)
        } else {
            setCoverButton.backgroundColor = .clear
            setCoverButton.setTitleColor(.white, for: .normal)
            setCoverButton.setTitle("Set as cover", for: .normal)
        }
        
        self.activePhoto = sender.photo
    }
    
    
}




class HoleSelector: UIView {
    var photo: Photo!
    var verticalStackView: UIStackView!
    var selected: Int?
    var allButtons: [[RoundedButton]] = []
    var selectedButton: RoundedButton?
    
    init(for photo: Photo) {
        super.init(frame: .zero)
        setupButtons()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupButtons() {
        let arr = [1...4, 5...8, 9...12, 13...16, 17...19]
        var numberRows: [UIStackView] = []
        for numbers in arr {
            var row = (numbers).map { makeButton($0) }
            if (numbers == 17...19) {
                let button = makeButton(0)
                row.append(button)
                button.alpha = 0.0
                //                button.isHidden = true
            }
            numberRows.append(makeHorizontalStackView(row))
            allButtons.append(row)
        }
        
        createVerticalStackView(numberRows)
        
        let effect = UIBlurEffect(style: .dark)
        let blurredEffectView = UIVisualEffectView(effect: effect)
        self.addSubview(blurredEffectView)
        blurredEffectView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        self.addSubview(verticalStackView)
        verticalStackView.snp.makeConstraints { make in
            make.height.equalTo(352)
            make.width.equalTo(278)
            make.center.equalToSuperview()
        }
        
        let button = WampButton(frame: .zero)
        button.layer.cornerRadius = 20
        button.setTitleColor(UIColor(hex: "007BFF"), for: .normal)
        button.setTitle("Confirm hole", for: .normal)
        button.backgroundColor = .white
        button.titleLabel?.font = .medium(12)
        self.addSubview(button)
        button.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(48)
            make.trailing.equalToSuperview().offset(-48)
            make.bottom.equalToSuperview().offset(-40)
            make.height.equalTo(40)
        }
        
        
        button.addAction {
            WampCameraViewModel.shared.setHole(WampCameraViewModel.shared.currentPhoto.value!, self.selected!)
            WampCameraViewModel.shared.sortPhotos()
            
            let vc = self.superview?.parentViewController as! MultiplePicturesViewController
            vc.resetImages()
            self.removeFromSuperview()
        }
        
        let title = UILabel(frame: .zero)
        title.font = .bold(32)
        title.text = "Select hole"
        title.textColor = .white
        self.addSubview(title)
        title.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(80)
            make.centerX.equalToSuperview()
        }
        
        let closeButton = UIButton(frame: .zero)
        closeButton.setImage(UIImage(named: "white cross"), for: .normal)
        self.addSubview(closeButton)
        closeButton.snp.makeConstraints { make in
            make.width.height.equalTo(32)
            make.leading.equalToSuperview().offset(8)
            make.top.equalToSuperview().offset(26)
        }
        
        closeButton.addAction {
            self.removeFromSuperview()
        }
    }
    
    private func makeHorizontalStackView(_ views: [UIView]) -> UIStackView {
        let stackView = UIStackView(arrangedSubviews: views)
        stackView.alignment = .fill
        stackView.axis = .horizontal
        stackView.spacing = 18
        stackView.distribution = .fillEqually
        stackView.snp.makeConstraints { make in
            make.height.equalTo(54)
        }
        
        return stackView
    }
    
    private func createVerticalStackView(_ views: [UIView]) {
        verticalStackView = UIStackView(arrangedSubviews: views)
        verticalStackView.alignment = .fill
        verticalStackView.axis = .vertical
        verticalStackView.spacing = 18
        verticalStackView.distribution = .fillEqually
    }
    
    func makeButton(_ hole: Int) -> RoundedButton {
        let button = RoundedButton(frame: .zero)
        button.setTitleColor(.white, for: .normal)
        button.setTitleColor(UIColor(hex: "007BFF"), for: .selected)
        button.WAMPBorderColor = .white
        button.setTitle(String(hole), for: .normal)
        button.snp.makeConstraints { make in
            make.height.width.equalTo(56)
        }
        
        button.addAction {
            for row in self.allButtons {
                for button in row {
                    button.isSelected = false
                }
            }
            
            self.selected = hole
            self.selectedButton = button
            button.isSelected = true
        }
        
        return button
    }
}


