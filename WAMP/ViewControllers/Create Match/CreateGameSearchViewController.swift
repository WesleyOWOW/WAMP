//
//  CreateMatchSearchViewController.swift
//  WAMP
//

/// A subclass of SearchViewController, used for searching players in PickOpponentViewController

import UIKit
import SnapKit
import RxSwift
import RxKeyboard

class CreateGameSearchViewController : UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    let tableView = UITableView(frame: .zero)
    var onAddGuest: (() -> Void)?
    let viewModel = SearchViewModel()
    var parentvc: PickOpponentViewController?
    var header = UIView(frame: .zero)
    var onSelect: ((User) -> ()) = { _ in }
    var results = [Player]() {
        didSet {
            tableView.reloadData()
        }
    }
    
    var isSetup = false
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        guard !isSetup else {
            return
        }
        
        let searchView = UIView()
        rebindSearchResults()
        let searchBack = UIView(frame: .zero)
        searchBack.backgroundColor = UIColor(hex: "F2F2F2")
        searchBack.layer.cornerRadius = 5
        searchBack.clipsToBounds = true
        searchView.backgroundColor = UIColor(hex: "FCFCFC")
        searchView.addSubview(searchBack)
        searchBack.snp.makeConstraints { make in
            make.height.equalTo(32)
            make.edges.equalToSuperview().inset(16)
        }
        
        self.tableView.rowHeight = 70
        
        let searchImage = UIImageView(image: UIImage(named: "searchIcon"))
        searchBack.addSubview(searchImage)
        searchImage.snp.makeConstraints { make in
            make.size.equalTo(15)
            make.centerY.equalToSuperview()
            make.leading.equalToSuperview().offset(16)
        }
        
        let searchField = UITextField(frame: .zero)
        searchField.placeholder = "Search for players"
        searchField.font = .medium(16)
        searchField.delegate = self
        searchField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        searchBack.addSubview(searchField)
        searchField.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.leading.equalTo(searchImage.snp.trailing).offset(12)
        }
        
        searchField.rx.text
            .orEmpty
            .bind(to: viewModel.searchText)
            .disposed(by: searchResultsDisposeBag)
        
        self.view.addSubview(searchView)
        searchView.snp.makeConstraints { make in
            make.height.equalTo(64)
            make.top.trailing.leading.equalToSuperview()
        }
        
        
        
        self.view.addSubview(tableView)
        tableView.register(SearchResultCell.self, forCellReuseIdentifier: SearchResultCell.identifier)
        
        tableView.snp.makeConstraints { make in
            make.left.right.bottom.equalToSuperview()
            make.top.equalTo(searchView.snp.bottom)
        }
        tableView.delegate = self
        tableView.dataSource = self
        
        let text = UILabel(frame: CGRect(x:0,y:0,width:UIScreen.main.bounds.size.width,height:130))
        text.text = "Recent matchplayers"
        text.font = .medium(14)
        header.addSubview(text)
        text.snp.makeConstraints { make in
            make.top.equalToSuperview()
            make.leading.equalToSuperview().offset(16)
        }
        
        
        
        let recent = RecentContactsView()
        recent.onAddGuest = self.onAddGuest
        recent.onSelectContact =  { profile in
            guard let user = profile as? User else {
                assertionFailure()
                log.error("Selected profile is not a user")
                return
            }
            
            self.onSelect(user)
        }
        header.addSubview(recent)
        recent.snp.makeConstraints { make in
            make.top.equalTo(text.snp.bottom)
            make.bottom.trailing.leading.equalToSuperview()
        }
        
        self.tableView.tableHeaderView = header
        self.tableView.tableHeaderView?.isUserInteractionEnabled = true
        header.frame.size.height = 130
        header.clipsToBounds = true
        
        isSetup = true
    }

    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return results.count
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let result = results[indexPath.row]
        print("selected row")
        if let indexPath = self.tableView.indexPathForSelectedRow {
            self.tableView.deselectRow(at: indexPath, animated: false)
        }
        
        self.select(user: result)
    }
    
    /// Displays the profile page for the given search result
    func select(user: Player) {
        // fetch the user
        User.fetch(id: user.id) { [weak self] result in
            if let user = result.value {
                self?.onSelect(user)
            }
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if let parent = self.parentvc as? PickOpponentViewController {
            parent.childTableDidScroll(distance: scrollView.contentOffset.y)
        }
        
    }
    
    
    func textFieldDidChange(_ textField: UITextField) {
        if textField.text != "" {
            if tableView.tableHeaderView?.frame.size.height == 130 {
                UIView.animate(withDuration: 0.2) {
                    self.tableView.tableHeaderView?.frame.size.height = 0
                }
            }
        } else {
            if self.tableView.tableHeaderView?.frame.size.height == 0 {
                UIView.animate(withDuration: 0.2) {
                     self.tableView.tableHeaderView?.frame.size.height = 130
                }
            }
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let result = results[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: SearchResultCell.identifier, for: indexPath) as! SearchResultCell
        cell.configure(result)
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        func makeHeader(_ title: String) -> UIView {
            let containing = UIView(frame: .zero)
            
            
            let label = UILabel(frame: .zero)
            containing.addSubview(label)
            label.snp.makeConstraints { make in
                make.left.right.bottom.equalToSuperview().inset(UIEdgeInsets(top: 6, left: 16, bottom: 6, right: 20))
            }
            
            label.font = .medium(14)
            label.textColor = .black
            label.textAlignment = .left
            label.text = title
            let separator = UIView(frame: .zero)
            separator.backgroundColor = UIColor(hex: "000000")
            separator.alpha = 0.05
            containing.addSubview(separator)
            separator.snp.makeConstraints { make in
                make.top.leading.trailing.width.equalToSuperview()
                make.height.equalTo(1)
            }
            containing.backgroundColor =  #colorLiteral(red: 0.9882352941, green: 0.9882352941, blue: 0.9882352941, alpha: 1)
            
            return containing
        }
        
        
        return makeHeader("All golfers")
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    var searchResultsDisposeBag = DisposeBag()
    func rebindSearchResults() {
        searchResultsDisposeBag = DisposeBag()
        viewModel.searchResults.bind { [weak self] results in
            
            self?.results = results
            }.disposed(by: searchResultsDisposeBag)
    }
}

class CreateGameSearchResultCell : SearchResultCell {
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        contentView.backgroundColor =  #colorLiteral(red: 0.9882352941, green: 0.9882352941, blue: 0.9882352941, alpha: 1)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

class RecentContactsCell : UITableViewCell {
    
    static let identifier = "recentcontactscell"
    let recentContactsView = RecentContactsView()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        contentView.addSubview(recentContactsView)
        contentView.backgroundColor = UIColor(hex: "FCFCFC")
        
        recentContactsView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
            make.height.equalTo(112)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}



