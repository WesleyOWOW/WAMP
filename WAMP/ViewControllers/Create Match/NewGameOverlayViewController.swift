import UIKit

protocol NewGameDelegate {
    func create()
}

class NewGameOverlayViewController: UIViewController {
    @IBOutlet weak var gameModesStack: UIStackView!
    @IBOutlet weak var flowStack: UIStackView!
    var delegate: NewGameDelegate!
    @IBOutlet weak var closeButton: WampButton!
    /// In this array the "unwind closures" are stored. They are added here whenever a new step is activated, and the cancel button always calls the last addition.
    var unwindClosures = [() -> ()]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.showGameModeSelection()
        closeButton.backgroundColor = #colorLiteral(red: 0, green: 0.4798448086, blue: 1, alpha: 1)
    }
    
    func showGameModeSelection() {
        UIView.animate(withDuration: 0.2) {
            self.view.backgroundColor = #colorLiteral(red: 0, green: 0.4823529412, blue: 1, alpha: 1)
        }
        
        gameModesStack.isHidden = false
    }
    
    @IBAction func cancel() {
        self.dismiss(animated: true)
    }
    
    @IBAction func selectFlowPostMatch() {
        
        showGameModeSelection()
    }
    
    @IBAction func selectFlowCalculateStrokes() {
        
        showGameModeSelection()
    }
    
    @IBAction func selectSingle() {
        select(type: .single)
    }
    
    @IBAction func selectDouble() {
        select(type: .double)
    }
    
    func select(type: MatchType) {
        self.dismiss(animated: true)
        CreateMatchViewModel.shared.matchType = type
        delegate?.create()
       
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
}

