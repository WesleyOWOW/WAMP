//
//  OnboardingViewController.swift
//  WAMP
//
//  Created by Wesley Peeters on 26/01/2018.
//  Copyright © 2018 Robbert Brandsma. All rights reserved.
//

import UIKit

class OnboardingViewController: WampViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    
    @IBOutlet weak var nextButton: WampButton!
    
    @IBOutlet weak var backButton: UIButton!
    
    // The custom UIPageControl
    @IBOutlet weak var pageControl: UIPageControl!
    
    @IBOutlet weak var topBar: UIView!
    // The UIPageViewController
    var pageContainer: UIPageViewController!
    
    // The pages it contains
    private(set) lazy var pages: [UIViewController] = {
        return [self.newStep(step: "One"), self.newStep(step: "Two"), self.newStep(step: "Three"), self.newStep(step: "Four"), self.newStep(step: "Five"), self.newStep(step: "Six"), self.newStep(step: "Seven"), self.newStep(step: "Eight")]
    }()
    
    // Track the current index
    var currentIndex: Int? = 0
    private var pendingIndex: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hidesKeyboardOnTap = false
        pageContainer = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
        pageContainer.delegate = self
        
        pageContainer.dataSource = self
        pageContainer.setViewControllers([pages[0]], direction: UIPageViewControllerNavigationDirection.forward, animated: false, completion: nil)
        view.addSubview(pageContainer.view)
        view.sendSubview(toBack: pageContainer.view)
        view.bringSubview(toFront: pageControl)
        view.bringSubview(toFront: nextButton)
        view.bringSubview(toFront: backButton)
        pageControl.numberOfPages = pages.count
        pageControl.currentPage = 0
       
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func disableSwipe() {
        for view in self.pageContainer.view.subviews {
            if let subView = view as? UIScrollView {
                subView.isScrollEnabled = false
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        self.disableSwipe()
        
        
    }
    
    @IBAction func nextButtonPressed(_ sender: Any) {
        pageControl.currentPage = (currentIndex!) + 1
        self.pendingIndex = (currentIndex!) + 1
        self.currentIndex = (currentIndex!) + 1
        pageContainer.setViewControllers([pages[currentIndex!]], direction: UIPageViewControllerNavigationDirection.forward, animated: true, completion: nil)
//         self.disableSwipe()
        if currentIndex! == 7 {
            makeViewDark()
            self.backButton.setImage(UIImage(named: "Backward arrow white"), for: .normal, animated: true)
            UIView.animate(withDuration: 0.2) {
                
                self.nextButton.alpha = 0
                self.pageControl.alpha = 0
            }
        }
    }
    
    @IBAction func backButtonPressed(_ sender: Any) {
        
        if currentIndex! == 0 {
            self.navigationController?.popViewController(animated: true)
        } else {
            pageControl.currentPage = (currentIndex!) - 1
            self.pendingIndex = (currentIndex!) - 1
            self.currentIndex = (currentIndex!) - 1
            pageContainer.setViewControllers([pages[currentIndex!]], direction: UIPageViewControllerNavigationDirection.reverse, animated: true, completion: nil)
            //         self.disableSwipe()
            view.bringSubview(toFront: pageControl)
            view.bringSubview(toFront: nextButton)
            view.bringSubview(toFront: backButton)
            if currentIndex! < 7 {
                makeViewLight()
                self.backButton.setImage(UIImage(named: "Backward arrow black"), for: .normal, animated: true)
                UIView.animate(withDuration: 0.2) {
                    
                    self.nextButton.alpha = 0
                    self.pageControl.alpha = 0
                }
            }
        }
       
    }
    
    @IBAction func closeButtonPressed(_ sender: Any) {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    
    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerBefore viewController: UIViewController) -> UIViewController? {
        let currentIndex = pages.index(of: viewController)!
        
        
        if currentIndex == 0 {
            return nil
        }
        
        let previousIndex = abs((currentIndex - 1) % pages.count)
        if currentIndex < 7 {
            makeViewLight()
            self.backButton.setImage(UIImage(named: "Backward arrow black"), for: .normal, animated: true)
            UIView.animate(withDuration: 0.2) {
                self.nextButton.alpha = 1
                self.pageControl.alpha = 1
            }
        }
//         self.disableSwipe()
        return pages[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerAfter viewController: UIViewController) -> UIViewController? {
        let currentIndex = pages.index(of: viewController)!
        
        if currentIndex == 7 {
            makeViewDark()
            self.backButton.setImage(UIImage(named: "Backward arrow white"), for: .normal, animated: true)
            UIView.animate(withDuration: 0.2) {
                self.nextButton.alpha = 0
                self.pageControl.alpha = 0
            }
        }
        if currentIndex == pages.count-1 {
            return nil
        }
        let nextIndex = abs((currentIndex + 1) % pages.count)
        
//         self.disableSwipe()
        return pages[nextIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
        pendingIndex = pages.index(of: pendingViewControllers.first!)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        
        
        if completed {
            currentIndex = pendingIndex
            if let index = currentIndex {
                pageControl.currentPage = index
            }
        }
    }
    
    
    private func newStep(step: String) -> UIViewController {
        return UIStoryboard(name: "Authentication", bundle: nil).instantiateViewController(withIdentifier: "onboardingStep\(step)")
    }
}

