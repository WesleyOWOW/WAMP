//
//  StepZeroViewController.swift
//  WAMP
//
//  Created by Wesley Peeters on 25/01/2018.
//  Copyright © 2018 Robbert Brandsma. All rights reserved.
//

import UIKit


class StepZeroViewController: UIViewController {
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func backButtonPressed(_ sender: Any) {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func skipButtonPressed(_ sender: Any) {
//        AppDelegate.shared.reset()
         self.navigationController?.dismiss(animated: true, completion: nil)
//        User.me.onboarded()
    }
}
