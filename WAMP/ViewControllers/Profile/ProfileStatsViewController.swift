import UIKit
import SnapKit
import SwiftNotificationCenter

fileprivate class ProfileStatsComparisonHeader : UIView {
    
    let leftAvatar = WAMPAvatarProfile()
    let rightAvatar = WAMPAvatarProfile()
    
    let leftNameLabel = UILabel()
    let rightNameLabel = UILabel()
    
    let vsLabel = UILabel()
    let matchCountLabel = UILabel()
    
    init() {
        super.init(frame: .zero)
        backgroundColor = UIColor(hex: "FCFCFC")
        vsLabel.text = "VS"
        vsLabel.font = .bold(32)
        vsLabel.textColor = #colorLiteral(red: 0.7490196078, green: 0.7490196078, blue: 0.7490196078, alpha: 1)
        
        leftNameLabel.font = .medium(14)
        leftNameLabel.textColor = UIColor(hex: "1A1A1A")
        rightNameLabel.font = .medium(14)
        rightNameLabel.textColor = UIColor(hex: "1A1A1A")
        
        matchCountLabel.font = .medium(12)
        matchCountLabel.textColor = #colorLiteral(red: 0.7490196078, green: 0.7490196078, blue: 0.7490196078, alpha: 1)
        
        [leftAvatar, rightAvatar, vsLabel, matchCountLabel, leftNameLabel, rightNameLabel].forEach(addSubview)
        
        
        leftAvatar.snp.makeConstraints { make in
            make.trailing.equalTo(vsLabel.snp.leading).offset(-70)
            make.leading.top.equalToSuperview().offset(40)
            make.size.equalTo(56)
        }
        
        leftNameLabel.snp.makeConstraints { make in
            make.top.equalTo(leftAvatar.snp.bottom).offset(10)
            make.bottom.equalToSuperview().offset(15)
            make.centerX.equalTo(leftAvatar)
        }
        
        
        rightAvatar.snp.makeConstraints { make in
            make.trailing.equalToSuperview().offset(-40)
            make.centerY.equalTo(leftAvatar)
            make.leading.equalTo(vsLabel.snp.trailing).offset(45)
            make.size.equalTo(leftAvatar)
        }
        
        rightNameLabel.snp.makeConstraints { make in
            make.centerX.equalTo(rightAvatar)
            make.top.bottom.equalTo(leftNameLabel)
        }
        
        vsLabel.snp.makeConstraints { make in
            make.centerY.equalTo(leftAvatar)
            make.centerX.equalToSuperview()
        }
        
        matchCountLabel.snp.makeConstraints { make in
            make.top.equalTo(vsLabel.snp.bottom).offset(5)
            make.centerX.equalToSuperview()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup(profile1: UserProtocol, profile2: UserProtocol, numberOfMatches: Int) {
        matchCountLabel.text = "\(numberOfMatches)"
        
        for (profile, avatarView, nameLabel) in [(profile1, leftAvatar, leftNameLabel), (profile2, rightAvatar, rightNameLabel)] {
            nameLabel.text = profile.isMe ? "You" : profile.firstName
            avatarView.setProfile(profile)
        }
        
        self.layoutSubviews()
        self.frame.size = systemLayoutSizeFitting(UILayoutFittingCompressedSize)
    }
}

class ProfileCell : UITableViewCell {
    let left = UIView(frame: .zero)
    let middle = UIView(frame: .zero)
    let right = UIView(frame: .zero)
    var stack : UIStackView!
    let container = UIView(frame: .zero)
    let background = RoundedShadowRect(frame: .zero)
    let leftBigLabel = UILabel()
    let leftSmallLabel = UILabel()
    let middleBigLabel = UILabel()
    let middleSmallLabel = UILabel()
    let rightBigLabel = UILabel()
    let rightSmallLabel = UILabel()
    var bigLabels: [UILabel] { return [leftBigLabel, middleBigLabel, rightBigLabel] }
    var smallLabels: [UILabel] { return [leftSmallLabel, middleSmallLabel, rightSmallLabel] }
    var allLabels: [UILabel] { return bigLabels + smallLabels }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        
        self.backgroundColor = UIColor(hex: "000000")
        
       
        
       
        
        stack = UIStackView(arrangedSubviews: [left, middle, right])
        stack.axis = .horizontal
        
        background.addSubview(container)
        container.addSubview(stack)
        container.layer.cornerRadius = 8
        container.clipsToBounds = true
        
        let leftStack = UIStackView(arrangedSubviews: [leftBigLabel, leftSmallLabel])
        let middleStack = UIStackView(arrangedSubviews: [middleBigLabel, middleSmallLabel])
        let rightStack = UIStackView(arrangedSubviews: [rightBigLabel, rightSmallLabel])
        
        left.addSubview(leftStack)
        middle.addSubview(middleStack)
        right.addSubview(rightStack)
        
        bigLabels.forEach { $0.font = .bold(22); $0.textColor = .black }
        smallLabels.forEach { $0.font = .medium(12); $0.textColor = #colorLiteral(red: 0.7490196078, green: 0.7490196078, blue: 0.7490196078, alpha: 1) }
        allLabels.forEach { $0.textAlignment = .center }
        
        for stack in [leftStack, middleStack, rightStack] {
            stack.axis = .vertical
            stack.spacing = 5
        }
        
        let space = UIView(frame: .zero)
        contentView.addSubview(space)
        contentView.addSubview(background)
        
        
        background.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(16)
            make.trailing.equalToSuperview().offset(-16)
            make.top.equalToSuperview()
            make.height.equalTo(80)
        }
        
        space.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.top.equalTo(background.snp.bottom)
            make.height.equalTo(50)
        }
        
        self.snp.makeConstraints { make in
            make.height.equalTo(100)
        }
        
        stack.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup(_ entry: ProfileStatsViewController.StatsEntry) {
        let total = entry.wins + entry.ties + entry.losses
        
        for (amount, bigLabel, smallLabel, description) in [(entry.wins, leftBigLabel, leftSmallLabel, "win"), (entry.ties, middleBigLabel, middleSmallLabel, "tie"), (entry.losses, rightBigLabel, rightSmallLabel, "lose")] {
            let percentage = total > 0 ? Int(Double(amount) / Double(total) * 100) : 0
            
            bigLabel.text = "\(amount)"
            smallLabel.text = "\(description) \(percentage)%"
        }
        
        let highlightWins = entry.wins > entry.ties && entry.wins > entry.losses
        left.backgroundColor = highlightWins ? Team.blue.color : .white
        leftBigLabel.textColor = highlightWins ? .white : .black
        leftSmallLabel.textColor = highlightWins ? .white : #colorLiteral(red: 0.7490196078, green: 0.7490196078, blue: 0.7490196078, alpha: 1)
        
        let highlightLosses = entry.losses > entry.ties && entry.losses > entry.wins
        right.backgroundColor = highlightLosses ? Team.red.color : .white
        right.backgroundColor = highlightLosses ? .white : .black
        rightSmallLabel.textColor = highlightLosses ? .white : #colorLiteral(red: 0.7490196078, green: 0.7490196078, blue: 0.7490196078, alpha: 1)
    }
    
    func setup(_ entry: UserProtocol.ComparisonStats) {
        let total = entry.leftWins + entry.ties + entry.rightWins
        
        for (amount, bigLabel, smallLabel, description) in [(entry.leftWins, leftBigLabel, leftSmallLabel, "win"), (entry.ties, middleBigLabel, middleSmallLabel, "tie"), (entry.rightWins, rightBigLabel, rightSmallLabel, "win")] {
            let percentage = total > 0 ? Int(Double(amount) / Double(total) * 100) : 0
            
            bigLabel.text = "\(amount)"
            smallLabel.text = "\(description) \(percentage)%"
        }
        
        let highlightLeft = entry.leftWins > entry.ties && entry.leftWins > entry.rightWins
        left.backgroundColor = highlightLeft ? Team.blue.color : .white
        leftBigLabel.textColor = highlightLeft ? .white : .black
        leftSmallLabel.textColor = highlightLeft ? .white : #colorLiteral(red: 0.7490196078, green: 0.7490196078, blue: 0.7490196078, alpha: 1)
        
        let highlightRight = entry.rightWins > entry.ties && entry.rightWins > entry.leftWins
        right.backgroundColor = highlightRight ? Team.red.color : .white
        rightBigLabel.textColor = highlightRight ? .white : .black
        rightSmallLabel.textColor = highlightRight ? .white : #colorLiteral(red: 0.7490196078, green: 0.7490196078, blue: 0.7490196078, alpha: 1)
    }
}

fileprivate class ProfileStatsCell : UITableViewCell {
    
    let left = UIView(frame: .zero)
    let middle = UIView(frame: .zero)
    let right = UIView(frame: .zero)
    var stack : UIStackView!
    let container = UIView(frame: .zero)
    let background = RoundedShadowRect(frame: .zero)
    let leftBigLabel = UILabel()
    let leftSmallLabel = UILabel()
    let middleBigLabel = UILabel()
    let middleSmallLabel = UILabel()
    let rightBigLabel = UILabel()
    let rightSmallLabel = UILabel()
    var bigLabels: [UILabel] { return [leftBigLabel, middleBigLabel, rightBigLabel] }
    var smallLabels: [UILabel] { return [leftSmallLabel, middleSmallLabel, rightSmallLabel] }
    var allLabels: [UILabel] { return bigLabels + smallLabels }
    
    
   
    let centerBox = UIView(frame: .zero)
    
    
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.backgroundColor = .black
        //        contentView.backgroundColor = .red
        
        background.layer.cornerRadius = 8
        
        backgroundColor = UIColor.white
        background.addSubview(container)
        
        container.layer.cornerRadius = 8
        container.clipsToBounds = true

        let leftStack = UIStackView(arrangedSubviews: [leftBigLabel, leftSmallLabel])
        let middleStack = UIStackView(arrangedSubviews: [middleBigLabel, middleSmallLabel])
        let rightStack = UIStackView(arrangedSubviews: [rightBigLabel, rightSmallLabel])
        
        left.addSubview(leftStack)
        middle.addSubview(middleStack)
        right.addSubview(rightStack)
        
        
        bigLabels.forEach { $0.font = .bold(22); $0.textColor = .black }
        smallLabels.forEach { $0.font = .medium(12); $0.textColor = #colorLiteral(red: 0.7490196078, green: 0.7490196078, blue: 0.7490196078, alpha: 1) }
        allLabels.forEach { $0.textAlignment = .center }
        
        for stack in [leftStack, middleStack, rightStack] {
            stack.axis = .vertical
            stack.spacing = 5
        }
        
        let space = UIView(frame: .zero)
        
        ([background, space, leftStack, middleStack, rightStack]).forEach(contentView.addSubview)
        
        background.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(16)
            make.trailing.equalToSuperview().offset(-16)
            make.top.equalToSuperview()
            make.height.equalTo(80)
        }
        
        space.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.top.equalTo(background.snp.bottom)
            make.height.equalTo(50)
        }
        
        container.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        container.addSubview(left)
        container.addSubview(middle)
        container.addSubview(right)
        
        addSpacer(.vertical, in: container, between: left, and: middle).snp.makeConstraints { make in
            make.top.bottom.equalToSuperview()
        }
        
        addSpacer(.vertical, in: container, between: middle, and: right).snp.makeConstraints { make in
            make.top.bottom.equalToSuperview()
        }
        
        left.snp.makeConstraints { make in
            make.width.equalTo((UIScreen.main.bounds.size.width - 32) / 3)
            make.height.equalTo(80)
            make.leading.equalToSuperview()
        }
        
        
        middle.snp.makeConstraints { make in
            make.width.equalTo((UIScreen.main.bounds.size.width - 32) / 3)
            make.height.equalTo(80)
            make.leading.equalTo(left.snp.trailing)
        }
        
        
        right.snp.makeConstraints { make in
            make.width.equalTo((UIScreen.main.bounds.size.width - 32) / 3)
            make.height.equalTo(80)
            make.leading.equalTo(middle.snp.trailing)
        }
        
        
        leftStack.snp.makeConstraints { make in
            make.center.equalTo(left)
        }
        
        middleStack.snp.makeConstraints { make in
            make.center.equalTo(middle)
        }
        
        rightStack.snp.makeConstraints { make in
            make.center.equalTo(right)
        }

        for view in self.contentView.subviews {
            view.translatesAutoresizingMaskIntoConstraints = false
        }
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup(_ entry: ProfileStatsViewController.StatsEntry) {
        let total = entry.wins + entry.ties + entry.losses
        
        for (amount, bigLabel, smallLabel, description) in [(entry.wins, leftBigLabel, leftSmallLabel, "win"), (entry.ties, middleBigLabel, middleSmallLabel, "tie"), (entry.losses, rightBigLabel, rightSmallLabel, "lose")] {
            let percentage = total > 0 ? Int(Double(amount) / Double(total) * 100) : 0
            
            bigLabel.text = "\(amount)"
            smallLabel.text = "\(description) \(percentage)%"
        }
        
        let highlightWins = entry.wins > entry.ties && entry.wins > entry.losses
        left.backgroundColor = highlightWins ? Team.blue.color : .white
        leftBigLabel.textColor = highlightWins ? .white : .black
        leftSmallLabel.textColor = highlightWins ? .white : #colorLiteral(red: 0.7490196078, green: 0.7490196078, blue: 0.7490196078, alpha: 1)
        
        let highlightLosses = entry.losses > entry.ties && entry.losses > entry.wins
        right.backgroundColor = highlightLosses ? Team.red.color : .white
        rightBigLabel.textColor = highlightLosses ? .white : .black
        rightSmallLabel.textColor = highlightLosses ? .white : #colorLiteral(red: 0.7490196078, green: 0.7490196078, blue: 0.7490196078, alpha: 1)
        
        

        
    }
    
    func setup(_ entry: UserProtocol.ComparisonStats) {
        let total = entry.leftWins + entry.ties + entry.rightWins
        
        for (amount, bigLabel, smallLabel, description) in [(entry.leftWins, leftBigLabel, leftSmallLabel, "win"), (entry.ties, middleBigLabel, middleSmallLabel, "tie"), (entry.rightWins, rightBigLabel, rightSmallLabel, "win")] {
            let percentage = total > 0 ? Int(Double(amount) / Double(total) * 100) : 0
            
            bigLabel.text = "\(amount)"
            smallLabel.text = "\(description) \(percentage)%"
        }
        
        let highlightLeft = entry.leftWins > entry.ties && entry.leftWins > entry.rightWins
        left.backgroundColor = highlightLeft ? Team.blue.color : .white
        leftBigLabel.textColor = highlightLeft ? .white : .black
        leftSmallLabel.textColor = highlightLeft ? .white : #colorLiteral(red: 0.7490196078, green: 0.7490196078, blue: 0.7490196078, alpha: 1)
        
        let highlightRight = entry.rightWins > entry.ties && entry.rightWins > entry.leftWins
        right.backgroundColor = highlightRight ? Team.red.color : .white
        rightBigLabel.textColor = highlightRight ? .white : .black
        rightSmallLabel.textColor = highlightRight ? .white : #colorLiteral(red: 0.7490196078, green: 0.7490196078, blue: 0.7490196078, alpha: 1)
    }
}

fileprivate class ProfileStatsHeaderCell : UITableViewCell {
    let label = UILabel()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        label.font = .medium(13)
        label.textAlignment = .center
        label.textColor = #colorLiteral(red: 0.7490196078, green: 0.7490196078, blue: 0.7490196078, alpha: 1)
        contentView.addSubview(label)
        label.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        contentView.backgroundColor = #colorLiteral(red: 0.9882352941, green: 0.9882352941, blue: 0.9882352941, alpha: 1)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup(_ entry: ProfileStatsViewController.StatsEntry) {
        label.text = "\(entry.type.localizedDescription)"
    }
    
    func setup(_ entry: User.ComparisonStats) {
        label.text = "\(entry.group) / \(entry.leftWins + entry.ties + entry.rightWins)"
    }
}

class ProfileStatsViewController : WampViewController, UITableViewDataSource, UITableViewDelegate, ScrollDelegateForwarding {
    
    weak var forwardedScrollDelegate: UIScrollViewDelegate?
    
    typealias StatsEntry = (type: MatchType, wins: Int, ties: Int, losses: Int)
    var entries: [StatsEntry] = []
    var compareEntries: [UserProtocol.ComparisonStats] = []
    
    let tableView: UITableView! = ClickThroughTableView(frame: .zero, style: .plain)
    var profile: UserProtocol! = nil
    var compareMode = false
    
    fileprivate lazy var comparisonHeader = ProfileStatsComparisonHeader()
    
    override func loadView() {
        view = ClickThroughView(frame: .zero)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addSubview(tableView)
        tableView.snp.makeConstraints { make in
            make.edges.equalToSuperview().inset(UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0))
        }
        
        tableView.backgroundColor = #colorLiteral(red: 0.1490196078, green: 0.1725490196, blue: 0.2, alpha: 1)
        tableView.register(ProfileStatsCell.self, forCellReuseIdentifier: "stats")
        tableView.register(ProfileStatsHeaderCell.self, forCellReuseIdentifier: "header")
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        tableView.allowsSelection = false
        
        if compareMode {
            tableView.tableHeaderView = comparisonHeader
            comparisonHeader.setup(profile1: User.me, profile2: profile, numberOfMatches: 0)
        }
        
        loadData()
        
        
    }
    
    func loadData() {
        if compareMode {
            // TODO: Remove ignoreErrors when the backend is fixed
            profile.fetchComparisonStats().catchErrorJustComplete().bind { stats in
                self.compareEntries = stats
                self.tableView.reloadData()
                }.disposed(by: disposeBag)
        } else {
            profile.fetchStats().subscribe(onNext: { stats in
                self.entries = stats.flatMap { stats in
                    guard let matchType = MatchType(rawValue: stats.group) else { return nil }
                    return (matchType, stats.wins, stats.ties, stats.losses)
                }
                
 
                self.tableView.reloadData()
            }, onError: { error in
                self.displayError("Error loading stats: \((error as NSError).localizedDescription)")
            }).disposed(by: disposeBag)
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return compareMode ? compareEntries.count : entries.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if compareMode {
            let entry = compareEntries[indexPath.section]
            
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "header", for: indexPath) as! ProfileStatsHeaderCell
                cell.setup(entry)
                
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "stats", for: indexPath) as! ProfileStatsCell
                cell.setup(entry)
                cell.backgroundColor = #colorLiteral(red: 0.9881522059, green: 0.9882904887, blue: 0.9881086946, alpha: 1)
                
                return cell
            }
        } else {
            let entry = entries[indexPath.section]
            
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "header", for: indexPath) as! ProfileStatsHeaderCell
                cell.setup(entry)
                
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "stats", for: indexPath) as! ProfileStatsCell
                cell.setup(entry)
                cell.backgroundColor = #colorLiteral(red: 0.9881522059, green: 0.9882904887, blue: 0.9881086946, alpha: 1)
                
                return cell
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return indexPath.row == 0 ? 35 : 84
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        forwardedScrollDelegate?.scrollViewDidScroll?(scrollView)
    }
    
}

