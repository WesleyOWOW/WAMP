//
//  EditProfileViewController.swift
//  WAMP
//
//  Created by Wesley Peeters on 24-01-18.
//  Copyright © 2018 Robbert Brandsma. All rights reserved.
//

import UIKit
import RxSwift
import Alamofire


class EditProfileViewController: WampViewController {

    @IBOutlet weak var firstName: UITextField!
    @IBOutlet weak var lastName: UITextField!
    @IBOutlet weak var handicap: UITextField! {
        didSet {
            self.handicap.delegate = self
            self.handicap.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        }
    }
    @IBOutlet weak var homeCourse: UITextField!
    @IBOutlet weak var email: UITextField!
    
    @IBOutlet weak var homeCourseButton: ButtonWithImage!
    @IBOutlet weak var avatar: WAMPAvatarProfile!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.refresh), name: Notification.Name("refreshSettings"), object: nil)
    }
    
    @objc func refresh() {
        print("REFRESH SETTINGS")
        HomeCourseLabel.text = User.me.homeCourseName
        
    }
    
    func showLoading(text: String) {
        let alert = UIAlertController(title: nil, message: text, preferredStyle: .alert)
        
        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        loadingIndicator.startAnimating();
        
        alert.view.addSubview(loadingIndicator)
        present(alert, animated: true, completion: nil)
    }
    
    func uploadProfilePic(image: UIImage) {
        let data = UIImageJPEGRepresentation(image.resizeWithPercent(percentage: 0.5)!, 0.8)
        _ = Alamofire.upload(data!, to: "\(api_url)/users/me/profile-picture", method: .post, headers: AuthenticationManager.shared.headers)
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        if textField == handicap {
            if let intValue = Double(textField.text!), intValue > 54.00 {
                textField.text = "54"
                CreateGuestViewModel.shared.handicap.value = "54"
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == handicap {
            if string.isEmpty {
                return true
            }
            
            let countdots = textField.text!.components(separatedBy: ".").count - 1
            
            if countdots > 0 && (string == "." || string == ",")
            {
                return false
            }
            
            let decimals = textField.text!.components(separatedBy: ".")
            
            if decimals.count > 1, decimals[1].count > 0 {
                return false
            }
            
            
            
            let aSet = NSCharacterSet(charactersIn:"0123456789,.").inverted
            let compSepByCharInSet = string.components(separatedBy: aSet)
            let numberFiltered = compSepByCharInSet.joined(separator: "")
            if string == "," {
                textField.text! += "."
                return false
            }
            return string == numberFiltered
        }
        
        return true
        
    }
    
    @IBAction func editHCPressed(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "EditHomeController") as! EditHomecourseViewController
    
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
WampCameraViewModel.shared.reset()
    }

    @IBOutlet weak var HomeCourseLabel: UILabel!
    override func viewWillAppear(_ animated: Bool) {
        print("appear")
        firstName.text = User.me.firstName
        lastName.text = User.me.lastName
        email.text = User.me.email
        avatar.setProfile(User.me)
        handicap.text = "\(User.me.handicap)"
//        homeCourse.text = User.me.home
        WampCameraViewModel.shared.amount = .single
        WampCameraViewModel.shared.saveToServer = true
        HomeCourseLabel.addAction {
            self.editHCPressed(self)
        }
        
        if let photo = WampCameraViewModel.shared.allPhotos.value.first {
            uploadProfilePic(image: photo.image!)
        }
        
        HomeCourseLabel.text = User.me.homeCourseName
//        homeCourseButton.titleLabel?.text = User.me.homeCourseObj
//             homeCourseButton.setTitle("", for: .normal)
//         homeCourseButton.setTitle(User.me.homeCourseObj, for: .normal)
        
       
        if (WampCameraViewModel.shared.currentPhoto.value != nil ) {
            
            self.avatar.setPhoto(WampCameraViewModel.shared.currentPhoto.value?.image)
            WampCameraViewModel.shared.reset()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }

    @IBOutlet weak var errorHeight: NSLayoutConstraint!
    @IBOutlet weak var emailLabel: UILabel!
   
    
    @IBAction func doneButtonPressed() {
        if self.isValidEmail(testStr: self.email.text!) {
            User.me.setFirstname(self.firstName.text!)
            User.me.setLastname(self.lastName.text!)
            User.me.setHandicap(Double(self.handicap.text!)!)
            User.me.setEmail(self.email.text!)
            if (WampCameraViewModel.shared.currentPhoto.value != nil ) {
                uploadProfilePic(image: (WampCameraViewModel.shared.allPhotos.value.first?.image!)!)
                // TODO: Update profile image
            }
            User.me.updateUser(callback: { _ in })
            
            navigationController?.popViewController(animated: true)
        } else {
            self.email.text = ""
            self.email.placeholder = "Your email address"
            self.errorHeight.constant = 72
            UIView.animate(withDuration: 0.2) {
                self.emailLabel.textColor = UIColor(hex: "FF3344")
                self.view.layoutIfNeeded()
            }
            
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
                self.errorHeight.constant = 0
                UIView.animate(withDuration: 0.2) {
                    self.emailLabel.textColor = UIColor(hex: "000000")
                    self.view.layoutIfNeeded()
                }
            }
        }
        
    }
    
    var displayTitleBar: Bool {
        return true
    }
    
    var tabTitle: String {
        return "Edit profile"
    }
    
    
}
