import UIKit

class TextSettingViewController: WampViewController {
    
    @IBOutlet weak var text: UILabel!
    
    enum type {
        case terms
        case faq
        case privacy
    }
    
    @IBOutlet weak var scrollview: UIScrollView!
    var docType: type = .terms
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        scrollview.contentInset = UIEdgeInsetsMake(48, 0, 0, 0)
        scrollview.contentSize = CGSize(width: UIScreen.main.bounds.size.width, height: text.frame.size.height)
    }
    
    var displayTitleBar: Bool {
        return true
    }
    
    @IBAction func backButtonPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    var tabTitle: String {
        switch docType {
        case .terms:
            return "Terms of \nService"
            
            
        case .faq:
            return "Frequently Asked \nQuestions"
            
            
        case .privacy:
            return "Privacy Policy"
            
        }
        
    }
}

