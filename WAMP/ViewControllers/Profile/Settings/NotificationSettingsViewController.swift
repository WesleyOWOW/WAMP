//
//  NotificationSettingsViewController.swift
//  WAMP
//
//  Created by Wesley Peeters on 24-01-18.
//  Copyright © 2018 Robbert Brandsma. All rights reserved.
//

import UIKit

import Alamofire
import SwiftyJSON

class SettingsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var `switch`: WampSwitch!
    
    
    var changeHandler: ((Bool) -> Void)?
    var initialValue: Bool!
    var title: String = ""
    func configure(title: String, initialValue: Bool, onChange: @escaping (Bool) -> Void) {
        print ("makeSettings", title, initialValue)
        nameLabel.text = title
        self.switch.isOn = initialValue
        self.switch.setupUI()
        self.changeHandler = onChange
        
        self.switch.addTarget(self, action: #selector(handleValueChange), for: .valueChanged)
        
    }
    
    func handleValueChange() {
        changeHandler?(self.switch.isOn)
    }

}

class NotificationSettingsViewController: WampViewController, UITableViewDataSource, UITableViewDelegate {
    var remoteSettings: JSON = []
    var remoteSettingsValues: [String: Bool] = [:]
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hidesKeyboardOnTap = false
        tableView.allowsSelection = false
        tableView.dataSource = self
        tableView.rowHeight = 55
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadRemoteData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func SaveNotificationSettings(_ sender: Any) {
         saveRemoteSettings()
    }
    
    @IBAction func backButtonPressed(_ sender: Any) {
         saveRemoteSettings()
        navigationController?.popViewController(animated: true)
    }
    
    var displayTitleBar: Bool {
        return true
    }
    
    var tabTitle: String {
        return "Notification Settings"
    }
    
    func loadRemoteData() {
        Alamofire.request(api_url + "/settings", headers: AuthenticationManager.shared.headers)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .failure(let error):
                    self.displayError("Error loading settings: \((error as NSError).localizedDescription)")
                case .success(let data):
                    self.remoteSettings = JSON(data)
                    self.tableView.reloadData()
                }
        }
        
        Alamofire.request(api_url + "/users/me/settings", headers: AuthenticationManager.shared.headers)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .failure(let error):
                    self.displayError("Error loading settings: \((error as NSError).localizedDescription)")
                case .success(let data):
                    /*
                     [
                     {
                     "key": 1,
                     "value": true
                     },
                     {
                     "key": 2,
                     "value": false
                     }
                     ]
                     */
                    for (_, kv) in JSON(data) {
                        guard let id = kv["key"].int, let val = kv["value"].bool else {
                            continue
                        }
                        
                        self.remoteSettingsValues["\(id)"] = val
                    }
                    self.tableView.reloadData()
                }
        }
    }
    

    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return self.remoteSettings.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        print ("makeSettings")
    
        let toggleCell = tableView.dequeueReusableCell(withIdentifier: "settings", for: indexPath) as! SettingsTableViewCell
        let settingDescription = remoteSettings[indexPath.row]
        let id = settingDescription["id"].intValue
        let value = remoteSettingsValues["\(id)"] ?? true
        log.debug("settings: \(settingDescription)")
        toggleCell.configure(title: settingDescription["name"].stringValue, initialValue: value) { [unowned self] in
            self.remoteSettingsValues["\(id)"] = $0
            self.saveRemoteSettings()
        }
            return toggleCell
     
    }
    
    
    func saveRemoteSettings() {
        guard remoteSettings.count > 0 else { return }
        print("settings", remoteSettingsValues)
        var request = try! URLRequest(url: api_url + "/users/me/settings", method: .put, headers: AuthenticationManager.shared.headers)
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpBody = try! JSONSerialization.data(withJSONObject: ["settings": remoteSettingsValues], options: [])
        
        Alamofire.request(request)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .failure(let error):
                    log.error("could not update remote settings")
                    log.error("\(error)")
                    assertionFailure()
                case .success(let value):
                    log.info("remote settings updated")
                    log.verbose("\(value)")
                }
        }
    }
}
