//
//  EditHomecourseViewController.swift
//  WAMP
//
//  Created by Wesley Peeters on 21-02-18.
//  Copyright © 2018 Robbert Brandsma. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxKeyboard
import Alamofire
import CoreLocation

class EditHomecourseViewController: WampViewController,  UITableViewDelegate, UITableViewDataSource {
    
    var displayTitleBar: Bool {
        return false
    }
    
    var courses: [Course] = []
    var locationManager: CLLocationManager = CLLocationManager()
    var currentLocation : CLLocation!
    var search: String?
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var tableView: UITableView! {
        didSet{
            self.tableView.delegate = self
            self.tableView.dataSource = self
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hidesKeyboardOnTap = false
        setupRX()
        updateLocation()
    }
    
    
    
    func updateLocation() {
        locationManager.requestAlwaysAuthorization()
        self.currentLocation = self.locationManager.location
        self.refreshLocation()
        self.update()
    }
    
    func refreshLocation() {
        locationManager.requestAlwaysAuthorization()
        
        if CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways {
            self.currentLocation = self.locationManager.location
            print(currentLocation)
        }
    }
    
    func update() {
        
        if let searchText = search {
            _ = Course.find(query: searchText, callback: self.makeClosestCourses)
        } else if let currentLocation = currentLocation {
            
            _ = Course.findNearby(latitude: currentLocation.coordinate.latitude, longitude: currentLocation.coordinate.longitude, callback: self.makeClosestCourses)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    
    @IBAction func backButtonPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func setupRX() {
        self.searchTextField.rx.text.orEmpty.throttle(0.3, scheduler: MainScheduler.instance).distinctUntilChanged().bind { [unowned self] text in
            self.search = nil
            
            if text.count > 2 {
                self.search = text
            }
            
            self.update()
        }
    }
    
    func makeClosestCourses(result: Result<[Course]>) {
        switch result {
        case .success(let courses):
            self.courses = courses
            print(self.courses)
            self.tableView.reloadData()
        case .failure(let error):
            print("courses:", error)
        }
    }
    
    func select (_ course: Course) {
        print("select")
        EditHCViewModel.shared.homeCourse = course
        self.performSegue(withIdentifier: "nextStep", sender: nil)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let course = courses[indexPath.row]
        select(course)
        tableView.deselectRow(at: indexPath, animated: false)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let course = self.courses[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: CourseTableViewCell.identifier, for: indexPath) as! CourseTableViewCell
        
        cell.make(course, location: self.currentLocation.coordinate)
        
        return cell
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return courses.count
    }
}


