//
//  NewPasswordViewController.swift
//  WAMP
//
//  Created by Wesley Peeters on 24-01-18.
//  Copyright © 2018 Robbert Brandsma. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxKeyboard

class NewPasswordViewController: WampViewController {
    @IBOutlet weak var errorHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var backButton: UIButton!
     @IBOutlet weak var nextButton: WampButton!
    @IBOutlet weak var nextButtonBottomConstaint: NSLayoutConstraint!
    @IBOutlet weak var input: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.input.rx.text.orEmpty.bind(to: ChangePasswordViewModel.shared.new).disposed(by: disposeBag)
        RxKeyboard.instance.visibleHeight.drive(onNext: { [weak self] visibleHeight in
            self?.nextButtonBottomConstaint.constant = visibleHeight + 16
            
            self?.view.setNeedsLayout()
            UIView.animate(withDuration: 0.1) {
                self?.view.layoutIfNeeded()
            }
        }).disposed(by: disposeBag)
        self.input.becomeFirstResponder()
    }
    
    @IBAction func nextButtonPressed(_ sender: Any) {
        if !((input.text?.count)! < 6) {
            self.performSegue(withIdentifier: "nextStep", sender: nil)
        } else {
            self.errorHeightConstraint.constant = 160
            self.makeViewDark()
            self.backButton.setImage(UIImage(named: "Backward arrow white"), for: .normal, animated: true)
            UIView.animate(withDuration: 0.2) {
                self.view.layoutIfNeeded()
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 5.0) {
                self.errorHeightConstraint.constant = 0
                self.makeViewLight()
                self.backButton.setImage(UIImage(named: "Backward arrow black"), for: .normal, animated: true)
                UIView.animate(withDuration: 0.2) {
                    self.view.layoutIfNeeded()
                }
            }
        }
        
    }
    
    
    @IBAction func backButtonPressed(_ sender: Any) {
        self.navigationController!.popViewController(animated: true)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
