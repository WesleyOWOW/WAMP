//
//  ChangePasswordFinishedViewController.swift
//  WAMP
//
//  Created by Wesley Peeters on 02-02-18.
//  Copyright © 2018 Robbert Brandsma. All rights reserved.
//

import UIKit
import SAConfettiView

class ChangePasswordFinishedViewController: WampViewController {

    var finishButton: UIButton!
    @IBOutlet weak var letsGoButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        makeViewDark()
        self.view.backgroundColor = UIColor(hex: "007BFF")
        letsGoButton.layer.cornerRadius = 20
        letsGoButton.layer.shadowColor = UIColor.black.cgColor
        letsGoButton.layer.shadowOffset = CGSize(width: 0.5, height: 1.5)
        letsGoButton.layer.shadowOpacity = 0.15
        letsGoButton.layer.masksToBounds = false
        letsGoButton.backgroundColor =  UIColor(hex: "007BFF")
        letsGoButton.layer.shadowRadius = 5.0
        letsGoButton.layer.shouldRasterize = true
        letsGoButton.layer.shadowPath = UIBezierPath(roundedRect: letsGoButton.bounds, cornerRadius: 20).cgPath
        
        self.finishButton = UIButton(frame: self.letsGoButton.frame)
        finishButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.letsgo)))
        
        let confetti = SAConfettiView(frame: self.view.bounds)
        confetti.type = .Confetti
        self.view.addSubview(confetti)
        self.view.addSubview(finishButton)
        confetti.startConfetti()
        self.view.bringSubview(toFront: self.letsGoButton)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func letsgo() {
        self.navigationController?.dismiss(animated: true, completion: nil)
        //        self.dismiss(animated: true, completion: nil)
//        WampCameraViewModel.shared.reset()
    }
    
    @IBAction func letsGoButtonPressed(_ sender: Any) {
        self.navigationController?.dismiss(animated: true, completion: nil)
        //        self.dismiss(animated: true, completion: nil)
//        WampCameraViewModel.shared.reset()
    }

}
