import UIKit
import SafariServices
import MessageUI


class SettingsViewController: WampViewController, MFMailComposeViewControllerDelegate, UIScrollViewDelegate {
    //    @IBOutlet weak var privateSwitch: WampSwitch!
    //
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var savePhotosSwitch: WampSwitch!
    var header: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        scrollView.delegate = self
        header = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 48))
        header.backgroundColor = UIColor(hex: "FCFCFC")
        let text = UILabel()
        text.font = .bold(32)
        text.textColor = .black
        text.numberOfLines = 0
        header.addSubview(text)
        text.text = "Settings"
        
        text.snp.makeConstraints { make in
            make.right.equalToSuperview()
            make.left.equalToSuperview().offset(16)
            make.top.equalToSuperview()
            make.bottom.equalToSuperview().offset(16)
        }
        
    }
    
    @IBAction func backButtonPressed(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
    }
    
    //    @objc func makePrivate() {
    //
    //        User.me.setPrivacy(privateSwitch.isOn)
    //        print("makePrivate", privateSwitch.isOn, User.me.privacy)
    //        User.me.updateServer(callback: {_ in })
    //    }
    
    @objc func savePictures() {
        User.me.setSavePhotos(savePhotosSwitch.isOn)
    
    }

    override func viewWillAppear(_ animated: Bool) {
        savePhotosSwitch.isOn = User.me.savePicturesToLibrary
        savePhotosSwitch.setupUI()
        savePhotosSwitch.addTarget(self, action: #selector(savePictures), for: .valueChanged)
        
        scrollView.contentInset = UIEdgeInsetsMake(48, 0, 0, 0)
    }
    
    func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    var displayTitleBar: Bool {
        return true
    }
    
    var tabTitle: String {
        return "Settings"
    }
    
    @IBAction func editProfile(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "editProfile") as! EditProfileViewController

        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func changePassword(_ sender: Any) {
        
    }
    
    @IBAction func notificationSettings(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "notificationSettings") as! NotificationSettingsViewController
        
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func logOut(_ sender: Any) {
        let alert = UIAlertController(title: "Are you sure you want to logout?",
                                      message: "You will be returned to the login screen.",
                                      preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "Logout", style: .destructive) { _ in
            
            AuthenticationManager.shared.logout()
        })
        
        self.present(alert, animated: true)
    }
    @IBAction func openFaq(_ sender: Any) {
        let svc = SFSafariViewController(url: URL(string: "https://wearematchplay.com/faq/")!)
        present(svc, animated: true, completion: nil)
    }
    
    @IBAction func openFeedback(_ sender: Any) {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients(["info@wearematchplay.com"])
            mail.setSubject("Feedback WAMP app - Version 2.0.0")
            
            present(mail, animated: true, completion: nil)
        } else {
            // show failure alert
        }
    }
    
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        switch result {
        case MFMailComposeResult.cancelled:
            print("Mail cancelled");
            break;
        case MFMailComposeResult.saved:
            print("Mail saved");
            break;
        case MFMailComposeResult.sent:
            print("Mail sent");
            break;
        case MFMailComposeResult.failed:
            print("Mail sent failure: %@", error?.localizedDescription);
            break;
        default:
            break;
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func openPolicy(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "privacy") as! TextSettingViewController
        vc.docType = .privacy
        self.present(vc, animated: true, completion: nil)
//
//        self.navigationController?.pushViewController(vc.navigationWrapped, animated: true)
    }
    
    @IBAction func openTerms(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "TOS") as! TextSettingViewController
        vc.docType = .terms
        self.present(vc, animated: true, completion: nil)
//        self.navigationController?.pushViewController(vc.navigationWrapped, animated: true)
    }
}

