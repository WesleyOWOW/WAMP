import Alamofire
import Foundation

enum LinkOpenError : Error {
    case invalidLinkFormat
    case unknownURL
}

//func instantiateViewControllerForAPILink(_ link: String, callback: @escaping (Result<UIViewController>) -> ()) {
//    do {
//        guard let url = URL(string: link) else {
//            throw LinkOpenError.invalidLinkFormat
//        }
//        
//        let lastTwoComponents = Array(url.pathComponents.suffix(2))
//        guard lastTwoComponents.count == 2 else {
//            throw LinkOpenError.unknownURL
//        }
//        
//        log.debug("\(lastTwoComponents)")
//        
//        if lastTwoComponents[0] == "matches", let matchId = Int(lastTwoComponents[1]) {
//            // This is a match URL
//            // Fetch the match
//            APIMatch.byId(matchId) { result in
//                switch result {
//                case .success(let match):
//                    let controller = SingleMatchViewController(match: match)
//                    callback(.success(controller))
//                case .failure(let error):
//                    callback(.failure(error))
//                }
//            }
//            return
//        }
//        
//        if lastTwoComponents[0] == "users" {
//            let userId = lastTwoComponents[1]
//            
//            // not disposed
//            User.fetch(id: userId) { result in
//                switch result {
//                case .success(let user):
//                    let vc = StoryboardScene.Main.instantiateProfileView()
//                    vc.profile = user
//                    callback(.success(vc))
//                case .failure(let error):
//                    callback(.failure(error))
//                }
//            }
//            return
//        }
//    } catch {
//        callback(.failure(error))
//    }
//}
//
