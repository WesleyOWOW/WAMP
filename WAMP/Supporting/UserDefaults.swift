import SwiftyUserDefaults

public extension DefaultsKeys {
    public static let userId = DefaultsKey<String>("userId")
    public static let username = DefaultsKey<String>("username")
    public static let firstName = DefaultsKey<String>("firstName")
    public static let lastName = DefaultsKey<String>("lastName")
    public static let handicap = DefaultsKey<Double>("handicap")
    public static let userIsVerified = DefaultsKey<Bool>("userIsVerified")
    public static let userIsPrivate = DefaultsKey<Bool>("userIsPrivate")
//    public static let team = DefaultsKey<Team>("team")
    public static let followerCount = DefaultsKey<Int>("followerCount")
    public static let email = DefaultsKey<String>("email")
    public static let homeCourseId = DefaultsKey<Int>("homeCourseId")
    public static let followingCount = DefaultsKey<Int>("followingCount")
    public static let homeCourseName = DefaultsKey<String>("homeCourseName")
    public static let matchCount = DefaultsKey<Int>("matchCount")
    public static let recentContacts = DefaultsKey<[Any]>("recentContacts")
    public static let savedMatch = DefaultsKey<[String: Any]?>("savedMatch")
    public static let savePhotos = DefaultsKey<Bool>("savePhotos")
}
