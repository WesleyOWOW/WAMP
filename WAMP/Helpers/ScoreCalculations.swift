/// Helpers used in the score calculation
/// They speak for themselves

func calculateCourseHandicap(handicap: Double, slope: Int) -> Double {
    guard slope > 0 else {
        log.info("Course slope is \(slope), returning EGC handicap \(handicap)")
        return handicap
    }
    
    var courseHandicap = handicap * (Double(slope) / 113.0)
    
    // Round
    courseHandicap = courseHandicap.rounded()
    
    log.info("Course handicap calculated based on a slope of \(slope), handicap \(handicap): \(courseHandicap)")
    return courseHandicap
}

func numberOfStrokesPerPlayer(withPlayingHandicaps handicaps: [Double], matchType type: MatchType) -> [Int] {
    // Calculate the amount of strokes, by substracting player handicaps
    var playingHandicaps = handicaps
    
    // Correct them with the lowest handicap for the amount of strokes
    let lowest = playingHandicaps.min()!
    playingHandicaps = playingHandicaps.map { ($0 - lowest) }
    
    // Playing handicaps for double are calculated (⨉ 90%)
    if type == .double {
        playingHandicaps = playingHandicaps.map { $0 * 0.9 }
    }
    
    return playingHandicaps.map { Int($0.rounded()) }
}

func numberOfStrokesPerHole(totalStrokes: Int, tee: Tee) -> [(index: Int, hole: Hole, strokes: Int)] {
    var strokeIndications = tee.holes.enumerated().map { (index: $0.offset, hole: $0.element, strokes: 1) }.sorted { $1.hole.strokeIndex ?? 0 > $0.hole.strokeIndex ?? 0 }
    
    let doubleStrokeHoleCount = totalStrokes - tee.holes.count
    if doubleStrokeHoleCount > 0 {
        for i in 0..<doubleStrokeHoleCount where i < strokeIndications.count {
            strokeIndications[i].strokes = 2
        }
    }
    
    return Array(strokeIndications.prefix(upTo: totalStrokes > tee.holes.count ? tee.holes.count : totalStrokes))
}


