import UIKit

public extension Int {
    public static func random(min: Int = 0, max: Int = Int.max) -> Int {
        let random = Int(arc4random_uniform(UInt32.max))
        
        return (random + min) % max
    }
}

public extension Array {
    public var random: Element {
        return self[Int.random() % self.count]
    }
}

public extension Date {
    public static var random: Date {
        return Date(timeIntervalSinceNow: TimeInterval(Int.random() % 94608000))
    }
}

